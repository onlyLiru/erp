import React, {
	Component
} from 'react';
import {
	Route
} from 'react-router-dom';
import {
	default as Layout
} from '../common/layout.js';
import './main.less';
import {
	StreamAccount
} from './streamAccount/main.js'; //流水账

//流水统计
//按校区
import {
	BaseCampus
} from './streamCount/baseCampus.js';
import {
	BaseCourse
} from './streamCount/baseCourse.js'; //按课程
import {
	BaseSubject
} from './streamCount/baseSubject.js'; //按科目
import {
	BaseClass
} from './streamCount/baseClass.js'; //按班级
import {
	PayStream
} from './streamCount/payStream.js'; //支付方式
import {
	BusinessStream
} from './streamCount/businessStream.js'; //业务类型
//学费统计
import {
	CampusTuition
} from './tuitionCount/baseCampus.js'; //按校区
import {
	CourseTuition
} from './tuitionCount/baseCourse.js'; //按课程
import {
	SubjectTuition
} from './tuitionCount/baseSubject.js'; //按科目
import {
	ClassTuition
} from './tuitionCount/baseClass.js'; //按班级
//物品统计
import {
	CampusGoods
} from './goodsCount/baseCampus.js'; //按校区
import {
	GoodsStatistics
} from './goodsCount/baseGoods.js'; //按物品
//课消统计
import {
	CampusConsume
} from './classConsume/baseCampus.js'; //按校区
import {
	CourseConsume
} from './classConsume/baseCourse.js'; //按课程
import {
	SubjectConsume
} from './classConsume/baseSubject.js'; //按科目
import {
	ClassConsume
} from './classConsume/baseClass.js'; //按班级
//未消耗学费统计
import {
	CampusUnused
} from './unusedTuition/baseCampus.js'; //按校区
import {
	CourseUnused
} from './unusedTuition/baseCourse.js'; //按课程
import {
	SubjectUnused
} from './unusedTuition/baseSubject.js'; //按科目
import {
	ClassUnused
} from './unusedTuition/baseClass.js'; //按班级


export default ({
	match
}) => {
	window.store.session('tMenu', {
		selectedKeys: 'cwtj'
	})
	return (<div>
		<Layout main={ Main } />
	</div>);
}

class Main extends Component {
	render() {
		return (
			<div>
				<Route path='/finance' exact component={ StreamAccount } />
				{/*流水账*/}
				<Route path='/finance/streamAct' component={ StreamAccount } />
				
				{/*流水统计*/}
				{/*按校区*/}
				<Route path='/finance/campusStream' component={ BaseCampus } />
				{/*按课程*/}
				<Route path='/finance/courseStream' component={ BaseCourse } />
				{/*按科目*/}
				<Route path='/finance/subjectStream' component={ BaseSubject } />
				{/*按班级*/}
				<Route path='/finance/classStream' component={ BaseClass } />
				{/*按支付方式*/}
				<Route path='/finance/payStream' component={ PayStream } />
				{/*按业务类型*/}
				<Route path='/finance/businessStream' component={ BusinessStream } />
				
				{/*学费统计*/}
				{/*按校区*/}
				<Route path='/finance/campusTuition' component={ CampusTuition } /> 
				{/*按课程*/}
				<Route path='/finance/courseTuition' component={ CourseTuition } />  
				{/*按科目*/}
				<Route path='/finance/subjectTuition' component={ SubjectTuition } />  
				{/*按班级*/}
				<Route path='/finance/classTuition' component={ ClassTuition } /> 
				
				{/*物品统计*/}
				{/*按校区*/}
				<Route path='/finance/campusGoods' component={ CampusGoods } /> 
				{/*按物品*/}
				<Route path='/finance/goodsStatistics' component={ GoodsStatistics } /> 
				
				{/*课消统计*/}
				{/*按校区*/}
				<Route path='/finance/campusConsume' component={ CampusConsume } /> 
				{/*按课程*/}
				<Route path='/finance/courseConsume' component={ CourseConsume } />  
				{/*按科目*/}
				<Route path='/finance/subjectConsume' component={ SubjectConsume } />  
				{/*按班级*/}
				<Route path='/finance/classConsume' component={ ClassConsume } />
				
				
				{/*未消耗学费统计*/}
				{/*按校区*/}
				<Route path='/finance/campusUnused' component={ CampusUnused } /> 
				{/*按课程*/}
				<Route path='/finance/courseUnused' component={ CourseUnused } />  
				{/*按科目*/}
				<Route path='/finance/subjectUnused' component={ SubjectUnused } />  
				{/*按班级*/}
				<Route path='/finance/classUnused' component={ ClassUnused } />
			</div>
		);
	}
}