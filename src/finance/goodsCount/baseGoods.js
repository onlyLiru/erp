import React, {
	Component
} from 'react';
import { Breadcrumb, DatePicker, Form, Row, Col, message, Table } from 'antd';
import '../main.less';
import $ from 'jquery';
import { _getHistogram1 } from '../../common/g.js';
import echarts from 'echarts';
import { fpost } from '../../common/io.js';
const FormItem = Form.Item;
const goodDetail = []; //物品id

class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			startValue: null,
			endValue: null,
			endOpen: false,
			allGoods: [],
			checked: true,
			checked1: true,
		}
	}
	disabledStartDate = (startValue) => { //只读的开始日期
		const endValue = this.state.endValue;
		if(!startValue || !endValue) {
			return false;
		}
		return startValue.valueOf() > endValue.valueOf();
	}

	disabledEndDate = (endValue) => { //只读的结束日期
		const startValue = this.state.startValue;
		if(!endValue || !startValue) {
			return false;
		}
		return endValue.valueOf() <= startValue.valueOf();
	}

	onChange = (field, value) => {
		this.setState({
			[field]: value,
		});
	}
	//开始日期改变
	onStartChange = (value) => {
		this.onChange('startValue', value);
	}
	//结束日期改变
	onEndChange = (value) => {
		this.onChange('endValue', value);
	}

	handleStartOpenChange = (open) => {
		if(!open) {
			this.setState({
				endOpen: true
			});
		}
	}

	handleEndOpenChange = (open) => {
		this.setState({
			endOpen: open
		});
	}
	_getHistogram(fieldsValue) { //流水账柱状图
		let chart = echarts.init(document.getElementById('histogram'));
		let $this = $('#histogram');
		let color = {
			color1: ['#2187FF'],
			color2: ['#FFC61C']
		};
		let $name = {
			text: '',
			subtext: '流水账（元）'
		}
		let chart1 = '';
		let $this1;
		let chart2 = '';
		let $this2;
		let type = 1;
		let type1;
		let type2;
		let $pageShow = $('.pageShow'); //图表和明细整体所在的容器
		let $noData = $('.noData'); //点击生成报表之前默认展示暂无数据......
		let urldata = '/api/finance/financeFundFlow/statisticsMaterialFundFlow';
		_getHistogram1(fieldsValue, urldata, chart, $this, $name, color, chart1, $this1, chart2, $this2, type, type1, type2, $pageShow, $noData, function(res) {});
	}
	componentDidMount() {
		this._getGoods(); //物品
		setTimeout(() => window.scrollTo(0, 0), 150);
		$('.pageShow').hide();
	}
	_getGoods() {
		const self = this;
		fpost('/api/material/pageMaterialVO', {
				currentPage: 1,
				pageSize: 9999
			})
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if(res.success == true) {
					if(res.result) {
						res.result.records.forEach((item, i) => {
							item.key = i;
						});
						self.setState({
							allGoods: res.result.records
						});
					}
				} else {
					message.error(res.message)
				}
			});
	}
	makeReport() { //点击生成报表事件
		for(var i = 0; i < $('.childCheck').length; i++) {
			if($('.childCheck').eq(i).prop("checked")) {
				goodDetail.push($('.childCheck').eq(i).attr('data-id'));
			}
		}
		this.props.form.validateFields(
			(err, fieldsValue) => {
				if(!err) {
					fieldsValue['startTime'] = fieldsValue['startTime'] ? fieldsValue['startTime'].format('YYYY-MM-DD HH:mm:ss') : '';
					fieldsValue['endTime'] = fieldsValue['endTime'] ? fieldsValue['endTime'].format('YYYY-MM-DD HH:mm:ss') : '';
					fieldsValue.type = '32';
					if(goodDetail.length > 0) {
						fieldsValue.materialIds = goodDetail;
					}
					this._getHistogram(fieldsValue); //柱状图
				} else {
					message.error('请先选好必填条件才可以生成报表哦');
				}
			}
		)
		goodDetail.splice(0, goodDetail.length)
	}
	allSelect(v) { //全选
		if(v.target.checked) {
			$('.childCheck').prop("checked", true)
		} else {
			$('.childCheck').prop("checked", false)
		}
		this.setState({
			checked: v.target.checked,
			checked1: v.target.checked,
		})
	}
	render() {
		const dataSource = [{
			key: '1',
			name: '胡彦斌',
			age: 32,
			address: '西湖区湖底公园1号',
		}, {
			key: '2',
			name: '胡彦祖',
			age: 42,
			address: '西湖区湖底公园1号'
		}];

		const columns = [{
			title: '业务发生时间',
			dataIndex: 'address',
			key: '0',
			width: 150,
			className: 'f-align-center'
		}, {
			title: '销售渠道',
			dataIndex: 'name',
			key: '1',
			className: 'f-align-center',
			width: 150
		}, {
			title: '物品名称',
			dataIndex: 'age',
			className: 'f-align-center',
			key: '2',
			width: 150
		}, {
			title: '数量',
			className: 'f-align-center',
			dataIndex: 'address',
			key: '3',
			width: 150
		}, {
			title: '金额',
			dataIndex: 'address',
			className: 'f-align-center',
			key: '4',
			width: 150
		}, {
			title: '购买人',
			dataIndex: 'address',
			className: 'f-align-center',
			key: '5',
			width: 150
		}, {
			title: '手机号',
			dataIndex: 'address',
			className: 'f-align-center',
			key: '6',
			width: 150
		}, {
			title: '校区',
			dataIndex: 'address',
			className: 'f-align-center',
			key: '7',
			width: 150
		}, {
			title: '销售员',
			dataIndex: 'address',
			className: 'f-align-center',
			key: '8',
			width: 150
		}];
		const formItemLayout = {
			labelCol: {
				span: 6
			},
			wrapperCol: {
				span: 18
			}
		};
		const {
			getFieldDecorator
		} = this.props.form;
		return(<div className='courseArranging Topselect classListPage wid100'>
				<Breadcrumb className="f-mb3">
				    <Breadcrumb.Item>财务统计</Breadcrumb.Item>
				    <Breadcrumb.Item>物品统计</Breadcrumb.Item>
				    <Breadcrumb.Item>
				   		<span className="f-fz5 f-bold">按物品</span>
				    </Breadcrumb.Item>
				</Breadcrumb>
				<Form className='f-box-shadow1 f-radius1 f-bg-white' style={{padding:'16px 16px 0 16px'}}>
					<Row className='f-mb3'>
						<Col span={5}>
							<span className='f-fz4 f-black f-mr4'>物品名称:&emsp;</span>
							<input type='checkbox' className='f-mr2' style={{
           							border: '1px solid #D9D9D9',
									borderRadius: '2px',
       						}} onClick={this.allSelect.bind(this)}/>
       						<i className='f-fz3' style={{color: '#4A4A4A'}}>全选</i>
						</Col>
						<Col span={19} className='f-left'>
						 { this.state.allGoods.map((elem, index) => {
			                  return(
			                  	 <div key={index} style={{marginRight:'40px'}} className='f-left f-mb3'>
									<input type='checkbox' className='f-mr2 childCheck' data-id={elem.materialId} style={{
	           							border: '1px solid #D9D9D9',
										borderRadius: '2px',
	       							}}/>
	       							<i className='f-fz3' style={{color: '#4A4A4A'}}>{elem.materialName}</i>
								</div>
			                  )
			                }) 
						 }
						</Col>
					</Row>
					<Row>
						<Col span={12}>
							<Col span={5} className='f-fz4 f-black f-h4-lh4 f-label'>时间段:&emsp;</Col>
				        		<Col span={19} >
					    			 <Col span={11}>
						    			 <FormItem {...formItemLayout}>
									      {getFieldDecorator('startTime',{
									      	rules: [{
								              required: true,
								              message: '请先选择开始时间',
								            }],
									      })(
							            		<DatePicker
							            		  showTime
									          disabledDate={this.disabledStartDate.bind(this)}
									          format="YYYY-MM-DD HH:mm:ss"
									          setFieldsValue={this.state.startValue}
									          placeholder="开始时间"
									          onChange={this.onStartChange.bind(this)}
									          onOpenChange={this.handleStartOpenChange.bind(this)}/>
							            		)}
								     </FormItem>
					    			 </Col>
					    			 <Col span={2} className='f-align-center f-h4-lh4'>--</Col>
					    			  <Col span={11}>
					    			  	<FormItem {...formItemLayout}>
								          {getFieldDecorator('endTime',{
								          	rules: [{
								              required: true,
								              message: '请先选择结束时间',
								            }],
								          })(
								            <DatePicker
								              showTime
									          disabledDate={this.disabledEndDate.bind(this)}
									          format="YYYY-MM-DD HH:mm:ss"
									          setFieldsValue={this.state.endValue}
									          placeholder="结束时间"
									          onChange={this.onEndChange.bind(this)}
									          open={this.state.endOpen}
									          onOpenChange={this.handleEndOpenChange.bind(this)}/>
								            )}
									     </FormItem>
						         	</Col>
							    </Col>
					       </Col>
					    <Col span={12} className='f-align-right f-pr4'>
							<a className="f-btn-blue" onClick={this.makeReport.bind(this)}>生成报表</a>
					    </Col>
					</Row>
				</Form>
				<div className='f-box-shadow1 f-radius1 f-bg-white f-pd4 f-mt5 pageShow'>
					<div>收入=售卖-退货&emsp;&emsp; 规则：展示top20</div>
					<div id="histogram" style={{width:' 1000px',height:'500px'}} className='f-mb2'></div>
					{/*<div className='f-mt5 f-mb4'>
						<div className='f-flex f-mb3' style={{justifyContent: 'space-between'}}>
							<h3 className="f-title-blue">明细列表</h3>
							<a className="f-btn-blue _export">
							<i className='iconfont icon-chuangjian f-mr1'></i>
							导出报表</a>
						</div>
						<Table columns={columns} dataSource={dataSource} bordered pagination={false}/>
					</div>*/}
				</div>
				<div className='noData f-mt4 f-align-center'>暂无数据......</div>
		</div>);
	}
}
const GoodsStatistics = Form.create()(Main);
export {
	GoodsStatistics
}