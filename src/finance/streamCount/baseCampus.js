import React, {
	Component
} from 'react';
import { Breadcrumb, DatePicker, Form, Row, Col } from 'antd';
import '../main.less';
import $ from 'jquery';
import { _getHistogram1 } from '../../common/g.js';
import echarts from 'echarts';

class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			startValue: null,
			endValue: null,
			endOpen: false,
		}
	}
	disabledStartDate = (startValue) => { //只读的开始日期
		const endValue = this.state.endValue;
		if(!startValue || !endValue) {
			return false;
		}
		return startValue.valueOf() > endValue.valueOf();
	}

	disabledEndDate = (endValue) => { //只读的结束日期
		const startValue = this.state.startValue;
		if(!endValue || !startValue) {
			return false;
		}
		return endValue.valueOf() <= startValue.valueOf();
	}

	onChange = (field, value) => {
		this.setState({
			[field]: value,
		});
	}
	//开始日期改变
	onStartChange = (value) => {
		this.onChange('startValue', value);
	}
	//结束日期改变
	onEndChange = (value) => {
		this.onChange('endValue', value);
	}

	handleStartOpenChange = (open) => {
		if(!open) {
			this.setState({
				endOpen: true
			});
		}
	}

	handleEndOpenChange = (open) => {
		this.setState({
			endOpen: open
		});
	}
	_getHistogram() { //流水账柱状图
		let chart = echarts.init(document.getElementById('histogram'));
		let $this = $('#histogram');
		let data = {};
		let gardenval = $('.kindergarten').val();
		if(!gardenval) {
			data["mode"] = 'Total';
			data["edu_id"] = '';
		} else {
			data["mode"] = 'Edu';
			data["edu_id"] = gardenval;
		}
		let range_type = $('.consumeSelect').val();
		if(range_type == 4) {
			data['range_type'] = 4;
			data['start_date'] = $('#start_time').val();
			data['end_date'] = $('#end_time').val();
		} else {
			data['start_date'] = '';
			data['end_date'] = '';
			data['range_type'] = range_type;
		}
		let $name = {
			text: '',
			subtext: '流水账（元）'
		}
		let urldata = '/admin/investor/report/home/distribute/analysis/json/';
		_getHistogram1(data, urldata, chart, $this, $name, function(res) {});
	}
	componentDidMount() {
		this._getHistogram(); //柱状图
	}
	makeReport() { //点击生成报表事件
		const tagDetail = []; //校区id
		for(var i = 0; i < $('.childCheck').length; i++) {
			if($('.childCheck').eq(i).prop("checked")) {
				tagDetail.push($('.childCheck').eq(i).attr('data-id'));
			}
		}
		this._getHistogram(); //柱状图
	}
	allSelect(v) { //全选
		if(v.target.checked) {
			$('.childCheck').prop("checked", true)
		} else {
			$('.childCheck').prop("checked", false)
		}
	}
	render() {
		return(<div className='courseArranging Topselect classListPage wid100'>
				<Breadcrumb className="f-mb3">
				    <Breadcrumb.Item>财务统计</Breadcrumb.Item>
				    <Breadcrumb.Item>流水统计</Breadcrumb.Item>
				    <Breadcrumb.Item>
				   		<span className="f-fz5 f-bold">按校区</span>
				    </Breadcrumb.Item>
				</Breadcrumb>
				<Form className='f-box-shadow1 f-radius1 f-bg-white f-pd4'>
					<Row className='f-mb3'>
						<Col span={4}>
							<span className='f-fz4 f-black f-mr4'>校区:&emsp;</span>
							<input type='checkbox' className='f-mr2' style={{
           							border: '1px solid #D9D9D9',
									borderRadius: '2px',
									width:'18px',
									height:'18px'
       						}} onClick={this.allSelect.bind(this)}/>
       						<i className='f-fz3' style={{color: '#4A4A4A'}}>全选</i>
						</Col>
						<Col span={20} className='f-left'>
							<div style={{marginRight:'40px'}} className='f-left f-mb3'>
								<input type='checkbox' className='f-mr2 childCheck' data-id='1' style={{
           							border: '1px solid #D9D9D9',
									borderRadius: '2px',
									width:'18px',
									height:'18px'
       							}}/>
       							<i className='f-fz3' style={{color: '#4A4A4A'}}>昂立湖塘昂立湖塘</i>
							</div>
							<div style={{marginRight:'40px'}} className='f-left f-mb3'>
								<input type='checkbox' className='f-mr2 childCheck' data-id='2' style={{
           							border: '1px solid #D9D9D9',
									borderRadius: '2px',
									width:'18px',
									height:'18px'
       							}}/>
       							<i className='f-fz3' style={{color: '#4A4A4A'}}>昂立湖塘昂立湖塘</i>
							</div>
							<div style={{marginRight:'40px'}} className='f-left f-mb3'>
								<input type='checkbox' className='f-mr2 childCheck' data-id='3' style={{
           							border: '1px solid #D9D9D9',
									borderRadius: '2px',
									width:'18px',
									height:'18px'
       							}}/>
       							<i className='f-fz3' style={{color: '#4A4A4A'}}>昂立湖塘昂立湖塘</i>
							</div>
							<div style={{marginRight:'40px'}} className='f-left f-mb3'>
								<input type='checkbox' className='f-mr2 childCheck' data-id='4' style={{
           							border: '1px solid #D9D9D9',
									borderRadius: '2px',
									width:'18px',
									height:'18px'
       							}}/>
       							<i className='f-fz3' style={{color: '#4A4A4A'}}>昂立湖塘昂立湖塘</i>
							</div>
							<div style={{marginRight:'40px'}} className='f-left f-mb3'>
								<input type='checkbox' className='f-mr2 childCheck' data-id='5' style={{
           							border: '1px solid #D9D9D9',
									borderRadius: '2px',
									width:'18px',
									height:'18px'
       							}}/>
       							<i className='f-fz3' style={{color: '#4A4A4A'}}>昂立湖塘昂立湖塘</i>
							</div>
							<div style={{marginRight:'40px'}} className='f-left f-mb3'>
								<input type='checkbox' className='f-mr2 childCheck' data-id='6' style={{
           							border: '1px solid #D9D9D9',
									borderRadius: '2px',
									width:'18px',
									height:'18px'
       							}}/>
       							<i className='f-fz3' style={{color: '#4A4A4A'}}>昂立湖塘昂立湖塘</i>
							</div>
						</Col>
					</Row>
					<Row>
						<Col span={12}>
							<Col span={4} className='f-fz4 f-black'>时间段:&emsp;</Col>
				        		<Col span={20} >
					    			 <Col span={11}>
						            	<DatePicker
							          disabledDate={this.disabledStartDate.bind(this)}
							          showTime
							          format="YYYY-MM-DD HH:mm:ss"
							          setFieldsValue={this.state.startValue}
							          placeholder="开始时间"
							          onChange={this.onStartChange.bind(this)}
							          onOpenChange={this.handleStartOpenChange.bind(this)} />
					    			 </Col>
					    			 <Col span={2} className='f-align-center'>--</Col>
					    			  <Col span={11}>
							            <DatePicker
								          disabledDate={this.disabledEndDate.bind(this)}
								          showTime
								          format="YYYY-MM-DD HH:mm:ss"
								          setFieldsValue={this.state.endValue}
								          placeholder="结束时间"
								          onChange={this.onEndChange.bind(this)}
								          open={this.state.endOpen}
								          onOpenChange={this.handleEndOpenChange.bind(this)}/>
						         	</Col>
							    </Col>
					       </Col>
					        	<Col span={12} className='f-align-right f-pr4'>
								<a className="f-btn-blue" onClick={this.makeReport.bind(this)}>生成报表</a>
					        	</Col>
					</Row>
				</Form>
				<div className='f-box-shadow1 f-radius1 f-bg-white f-pd4 f-mt5'>
					<div id="histogram" style={{width:' 700px',height:'500px'}} className='f-mb2'></div>
					<div id="income" style={{width:' 700px',height:'400px'}}></div> 
					<div id="expenditure" style={{width:' 700px',height:'400px'}}></div>  
				</div>
		</div>);
	}
}
const BaseCampus = Form.create()(Main);
export {
	BaseCampus
}