import React, {
	Component
} from 'react';
import { Breadcrumb, DatePicker, Form, Row, Col, message, Table } from 'antd';
import '../main.less';
import $ from 'jquery';
import { _getHistogram1 } from '../../common/g.js';
import echarts from 'echarts';
const FormItem = Form.Item;

class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			startValue: null,
			endValue: null,
			endOpen: false,
			dataSource: []
		}
	}
	disabledStartDate = (startValue) => { //只读的开始日期
		const endValue = this.state.endValue;
		if(!startValue || !endValue) {
			return false;
		}
		return startValue.valueOf() > endValue.valueOf();
	}

	disabledEndDate = (endValue) => { //只读的结束日期
		const startValue = this.state.startValue;
		if(!endValue || !startValue) {
			return false;
		}
		return endValue.valueOf() <= startValue.valueOf();
	}

	onChange = (field, value) => {
		this.setState({
			[field]: value,
		});
	}
	//开始日期改变
	onStartChange = (value) => {
		this.onChange('startValue', value);
	}
	//结束日期改变
	onEndChange = (value) => {
		this.onChange('endValue', value);
	}

	handleStartOpenChange = (open) => {
		if(!open) {
			this.setState({
				endOpen: true
			});
		}
	}

	handleEndOpenChange = (open) => {
		this.setState({
			endOpen: open
		});
	}
	_getHistogram(fieldsValue) { //流水账柱状图
		let chart = echarts.init(document.getElementById('histogram'));
		let $this = $('#histogram');
		let color = {
			color1: ['#2187FF'],
			color2: ['#FFC61C']
		};
		let $name = {
			text: '',
			subtext: '流水账（元）'
		}
		let chart1 = echarts.init(document.getElementById('income'));
		let $this1 = $('#income');
		let chart2 = echarts.init(document.getElementById('expenditure'));
		let $this2 = $('#expenditure');
		let type = 1; //柱状图
		let type1 = 2; //第一个饼图
		let type2 = 3; //第二个饼图
		let $pageShow = $('.pageShow'); //图表和明细整体所在的容器
		let $noData = $('.noData'); //点击生成报表之前默认展示暂无数据......
		let urldata = '/api/finance/financeFundFlow/statisticsFinanceFundFlow';
		_getHistogram1(fieldsValue, urldata, chart, $this, $name, color, chart1, $this1, chart2, $this2, type, type1, type2, $pageShow, $noData, function(res) {});
	}
	componentDidMount() {
		setTimeout(() => window.scrollTo(0, 0), 150);
		$('.pageShow').hide();
	}
	makeReport() { //点击生成报表事件
		this.props.form.validateFields(
			(err, fieldsValue) => {
				if(!err) {
					fieldsValue['startTime'] = fieldsValue['startTime'] ? fieldsValue['startTime'].format('YYYY-MM-DD HH:mm:ss') : '';
					fieldsValue['endTime'] = fieldsValue['endTime'] ? fieldsValue['endTime'].format('YYYY-MM-DD HH:mm:ss') : '';
					fieldsValue.type = '11';
					this._getHistogram(fieldsValue); //柱状图
				} else {
					message.error('请先选好必填条件才可以生成报表哦')
				}
			}
		)
	}
	render() {
		const dataSource = [{
			key: '1',
			name: '胡彦斌',
			age: 32,
			address: '西湖区湖底公园1号',
		}, {
			key: '2',
			name: '胡彦祖',
			age: 42,
			address: '西湖区湖底公园1号'
		}];

		const columns = [{
			title: '学员姓名（学号）',
			dataIndex: 'address',
			key: '0',
			fixed: 'left',
			width: 100,
			className: 'f-align-center'
		}, {
			title: '时间段',
			dataIndex: 'name',
			key: '1',
			className: 'f-align-center',
			width: 150
		}, {
			title: '校区',
			dataIndex: 'age',
			className: 'f-align-center',
			key: '2',
			width: 150
		}, {
			title: '支付方式',
			className: 'f-align-center',
			dataIndex: 'address',
			key: '3',
			width: 150
		}, {
			title: '收支类别',
			dataIndex: 'address',
			className: 'f-align-center',
			key: '4',
			width: 150
		}, {
			title: '金额',
			dataIndex: 'address',
			className: 'f-align-center',
			key: '5',
			width: 150
		}, {
			title: '业务类型',
			dataIndex: 'address',
			className: 'f-align-center',
			key: '6',
			width: 150
		}, {
			title: '学费状态',
			dataIndex: 'address',
			className: 'f-align-center',
			key: '7',
			width: 150
		}, {
			title: '是否欠费',
			dataIndex: 'address',
			className: 'f-align-center',
			key: '8',
			width: 150
		}, {
			title: '是否优惠',
			dataIndex: 'address',
			className: 'f-align-center',
			key: '9',
			width: 150
		}, {
			title: '报课来源',
			dataIndex: 'address',
			className: 'f-align-center',
			key: '10',
			width: 150
		}, {
			title: '上课老师',
			dataIndex: 'address',
			className: 'f-align-center',
			key: '11',
			width: 150
		}, {
			title: '业务信息描述',
			dataIndex: 'address',
			className: 'f-align-center',
			key: '12',
			width: 150
		}, {
			title: '销售员',
			dataIndex: 'address',
			className: 'f-align-center',
			key: '13',
			width: 150
		}, {
			title: '操作员',
			dataIndex: 'address',
			className: 'f-align-center',
			key: '14',
			width: 150,
			fixed: 'right'
		}];
		const formItemLayout = {
			labelCol: {
				span: 6
			},
			wrapperCol: {
				span: 18
			}
		};
		const {
			getFieldDecorator
		} = this.props.form;
		return(<div className='courseArranging Topselect classListPage wid100'>
				<Breadcrumb className="f-mb3">
				    <Breadcrumb.Item>财务统计</Breadcrumb.Item>
				    <Breadcrumb.Item>流水统计</Breadcrumb.Item>
				    <Breadcrumb.Item>
				   		<span className="f-fz5 f-bold">按支付方式</span>
				    </Breadcrumb.Item>
				</Breadcrumb>
				<Form className='f-box-shadow1 f-radius1 f-bg-white' style={{padding:'16px 16px 0 16px'}}>
					<Row>
						<Col span={12}>
							<Col span={4} className='f-fz4 f-black f-label f-h4-lh4'>时间段:&emsp;</Col>
				        		<Col span={20} >
					    			 <Col span={11}>
					    			  <FormItem {...formItemLayout}>
								      {getFieldDecorator('startTime',{
								      	rules: [{
							              required: true,
							              message: '请先选择开始时间',
							            }],
								      })(
							            	<DatePicker
							            	 showTime
								          disabledDate={this.disabledStartDate.bind(this)}
								          format="YYYY-MM-DD HH:mm:ss"
								          setFieldsValue={this.state.startValue}
								          placeholder="开始时间"
								          onChange={this.onStartChange.bind(this)}
								          onOpenChange={this.handleStartOpenChange.bind(this)} />
						            		)}
								     </FormItem>
					    			 </Col>
					    			 <Col span={2} className='f-align-center f-h4-lh4'>--</Col>
					    			  <Col span={11}>
					    			  	<FormItem {...formItemLayout}>
								          {getFieldDecorator('endTime',{
								          	rules: [{
								              required: true,
								              message: '请先选择结束时间',
								            }],
								          })(
								            <DatePicker
								             showTime
									          disabledDate={this.disabledEndDate.bind(this)}
									          format="YYYY-MM-DD HH:mm:ss"
									          setFieldsValue={this.state.endValue}
									          placeholder="结束时间"
									          onChange={this.onEndChange.bind(this)}
									          open={this.state.endOpen}
									          onOpenChange={this.handleEndOpenChange.bind(this)}/>
							            )}
								     </FormItem>
						         	</Col>
							    </Col>
					       </Col>
					        	<Col span={12} className='f-align-right f-pr4'>
								<a className="f-btn-blue" onClick={this.makeReport.bind(this)}>生成报表</a>
					        	</Col>
					</Row>
				</Form>
				<div className='f-box-shadow1 f-radius1 f-bg-white f-pd4 f-mt5 pageShow'>
					<div id="histogram" style={{width:' 1000px',height:'500px'}} className='f-mb2'></div>
					<div id="income" style={{width:' 1000px',height:'400px'}}></div> 
					<div id="expenditure" style={{width:' 1000px',height:'400px'}}></div>  
					{/*<div className='f-mt5 f-mb4'>
						<div className='f-flex f-mb3' style={{justifyContent: 'space-between'}}>
							<h3 className="f-title-blue">明细列表</h3>
							<a className="f-btn-blue _export">
							<i className='iconfont icon-chuangjian f-mr1'></i>
							导出报表</a>
						</div>
						<Table columns={columns} dataSource={dataSource} scroll={{ x: 2200}} bordered pagination={false}/>
					</div>*/}
				</div>
				<div className='noData f-mt4 f-align-center'>暂无数据......</div>
				
		</div>);
	}
}
const PayStream = Form.create()(Main);
export {
	PayStream
}