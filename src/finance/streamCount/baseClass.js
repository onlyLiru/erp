import React, {
	Component
} from 'react';
import { Breadcrumb, DatePicker, Form, Row, Col, Select } from 'antd';
import '../main.less';
import $ from 'jquery';
import { _getHistogram1 } from '../../common/g.js';
import echarts from 'echarts';

const Option = Select.Option;
const FormItem = Form.Item;

class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			batch: false, //是否批量
			startValue: null,
			endValue: null,
			endOpen: false,
			schoolVal: null, //校区value
			classVal: null //班级value
		}
	}
	disabledStartDate = (startValue) => { //只读的开始日期
		const endValue = this.state.endValue;
		if(!startValue || !endValue) {
			return false;
		}
		return startValue.valueOf() > endValue.valueOf();
	}

	disabledEndDate = (endValue) => { //只读的结束日期
		const startValue = this.state.startValue;
		if(!endValue || !startValue) {
			return false;
		}
		return endValue.valueOf() <= startValue.valueOf();
	}

	onChange = (field, value) => {
		this.setState({
			[field]: value,
		});
	}
	//开始日期改变
	onStartChange = (value) => {
		this.onChange('startValue', value);
	}
	//结束日期改变
	onEndChange = (value) => {
		this.onChange('endValue', value);
	}

	handleStartOpenChange = (open) => {
		if(!open) {
			this.setState({
				endOpen: true
			});
		}
	}

	handleEndOpenChange = (open) => {
		this.setState({
			endOpen: open
		});
	}
	_getHistogram() { //收入和支出
		let chart = echarts.init(document.getElementById('histogram'));
		let $this = $('#histogram');
		let data = {};
		let gardenval = $('.kindergarten').val();
		if(!gardenval) {
			data["mode"] = 'Total';
			data["edu_id"] = '';
		} else {
			data["mode"] = 'Edu';
			data["edu_id"] = gardenval;
		}
		let range_type = $('.consumeSelect').val();
		if(range_type == 4) {
			data['range_type'] = 4;
			data['start_date'] = $('#start_time').val();
			data['end_date'] = $('#end_time').val();
		} else {
			data['start_date'] = '';
			data['end_date'] = '';
			data['range_type'] = range_type;
		}
		let $name = {
			text: '',
			subtext: '流水账（元）'
		}
		let urldata = '/admin/investor/report/home/distribute/analysis/json/';
		_getHistogram1(data, urldata, chart, $this, $name, function(res) {});
	}
	componentDidMount() {
		this._getHistogram(); //柱状图
	}
	makeReport() { //点击生成报表事件
		this._getHistogram(); //柱状图
	}
	render() {
		const {
			getFieldDecorator
		} = this.props.form;
		const formItemLayout = {
			labelCol: {
				span: 6
			},
			wrapperCol: {
				span: 18
			}
		};
		return(<div className='courseArranging Topselect classListPage wid100'>
				<Breadcrumb className="f-mb3">
				    <Breadcrumb.Item>财务统计</Breadcrumb.Item>
				    <Breadcrumb.Item>流水统计</Breadcrumb.Item>
				    <Breadcrumb.Item>
				   		<span className="f-fz5 f-bold">按班级</span>
				    </Breadcrumb.Item>
				</Breadcrumb>
				<Form className='f-box-shadow1 f-radius1 f-bg-white f-pd4'>
					<Row className='f-mb3'>
						 <Col span={6} className='f-mr5'>
						 	<Col span={8} ><span className='f-fz4 f-black f-h4-lh4'>校区:&emsp;</span></Col>
						 	<Col span={16}>
				              <FormItem {...formItemLayout} label=''>
				                  {getFieldDecorator('school', {
				                  	valuePropName: 'value',
				                  })(
				                    <Select
				                        showSearch
				                        allowClear
				                        placeholder="请选择"
				                        optionFilterProp="children"
				                        filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
				                      >
				                        	<Option value="">收入</Option>
					              		<Option value="male">支出</Option>
				                      </Select>  
				                  )}
				                </FormItem>
				               </Col>
				            </Col>
				            <Col span={6} >
						 	<Col span={8} className='f-align-right'><span className='f-fz4 f-black f-h4-lh4'>班级:&emsp;</span></Col>
						 	<Col span={16} className='zdschool'>
				              <FormItem {...formItemLayout} label=''>
				                  {getFieldDecorator('class', {
				                  	valuePropName: 'value',
				                  })(
				                    <Select
							          mode="multiple"
							          placeholder="请选择"
							          style={{ width: '100%' }}>
							          	<Option value="">收入</Option>
					              		<Option value="1">支出</Option>
					              		<Option value="2">收入</Option>
					              		<Option value="3">支出</Option>
					              		<Option value="4">收入</Option>
					              		<Option value="5">支出</Option>
					              		<Option value="6">收入</Option>
					              		<Option value="7">支出</Option>
							        </Select> 
				                  )}
				                </FormItem>
				               </Col>
				            </Col>
						
					</Row>
					<Row>
						<Col span={12}>
							<Col span={4} className='f-fz4 f-black f-h4-lh4'>时间段:&emsp;</Col>
				        		<Col span={20} >
					    			 <Col span={11}>
						            	<DatePicker
								          disabledDate={this.disabledStartDate.bind(this)}
								          showTime
								          format="YYYY-MM-DD HH:mm:ss"
								          setFieldsValue={this.state.startValue}
								          placeholder="开始时间"
								          onChange={this.onStartChange.bind(this)}
								          onOpenChange={this.handleStartOpenChange.bind(this)}/>
					    			 </Col>
					    			 <Col span={2} className='f-align-center'>--</Col>
					    			  <Col span={11}>
							            <DatePicker
								          disabledDate={this.disabledEndDate.bind(this)}
								          showTime
								          format="YYYY-MM-DD HH:mm:ss"
								          setFieldsValue={this.state.endValue}
								          placeholder="结束时间"
								          onChange={this.onEndChange.bind(this)}
								          open={this.state.endOpen}
								          onOpenChange={this.handleEndOpenChange.bind(this)}/>
						         	</Col>
							    </Col>
					       </Col>
					        	<Col span={12} className='f-align-right f-pr4'>
								<a className="f-btn-blue" onClick={this.makeReport.bind(this)}>生成报表</a>
					        	</Col>
					</Row>
				</Form>
				<div className='f-box-shadow1 f-radius1 f-bg-white f-pd4 f-mt5'>
					<div id="histogram" style={{width:' 700px',height:'500px'}} className='f-mb2'></div>
					<div id="income" style={{width:' 700px',height:'400px'}}></div> 
					<div id="expenditure" style={{width:' 700px',height:'400px'}}></div>  
				</div>
		</div>);
	}
}
const BaseClass = Form.create()(Main);
export {
	BaseClass
}