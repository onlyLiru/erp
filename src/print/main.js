import React, {
	Component
} from 'react';
import {
	Button,
	message
} from 'antd';
import printJS from 'print-js';
import {
	fpost
} from '../common/io.js';
import {
	Loading,
	NoData,
	getUrlParam,
	undefinedToEmpty
} from '../common/g.js';

import './main.less';

export default class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			type: getUrlParam('type'),
			//0，充值，1，退费，补欠费，退差价，停课，2，物品售卖，3，单期报名，4，连报，5，转班
			printData: null,
		}
	}
	render() {
		let {
			type,
			printData
		} = this.state;

		if (!printData) {
			return <Loading />;
		}

		let Html = '';

		switch (type) {
			case '0': //充值
				Html = <table>
			      		<caption>{printData.schoolGroupName}充值业务凭证</caption>
			      		<thead>
			      			<tr>
			      				<th>业务类型</th>
			      				<th>学员姓名(学号)</th>
			      			</tr>
			      		</thead>
			      		<tbody>
			      			<tr>
				      			<td>余额充值</td>
				      			<td>{printData.name}({printData.stuNo})</td>
			      			</tr>
			      			<tr>
				      			<td>充值: ¥{printData.amount}</td>
				      			<td>备注:{printData.foreignRemark}</td>
			      			</tr>
			      			<tr>
			      				<td colSpan="2">
						      		<p>（交易号：{printData.seriesNo}）</p>
			      				</td>
			      			</tr>
			      		</tbody>
			      	</table>;

				break;
			case '1': //退费，补欠费，退差价，停课
				let {
					businessType,
					courseName,
					arrearAmount,
					classId,
					className,
					classroom,
					consumeCoursePeriod,
					consumedAmount,
					currTime,
					foreignRemark,
					offsetBalanceAmount,
					operator,
					originalAmount,
					preferentialAmount,
					registrationCourseId,
					restAmount,
					schoolAreaAddress,
					schoolAreaName,
					schoolAreaContractPhone,
					schoolGroupName,
					seriesNo,
					studentId,
					studentName,
					teacherName,
					totalMaterialAmount,
					trades,
					amount,
					businessTypeStr
				} = printData;

				let tradesHtml;

				if (trades && trades.length) {
					let tradesInnerHtml = trades.map((d, i) => {
						let {
							tradeAmount,
							tradeType
						} = d;
						let tradeName = ['', '现金', '微信', '支付宝', '刷卡', '转账', '支票'][tradeType];

						return `${tradeName} : ¥${tradeAmount} `
					});
					tradesHtml = `【 ${tradesInnerHtml }】`;
				}

				Html = <table>
			      		<caption>{schoolGroupName}（{schoolAreaName}）{businessTypeStr}业务凭证</caption>
			      		<thead>
			      			<tr>
			      				<th>业务类型</th>
			      				<th>班级／课程</th>
			      				<th>已上课时／消耗学费</th>
			      				<th>剩余学费</th>
			      			</tr>
			      		</thead>
			      		<tbody>
			      			<tr>
				      			<td>{businessTypeStr}</td>
				      			<td>
				      				{className} / {courseName}
				      				【剩余学费:¥{restAmount || 0} / 欠费: ¥{arrearAmount}】
			      				</td>
				      			<td>{consumeCoursePeriod || 0} / ¥{consumedAmount || 0}</td>
				      			<td>{restAmount}</td>
			      			</tr>
			      			<tr>
				      			<td>支付：¥{amount}</td>
				      			<td colSpan="3">
				      				<span className="f-mr5">教师：{teacherName}</span>
				      				<span className="f-mr5">{schoolAreaName} ：{classroom}</span>
				      				<span className="f-mr5">（{schoolAreaAddress}）{schoolAreaContractPhone}</span>
			      				</td>
			      			</tr>
			      			<tr>
			      				<td colSpan="4">
			      					备注：{foreignRemark}
			      				</td>
			      			</tr>
			      			<tr>
			      				<td colSpan="4">
						      		<p>（交易号：{seriesNo}）</p>
						      		<p>学费：¥{originalAmount} | 优惠：¥{preferentialAmount}| 余额抵用：¥{offsetBalanceAmount} |欠费：¥{arrearAmount}| 物品费用：¥{totalMaterialAmount} | 总计：¥{amount} {tradesHtml}</p>
						      		<p className="f-blue">此凭证丢失不予补办：4课时后不予退费；开结课时间以实际为准。</p>
						      		<p>
						      			学员姓名：{studentName} | 经办人：{operator} | {currTime} |  经办人签字：   
						      			<span style={{marginLeft:'60px'}}>客户签字：</span>
					      			</p>
			      				</td>
			      			</tr>
			      		</tbody>
			      	</table>;

				break;
			case '2': //物品售卖
				trades = printData.trades;
				let orderCertificateDetailVOs = printData.orderCertificateDetailVOs;

				if (trades && trades.length) {
					let tradesInnerHtml = trades.map((d, i) => {
						let {
							tradeAmount,
							tradeType
						} = d;
						let tradeName = ['', '现金', '微信', '支付宝', '刷卡', '转账', '支票'][tradeType];

						return `${tradeName} : ¥${tradeAmount} `
					});
					tradesHtml = `【 ${tradesInnerHtml }】`;
				}

				Html = <table>
			      		<caption>{printData.schoolGroupName}（{printData.schoolAreaName}）{printData.businessTypeStr}业务凭证</caption>
			      		<thead>
			      			<tr>
			      				<th>业务类型</th>
			      				<th>物品名称／类别（领取状态）</th>
			      			</tr>
			      		</thead>
			      		<tbody>
			      			<tr>
				      			<td>{printData.businessTypeStr}</td>
				      			<td>
				      				{
				      					orderCertificateDetailVOs.map((d,i)=> {
				      						console.log(d);
				      						return <p key={i}>
				      							{d.materialName}
				      							/
				      							{d.category} x {d.qty}
				      							/
				      							{ d.isTaken == '1' ? '已领取' : '未领取' }
			      							</p>
				      					})
				      				}
			      				</td>
			      			</tr>
			      			<tr>
			      				<td colSpan="4">
			      					备注：{printData.foreignRemark}
			      				</td>
			      			</tr>
			      			<tr>
			      				<td colSpan="4">
						      		<p>（交易号：{printData.seriesNo}）</p>
						      		<p>优惠：¥{printData.preferentialAmount	} | 物品费用：¥{printData.paymentAmount} | 总计：¥{printData.totalMaterialAmount} {tradesHtml}</p>
						      		<p className="f-blue">此凭证丢失不予补办：4课时后不予退费；开结课时间以实际为准。</p>
						      		<p>
						      			学员姓名：{printData.buyName} | 经办人：{printData.operatorName} | {printData.conductTime} |  经办人签字：   
						      			<span style={{marginLeft:'60px'}}>客户签字：</span>
					      			</p>
			      				</td>
			      			</tr>
			      		</tbody>
			      	</table>;

				break;
			case '3': //单期报课
				trades = printData.printTradeVOS;

				if (trades && trades.length) {
					let tradesInnerHtml = trades.map((d, i) => {
						let {
							tradeAmount,
							tradeType
						} = d;
						let tradeName = ['', '现金', '微信', '支付宝', '刷卡', '转账', '支票'][tradeType];

						return `${tradeName} : ¥${tradeAmount} `
					});
					tradesHtml = `【 ${tradesInnerHtml }】`;
				}

				Html = <table>
		      		<caption>{printData.schoolGroupName}（{printData.schoolAreaName}）单期报名业务凭证</caption>
		      		<thead>
		      			<tr>
		      				<th>业务类型</th>
		      				<th>班级／课程</th>
		      				<th>上课时长</th>
		      				<th>上课时段</th>
		      			</tr>
		      		</thead>
		      		<tbody>
		      			<tr>
			      			<td rowspan={printData.printCourseSignUpVOS.length}>单期报名</td>
			      			<td>
		      					{ printData.printCourseSignUpVOS[0].className } 
		      					 / 
		      					{ printData.printCourseSignUpVOS[0].courseName } 
		      				</td>
		      				<td>
		      					{ printData.printCourseSignUpVOS[0].classStartDate } 
		      					 到 
		      					{ printData.printCourseSignUpVOS[0].classEndDate } 
		      				</td>
		      				<td>
		      					{ printData.printCourseSignUpVOS[0].classStartTime } 
		      					到 
		      					{ printData.printCourseSignUpVOS[0].classEndTime } 
		      				</td>
		      			</tr>
		      			{
		      				printData.printCourseSignUpVOS.map((d,i)=> {
		      					if(i>0) {
			      					return (
						      			<tr key={i}>
						      				<td>
						      					{ d.className } 
						      					 / 
						      					{ d.courseName } 
						      				</td>
						      				<td>
						      					{ d.classStartDate } 
						      					 到 
						      					{ d.classEndDate } 
						      				</td>
						      				<td>
						      					{ d.classStartTime } 
						      					到 
						      					{ d.classEndTime } 
						      				</td>
						      			</tr>
		      						);
		      					}
		      				})
		      			}
		      			<tr>
			      			<td>支付：¥{printData.amount}</td>
			      			<td colSpan="3">
			      				<span className="f-mr5">教师: {printData.teacherName}</span>
			      				<span className="f-mr5">教室: {printData.classRoomName}</span>
			      				<span className="f-mr5">地址: {printData.address} 电话:{printData.contractPhone}</span>
		      				</td>
		      			</tr>
		      			<tr>
		      				<td colSpan="4">
		      					备注：{printData.foreignRemark}
		      				</td>
		      			</tr>
		      			<tr>
		      				<td colSpan="4">
					      		<p>（交易号：{printData.serialNo}）</p>
					      		<p>
					      			学费：¥{printData.printCourseSignUpAmountVO.receivableAmount} | 
					      			优惠：¥{printData.printCourseSignUpAmountVO.preferentialAmount} |  
					      			余额抵用：¥{printData.printCourseSignUpAmountVO.offsetBalanceAmount} | 
					      			欠费：¥{printData.printCourseSignUpAmountVO.arrearAmount} |  
					      			物品费用：¥{printData.printCourseSignUpAmountVO.totalMaterialAmount} |  
					      			总计：¥{printData.printCourseSignUpAmountVO.resultAmount} 
					      			{tradesHtml}
				      			</p>
					      		<p className="f-blue">此凭证丢失不予补办：4课时后不予退费；开结课时间以实际为准。</p>
					      		<p>
					      			学员姓名：{printData.studentName} |   
					      			经办人：{printData.operatorName} |   
					      			{ printData.createTime } | 经办人签字：   
					      			<span style={{marginLeft:'60px'}}>客户签字：</span>
				      			</p>
		      				</td>
		      			</tr>
		      		</tbody>
		      	</table>;
				break;
			case '4': //多期连报
				trades = printData.printTradeVOS;

				if (trades && trades.length) {
					let tradesInnerHtml = trades.map((d, i) => {
						let {
							tradeAmount,
							tradeType
						} = d;
						let tradeName = ['', '现金', '微信', '支付宝', '刷卡', '转账', '支票'][tradeType];

						return `${tradeName} : ¥${tradeAmount} `
					});
					tradesHtml = `【 ${tradesInnerHtml }】`;
				}

				Html = <table>
		      		<caption>{printData.schoolGroupName}（{printData.schoolAreaName}）单期报名业务凭证</caption>
		      		<thead>
		      			<tr>
		      				<th>业务类型</th>
		      				<th>第一期班级／课程</th>
		      				<th>上课时长</th>
		      				<th>上课时段</th>
		      			</tr>
		      		</thead>
		      		<tbody>
		      			<tr>
			      			<td>连报{printData.printCourseSignUpVOS[0].totalPeriod}期课程</td>
			      			<td>
			      				{ printData.printCourseSignUpVOS[0].className } 
			      				/ 
			      				{ printData.printCourseSignUpVOS[0].courseName } 
			      			</td>
			      			<td>
			      				{ printData.printCourseSignUpVOS[0].classStartDate } 
			      				到 
			      				{ printData.printCourseSignUpVOS[0].classEndDate } 
			      			</td>
			      			<td>
			      				{ printData.printCourseSignUpVOS[0].classStartTime } 
			      				到 
			      				{ printData.printCourseSignUpVOS[0].classEndTime } 
			      			</td>
		      			</tr>
		      			<tr>
			      			<td>支付：¥{printData.amount}</td>
			      			<td colSpan="3">
			      				<span className="f-mr5">教师: {printData.teacherName}</span>
			      				<span className="f-mr5">教室: {printData.classRoomName}</span>
			      				<span className="f-mr5">地址: {printData.address} 电话:{printData.contractPhone}</span>
		      				</td>
		      			</tr>
		      			<tr>
		      				<td colSpan="4">
		      					备注：{printData.foreignRemark}
		      				</td>
		      			</tr>
		      			<tr>
		      				<td colSpan="4">
					      		<p>（交易号：{printData.serialNo}）</p>
					      		<p>
					      			学费：¥{printData.printCourseSignUpAmountVO.receivableAmount} | 
					      			优惠：¥{printData.printCourseSignUpAmountVO.preferentialAmount} |  
					      			余额抵用：¥{printData.printCourseSignUpAmountVO.offsetBalanceAmount} | 
					      			欠费：¥{printData.printCourseSignUpAmountVO.arrearAmount} |  
					      			物品费用：¥{printData.printCourseSignUpAmountVO.totalMaterialAmount} |  
					      			总计：¥{printData.printCourseSignUpAmountVO.resultAmount} 
					      			{tradesHtml}
				      			</p>
					      		<p className="f-blue">此凭证丢失不予补办：4课时后不予退费；开结课时间以实际为准。</p>
					      		<p>
					      			学员姓名：{printData.studentName} |   
					      			经办人：{printData.operatorName} |   
					      			{ printData.createTime } | 经办人签字：   
					      			<span style={{marginLeft:'60px'}}>客户签字：</span>
				      			</p>
		      				</td>
		      			</tr>
		      		</tbody>
		      	</table>;
				break;
			case '5': //转班
				trades = printData.printTradeVOS;

				if (trades && trades.length) {
					let tradesInnerHtml = trades.map((d, i) => {
						let {
							tradeAmount,
							tradeType
						} = d;
						let tradeName = ['', '现金', '微信', '支付宝', '刷卡', '转账', '支票'][tradeType];

						return `${tradeName} : ¥${tradeAmount} `
					});
					tradesHtml = `【 ${tradesInnerHtml }】`;
				}

				Html = <table>
		      		<caption>{printData.schoolGroupName}（{printData.schoolAreaName}）单期报名业务凭证</caption>
		      		<thead>
		      			<tr>
		      				<th>业务类型</th>
		      				<th>转出班级／课程</th>
		      				<th>转入班级／课程</th>
		      				<th>上课时长</th>
		      				<th>上课时段</th>
		      			</tr>
		      		</thead>
		      		<tbody>
		      			<tr>
			      			<td>转班</td>
			      			<td>
			      				{ printData.transferClassVO.className } 
			      				/ 
			      				{ printData.transferClassVO.courseName } 
			      				【 剩余学费 
			      					{printData.transferClassVO.remainTuitionAmount}
			      					/
			      					欠费: {printData.transferClassVO.arrearAmount} 
		      					】
			      			</td>
			      			<td>
			      				{ printData.printCourseSignUpVOS[0].className } 
			      				/ 
			      				{ printData.printCourseSignUpVOS[0].courseName } 
			      			</td>
			      			<td>
			      				{ printData.printCourseSignUpVOS[0].classStartDate } 
			      				到 
			      				{ printData.printCourseSignUpVOS[0].classEndDate } 
			      			</td>
			      			<td>
			      				{ printData.printCourseSignUpVOS[0].classStartTime } 
			      				到 
			      				{ printData.printCourseSignUpVOS[0].classEndTime } 
			      			</td>
		      			</tr>
		      			<tr>
			      			<td>支付：¥{printData.amount}</td>
			      			<td colSpan="4">
			      				<span className="f-mr5">教师: {printData.teacherName}</span>
			      				<span className="f-mr5">教室: {printData.classRoomName}</span>
			      				<span className="f-mr5">地址: {printData.address} 电话:{printData.contractPhone}</span>
		      				</td>
		      			</tr>
		      			<tr>
		      				<td colSpan="5">
		      					备注：{printData.foreignRemark}
		      				</td>
		      			</tr>
		      			<tr>
		      				<td colSpan="5">
					      		<p>（交易号：{printData.serialNo}）</p>
					      		<p>
					      			学费：¥{printData.printCourseSignUpAmountVO.receivableAmount} | 
					      			优惠：¥{printData.printCourseSignUpAmountVO.preferentialAmount} |  
					      			余额抵用：¥{printData.printCourseSignUpAmountVO.offsetBalanceAmount} | 
					      			欠费：¥{printData.printCourseSignUpAmountVO.arrearAmount} |  
					      			物品费用：¥{printData.printCourseSignUpAmountVO.totalMaterialAmount} |  
					      			总计：¥{printData.printCourseSignUpAmountVO.resultAmount} 
					      			{tradesHtml}
				      			</p>
					      		<p className="f-blue">此凭证丢失不予补办：4课时后不予退费；开结课时间以实际为准。</p>
					      		<p>
					      			学员姓名：{printData.studentName} |   
					      			经办人：{printData.operatorName} |   
					      			{ printData.createTime } | 经办人签字：   
					      			<span style={{marginLeft:'60px'}}>客户签字：</span>
				      			</p>
		      				</td>
		      			</tr>
		      		</tbody>
		      	</table>;
				break;
			default:
				Html = <NoData />;
		}


		return (<div className="print-container">
			<div id="J-print-content" className="f-print-table">
				{Html}
			</div>
	      	<div className="f-align-right">
	      		<Button
					size = "large"
					className="f-mr5"
					onClick = {
						()=> {
							window.history.back();
						}
					} 
				>
					返回 
				</Button>
				<Button
					size = "large"
					type = "primary"
					onClick = {
						() => {
							printJS({
								printable: 'J-print-content',
								type: 'html',
								documentTitle: '凡学教育集团',
								honorColor: true
							});
						}
					} 
				>
					打印 
				</Button>
			</div>
		</div>);
	}
	componentDidMount() {
		this._getPrintData();
	}
	_getPrintData() {
		let {
			type
		} = this.state;

		let studentId = getUrlParam('studentId');
		let id = getUrlParam('id');

		let businessType = getUrlParam('businessType');
		let businessId = getUrlParam('businessId');
		let financeFundFlowIds = getUrlParam('financeFundFlowIds');

		let orderInfoId = getUrlParam('orderInfoId');

		let registrationId = getUrlParam('registrationId');
		let schoolAreaId = getUrlParam('schoolAreaId');

		let url;
		let param = {};

		switch (type) {
			case '0': //充值
				url = '/api/reception/student/studentRecharge/print';
				param = {
					studentId,
					studentBalanceFlowId: id
				}
				break;
			case '1': //退费，补欠费，退差价，停课
				url = '/api/reception/transaction/credentialsPrint';
				param = {
					businessId,
					businessType,
					financeFundFlowIds
				}
				break;
			case '2': //物品售卖 
				url = '/api/reception/order/credentialsPrint';
				param = {
					orderInfoId
				}
				break;
			case '3': //单期报名 
				url = '/api/reception/single/course/signup/print';
				param = {
					registrationId,
					schoolAreaId
				}
				break;
			case '4': //多期连报 
				url = '/api/reception/multiple/course/signup/print';
				param = {
					registrationId,
					schoolAreaId
				}
				break;
			case '5': //转班
				url = '/api/reception/transfer/course/print';
				param = {
					registrationId,
					schoolAreaId
				}
				break;
			default:
				return false;
		}

		fpost(url, param)
			.then(res => res.json())
			.then((res) => {
				if (!res.success) {
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res);
			})
			.then((res) => {
				console.log(res);
				this.setState({
					printData: res.result
				}, () => {
					this._print();
				});
			})
			.catch((err) => {
				console.log(err);
			});
	}
	_print() {
		this.setState({
			showPrintModal: true
		});
	}
}