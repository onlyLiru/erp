import React, {
	Component
} from 'react';
import { Breadcrumb, Button, Table, Form, Row, message, Input, Pagination,Tabs ,Col,Select} from 'antd';
import '../main.less';
import $ from 'jquery';
import { Loading } from '../../common/g.js';
import {fpost } from '../../common/io.js';
let classname={};
const TabPane = Tabs.TabPane;
const FormItem = Form.Item;
const Option = Select.Option;
class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			name: null,
			pageResult: null, //每页显示的列表内容
			currentPage: 1, //当前页码
			pageSize: 10, //每页显示条数
			total: null, //总条数
			allSchool: [],
			depart1:[],
			posit1:[]
		}
	}
	_getSchool() { //获取全部校区信息
		const self = this;
		fpost('/api/system/schoolarea/listSchoolArea')
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					if (res.result) {
						self.setState({
							allSchool: res.result
						});
					}
				} else {
					message.error(res.message)
				}
			});
	}
	_getDepart() { //获取全部校区信息
		const self = this;
		fpost('/api/hr/staff/listDepartment')
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					if (res.result) {
						self.setState({
							depart1: res.result
						});
					}
				} else {
					message.error(res.message)
				}
			});
	}
	_getPosition() { //获取全部职位信息
		const self = this;
		fpost('/api/hr/staff/listPosition')
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					if (res.result) {
						self.setState({
							posit1: res.result
						});
					}
				} else {
					message.error(res.message)
				}
			});
	}
	componentDidMount() {
		this.jqSearch();
		this._getPosition();  //获取职位
		this._getDepart();//获取部门
		this._getSchool();  //获取校区
	}
	onShowSizeChange (current, pageSize) {
		this.setState({
			currentPage: current,
			pageSize: pageSize
		}, function() {
			if (this.state.searchState == 0) {
				this.jqSearch();
			} else {
				this.advancedSearch()
			}
		})
	}
	callback(key) {
		if (key == 2) {
			this.setState({
				searchState: 1
			})
		} else {
			this.setState({
				searchState: 0
			})
		}
	}
	reset() { //高级筛选里的重置
		this.props.form.resetFields();
		this.setState({
			currentPage: 1
		}, function() {
			this.advancedSearch()
		});
	}
	_getForm() {
		this.props.form.validateFields(
			(err, fieldsValue) => {
				fieldsValue['department'] = fieldsValue['department'] ? fieldsValue['department'] : '';
				fieldsValue['position'] = fieldsValue['position'] ? fieldsValue['position'] : '';
				fieldsValue['schoolAreaId'] = fieldsValue['schoolAreaId'] ? fieldsValue['schoolAreaId'] : '';
				fieldsValue.pageSize = this.state.pageSize;
				fieldsValue.currentPage = this.state.currentPage;
				this.getList(fieldsValue);
			}
		)
	}
	advancedSearch() { //筛选搜索
		this.setState({
			currentPage: 1
		}, function() {
			this._getForm(); //表单内容
		});
	}
	jqSearch() { //精确搜索
		this.setState({
			currentPage: 1,
		}, function() {
			classname['search'] = $.trim($('.inputBG').val());
			classname.pageSize = this.state.pageSize;
			classname.currentPage = this.state.currentPage;
			this.getList(classname);
		});
	}
	getList(classname) { //获取列表数据
		let self = this;
		fpost('/api/hr/staff/pageStaffList',classname)
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if(res.success == true) {
					if(res.result) {
						res.result.records.forEach((item, i) => {
							item.key = i;
						});
						self.setState({
							pageResult: res.result.records,
							total: parseInt(res.result.total)
						})
					}
				} else{
					message.error(res.message)
				}
			});
	}
	pageChange(pageNumber) { //列表分页获取当前页码
		this.setState({
			currentPage: pageNumber
		}, function() {
			if (!this.state.searchState) {
				classname['name'] = $.trim($('.inputBG').val());
				classname.pageSize = this.state.pageSize;
				classname.currentPage = this.state.currentPage;
				this.getList(classname);
			} else {
				this._getForm(); //表单内容
			}
		});
	}
	render() {
		const columns = [{
			title: '头像',
			key: '0',
			fixed: 'left',
			width: 100,
			className: 'f-align-center',
			render: (text, record, index) => {
				return(
					<div>
					{record.headPath?
						<img src={record.headPath} style={{width:'50px',height:'50px',borderRadius:'100%'}}/>:''
					}
					</div>
				)
			}
		}, {
			title: '员工姓名（工号）',
			key: '1',
			className: 'f-align-center',
			width: 250,
			render: (text, record, index) => {
				return(
					<i>{record.name}({record.staffNo})</i>
				)
			}
		}, {
			title: '性别',
			dataIndex: 'gender',
			className: 'f-align-center',
			key: '2',
			width: 100,
		}, {
			title: '手机',
			dataIndex: 'mobile',
			key: '3',
			className: 'f-align-center',
			width: 150,
		}, {
			title: '邮箱',
			className: 'f-align-center',
			dataIndex: 'email',
			key: '4',
			width: 150,
		}, {
			title: '籍贯',
			className: 'f-align-center',
			dataIndex: 'nativePlace',
			key: '5',
			width: 150,
		}, {
			title: '民族',
			className: 'f-align-center',
			dataIndex: 'nation',
			key: '6',
			width: 100,
		}, {
			title: '政治面貌',
			className: 'f-align-center',
			dataIndex: 'politics',
			key: '7',
			width: 150,
		}, {
			title: '婚姻状况',
			className: 'f-align-center',
			dataIndex: 'marriage',
			key: '8',
			width: 150,
		}, {
			title: '校区',
			className: 'f-align-center',
			key: '9',
			width: 150,
			render: (text, record, index) => {
				return(<div>
					{record.isAllArea==1?<i>全部校区</i>:<i>{record.schoolAreas}</i>}
					</div>)
			}
		}, {
			title: '部门',
			className: 'f-align-center',
			dataIndex: 'department',
			key: '10',
			width: 150,
		}, {
			title: '职位',
			className: 'f-align-center',
			dataIndex: 'position',
			key: '11',
			width: 100,
		}, {
			title: '在职状态',
			className: 'f-align-center',
			dataIndex: 'workState',
			key: '12',
			width: 150,
		}, {
			title: '操作',
			key: '13',
			fixed: 'right',
			className: 'f-align-center',
			width: 150,
			render: (text, record, index) => {
				return(
					<div>
                			<Button className='f-radius4 f-bg-green f-white f-viewdetail-check' onClick={()=>{window.location.href='/person/editArchives?id='+record.id+'&check=1'}}><i className='iconfont icon-chakan f-mr1 f-viewdetail-down f-fz5'></i>查看</Button>
			  		</div>
				)
			}
		}];
		const {
			getFieldDecorator
		} = this.props.form;
		const formItemLayout = {
			labelCol: {
				span: 6
			},
			wrapperCol: {
				span: 18
			}
		};
		return(<div className='courseArranging classListPage wid100'>
				<Breadcrumb className="f-mb3">
				    <Breadcrumb.Item>人事管理</Breadcrumb.Item>
				    <Breadcrumb.Item>员工管理</Breadcrumb.Item>
				    <Breadcrumb.Item>
				   		<span className="f-fz5 f-bold">员工列表</span>
				    </Breadcrumb.Item>
				</Breadcrumb>
				<div>
				<div className='Topselect f-bg-white f-radius1 f-box-shadow1' style={{width:'100%'}}>
					<Tabs defaultActiveKey="1" style={{textAlign:'center'}} tabPosition='top' animated={false} onChange={this.callback.bind(this)}>
						<TabPane tab={<span><i className='iconfont icon-hricon33 f-mr2'></i>精确查找</span>} key="1" style={{textAlign:'left'}}>
				    			<div style={{padding:'0 0 30px 26px'}}>
				    				<Input placeholder="请输入员工姓名/工号" className='inputBG pageInpHeight f-radius1' style={{width:'352px',border:0}}/>
				    				<Button type="primary" icon="search" style={{height:'40px'}} onClick={this.jqSearch.bind(this)}>搜索</Button>
				    			</div>
				    		</TabPane>
					    <TabPane tab={<span><i className='iconfont icon-shaixuan f-mr2' ></i>高级筛选</span>} key="2">
					    		<Form style={{padding:'0 20px'}}>
					    		<Row gutter={40}>
					          <Col span={8} >
					          	 <Col span={5} className='f-align-right f-fz4 f-black f-h4-lh4'>校区:&emsp;</Col>
					              <Col span={19} >
					              <FormItem {...formItemLayout}>
					                  {getFieldDecorator('schoolAreaId', {
					                  })(
					                   <Select
				                        showSearch
				                        allowClear
				                        placeholder="请选择"
				                        optionFilterProp="children"
				                        filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
				                      >
				                        	{ this.state.allSchool.map((elem, index) => {
							                  return(
							                  	<Option key={elem.id} value={elem.id}>{elem.name}</Option>
							                  )
							                }) 
										 }
				                      </Select> 
					                  )}
					                </FormItem>
						            </Col>
					          </Col>
					          <Col span={8} >
					          	 <Col span={5} className='f-align-right f-fz4 f-black f-h4-lh4'>部门:&emsp;</Col>
					              <Col span={19} >
					              <FormItem {...formItemLayout}>
					                  {getFieldDecorator('department', {
					                  })(
					                    <Select showSearch allowClear placeholder="全部部门" optionFilterProp="children"
					                        filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}>
					                        { this.state.depart1.map((item, index) => {
							                  return (<Option key={item} value={item}>{item}</Option>)
							                }) 
										 }
					                      </Select>  
					                  )}
					                </FormItem>
						            </Col>
					          </Col>
					          <Col span={8} >
					             <Col span={5} className='f-align-right f-fz4 f-black f-h4-lh4'>职位:&emsp;</Col>
					              <Col span={19} >
					              <FormItem {...formItemLayout}>
					                  {getFieldDecorator('position', {
					                  })(
					                    <Select showSearch allowClear placeholder="全部职位" optionFilterProp="children"
					                        filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}>
					                        	{ this.state.posit1.map((item, index) => {
								                  return (<Option key={item} value={item}>{item}</Option>)
								                }) 
											 }
					                      </Select>  
					                  )}
					                </FormItem>
						            </Col>
					          </Col>
					        </Row>
					        <Row className='f-pb3'>
					        		<Col span={12} className='f-align-right f-pr4'>
					        			<Button style={{border: '1px solid #999999',borderRadius: '4px',width:'110px',height:'40px'}} onClick={this.reset.bind(this)}>
					        				<i className='iconfont icon-reset f-mr2'></i>重置
					        			</Button>
					        		</Col>
					        		<Col span={12} className='f-align-left'>
					        			<Button type="primary" style={{borderRadius: '4px',width:'110px',height:'40px'}} onClick={this.advancedSearch.bind(this)}>
					        				<i className='iconfont icon-hricon33 f-mr2'></i>搜索
					        			</Button>
					        		</Col>
					        </Row>
					    		</Form>
					    </TabPane>
					 </Tabs>
				</div>
				<div className='f-bg-white f-pd4 f-box-shadow1 f-mt5 f-radius1 '>
					<div className='f-pb3 f-flex' style={{justifyContent: 'space-between'}}>
						<h3 className="f-title-blue">员工列表</h3>
						<a className="f-btn-green" onClick={()=>{window.location.href='/person/createArchives'}}>
							<i className='iconfont icon-tianjiajiahaowubiankuang f-mr1'></i>新建员工
						</a>
					</div>
					{this.state.pageResult?
						<div>
							<Table columns={columns} dataSource={this.state.pageResult} scroll={{ x: 1600}} bordered pagination={false}/>
								<div className='f-flex f-mt3' style={{justifyContent:'flex-end'}}>
									<div>
										<Pagination current={this.state.currentPage} total={this.state.total} onShowSizeChange={this.onShowSizeChange.bind(this)} showSizeChanger showQuickJumper onChange={this.pageChange.bind(this)}/>
									</div>
								</div>
						</div>:<Loading/>
					}
				</div>
				</div>
		</div>);
	}
}
const ArchivesList = Form.create()(Main);
export {
	ArchivesList
}