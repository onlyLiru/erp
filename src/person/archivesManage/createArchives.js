import React, {
	Component
} from 'react';
import { Breadcrumb, Button, Icon, Modal, DatePicker, Form, Row, Col, Select, message, Input, Radio, Upload, Tooltip, BackTop } from 'antd';
import moment from 'moment';
import '../main.less';
import { getUrlParam } from '../../common/g.js';
import $ from 'jquery';
import {fpost } from '../../common/io.js';
const u = require('../../common/io.js')
const Option = Select.Option;
const FormItem = Form.Item;
const RadioGroup = Radio.Group;
const dateFormat = 'YYYY-MM-DD HH:mm:ss';
let pageId = getUrlParam('id') || '';
let checkState=getUrlParam('check') || '';
let depart = null;
let posit = null;
let schoolLeng = 1;
let zdSchData;
u.hostname();
let actionUrl = u.hostname() + '/api/common/resource/uploadImg';

function zdschoolChange(value) {
	zdSchData = `${value}`;
	if(value.length < 1) {
		schoolLeng = null;
		message.error('指定校区不能为空');
	} else {
		schoolLeng = 1;
	}
}

class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			visible: false,
			startValue: null,
			endValue: null,
			gender: '1', //性别
			schoolState: null,
			audioCoverFileList: [],
			headResourceId: null, //头像
			previewVisible: false,
			previewImage: '',
			imageUrl: null,
			enName: '', //英文名
			identityNo: '', //身份证号
			birthday: null, //生日
			name: '', //员工姓名
			staffNo: '', //工号
			marriage: 0, //婚姻状况
			nativePlace: '', //籍贯
			nation: '', //民族
			mobile: '', //手机号
			politics: null, //政治面貌
			graduateFrom: '', //院校
			specialty: '', //专业
			education: '', //学历
			email: '', //邮箱
			personnelStatus: null, //人事状态
			laborContract: null, //劳动合同
			contractStartTime: null, //合同开始时间
			contractEndTime: null, // 合同结束时间
			positiveTime: null, //转正时间
			laborRelations: null, //劳动关系
			payrollCardBank: '', //开户行
			payrollName: '', //户名
			payrollCardNo: '', //卡号
			accumulationFundNo: '', //公积金号
			hasAccumulationFund: null, //是否办理公积金
			socialSecurityNo: '', //社保号
			hasSocialSecurity: null, //是否办理社保
			workState: '0', //在职状态
			allSchool: [], //全部校区
			isAllArea: null, //校区状态
			department1: [], //部门
			department: null, //部门
			position1: [], //职位
			position: '',
			staffSchoolAreaIds: null, //校区id
			schoolAreas: [],
			createS: null,
			 iconLoading: false,
			 checkState:0
		}
	}
	url() {  //初始化进入页面时做一下自动刷新一次
		if(window.location.href.indexOf('#reloaded') == -1) {
			window.location.href = window.location.href + "#reloaded";
			window.location.reload();
		}
	}
	componentDidMount() {
//		this.url();
		if(pageId) {
			this._getInfo(); //获取页面信息
			$('.new').html('查看员工');
		} else {
//			this.url();
			this.setState({
				createS: '1'
			})
		}
		this._getSchool(); //获取全部校区信息
		this._getDepart(); //获取所有部门信息
		this._getPosition(); //获取所有职位信息
	}
	_getPosition() { //获取全部职位信息
		const self = this;
		fpost('/api/hr/staff/listPosition')
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if(res.success == true) {
					if(res.result) {
						self.setState({
							position1: res.result
						});
						posit =
							self.state.position1.map((item, i) => {
								return(<Option key={item} value={item}>{item}</Option>)
							})
					}
				} else {
					message.error(res.message)
				}
			});
	}
	_getSchool() { //获取全部校区信息
		const self = this;
		fpost('/api/system/schoolarea/listSchoolArea')
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if(res.success == true) {
					if(res.result) {
						self.setState({
							allSchool: res.result
						})
					}
				} else {
					message.error(res.message)
				}
			});
	}
	_getDepart() { //获取全部部门信息
		const self = this;
		fpost('/api/hr/staff/listDepartment')
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if(res.success == true) {
					if(res.result) {
						self.setState({
							department1: res.result
						});
						depart =
							self.state.department1.map((item, i) => {
								return(<Option key={item} value={item}>{item}</Option>)
							})
					}
				} else {
					message.error(res.message)
				}
			});
	}
	_getInfo() {
		const self = this;
		fpost('/api/hr/staff/findStaff', {
				id: pageId
			})
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if(res.success == true) {
					if(res.result) {
						const resData = res.result;
						self.setState({
							name: resData.name, //姓名
							gender: resData.gender, //性别
							enName: resData.enName, //英文名
							identityNo: resData.identityNo, //身份证号
							marriage: resData.marriage, //婚姻状况
							nativePlace: resData.nativePlace, //籍贯
							mobile: resData.mobile, //手机号
							nation: resData.nation, //民族
							birthday: resData.birthday, //生日
							politics: resData.politics, //政治面貌
							graduateFrom: resData.graduateFrom, //院校
							specialty: resData.specialty, //专业
							education: resData.education, //学历
							email: resData.email, //邮箱
							personnelStatus: resData.personnelStatus, //人事状态
							laborContract: resData.laborContract, //劳动合同
							startValue: resData.contractStartTime, //合同开始时间
							endValue: resData.contractEndTime, //合同结束时间
							positiveTime: resData.positiveTime,
							laborRelations: resData.laborRelations,
							payrollCardBank: resData.payrollCardBank,
							payrollName: resData.payrollName,
							payrollCardNo: resData.payrollCardNo,
							accumulationFundNo: resData.accumulationFundNo,
							hasAccumulationFund: resData.hasAccumulationFund,
							socialSecurityNo: resData.socialSecurityNo,
							hasSocialSecurity: resData.hasSocialSecurity,
							workState: resData.workState,
							isAllArea: resData.isAllArea,
							imageUrl: resData.headPath,
							headResourceId: resData.headResourceId,
							schoolAreas: resData.schoolAreas,
							position: resData.position,
							department: resData.department,
						});
						if(checkState){
							self.setState({
								checkState:1,
								staffNo: resData.staffNo, //工号
							})
						}
						if(resData.headPath){
							self.setState({
								audioCoverFileList: [{
									uid: -1,
									name: 'xxx.png',
									status: 'done',
									url: resData.headPath,
								}]
							})
						}
						if(resData.isAllArea == '0') {
							self.setState({
								schoolState: 3
							});
							if(pageId) {
								self.setState({
									createS: '0'
								})
							}
						}
					}
				} else {
					message.error(res.message)
				}
			});
	}
	
	_audioCoverChange({
		file,
		fileList
	}) { //上传图片
		if(file.status == 'uploading') {
			this.setState({
				isdisabled: true
			});
		} else {
			this.setState({
				isdisabled: false
			});
		}
		let url = null;
		let imgId = null;
		if(fileList[0] && fileList[0].response && fileList[0].response.result) {
			url = fileList[0].response.result.url;
			imgId = fileList[0].response.result.id;
			this.setState({
				imageUrl: url,
				headResourceId: imgId
			})
		} else {
			this.setState({
				audioCoverFileList: fileList,
				imageUrl: null,
				headResourceId: null
			})
		};
	}
	handlePreview = (file) => { //对上传的照片进行预览
		this.setState({
			previewImage: file.url || file.thumbUrl,
			previewVisible: true,
		});
	}
	handleCancel = () => this.setState({
		previewVisible: false
	}) //取消预览效果
	disabledDate=(current) =>{
	  // Can not select days before today and today
	  return current && current.valueOf() > Date.now();
	}
	disabledStartDate = (startValue) => { //只读的开始日期
		const endValue = this.state.endValue;
		if(!startValue || !endValue) {
			return false;
		}
		return startValue.valueOf() >endValue.valueOf();
	}

	disabledEndDate = (endValue) => { //只读的结束日期
		const startValue = this.state.startValue;
		if(!endValue || !startValue) {
			return false;
		}
		return endValue.valueOf() <= startValue.valueOf();
	}

	onChange = (field, value) => {
		this.setState({
			[field]: value,
		},function(){
			if(pageId&&this.props.form.getFieldValue('contractStartTime')&&this.props.form.getFieldValue('contractEndTime')){
				if(this.state.startValue){
					this.state.startValue=this.props.form.getFieldValue('contractStartTime').format('YYYY-MM-DD');
				}
				if(this.state.endValue){
					this.state.endValue=this.props.form.getFieldValue('contractEndTime').format('YYYY-MM-DD');
				}
				if(this.state.startValue>this.state.endValue){
					this.setState({
						endValue:this.state.startValue
					})
				}
			}
		});
	}
	//开始日期改变
	onStartChange = (value) => {
		this.onChange('startValue', value);
	}
	//结束日期改变
	onEndChange = (value) => {
		this.onChange('endValue', value);
	}

	handleStartOpenChange = (open) => {
		if(!open) {
			this.setState({
				endOpen: true
			});
		}
	}

	handleEndOpenChange = (open) => {
		this.setState({
			endOpen: open
		});
	}
	school(value) {
		let v = this.props.form.getFieldValue('isAllArea');
		if(v == 1) {
			this.setState({
				schoolState: 3
			});
		} else {
			this.setState({
				schoolState: 0
			})
		}
	}
	_save() { //页面的保存
		if(this.props.form.getFieldValue('isAllArea') == 0) {
			if(!zdSchData && this.props.form.getFieldValue('staffSchoolAreaIds').length > 0) {} else if(!zdSchData && this.props.form.getFieldValue('staffSchoolAreaIds').length <= 0) {
				message.error('指定校区不能为空');
				return false;
			}
		}
		this.props.form.validateFields(
			(err, fieldsValue) => {
				if(!err) {
					if(fieldsValue.personnelStatus.length==0){
						fieldsValue.personnelStatus='';
					}
					const imageUrl = this.state.imageUrl;
					const headResourceId = this.state.headResourceId;
					fieldsValue['contractStartTime'] = fieldsValue['contractStartTime'] ? fieldsValue['contractStartTime'].format('YYYY-MM-DD') : '';

					fieldsValue['contractEndTime'] = fieldsValue['contractEndTime'] ? fieldsValue['contractEndTime'].format('YYYY-MM-DD') : '';
					fieldsValue['birthday'] = fieldsValue['birthday'] ? fieldsValue['birthday'].format('YYYY-MM-DD') : '';
					fieldsValue['positiveTime'] = fieldsValue['positiveTime'] ? fieldsValue['positiveTime'].format('YYYY-MM-DD') : '';
					if(fieldsValue['positiveTime'] > fieldsValue['contractEndTime']) {
						message.error('转正日期不能大于合同期的结束时间');
						return false;
					}
//						fieldsValue.headPath = imageUrl;
						fieldsValue.headResourceId = headResourceId?headResourceId:0;
					if(fieldsValue.staffSchoolAreaIds) {
						if(zdSchData) {
							fieldsValue.staffSchoolAreaIds = zdSchData;
						} else {
							fieldsValue.staffSchoolAreaIds = fieldsValue.staffSchoolAreaIds.toString();
						}

					}
					if(!fieldsValue.department) {
						fieldsValue.department = '';
					}
					if(pageId) {
						fieldsValue.id = pageId;
					}
					if(!fieldsValue.position) {
						fieldsValue.position = '';
					}
					this._saveAll(fieldsValue);
				}else{
					message.error('档案信息还未完善哦，请先完善档案信息');
				}
			}
		)
	}
	_saveAll(fieldsValue) {
		let self = this;
		fpost('/api/hr/staff/saveStaff', fieldsValue)
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if(res.success == true) {
					message.success('保存成功');
					self.setState({ iconLoading: true });
					 setTimeout(() => {
						window.location.href = '/person/archivesList';
				    }, 500);
				} else {
					message.error(res.message)
				}
			});
	}
	_phone = (rule, value, callback) => { //手机号正则
		const form = this.props.form;
		let reg = /^1(3|4|5|7|8|9)\d{9}$/;
		if(value && !reg.test(value)) {
			callback('请输入正确的手机号格式');
		} else {
			callback();
		}
	}
	_getEnglishName = (rule, value, callback) => { //英文名正则
//		let reg = /^[a-zA-Z]+[\s\.]?([a-zA-Z]+[\s\.]?){0,4}$/ ;
		let reg=/^[a-zA-Z]+[/s.]?([a-zA-Z]+[/s.]?){0,4}[a-zA-Z]$/;
		if(value && !reg.test($.trim(value))) {
			callback('您输入的英文名称不符合规范哦,请重新输入');
		} else if(value &&reg.test($.trim(value))&&value.length>50){
			callback('英文名的长度不能超过50哦');
		}else{
			callback();
		}
	}
	_position = (rule, value, callback) => { //身份证号正则
		let reg=/(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
		if(value && !reg.test(value)) {
			callback('请输入正确的身份证号');
		} else {
			callback();
		}
	}
	department= (rule, value, callback) => { //部门正则
		if(value&&value.length>50){
			callback('部门长度不能超过50哦');
		}else{
			callback();
		}
	}
	specialty= (rule, value, callback) => { //专业正则
		if(value&&value.length>100){
			callback('专业长度不能超过100哦');
		}else{
			callback();
		}
	}
	nativePlace= (rule, value, callback) => { //籍贯正则
		if(value&&value.length>50){
			callback('籍贯长度不能超过50哦');
		}else{
			callback();
		}
	}
	_getNameValid= (rule, value, callback) => { //姓名正则
		if(value&&value.length>50){
			callback('姓名长度不能超过50哦');
		}else{
			callback();
		}
	}
	graduateFrom= (rule, value, callback) => { //院校正则
		if(value&&value.length>100){
			callback('院校长度不能超过100哦');
		}else{
			callback();
		}
	}
	nation= (rule, value, callback) => { //院校正则
		if(value&&value.length>50){
			callback('民族长度不能超过50哦');
		}else{
			callback();
		}
	}
	education= (rule, value, callback) => { //学历正则
		if(value&&value.length>100){
			callback('学历长度不能超过100哦');
		}else{
			callback();
		}
	}
	payrollName= (rule, value, callback) => { //户名正则
		if(value&&value.length>50){
			callback('户名长度不能超过50哦');
		}else{
			callback();
		}
	}
	position= (rule, value, callback) => { //职位正则
		if(value&&value.length>200){
			callback('职位长度不能超过200哦');
		}else{
			callback();
		}
	}
	socialSecurityNo= (rule, value, callback) => { //社保号正则
		let reg = /^(\d+)$/ ;
		if(value && !reg.test(value)) {
			callback('社保号只能为数字哦,请重新输入');
		} else if(value&& reg.test(value)&&value.length>50){
			callback('社保号长度不能超过50哦');
		}else{
			callback();
		}
	}
	payrollCardBank= (rule, value, callback) => { //开户行正则
		if(value&&value.length>100){
			callback('开户行长度不能超过100哦');
		}else{
			callback();
		}
	}
	_cardNum = (rule, value, callback) => { //工资卡号正则
		let reg = /^(\d+)$/ ;
		if(value && !reg.test(value)) {
			callback('工资卡号只能为数字哦,请重新输入');
		} else if(value&& reg.test(value)&&value.length>50){
			callback('工资卡号长度不能超过50哦');
		}else{
			callback();
		}
	}
	_fund = (rule, value, callback) => { //公积金号正则
		let reg = /^(\d+)$/ ;
		if(value && !reg.test(value)) {
			callback('公积金号只能为数字哦,请重新输入');
		} else if(value&& reg.test(value)&&value.length>20){
			callback('公积金号长度不能超过20哦');
		}else{
			callback();
		}
	}

	render() {
		const uploadImgButton = (
			<div>
	        <Icon type="plus" />
        		<div className="ant-upload-text">上传照片</div>
	      </div>
		);
		const {
			getFieldDecorator
		} = this.props.form;
		const formItemLayout = {
			labelCol: {
				span: 6
			},
			wrapperCol: {
				span: 18
			}
		};
		const formItemLayout1 = {
			labelCol: {
				span: 4
			},
			wrapperCol: {
				span: 17
			}
		};
		const formItemLayout2 = {
			labelCol: {
				span: 7
			},
			wrapperCol: {
				span: 17
			}
		};
		const {
			fileList,
			audioCoverFileList,
			previewVisible,
			previewImage
		} = this.state;
		const regularText = <span>（用于试用到期提醒、累计工作天数计算）</span>;
		const periodText = <span>（用于合同到期提醒）</span>;
		return(<div className='courseArranging classListPage createArchives'>
				<Breadcrumb className="f-mb3">
				    <Breadcrumb.Item>人事管理</Breadcrumb.Item>
				    <Breadcrumb.Item>员工管理</Breadcrumb.Item>
				    <Breadcrumb.Item>
				   		<span className="f-fz5 f-bold new">新建员工</span>
				    </Breadcrumb.Item>
				</Breadcrumb>
				<div>
				<div className='f-flex' style={{justifyContent: 'space-between'}}>
					<h3 className="f-title-blue">基本信息</h3>
				</div>
				<Form className='f-mt3'>
					<Row className='f-bg-white f-pd4 f-relative f-mb5 f-box-shadow1 f-radius1' style={{paddingTop:'30px'}}>
						<Col span={23}>
							<Col span={20}>
							<Row>
								<Col span={12}>
									<Col span={14}>
										<FormItem {...formItemLayout2} label="姓名">
								          {getFieldDecorator('name', {
								            rules: [{
								              required: true,
								              message: '请输入姓名',
								            },
								            {validator:this._getNameValid}
								            ],
								            initialValue:this.state.name
								          })(
								            <Input placeholder="请输入姓名" />
								          )}
								        </FormItem>
									</Col>
									{this.state.checkState?
									<Col span={10}>
										<FormItem {...formItemLayout2} label="工号">
											<span>{this.state.staffNo}</span>
								        </FormItem>
									</Col>:
									''
									}
									
									
								</Col>
								<Col span={12}>
									<FormItem label="性别" {...formItemLayout}>
							                {getFieldDecorator('gender', {
									        	rules: [{ required: true ,message:'请选择性别'}],
									        	initialValue:this.state.gender?this.state.gender:'0'
									    	})(
								            <RadioGroup>
							                  <Radio value='1'>男</Radio>
							                  <Radio value='0'>女</Radio>
							                </RadioGroup>
						                	)}
					        			</FormItem>
								</Col>
							</Row>
								<Row>
								<Col span={12}>
									<FormItem {...formItemLayout1} label="英文名">
								          {getFieldDecorator('enName', {
								            rules: [{validator:this._getEnglishName}],
								            initialValue:this.state.enName
								          })(
								            <Input placeholder="请输入英文名" />
								          )}
								    </FormItem>
								    <FormItem {...formItemLayout1} label="婚姻">
								          {getFieldDecorator('marriage', {
								            rules: [{
								              required: true,
								              message: '请选择婚姻状况',
								            }],
								             initialValue: this.state.marriage?this.state.marriage.toString():'0'
								          })(
								            <Select>
										        <Option value="0" key='0'>未婚</Option>
										        <Option value="1" key='1'>已婚</Option>
										    </Select>
								          )}
								     </FormItem>
								     <FormItem {...formItemLayout1} label="民族">
								          {getFieldDecorator('nation', {
								            rules: [{validator:this.nation}],
								          	initialValue:this.state.nation
								          })(
								            <Input placeholder="请输入民族" />
								          )}
								    </FormItem>
								    <FormItem {...formItemLayout1} label="生日">
								          {getFieldDecorator('birthday', {
			                  				initialValue:this.state.birthday? moment(this.state.birthday, dateFormat):null,
								          })(
								            <DatePicker 
								             	format="YYYY-MM-DD"
      											disabledDate={this.disabledDate.bind(this)}
      											placeholder='请选择日期'
								            />
								          )}
								    </FormItem>
								    <FormItem {...formItemLayout1} label="院校">
								          {getFieldDecorator('graduateFrom', {
								          	rules:[{validator:this.graduateFrom}],
								          	initialValue:this.state.graduateFrom
								          })(
								            <Input placeholder="请输入院校名称" />
								          )}
								    </FormItem>
								    <FormItem {...formItemLayout1} label="学历">
								          {getFieldDecorator('education', {
								          	rules:[{validator:this.education}],
								          	initialValue:this.state.education
								          })(
								            <Input placeholder="请输入学历名称" />
								          )}
								    </FormItem>
								</Col>
								<Col span={12}>
									<FormItem {...formItemLayout} label="身份证号">
								          {getFieldDecorator('identityNo', {
								          	rules:[{validator:this._position}],
								          	initialValue:this.state.identityNo
								          })(
								            <Input placeholder="请输入身份证号" />
								          )}
								    </FormItem>
								    <FormItem {...formItemLayout} label="籍贯">
								          {getFieldDecorator('nativePlace', {
								          	rules:[{validator:this.nativePlace}],
								          	initialValue:this.state.nativePlace
								          })(
								            <Input placeholder="请输入籍贯" />
								          )}
								    </FormItem>
								    <FormItem {...formItemLayout} label="手机号">
								          {getFieldDecorator('mobile', {
								          	rules: [{
								              required: true,
								              message: '请输入手机号!',
								            },{
								            		validator: this._phone
								            }
								          	],
								          	initialValue:this.state.mobile,
								            valuePropName: 'value',
								          })(
								            <Input placeholder="请输入手机号" />
								          )}
								    </FormItem>
								    <FormItem {...formItemLayout} label="政治面貌">
								          {getFieldDecorator('politics', {
								            rules: [{
								              required: true,
								              message: '请选择政治面貌',
								            }],
								             initialValue: this.state.politics?this.state.politics.toString():'群众'
								          })(
								            <Select>
							              		<Option value="群众" key='群众'>群众</Option>
										        <Option value="共青团员" key='共青团员'>共青团员</Option>
										        <Option value="中共党员" key='中共党员'>中共党员</Option>
										    </Select>
								          )}
								     </FormItem>
								     <FormItem {...formItemLayout} label="专业">
								          {getFieldDecorator('specialty', {
								          	rules:[{validator:this.specialty}],
								          	initialValue:this.state.specialty
								          })(
								            <Input placeholder="请输入专业" />
								          )}
								    </FormItem>
								    <FormItem {...formItemLayout} label="邮箱">
								          {getFieldDecorator('email', {
								          	rules:[{type: 'email',message:'这是一个不存在的邮箱'}],
								          	initialValue:this.state.email
								          })(
								            <Input placeholder="请输入邮箱" />
								          )}
								    </FormItem>
								</Col>
								
							</Row>
							</Col>
							
							<Col span={4} className='f-align-left archUp' style={{padding:'30px 0 0 50px'}}>
								<FormItem label="">
					          <Upload
				    		          action={actionUrl}
				    		          listType="picture-card"
				    		          accept="image/*"
				    		          fileList={ audioCoverFileList }
				    		          onPreview={this.handlePreview}
				    		          onChange={this._audioCoverChange.bind(this)}
				    		          className="previewCover"
				    		        >
				    		          {audioCoverFileList.length >= 1 ? null : uploadImgButton}
				    		       </Upload>
				    		        <Modal visible={previewVisible} footer={null} onCancel={this.handleCancel}>
						          <img alt="example" style={{ width: '100%' }} src={previewImage} />
						        </Modal>
				    		        </FormItem>
				    		        
							</Col>
						</Col>
						<Col span={1} className='f-bg-blue f-absolute f-white f-fz5 f-flex' style={{top:0,right:0,bottom:0,alignItems:'center'}}>
							<div style={{width:'18px',margin:'0 auto'}}>基本信息</div>
						</Col>
					</Row>
					
					
					<div className='f-pb3 f-flex' style={{justifyContent: 'space-between'}}>
						<h3 className="f-title-blue">合同信息</h3>
					</div>
					<Row className='f-bg-white f-pd4 f-relative f-mb5 f-box-shadow1 f-radius1' style={{paddingTop:'30px'}}>
						<Col span={23}>
							<Col span={20}>
								<Row>
								<Col span={12}>
									<FormItem {...formItemLayout1} label="人事状态">
							          {getFieldDecorator('personnelStatus', {
							          	initialValue:this.state.personnelStatus?this.state.personnelStatus.toString():[]
							          })(
							            <Select placeholder="请选择">
						              		<Option value="1" key='1'>面试期</Option>
									        <Option value="2" key='2'>培训期</Option>
									        <Option value="3" key='3'>试用期</Option>
									        <Option value="4" key='4'>正式员工</Option>
									        <Option value="5" key='5'>转正失败</Option>
									        <Option value="6" key='6'>停薪留职</Option>
									        <Option value="7" key='7'>主动离职</Option>
									        <Option value="8" key='8'>被解雇</Option>
									    </Select>
							          )}
							        </FormItem>
							        <Row className='f-mb1'>
									<Col span={14}>
										<FormItem {...formItemLayout2} label="校区">
								          {
											getFieldDecorator('isAllArea', {
												rules: [{ 
													required: true, 
													message: '请选择校区' 
												}],
												initialValue:this.state.isAllArea?this.state.isAllArea.toString():this.state.createS
											})(
												<Select size="large"  style={{width:'100%'}}  onChange={this.school.bind(this)}>
													<Option value="1" key='1'>全部校区</Option>
													<Option value="0" key='0'>指定校区</Option>
												</Select>
											)
										}
								    </FormItem>
									</Col>
									{this.state.schoolState==3?
										<Col span={9} className='zdschool'>
										{getFieldDecorator('staffSchoolAreaIds', {
											rules: [{required: true, message: '请选择指定校区!' }],
											initialValue: this.state.schoolAreas && this.state.schoolAreas.length? this.state.schoolAreas.map((e, i)=>{
        													return e.id.toString()
        											    }) : []
											})(
											<Select   
											 onChange={zdschoolChange} style={{width:'76%'}} mode="multiple" placeholder="请选择(必选)">
											  {
						                        	this.state.allSchool.map((item,i)=> {
						                        		return(<Option key={i} value={item.id}>{ item.name }</Option>)
						                        	})
						                        }
											</Select>
											)}
									</Col>
										:null}
									</Row>
								    <FormItem {...formItemLayout1} label="部门">
								          {getFieldDecorator('department', {
								            rules: [{validator: this.department}],
								          	initialValue:this.state.department?this.state.department:''
								          })(
								          	 <Select
									          mode="combobox"
									          placeholder='请输入部门'
									          style={{ width: '100%' }}
									          optionFilterProp="children"
	                        					filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
									        >
												{depart}
									        </Select>
								          )}
								    </FormItem>
								    <FormItem {...formItemLayout1} label="岗位">
								          {getFieldDecorator('position', {
								            rules: [{validator: this.position}],
								          	initialValue:this.state.position
								          })(
								            <Select
									          mode="combobox"
									          style={{ width: '100%' }}
									          placeholder='请选择'
									          optionFilterProp="children"
	                        					filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
									        >
												{posit}
									        </Select>
								          )}
								     </FormItem>
								     <FormItem {...formItemLayout1} label="社保">
								          {getFieldDecorator('hasSocialSecurity', {
								          	rules: [{
								              required: true,
								              message: '请选择社保是否已办理',
								            }],
								             initialValue: this.state.hasSocialSecurity?this.state.hasSocialSecurity.toString():'0'
								          })(
								            <Select>
							              		<Option value="0" key='0'>未办理</Option>
										        <Option value="1" key='1'>已办理</Option>
										    </Select>
								          )}
								    </FormItem>
								    <FormItem {...formItemLayout1} label="社保号">
								          {getFieldDecorator('socialSecurityNo', {
								            rules: [{validator: this.socialSecurityNo}],
								            initialValue:this.state.socialSecurityNo
								          })(
								            <Input placeholder="请输入社保号" />
								          )}
								    </FormItem>
								    <FormItem {...formItemLayout1} label="公积金">
								          {getFieldDecorator('hasAccumulationFund', {
								          	rules: [{
								              required: true,
								              message: '请选择公积金是否已办理',
								           }],
								            initialValue: this.state.hasAccumulationFund?this.state.hasAccumulationFund.toString():'0'
								          })(
								            <Select>
							              		<Option value="0" key='0'>未办理</Option>
										        <Option value="1" key='1'>已办理</Option>
										    </Select>
								          )}
								    </FormItem>
								    <FormItem {...formItemLayout1} label="公积金号">
								          {getFieldDecorator('accumulationFundNo', {
								            rules: [{validator: this._fund}],
								            initialValue:this.state.accumulationFundNo
								          })(
								            <Input placeholder="请输入公积金号" />
								          )}
								    </FormItem>
								</Col>
								<Col span={12}>
									<FormItem {...formItemLayout} label="劳动合同">
							          {getFieldDecorator('laborContract', {
							          	rules: [{
								              required: true,
								              message: '请选择劳动合同是否已签',
							            }],
							             initialValue: this.state.laborContract?this.state.laborContract.toString():'0'
							          })(
							            <Select>
						              		<Option value="0" key='0'>未签</Option>
									        <Option value="1" key='1'>已签</Option>
									    </Select>
							          )}
							        </FormItem>
									<FormItem {...formItemLayout} label="合同期">
								          <Col span={23} className='period'>
								    			 <Col span={11}>
								    			 <FormItem {...formItemLayout}>
										          {getFieldDecorator('contractStartTime',{
			                  						initialValue:this.state.startValue? moment(this.state.startValue, dateFormat):null,
										          })(
										            	<DatePicker
											          disabledDate={this.disabledStartDate.bind(this)}
											          format="YYYY-MM-DD"
											          setFieldsValue={this.state.startValue}
											          placeholder="开始时间"
											          onChange={this.onStartChange.bind(this)}
											          onOpenChange={this.handleStartOpenChange.bind(this)}/>
										          )}
										     </FormItem>
								    			 </Col>
								    			 <Col span={2} className='f-align-center'>--</Col>
								    			  <Col span={11}>
								    			  <FormItem {...formItemLayout}>
										          {getFieldDecorator('contractEndTime',{
			                  						initialValue:this.state.endValue? moment(this.state.endValue, dateFormat):null,
										          })(
										            <DatePicker
											          disabledDate={this.disabledEndDate.bind(this)}
											          format="YYYY-MM-DD"
											          setFieldsValue={this.state.endValue}
											          placeholder="结束时间"
											          onChange={this.onEndChange.bind(this)}
											          open={this.state.endOpen}
											          onOpenChange={this.handleEndOpenChange.bind(this)}/>
										          )}
										     </FormItem>
									         	</Col>
								        	</Col>
								        	<Col span={1}>
									     	<Tooltip placement="topLeft" title={periodText}>
									     		<i className='iconfont icon-yiwen f-pointer'></i>
										    </Tooltip>
									     </Col>
								          
								    </FormItem>
								    <FormItem {...formItemLayout} label="转正日期">
								    		<Col span={15} className='regular'>
										<FormItem {...formItemLayout} label="">
									          {getFieldDecorator('positiveTime',{
			                  					initialValue:this.state.positiveTime? moment(this.state.positiveTime, dateFormat):null,
									          })(
									            <DatePicker/>
									          )}
									     </FormItem>
									     </Col>
									     <Col span={9}>
									     	<Tooltip placement="topLeft" title={regularText}>
									     		<i className='iconfont icon-yiwen f-pointer'></i>
										    </Tooltip>
									     </Col>
									</FormItem>
								    <FormItem {...formItemLayout} label="劳动关系">
							          {getFieldDecorator('laborRelations', {
							          	rules: [{
								              required: true,
								              message: '请选择劳动关系',
								            }],
							             initialValue: this.state.laborRelations?this.state.laborRelations.toString():'1'
							          })(
							            <Select>
						              		<Option value="1" key='1'>全职</Option>
									        <Option value="2" key='2'>兼职</Option>
									        <Option value="3" key='3'>合作</Option>
									    </Select>
							          )}
							        </FormItem>
								    <FormItem {...formItemLayout} label="开户行">
								          {getFieldDecorator('payrollCardBank', {
								          	rules:[{
								            		validator: this.payrollCardBank
								            }],
								          	initialValue:this.state.payrollCardBank
								          })(
								            <Input placeholder="请输入开户行" />
								          )}
								    </FormItem>
								    <FormItem {...formItemLayout} label="户名">
								          {getFieldDecorator('payrollName', {
								          	rules:[{
								            		validator: this.payrollName
								            }],
								          	initialValue:this.state.payrollName
								          })(
								            <Input placeholder="请输入户名" />
								          )}
								     </FormItem>
								     <FormItem {...formItemLayout} label="工资卡号">
								          {getFieldDecorator('payrollCardNo', {
								          	rules:[{
								            		validator: this._cardNum
								            }],
								            initialValue:this.state.payrollCardNo
								          })(
								            <Input placeholder="请输入工资卡号" />
								          )}
								    </FormItem>
								    <FormItem {...formItemLayout} label="在职状态">
								          {getFieldDecorator('workState', {
								            rules: [{
								              required: true,
								              message: '请选择在职状态',
								            }],
							             initialValue: this.state.workState
								          })(
								            <Select>
										        <Option value="0" key='0'>在职</Option>
										        <Option value="1" key='1'>离职</Option>
										    </Select>
								          )}
								     </FormItem>
								</Col>
								
							</Row>
							</Col>
						</Col>
						<Col span={1} className='f-bg-green f-absolute f-white f-fz5 f-flex' style={{top:0,right:0,bottom:0,alignItems:'center'}}>
							<div style={{width:'18px',margin:'0 auto'}}>合同信息</div>
						</Col>
					</Row>
					<Row className='f-bg-white f-pd4 f-box-shadow1 f-radius1'>
						<div className='f-align-right'>
							<Button style={{height:'40px'}} className='f-mr5' onClick={()=>{window.location.href='/person/archivesList'}}>
			        				<i className='iconfont icon-fanhui f-mr2 f-pointer'></i>
			        				返回
			        			</Button>
						    	<Button type="primary" style={{height:'40px'}} loading={this.state.iconLoading} onClick={this._save.bind(this)}><i className='iconfont icon-queding f-mr2 f-pointer'></i>确定</Button>
		        			</div>
	        			</Row>
				</Form>
				</div>
				<BackTop/>
		</div>);
	}
}
const CreateArchives = Form.create()(Main);
export {
	CreateArchives
}