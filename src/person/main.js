import React, {
	Component
} from 'react';
import {
	default as Layout
} from '../common/layout.js';
import {
	Route
} from 'react-router-dom';
import './main.less';
//档案管理
//档案列表
import {
	ArchivesList
} from './archivesManage/archivesList';
import {
	CreateArchives
} from './archivesManage/createArchives';
//工具
//员工工资
import {
	StaffWages
} from './tool/staffWages';
import {
	StaffBirthday
} from './tool/staffBirthday';
import {
	HourStatistics
} from './tool/hourStatistics';
import {
	StatisticsDetail
} from './tool/statisticsDetail';

export default ({
	match
}) => {
	window.store.session('tMenu', {
		selectedKeys: 'rsgl'
	})
	return (<div>
		<Layout main={ Main } />
	</div>);
}

class Main extends Component {
	render() {
		return (
			<div>
				<Route path='/person' exact component={ ArchivesList } />
				{/*档案列表*/}
				<Route path='/person/archivesList' exact component={ ArchivesList } />
				{/*新建档案*/}
				<Route path='/person/createArchives' exact component={ CreateArchives } />
				{/*编辑档案*/}
				<Route path='/person/editArchives' exact component={ CreateArchives } />
				
				
				
				{/* 员工工资*/}
				<Route path='/person/staffWages' exact component={ StaffWages } />
				{/*员工生日*/}
				<Route path='/person/staffBirthday' exact component={ StaffBirthday } />
				{/*教师课时统计*/}
				<Route path='/person/hourStatistics' exact component={ HourStatistics } />
				{/*教师课时统计详情页*/}
				<Route path='/person/statisticsDetail' exact component={ StatisticsDetail } />
				
			</div>
		);
	}
}