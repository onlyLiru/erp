import React, {
	Component
} from 'react';
import { Breadcrumb, Button, Table, Form, Row, Col, Select, message, Pagination } from 'antd';
import '../main.less';
import { Loading } from '../../common/g.js';
import {fpost } from '../../common/io.js';
let schoolList = null;
let depart = null;

const Option = Select.Option;
const FormItem = Form.Item;

class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			visible: false,
			teachersData: [],
			schoolName: null, //学校
			departName: null, //部门
			pageResult: null,
			currentPage: 1, //当前页面
			pageSize: 10, //每页显示条数
			total: null, //列表总数
			allSchool: null,
			department1: [],
			birthdayMonth: new Date().getMonth()+1
		}
	}
	componentDidMount() {
		this._getData();
		this._getSchool(); //获取全部校区信息
		this._getDepart(); //获取所有部门信息
	}
	onShowSizeChange (current, pageSize) {
		this.setState({
			currentPage:current,
			pageSize:pageSize
		},function(){
			this._getData();
		})
	}
	_getDepart() { //获取全部部门信息
		const self = this;
		fpost('/api/hr/staff/listDepartment')
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if(res.success == true) {
					if(res.result) {

						self.setState({
							department1: res.result
						});
						depart =
							self.state.department1.map((item, i) => {
								return(<Option key={item} value={item}>{item}</Option>)
							})
					}
				} else {
					message.error(res.message)
				}
			});
	}
	_getSchool() { //获取全部校区信息
		const self = this;
		fpost('/api/system/schoolarea/listSchoolArea')
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if(res.success == true) {
					if(res.result) {
						self.setState({
							allSchool: res.result
						});
						schoolList =
							self.state.allSchool.map((item, i) => {
								return(<Option key={item} value={item.id}>{item.name}</Option>)
							})
					}
				} else {
					message.error(res.message)
				}
			});
	}
	disabledStartDate = (startValue) => { //只读的开始日期
		const endValue = this.state.endValue;
		if(!startValue || !endValue) {
			return false;
		}
		return startValue.valueOf() > endValue.valueOf();
	}

	disabledEndDate = (endValue) => { //只读的结束日期
		const startValue = this.state.startValue;
		if(!endValue || !startValue) {
			return false;
		}
		return endValue.valueOf() <= startValue.valueOf();
	}

	onChange = (field, value) => {
		this.setState({
			[field]: value,
		});
	}
	//开始日期改变
	onStartChange = (value) => {
		this.onChange('startValue', value);
	}
	//结束日期改变
	onEndChange = (value) => {
		this.onChange('endValue', value);
	}

	handleStartOpenChange = (open) => {
		if(!open) {
			this.setState({
				endOpen: true
			});
		}
	}
	handleEndOpenChange = (open) => {
		this.setState({
			endOpen: open
		});
	}
	reset() { //高级筛选里的重置
		// 	let v=this.props.form.getFieldValue ('courseName');
		this.props.form.resetFields();
		this.setState({
			currentPage: 1
		}, function() {
			this.props.form.validateFields(
				(err, fieldsValue) => {
					this._getData(fieldsValue);
				}
			)
		});
	}
	_search() { //筛选搜索
		this.setState({
			currentPage: 1
		}, function() {
			this.props.form.validateFields(
				(err, fieldsValue) => {
					this._getData(fieldsValue);
				}
			)
		});
	}
	_getData(fieldsValue) { //获取列表
		let self = this;
		this.props.form.validateFields(
			(err, fieldsValue) => {
				fieldsValue.birthdayStartTime =fieldsValue.birthdayStartTime? fieldsValue['birthdayStartTime'].format('YYYY-MM-DD'):'';
				fieldsValue.birthdayEndTime = fieldsValue.birthdayEndTime?fieldsValue['birthdayEndTime'].format('YYYY-MM-DD'):'';
				fieldsValue.pageSize = self.state.pageSize;
				fieldsValue.currentPage = self.state.currentPage;
				self._getListData(fieldsValue);
			})
	}
	_getListData(fieldsValue) {
		let self = this;
		if(!fieldsValue.schoolAreaId) {
			fieldsValue.schoolAreaId = '';
		}
		if(!fieldsValue.department) {
			fieldsValue.department = '';
		}
		fpost('/api/hr/staff/pageStaffList ', fieldsValue)
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if(res.success == true) {
					if(res.result) {
						res.result.records.forEach((item, i) => {
							item.key = i;
						});
						self.setState({
							pageResult: res.result.records,
							total: parseInt(res.result.total)
						})
					}
				} else {
					message.error(res.message)
				}
			});
	}
	pageChange(pageNumber) { //列表分页获取当前页码
		this.setState({
			currentPage: pageNumber
		}, function() {
			this.props.form.validateFields(
				(err, fieldsValue) => {
					this._getData(fieldsValue);
				}
			)
		});
	}
	render() {
		const {
			getFieldDecorator
		} = this.props.form;
		const formItemLayout = {
			labelCol: {
				span: 6
			},
			wrapperCol: {
				span: 18
			}
		};
		const columns = [{
			title: '头像',
			key: '0',
			fixed: 'left',
			width: 100,
			className: 'f-align-center',
			render: (text, record, index) => {
				return(<div>
					{record.headPath?<img src={record.headPath} style={{width:'40px',height:'40px',borderRadius:'100%'}}/>
					:''}
					</div>
				)
			}
		}, {
			title: '员工姓名（工号）',
			key: '1',
			className: 'f-align-center',
			width: 180,
			render: (text, record, index) => {
				return(
					<i>{record.name}({record.staffNo})</i>
				)
			}
		}, {
			title: '性别',
			dataIndex: 'gender',
			className: 'f-align-center',
			key: '2',
			width: 100,
		}, {
			title: '手机',
			dataIndex: 'mobile',
			key: '3',
			className: 'f-align-center',
			width: 150,
		}, {
			title: '邮箱',
			className: 'f-align-center',
			dataIndex: 'email',
			key: '4',
			width: 150,
		}, {
			title: '生日',
			className: 'f-align-center',
			dataIndex: 'birthday',
			key: '5',
			width: 150,
		}, {
			title: '政治面貌',
			className: 'f-align-center',
			dataIndex: 'politics',
			key: '7',
			width: 150,
		}, {
			title: '婚姻状况',
			className: 'f-align-center',
			dataIndex: 'marriage',
			key: '8',
			width: 150,
		}, {
			title: '校区',
			className: 'f-align-center',
			key: '9',
			width: 150,
			dataIndex:'schoolAreas',
		}, {
			title: '部门',
			className: 'f-align-center',
			dataIndex: 'department',
			key: '10',
			width: 150,
		}, {
			title: '职位',
			className: 'f-align-center',
			dataIndex: 'position',
			key: '11',
			width: 150,
		}, {
			title: '在职状态',
			className: 'f-align-center',
			dataIndex: 'personnelStatus',
			key: '12',
			fixed: 'right',
			width: 150,
		}];
		return(<div className='courseArranging classListPage wid100'>
				<Breadcrumb className="f-mb3">
				    <Breadcrumb.Item>人事管理</Breadcrumb.Item>
				    <Breadcrumb.Item>工具</Breadcrumb.Item>
				    <Breadcrumb.Item>
				   		<span className="f-fz5 f-bold">生日员工</span>
				    </Breadcrumb.Item>
				</Breadcrumb>
				<div>
				<div className='f-bg-white Topselect f-radius1 f-box-shadow1' style={{padding:'20px 20px 15px 20px'}}>
					<Form>
						 <Row className='' gutter={40}>
				        		<Col span={5}>
				        		<Col span={8} className='f-fz4 f-black f-h4-lh4'>时间:&emsp;</Col>
				        		<Col span={16} >
				        			<FormItem {...formItemLayout}>
					    			 {getFieldDecorator('birthdayMonth', {
						            initialValue: (new Date().getMonth()+1)+''
						          })(
						            <Select>
						                <Option value="1">一月</Option>
						                <Option value="2">二月</Option>
						                <Option value="3">三月</Option>
						                <Option value="4">四月</Option>
						                <Option value="5">五月</Option>
						                <Option value="6">六月</Option>
						                <Option value="7">七月</Option>
						                <Option value="8">八月</Option>
						                <Option value="9">九月</Option>
						                <Option value="10">十月</Option>
						                <Option value="11">十一月</Option>
						                <Option value="12">十二月</Option>
						              </Select>
						          )}
					    			</FormItem>
							        	</Col>
					        		</Col>
					        		<Col span={6} >
					             <Col span={8} className='f-align-right f-fz4 f-black f-h4-lh4'>校区:&emsp;</Col>
					              <Col span={16} >
					              <FormItem {...formItemLayout}>
					                  {getFieldDecorator('schoolAreaId', {
					                  })(
					                    <Select
				                        showSearch
				                        allowClear
				                        placeholder="选择校区"
				                        optionFilterProp="children"
				                        filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
				                      >
				                       {schoolList}
				                      </Select> 
					                  )}
					                </FormItem>
						            </Col>
					          	</Col>
					          	<Col span={6} >
					             <Col span={8} className='f-align-right f-fz4 f-black f-h4-lh4'>部门:&emsp;</Col>
					              <Col span={16} >
					              <FormItem {...formItemLayout}>
					                  {getFieldDecorator('department', {
					                  })(
					                    <Select showSearch allowClear placeholder="全部部门" optionFilterProp="children"
					                        filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}>
					                        	{depart}
					                      </Select>  
					                  )}
					                </FormItem>
						            </Col>
					          	</Col>
					          	
					        </Row>
					        <Row className='f-pb3'>
					        		<Col span={12} className='f-align-right f-pr4'>
					        			<Button style={{border: '1px solid #999999',borderRadius: '4px',width:'110px',height:'40px'}} onClick={this.reset.bind(this)}>
					        				<i className='iconfont icon-reset f-mr2'></i>重置
					        			</Button>
					        		</Col>
					        		<Col span={12} className='f-align-left'>
					        			<Button type="primary" style={{borderRadius: '4px',width:'110px',height:'40px'}} onClick={this._search.bind(this)}>
					        				<i className='iconfont icon-hricon33 f-mr2'></i>搜索
					        			</Button>
					        		</Col>
					        </Row>
					</Form>
				</div>
			
				<div className='f-bg-white f-pd4 f-box-shadow1 f-mt5 f-radius1 '>
					<div className='f-pb3 f-flex' style={{justifyContent: 'space-between'}}>
						<h3 className="f-title-blue">员工列表</h3>
					</div>
					{this.state.pageResult?
						<div>
							<Table columns={columns} dataSource={this.state.pageResult} scroll={{ x: 1600}} bordered pagination={false}/>
								<div className='f-flex f-mt3' style={{justifyContent:'flex-end'}}>
									<div>
										<Pagination current={this.state.currentPage} total={this.state.total} onShowSizeChange={this.onShowSizeChange.bind(this)} showSizeChanger showQuickJumper onChange={this.pageChange.bind(this)}/>
									</div>
								</div>
						</div>:<Loading/>
					}
				</div>
				</div>
		</div>);
	}
}
const StaffBirthday = Form.create()(Main);
export {
	StaffBirthday
}