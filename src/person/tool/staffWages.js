import React, {
	Component
} from 'react';
import store from 'store2';
import {
	Breadcrumb,
	Button,
	Table,
	Modal,
	DatePicker,
	Form,
	Row,
	Col,
	Select,
	message,
	Upload,
	Pagination
} from 'antd';
import '../main.less';
import reqwest from 'reqwest';
import {
	Loading
} from '../../common/g.js';
import {
	fpost
} from '../../common/io.js';
const u = require('../../common/io.js');

const Option = Select.Option;
const FormItem = Form.Item;
let depart1 = null;
let posit1 = null;
class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			visible: false,
			teachersData: [],
			fileList: [],
			uploading: false,
			schoolName: null, //学校
			departName: null, //部门
			positionName: null,
			pageResult: null,
			currentPage: 1, //当前页面
			pageSize: 10, //每页条数
			total: null, //总条数
			allSchool: [],
			department1: null,
			position1: null,
		}
	}
	showModal = () => {
		this.setState({
			visible: true, //确定对话框的展示
		});
	}
	handleOk = (e) => { //对话框里的确定按钮对应事件
		this.handleUpload()
	}
	handleCancel = (e) => { //对话框里的取消按钮对应的事件
		this.props.form.resetFields(); //重置
		this.setState({
			visible: false,
			fileList: []
		});
	}
	onShowSizeChange(current, pageSize) {
		this.setState({
			currentPage: current,
			pageSize: pageSize
		}, function() {
			this._getData();
		})
	}
	componentDidMount() {
		this._getData();
		this._getSchool(); //获取全部校区信息
		this._getDepart(); //获取所有部门信息
		this._getPosition(); //获取所有职位信息
	}
	_getPosition() { //获取全部职位信息
		const self = this;
		fpost('/api/hr/staff/listPosition')
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					if (res.result) {
						self.setState({
							position1: res.result
						});
						posit1 =
							self.state.position1.map((item, i) => {
								return (<Option key={item} value={item}>{item}</Option>)
							})
					}
				} else {
					message.error(res.message)
				}
			});
	}
	_getDepart() { //获取全部部门信息
		const self = this;
		fpost('/api/hr/staff/listDepartment')
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					if (res.result) {

						self.setState({
							department1: res.result
						});
						depart1 =
							self.state.department1.map((item, i) => {
								return (<Option key={item} value={item}>{item}</Option>)
							})
					}
				} else {
					message.error(res.message)
				}
			});
	}
	_getSchool() { //获取全部校区信息
		const self = this;
		fpost('/api/system/schoolarea/listSchoolArea')
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					if (res.result) {
						self.setState({
							allSchool: res.result
						});
					}
				} else {
					message.error(res.message)
				}
			});
	}
	disabledStartDate = (startValue) => { //只读的开始日期
		const endValue = this.state.endValue;
		if (!startValue || !endValue) {
			return false;
		}
		return startValue.valueOf() > endValue.valueOf();
	}

	disabledEndDate = (endValue) => { //只读的结束日期
		const startValue = this.state.startValue;
		if (!endValue || !startValue) {
			return false;
		}
		return endValue.valueOf() <= startValue.valueOf();
	}

	onChange = (field, value) => {
		this.setState({
			[field]: value,
		});
	}
	//开始日期改变
	onStartChange = (value) => {
		this.onChange('startValue', value);
	}
	//结束日期改变
	onEndChange = (value) => {
		this.onChange('endValue', value);
	}

	handleStartOpenChange = (open) => {
		if (!open) {
			this.setState({
				endOpen: true
			});
		}
	}

	handleEndOpenChange = (open) => {
		this.setState({
			endOpen: open
		});
	}
	reset() { //高级筛选里的重置
		this.props.form.resetFields();
		this.setState({
			currentPage: 1
		}, function() {
			this.props.form.validateFields(
				(err, fieldsValue) => {
					this._getData(fieldsValue);
				}
			)
		});
	}
	handleUpload = () => {
		const {
			fileList
		} = this.state;
		const formData = new FormData();
		fileList.forEach((file) => {
			formData.append('file', file);
		});

		this.setState({
			uploading: true,
		});
		if (this.state.fileList.length > 1) {
			message.error('最多上传一个excel哦');
			return false;
		}
		reqwest({
			url: u.hostname() + "/api/hr/staff/uploadStaffWagesExcel",
			method: 'post',
			processData: false,
			data: formData,
			headers: {
				'x-auth-token': store.get('sessionId'), //登录功能放开后需要加上的
			},
			success: (res) => {
				if (fileList.length >= 1) {
					if (res.success == true) {
						this.setState({
							fileList: [],
							uploading: false,
							visible: false
						});
						message.success('上传成功！');
						this._getData();
					} else {
						message.error(res.message);
					}
				} else {
					message.error('请先导入员工工资！');
				}
			},
			error: () => {
				this.setState({
					uploading: false,
				});
				message.error('上传失败！');
			},
		});
	}
	_search() { //筛选搜索
		this.setState({
			currentPage: 1
		}, function() {
			this.props.form.validateFields(
				(err, fieldsValue) => {
					this._getData(fieldsValue);
				}
			)
		});
	}
	_getData(fieldsValue) { //获取列表
		let self = this;
		this.props.form.validateFields(
			(err, fieldsValue) => {
				fieldsValue.startDate = fieldsValue.startDate ? fieldsValue['startDate'].format('YYYY-MM-DD') : '';
				fieldsValue.endDate = fieldsValue.endDate ? fieldsValue['endDate'].format('YYYY-MM-DD') : '';
				fieldsValue.pageSize = self.state.pageSize;
				fieldsValue.currentPage = self.state.currentPage;
				fieldsValue.schoolAreaId = fieldsValue.schoolAreaId ? fieldsValue.schoolAreaId : '';
				fieldsValue.department = fieldsValue.department ? fieldsValue.department : '';
				fieldsValue.position = fieldsValue.position ? fieldsValue.position : '';
				self._getListData(fieldsValue);
			})
	}
	_getListData(fieldsValue) {
		let self = this;
		fpost('/api/hr/staff/pageStaffWages', fieldsValue)
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					if (res.result) {
						res.result.records.forEach((item, i) => {
							item.key = i;
						});
						self.setState({
							pageResult: res.result.records,
							total: parseInt(res.result.total)
						})
					}
				} else {
					message.error(res.message)
				}
			});
	}
	pageChange(pageNumber) { //列表分页获取当前页码
		this.setState({
			currentPage: pageNumber
		}, function() {
			this.props.form.validateFields(
				(err, fieldsValue) => {
					this._getData(fieldsValue);
				}
			)
		});
	}
	render() {
		const {
			getFieldDecorator
		} = this.props.form;
		const formItemLayout = {
			labelCol: {
				span: 6
			},
			wrapperCol: {
				span: 18
			}
		};
		const props = {
			action: u.hostname() + "/api/hr/staff/uploadStaffWagesExcel",
			onRemove: (file) => {
				this.setState(({
					fileList
				}) => {
					const index = fileList.indexOf(file);
					const newFileList = fileList.slice();
					newFileList.splice(index, 1);
					return {
						fileList: newFileList,
					};
				});
			},
			beforeUpload: (file) => {
				this.setState(({
					fileList
				}) => ({
					fileList: [...fileList, file],
				}));
				return false;
			},
			fileList: this.state.fileList,
		};
		const columns = [{
			title: '头像',
			key: '0',
			fixed: 'left',
			width: 150,
			className: 'f-align-center',
			render: (text, record, index) => {
				return (<div>
						{record.headPath?<img src={record.headPath} style={{width:'50px',height:'50px',borderRadius:'100%'}}/>
						:''}
					</div>)
			}
		}, {
			title: '员工姓名（工号）',
			key: '1',
			className: 'f-align-center',
			width: 200,
			dataIndex: 'name'
		}, {
			title: '性别',
			dataIndex: 'gender',
			className: 'f-align-center',
			key: '2',
			width: 100,
		}, {
			title: '员工收入',
			key: '3',
			className: 'f-align-center',
			width: 150,
			dataIndex: 'wages'
		}, {
			title: '公司成本',
			key: '4',
			className: 'f-align-center',
			width: 150,
			dataIndex: 'orgCost'
		}, {
			title: '手机',
			key: '5',
			className: 'f-align-center',
			width: 150,
			dataIndex: 'mobile'
		}, {
			title: '邮箱',
			className: 'f-align-center',
			key: '6',
			width: 150,
			dataIndex: 'email'
		}, {
			title: '校区',
			className: 'f-align-center',
			key: '7',
			width: 150,
			dataIndex: 'schoolAreas'
		}, {
			title: '部门',
			className: 'f-align-center',
			key: '8',
			width: 150,
			dataIndex: 'department'
		}, {
			title: '职位',
			className: 'f-align-center',
			key: '9',
			width: 150,
			fixed: 'right',
			dataIndex: 'position'
		}];
		return (<Form className='courseArranging classListPage wid100'>
				<Breadcrumb className="f-mb3">
				    <Breadcrumb.Item>人事管理</Breadcrumb.Item>
				    <Breadcrumb.Item>工具</Breadcrumb.Item>
				    <Breadcrumb.Item>
				   		<span className="f-fz5 f-bold">员工工资</span>
				    </Breadcrumb.Item>
				</Breadcrumb>
				<div>
				<div className='f-bg-white Topselect f-radius1 f-box-shadow1 ' style={{padding:'20px 20px 15px 20px'}}>
				 <Row className=''>
		        		<Col span={10}>
			        		<Col span={4} className='f-fz4 f-black f-h4-lh4'>时间段:&emsp;</Col>
			        		<Col span={20} >
				    			 <Col span={11}>
				    			  <FormItem>
								{getFieldDecorator('startDate')(
					            	<DatePicker
						          disabledDate={this.disabledStartDate.bind(this)}
						          format="YYYY-MM-DD"
						          setFieldsValue={this.state.startValue}
						          placeholder="开始时间"
						          onChange={this.onStartChange.bind(this)}
						          onOpenChange={this.handleStartOpenChange.bind(this)}/>
					            	)}
							</FormItem>
				    			 </Col>
				    			 <Col span={2} className='f-align-center f-h4-lh4'>--</Col>
				    			  <Col span={11}>
				    			  <FormItem>
								{getFieldDecorator('endDate')(
					            <DatePicker
						          disabledDate={this.disabledEndDate.bind(this)}
						          format="YYYY-MM-DD"
						          setFieldsValue={this.state.endValue}
						          placeholder="结束时间"
						          onChange={this.onEndChange.bind(this)}
						          open={this.state.endOpen}
						          onOpenChange={this.handleEndOpenChange.bind(this)} />
					            )}
							</FormItem>
					         	</Col>
					        	</Col>
			        		</Col>
			        		<Col span={4} >
			             <Col span={9} className='f-align-right f-fz4 f-black f-h4-lh4'>校区:&emsp;</Col>
			              <Col span={15} >
			              <FormItem {...formItemLayout}>
			                  {getFieldDecorator('schoolAreaId', {
			                  })(
			                   <Select
		                        showSearch
		                        allowClear
		                        placeholder="请选择"
		                        optionFilterProp="children"
		                        filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
		                      >
		                        	{ this.state.allSchool.map((elem, index) => {
					                  return(
					                  	<Option key={elem.id} value={elem.id}>{elem.name}</Option>
					                  )
					                }) 
								 }
		                      </Select> 
			                  )}
			                </FormItem>
				            </Col>
			          	</Col>
			          	<Col span={5} >
			             <Col span={8} className='f-align-right f-fz4 f-black f-h4-lh4'>部门:&emsp;</Col>
			              <Col span={16} >
			              <FormItem {...formItemLayout}>
			                  {getFieldDecorator('department', {
			                  })(
			                    <Select showSearch allowClear placeholder="全部部门" optionFilterProp="children"
			                        filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}>
			                        	{depart1}
			                      </Select>  
			                  )}
			                </FormItem>
				            </Col>
			          	</Col>
			          	<Col span={4} >
			             <Col span={9} className='f-align-right f-fz4 f-black f-h4-lh4'>职位:&emsp;</Col>
			              <Col span={15} >
			              <FormItem {...formItemLayout}>
			                  {getFieldDecorator('position', {
			                  })(
			                    <Select showSearch allowClear placeholder="全部职位" optionFilterProp="children"
			                        filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}>
			                        	{posit1}
			                      </Select>  
			                  )}
			                </FormItem>
				            </Col>
			          	</Col>
			        </Row>
			        <Row className='f-pb3'>
			        		<Col span={12} className='f-align-right f-pr4'>
			        			<Button style={{border: '1px solid #999999',borderRadius: '4px',width:'110px',height:'40px'}} onClick={this.reset.bind(this)}>
			        				<i className='iconfont icon-reset f-mr2'></i>重置
			        			</Button>
			        		</Col>
			        		<Col span={12} className='f-align-left'>
			        			<Button type="primary" style={{borderRadius: '4px',width:'110px',height:'40px'}} onClick={this._search.bind(this)}>
			        				<i className='iconfont icon-hricon33 f-mr2'></i>搜索
			        			</Button>
			        		</Col>
			        </Row>
				</div>
			
				<div className='f-bg-white f-pd4 f-box-shadow1 f-mt5 f-radius1 '>
					<div className='f-pb3 f-flex' style={{justifyContent: 'space-between'}}>
						<h3 className="f-title-blue">员工列表</h3>
						<a className="f-btn-blue" onClick={this.showModal}>
							<i className='iconfont icon-xiazai'></i>导入员工工资
						</a>
					</div>
					<Modal
				          title='操作'
				          visible={this.state.visible}
				          onOk={this.handleOk}
				          onCancel={this.handleCancel}
				          footer={[
				            <Button key="back" size="large" onClick={this.handleCancel}>取消</Button>,
				            <Button key="submit" type="primary" size="large" onClick={this.handleOk}>
				              确定
				            </Button>,
				          ]}>
						<Row gutter={40}>
							<Col span={12} className='uploadFile'>
								 <Upload {...props} accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel">
						          <Button className='f-bg-blue f-white import'>
						             导入员工工资
						          </Button>
						        </Upload>
									
							</Col>
							<Col span={12}>
								<a className='f-btn down' style={{border: "1px solid #D9D9D9",color:'#666666'}} onClick={()=>{window.location.href=u.hostname()+"/api/hr/staff/downloadStaffWagesTemplate?x-auth-token="+store.get('sessionId')}}
								>
									下载工资模板
								</a>
							</Col>
						</Row>
					</Modal>
					{this.state.pageResult?
						<div>
							<Table columns={columns} dataSource={this.state.pageResult} scroll={{ x: 1400}} bordered pagination={false}/>
								<div className='f-flex f-mt3' style={{justifyContent:'flex-end'}}>
									<div>
										<Pagination current={this.state.currentPage} total={this.state.total} onShowSizeChange={this.onShowSizeChange.bind(this)} showSizeChanger showQuickJumper onChange={this.pageChange.bind(this)}/>
									</div>
								</div>
						</div>:<Loading/>
					}
				</div>
				</div>
		</Form>);
	}
}
const StaffWages = Form.create()(Main);
export {
	StaffWages
}