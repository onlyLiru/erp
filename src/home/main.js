import React, { Component } from 'react';
import { Icon,Breadcrumb } from 'antd';
import './main.less';

export default class Main extends Component {
	render() {
		return(<div className="f-pd5">
			<h2>这里是首页</h2>

			<Breadcrumb className="f-mb3">
			    <Breadcrumb.Item>系统设置</Breadcrumb.Item>
			    <Breadcrumb.Item><a href="">基础设置</a></Breadcrumb.Item>
			    <Breadcrumb.Item>
			   		<span className="f-fz5 f-bold">课程设置</span>
			    </Breadcrumb.Item>
			</Breadcrumb>

			<h3 className="f-title-blue f-mb3">课程设置</h3>

			<div className="f-bg-white f-box-shadow1 f-radius2 f-pd4">
				<a className="f-btn-blue">添加大类</a>
		    	<a className="f-btn-green">
		    		<Icon className="f-fz7 f-vertical-middle f-bold" type="plus" />添加课程
		    	</a>
		    	<a className="f-btn-yellow">添加课程</a>
		    	<a className="f-btn-red">添加课程</a>
		    	<a className="f-btn-orange">添加课程</a>
		    	<p />
		    	<br />
		    	<h3 className="f-title-blue">课程设置</h3>
		    	<p />
		    	<h3 className="f-title-green">课程设置</h3>
		    	<p />
		    	<h3 className="f-title-yellow">课程设置</h3>
		    	<p />
		    	<h3 className="f-title-orange">课程设置</h3>
		    	<p />
		    	<h3 className="f-title-red">课程设置</h3>
	    	</div>
		</div>);
	}
}