import React, { Component } from 'react';
import { Form, Breadcrumb, Table, Modal, Input, Radio, Select, Button, Row, Col, message, Popconfirm } from 'antd';
import { fpost } from '../common/io.js';
import { Loading, Pagination, undefinedToEmpty } from '../common/g.js';
import { default as NumberInput } from '../common/numberInput.js';

const FormItem = Form.Item;
const RadioGroup = Radio.Group;
const Option = Select.Option;

export default class Main extends Component {
	constructor(props){
		super(props);
		this.state = {
			visibleType: false,
			isAdd: true,
			curId: '',
			goodsList: null,
			editData: {},
			total: 1,
			text: '',
			isRequest: false,
			isRequire: false,
			searchPara: {
				currentPage: 1, 
				pageSize: 10
			}
		}
	}
	setGoods(id){
		if(id){
			this.setState({
				isAdd: false
			})
			fpost('/api/material/findMaterialVO', { materialId: id }).then(res=>res.json()).then(res=>{
				if(res.success == true){
					this.setState({
						editData: res.result,
						visibleType: true,
						curId: id,
						isAdd: false,
						text: 'edit',
						isRequire: res.result.materialCourseList && res.result.materialCourseList.length? true : false
					})
				}
			})
		}else{
			this.setState({
				visibleType: true,
				text: '',
				isAdd: true
			})
		}
	}
	updateRequire(v) {
		if(v=='') {
			this.setState({
				isRequire: false
			})
		}else if(v=='1') {
			this.setState({
				isRequire: true
			})
		}
	}
	saveData(){
		let domForm = this.formRef.props.form, obj = domForm.getFieldsValue(), pageSize = this.state.searchPara.pageSize, para = this.searchForm.props.form.getFieldsValue(), self = this, isError = false;
		if(!this.state.isAdd) obj.materialId = this.state.curId;
		para.currentPage = 1;
		para.pageSize = pageSize;
		para.isDisable = para.isDisable? para.isDisable : '';
			obj.purchasePrice = obj.purchasePrice && obj.purchasePrice.number ? obj.purchasePrice.number : undefined;
			obj.salePrice = obj.salePrice && obj.salePrice.number ? obj.salePrice.number : undefined;

			if(obj.purchasePrice == undefined) domForm.setFieldsValue({ purchasePrice: undefined })
			if(obj.salePrice == undefined) domForm.setFieldsValue({ salePrice: undefined })

		this.formRef.props.form.validateFields(	
				(err, fieldsValue) => {
		        	if(err){
						message.error('请填写完整!');
						isError = true;
		           }		
			}
		)

		if(isError) return;

		if(obj.purchasePrice == undefined || obj.salePrice == undefined) {
			message.error('请填写完整!');
			return;
		}

		this.setState({
			isRequest: true
		})

		obj = undefinedToEmpty(obj);

		fpost('/api/material/saveMaterial', obj).then(res=>res.json()).then(res=>{
			if(res.success == true){
				self.setState({
					visibleType: false,
					editData: {},
					searchPara: {
						currentPage: 1,
						pageSize: 10
					},
					isRequire: false
				});
				message.success(res.message);
				this.formRef.props.form.resetFields();
				self.getDataList(para);
			}else {
				message.error(res.message)
			}
			self.setState({
				isRequest: false
			})
		});
	}
	removeGoods(id){
		let para = this.searchForm.props.form.getFieldsValue(), {currentPage, pageSize} = this.state.searchPara, self = this;
			para.currentPage = currentPage;
			para.pageSize = pageSize;
			para.isDisable = para.isDisable? para.isDisable : '';

		fpost('/api/material/deleteMaterial', { materialId: id }).then(res=>res.json()).then(res=>{
			if(res.success == true){
				message.success(res.message);
				self.getDataList(para);
			}else{
				message.error(res.message);
			}
		})
	}
	cancelSetGoods(){
		this.formRef.props.form.resetFields();
		this.setState({
			visibleType: false,
			editData: {},
			isRequire: false
		})
	}
	getDataList(para){
		//物品列表
		fpost('/api/material/pageMaterialVO', para).then(res=>res.json()).then(res=>{
			if(res.success == true) {
				let data = [];
				res.result.records.forEach((e, i) => {
					e.key = i;
					data.push(e);
				})
				this.setState({
					goodsList: data,
					total: res.result.total
				})
			}
		});
	}
	updateGoodsList(data){
		let arr=[];
		data.forEach((e, i) => {
			e.key = i;
			arr.push(e);
		})
		this.setState({
			goodsList: arr
		})
	}
	setPara(obj) {
		this.setState({
			searchPara: obj
		})
	}
	componentDidMount() {
		this.getDataList(this.state.searchPara);
	}
	render() {
		const columns = [
			{
			  title: '物品名称',
			  dataIndex: 'materialName',
			  key: 'materialName',
			  className:'f-align-center'
			}, {
			  title: '类别',
			  dataIndex: 'category',
			  key: 'category',
			  className:'f-align-center',
			}, {
			  title: '代码',
			  dataIndex: 'materialCode',
			  key: 'materialCode',
			  className:'f-align-center',
			}, {
			  title: '规格',
			  key: 'specify',
			  dataIndex: 'specify',
			  className:'f-align-center',
			}, {
			  title: '状态',
			  dataIndex: 'isDisable',
			  key: 'isDisable',
			  className:'f-align-center',
			  render: (text, record) => {
			  	if(record.isDisable == 0) {
			  		return (<span className="f-green">启用</span>)
			  	}else {
			  		return (<span className="f-red">停用</span>)
			  	}
			  }
			}, {
			  title: '采购单价',
			  dataIndex: 'purchasePrice',
			  key: 'purchasePrice',
			  className:'f-align-center',
			}, {
			  title: '销售单价',
			  dataIndex: 'salePrice',
			  key: 'salePrice',
			  className:'f-align-center',
			}, {
			  title: '单位',
			  dataIndex: 'unit',
			  key: 'unit',
			  className:'f-align-center',
			}, {
			  title: '操作',
			  dataIndex: 'action',
			  key: 'action',
			  className:'f-align-center',
			  render: (text, record) => (
			    <span>
					<Button type="primary" icon="edit" className="f-mr3 f-radius4" onClick={ ()=>{ this.setGoods(record.materialId) } }>编辑</Button><Popconfirm 
				            title="确定要删除吗?" 
				            onConfirm={
				              ()=> this.removeGoods(record.materialId)
				            }
				            okText="确定" 
				            cancelText="取消"
				          ><Button icon="delete" className='f-radius4'>删除</Button>
				        </Popconfirm>
			    </span>
			  )
			}];

		return (<div className='courseArranging classListPage wid100'>
					<Breadcrumb className="f-mb3">
					    <Breadcrumb.Item>仓库管理</Breadcrumb.Item>
					    <Breadcrumb.Item>仓库管理</Breadcrumb.Item>
					    <Breadcrumb.Item><span className='f-fz5 f-bold'>物品设置</span></Breadcrumb.Item>
					</Breadcrumb>
					<div className='f-bg-white f-pd4 f-box-shadow1 f-mt5 f-radius1'>
						<SearchFormCreate searchPara={ this.state.searchPara } setPara={this.setPara.bind(this)} wrappedComponentRef={ (inst) => this.searchForm = inst } updateGoodsList={ this.updateGoodsList.bind(this) } getAllData={ this.getDataList.bind(this) }/>
					</div>
					<div className='f-bg-white f-pd4 f-box-shadow1 f-mt5 f-radius1'>
						<div className='f-pb3 f-flex' style={{justifyContent: 'space-between'}}>
							<h3 className="f-title-blue">物品列表</h3>
							<a className="f-btn-green f-ml3" onClick={ ()=>{ this.setGoods() } }>
								<i className='iconfont icon-tianjiajiahaowubiankuang'></i>新增物品
							</a>
						</div>
						<Modal title={ this.state.text? '编辑物品' : '新增物品' }
							wrapClassName="vertical-center-modal"
							visible={ this.state.visibleType }
							onOk={ this.saveData.bind(this) }
							confirmLoading={ this.state.isRequest }
							onCancel={ this.cancelSetGoods.bind(this) }
							width={800}
						>
							<AddFormCreate isRequire={ this.state.isRequire } updateRequire={ this.updateRequire.bind(this) } goodsData={ this.state.editData } wrappedComponentRef={ (inst) => this.formRef = inst }/>
						</Modal>
						{
							this.state.goodsList != null? 
							<Table columns={columns} dataSource={this.state.goodsList} pagination={ Pagination(this.state.total, this.state.searchPara, this.getDataList.bind(this)) } bordered className='f-align-center'/>
							: <Loading />
						}
					</div>
				</div>)
	}
}


/*搜索条件*/
class SearchForm extends Component {
	constructor(props){
		super(props);
		this.state = {
			value:''
		}
	}
	searchCondition(){
		let obj = this.props.form.getFieldsValue(), pageSize = this.props.searchPara.pageSize;
			obj.currentPage = 1;
			obj.pageSize = pageSize;
			obj.materialName = obj.materialName1;
			obj.isDisable = obj.isDisable1;
			obj = undefinedToEmpty(obj);
		this.props.getAllData(obj);
		this.props.setPara(obj);
	}
	getAllData(){
		let pageSize = this.props.searchPara.pageSize;
		this.props.getAllData({currentPage: 1, pageSize: pageSize});
		this.props.setPara({currentPage: 1, pageSize: pageSize});
	}
	render(){
		const { getFieldDecorator } = this.props.form;

		return (<Form layout="inline">
					<FormItem label='物品名称' wrapperCol={{style:{width:'200px'}}}>
						{getFieldDecorator('materialName1', {
				            rules: [{
				              required: false,
				              message: '请输入物品名称',
				            }],
				            initialValue: ''
				          })(
				            <Input placeholder="请输入物品名称" />
				          )}
					</FormItem>
					<FormItem label='状态' wrapperCol={{style:{width:'200px'}}}>
						{getFieldDecorator('isDisable1', {
				            rules: [{
				              required: false,
				            }],
				            initialValue: undefined
				          })(
				            <Select
				                showSearch
				                placeholder="请选择"
				                onChange={ (value)=>{ return 'xx' } }
				              >
				                <Option value=''>全部</Option>
				                <Option value={'0'}>启用</Option>
				                <Option value={'1'}>停用</Option>
				              </Select>
				          )}
					</FormItem>
					<FormItem style={{ marginLeft:'100px' }}>
						<Button type="primary" onClick={ this.searchCondition.bind(this) }>查询</Button><Button className="f-ml5" style={{backgroundColor:'#F1F3F7'}} onClick={ ()=>{ this.props.form.resetFields(); this.getAllData() } }>重置</Button>
					</FormItem>
				</Form>)
	}
}

const formItemLayout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 18 },
};

/*新增物品*/
class AddForm extends Component {
	constructor(props){
		super(props);
		
		this.state = {
			categoryList: [],
			courseIds: '',
			courseList: [],
			value: '',
		}
		this.timeout='';
		this.currentValue='';
	}
	onChange = (e) => {
	    this.setState({
	      value: e.target.value,
	    });
	}
	fetch(value, callback) {
		let timeout = this.timeout, currentValue = this.currentValue;
		if (timeout) {
		    clearTimeout(timeout);
		    timeout = null;
		  }
		  currentValue = value;

		  function fake() {
		    
		    fpost('/api/material/listMaterialCategory', {categoryName: value || ''})
		      .then(response => response.json())
		      .then((d) => {
		        if (currentValue === value) {
		          const result = d.result;
		          const data = [];
		          result.forEach((r) => {
		            data.push({
		              value: r
		            });
		          });
		          callback(data);
		        }
		      });
		  }

		  timeout = setTimeout(fake, 300);
	}
	setCourse(e) {
		let { materialCourseList } = this.props.goodsData;
		if(e.target.value == '') {
			this.props.form.setFieldsValue({
				courseIds: []
			})
		}else if(e.target.value == '1') {
			this.props.form.setFieldsValue({
				courseIds: materialCourseList && materialCourseList.length? materialCourseList.map((e, i)=>{
								return e.courseId.toString()
						    }) : []
			});
		}
		this.props.updateRequire(e.target.value);
	}
	handleChange(value) {
		let val = (value && value.length)? value.replace(/(^\s*)|(\s*$)/g, '') : '';
		this.setState({ value: val});
		this.fetch(val, data => this.setState({ categoryList: data }));
	}
	componentDidMount() {

		//获取课程列表
		fpost('/api/system/course/listCourse').then(res=>res.json()).then(res=>{
			if(res.success == true) {
				this.setState({
					courseList: res.result
				})
			}else {
				message.error(res.message);
			}
		})
	}
	render(){

		const { getFieldDecorator } = this.props.form;
		const { category='', description='', isDisable, materialCode='', materialCourseList, materialName, purchasePrice, salePrice, specify='', unit } = this.props.goodsData;
		const radioStyle = {
		      display: 'block',
		      height: '30px',
		      lineHeight: '30px',
		    };

		const options = this.state.categoryList.map(d => <Option key={d.value}>{d.value}</Option>);

		return (<Form>
					<Row>
						<Col span={12} style={{ marginLeft:'-16px' }}>
							<FormItem {...formItemLayout} label='物品名称'>
								{getFieldDecorator('materialName', {
						            rules: [{
						              required: true,
						              message: '请输入物品名称且不能为空格',
						              whitespace: true
						            },
						            {
						            	max: 50,
						            	min: 1,
						            	transform: (val) =>{
						            		return val!=undefined? val.replace(/\s+/g,'').replace(/[^\x00-\xff]/g,'01') : '';
						            	},
						            	message: '长度不能超过50（25个汉字）'
						            }],
						            initialValue: materialName
						          })(
						            <Input placeholder="请输入物品名称" />
						          )}
							</FormItem>
						</Col>
						<Col span={12} style={{ marginLeft:'16px' }}>
							<FormItem {...formItemLayout} label='类别'>
								{getFieldDecorator('category', {
						            rules: [{
						              required: true,
						              message: '请输入及选择类别且不能为空',
						              whitespace: true
						            },
						            {
						            	max: 20,
						            	min: 1,
						            	transform: (val) =>{
						            		return val!=undefined? val.replace(/\s+/g,'').replace(/[^\x00-\xff]/g,'01') : '';
						            	},
						            	message: '长度不能超过20（10个汉字）'
						            }],
						            initialValue: category
						          })(
						            <Select
					                    mode="combobox"
					                    placeholder={'请输入及选择类别且不能为空'}
					                    defaultActiveFirstOption={false}
					                    showArrow={false}
					                    filterOption={false}
					                    onChange={this.handleChange.bind(this)}
					                    onFocus={this.handleChange.bind(this)}
					                  >
					                    {options}
					                  </Select>
						          )}
							</FormItem>
						</Col>
					</Row>
					<Row>
						<Col span={12} style={{ marginLeft:'-16px' }}>
							<FormItem {...formItemLayout} label='代码'>
								{getFieldDecorator('materialCode', {
						            rules: [{
						              required: false,
						              message: '请输入代码',
						            },
						            {
						            	max: 50,
						            	min: 1,
						            	transform: (val) =>{
						            		return val!=undefined? val.replace(/\s+/g,'').replace(/[^\x00-\xff]/g,'01') : '';
						            	},
						            	message: '长度不能超过50（25个汉字）'
						            }],
						            initialValue: materialCode || ''
						          })(
						            <Input placeholder="请输入代码" />
						          )}
							</FormItem>
						</Col>
						<Col span={12} style={{ marginLeft:'16px' }}>
							<FormItem {...formItemLayout} label='规格'>
								{getFieldDecorator('specify', {
						            rules: [{
						              required: false,
						              message: '请输入规格',
						              whitespace: true
						            },
						            {
						            	max: 100,
						            	min: 1,
						            	transform: (val) =>{
						            		return val!=undefined? val.replace(/\s+/g,'').replace(/[^\x00-\xff]/g,'01') : '';
						            	},
						            	message: '长度不能超过100（50个汉字）'
						            }],
						            initialValue: specify || ''
						          })(
						            <Input placeholder="请输入规格" />
						          )}
							</FormItem>
						</Col>
					</Row>
					<Row>
						<Col span={12} style={{ marginLeft:'-16px' }}>
							<FormItem {...formItemLayout} label='单位'>
								{getFieldDecorator('unit', {
						            rules: [{
						              required: true,
						              message: '请输入单位',
						              whitespace: true
						            }],
						            initialValue: unit
						          })(
						            <Input placeholder="请输入单位" />
						          )}
							</FormItem>
						</Col>
						<Col span={12} style={{ marginLeft:'16px' }}>
							<FormItem {...formItemLayout} label='采购单价'>
								{getFieldDecorator('purchasePrice', {
						            rules: [{
						              required: true,
						              message: '请输入采购单价',
						            }],
						            initialValue: {
						            	number: purchasePrice,
						            	min: 0,
						            	max: 99999.99
						            }
						          })(
						            <NumberInput style={{width:'290px'}} placeholder="请输入采购单价"/>
						          )}
							</FormItem>
						</Col>
					</Row>
					<Row>
						<Col span={12} style={{ marginLeft:'-16px' }}>
							<FormItem {...formItemLayout} label='销售单价'>
								{getFieldDecorator('salePrice', {
						            rules: [{
						              required: true,
						              message: '请输入销售单价',
						            }],
						            initialValue: {
						            	number: salePrice,
						            	min: 0,
						            	max: 99999.99
						            }
						          })(
						            <NumberInput style={{width:'290px'}} placeholder="请输入销售单价"/>
						          )}
							</FormItem>
						</Col>
						<Col span={12} style={{ marginLeft:'16px' }}>
							<FormItem {...formItemLayout} label='适用课程'>
								{getFieldDecorator('course', {
						            rules: [{
						              required: false,
						              message: '请选择适用课程',
						            }],
						            initialValue: materialCourseList && materialCourseList.length? '1' : ''
						          })(
						            <RadioGroup onChange={ this.setCourse.bind(this) }>
    							        <Radio style={radioStyle} value=''><i className='f-vertical-middle'>全部课程</i></Radio>
    							        <Radio style={{display:'block', lineHeight: '30px'}} value='1'>
    							        	<i className='f-vertical-middle'>指定课程</i>
    							        	<FormItem style={{ display:'inline-block', zIndex: '2' }}>
						        				{getFieldDecorator('courseIds', {
						        		            rules: [{
						        		              required: (materialCourseList && materialCourseList.length) || this.props.isRequire? true : false,
						        		              message: '请选择指定课程',
						        		            }],
						        		            initialValue: materialCourseList && materialCourseList.length? materialCourseList.map((e, i)=>{
        													return e.courseId.toString()
        											    }) : []
						        		          })(
						        		            <Select
        											    className='f-vertical-middle f-ml3'
        											    style={{ width: 160 }}
        											    placeholder="请选择"
        											    optionFilterProp="children"
        											    disabled={this.props.isRequire==false? true : false}
        											    filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
        											    mode='multiple'
        											  >
        											    {
        											    	this.state.courseList.map((e, i)=>{
        														return (<Option key={i} value={ e.id }>{ e.name }</Option>)
        											    	})
        											    }
        										  </Select>
						        		          )}
    							        	</FormItem>
    									</Radio>
    							      </RadioGroup>
						          )}
							</FormItem>
						</Col>
					</Row>
					<Row>
						<Col span={12} style={{ marginLeft:'-16px' }}>
							<FormItem {...formItemLayout} label='描述'>
								{getFieldDecorator('description', {
						            rules: [{
						              required: false,
						              message: '请输入描述',
						            }],
						            initialValue: description
						          })(
						            <Input placeholder="请输入描述" />
						          )}
							</FormItem>
						</Col>
						<Col span={12} style={{ marginLeft:'16px' }}>
							<FormItem {...formItemLayout} label='状态'>
								{getFieldDecorator('isDisable', {
						            rules: [{
						              required: true,
						              message: '请选择状态',
						            }],
						            initialValue: isDisable
						          })(
						            <RadioGroup>
								        <Radio value={'0'}><i className='f-vertical-middle'>启用</i></Radio>
								        <Radio value={'1'}><i className='f-vertical-middle'>停用</i></Radio>
								    </RadioGroup>
						          )}
							</FormItem>
						</Col>
					</Row>
				</Form>)
	}
}

const SearchFormCreate = Form.create()(SearchForm);
const AddFormCreate = Form .create()(AddForm);