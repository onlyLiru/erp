import React, { Component } from 'react';
import { Breadcrumb, Form, Select, Input, DatePicker, Button, Modal, Table, Row, Col } from 'antd';
import { default as Tablinks } from './links.js';

const FormItem = Form.Item;
const Option = Select.Option;
const RangePicker = DatePicker.RangePicker;

const columns = [
	{
	  title: '日期',
	  dataIndex: 'dateName',
	  key: 'dateName',
	  className:'f-align-center',
	  render: text => <a href="#">{text}</a>,
	}, {
	  title: '学生姓名（学号）',
	  key: 'user',
	  dataIndex: 'user',
	  className:'f-align-center',
	}, {
	  title: '操作人（工号）',
	  key: 'operator',
	  dataIndex: 'operator',
	  className:'f-align-center',
	}, {
	  title: '备注',
	  dataIndex: 'remark',
	  key: 'remark',
	  className:'f-align-center',
	}, {
	  title: '操作时间',
	  dataIndex: 'operatingTime',
	  key: 'operatingTime',
	  className:'f-align-center',
	}, {
	  title: '操作',
	  key: 'view',
	  className:'f-align-center',
	  render: (text, record) => (
		<Button className='f-radius f-bg-blue f-white'><i className='iconfont icon-chakan f-pointer f-mr1'></i>查看</Button>
	  )
	}];

	const data = [{
	  key: '1',
	  dateName: '九堡校区仓库',
	  user: '易磊',
	  operator: '',
	  remark: '',
	  operatingTime: '',
	  view: ''
	}];

	const addColumns = [
		{
		  title: '物品名称',
		  dataIndex: 'goodsName',
		  key: 'goodsName',
		  className:'f-align-center'
		},
		{
		  title: '规格',
		  dataIndex: 'goodsName',
		  key: 'goodsName',
		  className:'f-align-center'
		},
		{
		  title: '单位',
		  dataIndex: 'goodsName',
		  key: 'goodsName',
		  className:'f-align-center'
		},
		{
		  title: '数量',
		  dataIndex: 'goodsName',
		  key: 'goodsName',
		  className:'f-align-center'
		},
		{
		  title: '进货单价',
		  dataIndex: 'goodsName',
		  key: 'goodsName',
		  className:'f-align-center'
		},
		{
		  title: '金额',
		  dataIndex: 'goodsName',
		  key: 'goodsName',
		  className:'f-align-center'
		},
		{
		  title: '操作',
		  dataIndex: 'goodsName',
		  key: 'goodsName',
		  className:'f-align-center'
		}
	]

export default class Main extends Component {
	constructor(props){
		super(props);
		this.state = {
			visibleType: false
		}
	}
	addGoods(){
		this.setState({
			visibleType: true
		})
	}
	saveData(){

	}
	cancelAddGoods(){
		this.setState({
			visibleType: false
		})
	}
	render(){

		return (<div className='courseArranging classListPage wid100'>
					<Breadcrumb className="f-mb3">
					    <Breadcrumb.Item>库存管理</Breadcrumb.Item>
					    <Breadcrumb.Item>进出库管理</Breadcrumb.Item>
					</Breadcrumb>
					<Tablinks />
					<div className='f-bg-white f-pd4 f-box-shadow1 f-mt1 f-radius1'>
						<SearchFormCreate />
					</div>
					<div className='f-bg-white f-pd4 f-box-shadow1 f-mt1 f-radius1'>
						<div className='f-bg-white f-radius1'>
							<div className='f-pb3 f-flex' style={{justifyContent: 'space-between'}}>
								<h3 className="f-title-blue">报课物品领用列表</h3>
								<a className="f-btn-green" onClick={ this.addGoods.bind(this) }>
									<i className='iconfont icon-tianjiajiahaowubiankuang'></i>新增
								</a>
								<Modal title="领用：昂立局前仓库"
									wrapClassName="vertical-center-modal"
									visible={ this.state.visibleType }
									onOk={ this.saveData.bind(this) }
									width={ 800 }
									bodyStyle={{padding:'16px 0'}}
									onCancel={ this.cancelAddGoods.bind(this) }
								>
									<ChooseFormCreate />
									<Table columns={addColumns}/>
									<div style={{background:'#F3F3F8', paddingLeft:'50px', lineHeight:'50px'}}>合计</div>
									<Row className='f-mt5 f-mb5'>
										<Col span={12} style={{paddingLeft:'50px'}}>
											<span>备注：</span><Input style={{width:'50%'}} placeholder="请输入备注"/>
										</Col>
										<Col span={12} className='f-align-right f-pr4'>
											<span>操作人（工号）：鹿晗（0823）</span>
										</Col>
									</Row>
								</Modal>
							</div>
							<Table columns={columns} dataSource={data} bordered className='f-align-center'/>
						</div>
					</div>
				</div>)
	}
}

class SearchForm extends Component {
	resetForm(){

	}
	render(){
		const { getFieldDecorator } = this.props.form;
		return (<Form layout='inline' className='f-over-hide'>
					<FormItem style={{ marginBottom:'20px' }} label='仓库' wrapperCol={{style:{width:'50%'}}}>
						{getFieldDecorator('username', {
				            rules: [{
				              required: false,
				            }],
				          })(
				            <Select
				                showSearch
				                style={{ width: 200 }}
				                placeholder="松江仓库"
				                optionFilterProp="children"
				                filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
				              >
				                <Option value="jack">松江仓库</Option>
				                <Option value="lucy">黄浦仓库</Option>
				              </Select>
				          )}
					</FormItem>
					<FormItem style={{ marginBottom:'20px' }} label='操作人(工号）' wrapperCol={{style:{width:'200px'}}}>
						{getFieldDecorator('username', {
				            rules: [{
				              required: false,
				              message: '请输入仓库名称',
				            }],
				          })(
				            <Input placeholder="请输入仓库名称" />
				          )}
					</FormItem>
					<FormItem style={{ marginBottom:'20px' }} label='学员姓名（学号）' wrapperCol={{style:{width:'200px'}}}>
						{getFieldDecorator('username', {
				            rules: [{
				              required: false,
				              message: '请输入学员姓名／学号',
				            }],
				          })(
				            <Input placeholder="请输入学员姓名／学号" />
				          )}
					</FormItem>
					<FormItem
						label="时间"
						wrapperCol={{style:{width:'80%'}}}
					>
						{
							getFieldDecorator('myTime', { 
								rules: [{ 
									type: 'array', 
									required: false, 
									message: '请输入时间段' 
								}] 
							})(<RangePicker />)
						}
					</FormItem>
					<FormItem className='f-align-center f-mt5' style={{display: 'block'}}>
						<Button className='f-mr5' type="primary" style={{borderRadius: '4px',width:'110px',height:'40px'}}>
	        				<i className='iconfont icon-hricon33 f-mr2'></i>
	        				搜索
	        			</Button>
	        			<Button style={{border: '1px solid #999999',borderRadius: '4px',width:'110px',height:'40px'}} onClick={this.resetForm.bind(this)}>
	        				<i className='iconfont icon-reset f-mr2'></i>
	        				重置
	        			</Button>
					</FormItem>
				</Form>)
	}
}
class ChooseForm extends Component {
	onChange(){

	}
	render(){
		const { getFieldDecorator } = this.props.form;
		return (<Form>
					<FormItem className='f-inline-block f-mr5' style={{marginLeft:'50px'}} label='领用日期' wrapperCol={{style:{width:'200px', display:'inline-block'}}}>
						{getFieldDecorator('username', {
				            rules: [{
				              required: false,
				            }],
				          })(
				            <DatePicker onChange={this.onChange.bind(this)} />
				          )}
					</FormItem>
					<FormItem className='f-inline-block' label='学员姓名（学号）' wrapperCol={{style:{display:'inline-block', verticalAlign:'middle', width:'100px'}}}>
						{getFieldDecorator('username', {
				            rules: [{
				              required: false,
				              message: '请选择',
				            }],
				          })(
				            <span>只读</span>
				          )}
					</FormItem>
					<FormItem className='f-inline-block'>
						<Button style={{border:'none'}} className='f-blue'>选择物品</Button>
					</FormItem>
				</Form>)
	}
}

const SearchFormCreate = Form.create()(SearchForm);
const ChooseFormCreate = Form.create()(ChooseForm);