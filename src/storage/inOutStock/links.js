import React, { Component } from 'react';
import { Button } from 'antd';

export default class Main extends Component {
	render(){
		return (<div className='f-mt5'>
					<Button onClick={ ()=>{ window.location.href = '/storage/inOutStock/purchases' } } style={{ border:'none', background: window.location.href.indexOf('purchases') != -1 ? '#FFF' : '#F7F7F7', marginRight:'4px' }}>进货</Button>
					<Button onClick={ ()=>{ window.location.href = '/storage/inOutStock/salesReturn' } } style={{ border:'none', background:window.location.href.indexOf('salesReturn') != -1 ? '#FFF' : '#F7F7F7', marginRight:'4px' }}>退货</Button>
					<Button onClick={ ()=>{ window.location.href = '/storage/inOutStock/allot' } } style={{ border:'none', background:window.location.href.indexOf('allot') != -1 ? '#FFF' : '#F7F7F7', marginRight:'4px' }}>调拨</Button>
					<Button onClick={ ()=>{ window.location.href = '/storage/inOutStock/breakage' } } style={{ border:'none', background:window.location.href.indexOf('breakage') != -1 ? '#FFF' : '#F7F7F7', marginRight:'4px' }}>报损</Button>
					<Button onClick={ ()=>{ window.location.href = '/storage/inOutStock/employeeGoods' } } style={{ border:'none', background:window.location.href.indexOf('employeeGoods') != -1 ? '#FFF' : '#F7F7F7', marginRight:'4px' }}>员工物品领用</Button>
					<Button onClick={ ()=>{ window.location.href = '/storage/inOutStock/goodsBack' } } style={{ border:'none', background:window.location.href.indexOf('goodsBack') != -1 ? '#FFF' : '#F7F7F7', marginRight:'4px' }}>员工物品退领</Button>
					<Button onClick={ ()=>{ window.location.href = '/storage/inOutStock/stockAdjust' } } style={{ border:'none', background:window.location.href.indexOf('stockAdjust') != -1 ? '#FFF' : '#F7F7F7', marginRight:'4px' }}>库存调整</Button>
				</div>)
	}
}