import React, {
	Component
} from 'react';
import {
	Input,
	Button,
	Col,
	Row,
	Form,
	Select,
	message
} from 'antd';
import {
	default as SelectPerson
} from '../../common/selectPerson.js';
import SelectCourses from '../../common/selectCourses.js';
import {
	fpost
} from '../../common/io.js';
import {
	undefinedToEmpty
} from '../../common/g.js';
const FormItem = Form.Item;
const Option = Select.Option;

class MainForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			visible: true,
			stockList: [] //仓库信息
		}
	}
	render() {
		const {
			getFieldDecorator
		} = this.props.form;

		const {
			visible,
			stockList
		} = this.state;

		const formItemLayout = {
			labelCol: {
				span: 6
			},
			wrapperCol: {
				span: 18
			}
		};

		if (!visible) {
			return null;
		}

		return (<header className="f-box-shadow2 f-radius1 f-bg-white f-pd4">
			<Form>
				<Row gutter={ 40 }>
					<Col span={8}>
    					<FormItem
							label="仓库"
							{...formItemLayout}
						>
							{
								getFieldDecorator('warehouseId', {
									rules: [{  
										message: '请选择仓库' 
									}]
								})(
									<Select
					                showSearch
					                placeholder="请选择"
					                optionFilterProp="children"
					                filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
					              >
								    {
								    	stockList.length ? stockList.map((e, i)=>{
											return (<Option key={i} value={ e.warehouseId }>{ e.warehouseName }</Option>)
								    	}) : ''
								    }
					              </Select>
								)
							}
						</FormItem>
					</Col>
					<Col span={8}>
						<FormItem
							label="物品"
							{...formItemLayout}
						>
							{
								getFieldDecorator('materialName', {
									rules: [{  
										message: '请输入物品名称' 
									}]
								})(
									<Input placeholder="请输入物品名称" />
								)
							}
						</FormItem>
					</Col>
					<Col span={8}>
						<FormItem
							label="是否学员"
							{...formItemLayout}
						>
							{
								getFieldDecorator('isStudent', {
									rules: [{  
										message: '请选择' 
									}]
								})(
									<Select defaultValue="1" placeholder="请选择">
								      <Option value="1">是</Option>
								      <Option value="0">否</Option>
								    </Select>
								)
							}
						</FormItem>
    				</Col>
    	        </Row>
    			<Row gutter={ 40 }>
    				<Col span={8}>
	    				<FormItem
	    					label="是否领取"
	    					{...formItemLayout}
	    				>
	    					{
	    						getFieldDecorator('isTaken', {
	    							rules: [{  
	    								message: '请选择' 
	    							}]
	    						})(
	    							<Select defaultValue="1" placeholder="请选择">
								      <Option value="1">是</Option>
								      <Option value="0">否</Option>
								    </Select>
	    						)
	    					}
	    				</FormItem>
    				</Col>
    				<Col span={8}>
    					<FormItem
							label="领取人姓名"
							{...formItemLayout}
						>
							{
								getFieldDecorator('receriverName', { 
									rules: [{
										required: false, 
										message: '请输入领取人姓名' 
									}] 
								})
								(
									<Input placeholder="请输入领取人姓名" />
								)
							}
						</FormItem>
    				</Col>
    				<Col span={ 8 }>
						<FormItem
							label="领取人电话"
							{...formItemLayout}
						>
							{
								getFieldDecorator('receriverPhone', { 
									rules: [{
										required: false, 
										message: '请输入领取人电话' 
									}] 
								})
								(
									<Input placeholder="请输入领取人电话" />
								)
							}
						</FormItem>
					</Col>
    	        </Row>
    	        <div className="f-align-center">
    	        	<Button 
    	        		size="large" 
    	        		className="f-mr4" 
    	        		onClick={ this._reset.bind(this) }
    	        	>重置</Button>
    	        	<Button 
    	        		type="primary"
    	        		size="large" 
    	        		icon="search" 
    	        		onClick={ this._search.bind(this) }
    	        	>查询</Button>
    	        </div>
	      	</Form>
		</header>);
	}
	componentDidMount() {
		//查询仓库列表
		fpost('/api/warehouse/listWarehouseSimpleVO').then(res => res.json()).then(res => {
			if (res.success == true) {
				this.setState({
					stockList: res.result
				})
			} else {
				message.error(res.message);
			}
		});
	}
	_search(e) { //搜索
		let {
			onSearch
		} = this.props;

		this.props.form.validateFields((err, values) => {
			if (!err) {
				/*如果值为undefined则替换为空*/
				values = undefinedToEmpty(values);

				onSearch(values);
			}
		});
	}
	_reset() {
		this.props.form.resetFields();
		this.setState({
			visible: false
		}, () => {
			this.setState({
				visible: true
			}, () => {
				this._search();
			})
		});
	}
}

const MyForm = Form.create()(MainForm);

export default MyForm;