import React, {
	Component
} from 'react';
import store from 'store2';
import {
	Form,
	Breadcrumb,
	Table,
	Input,
	Select,
	Button,
	message
} from 'antd';
import {
	fpost,
	hostname
} from '../common/io.js';
import {
	Loading,
	Pagination,
	undefinedToEmpty
} from '../common/g.js';

const FormItem = Form.Item;

const Option = Select.Option;

export default class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			visibleType: false,
			stockListData: null,
			stockList: [],
			categoryList: [],
			warehouseId: '',
			total: 1,
			searchPara: {
				currentPage: 1,
				pageSize: 10,
				warehouseId: '',
				category: '',
				inventory: '',
				materialName: '',
				isDisable: ''
			}
		}
	}
	getStockList(para) {
		//查询库存列表
		fpost('/api/materialstock/pageMaterialStockVO', para).then(res => res.json()).then(res => {
			if (res.success == true) {
				let data = [];
				res.result.records.forEach((e, i) => {
					e.key = i;
					data.push(e);
				});
				this.setState({
					stockListData: data,
					total: res.result.total
				})
			} else {
				message.error(res.message);
			}
		});

	}
	setPara(obj) {
		this.setState({
			searchPara: obj
		})
	}
	componentDidMount() {
		let self = this;
		//查询仓库列表
		fpost('/api/warehouse/listWarehouseSimpleVO').then(res => res.json()).then(res => {
			if (res.success == true) {
				this.setState({
					stockList: res.result
				}, function() {
					let searchPara = self.state.searchPara;
					self.getStockList(searchPara);
				})
			} else {
				message.error(res.message);
			}
		});

		//查询分类列表
		fpost('/api/material/listMaterialCategory').then(res => res.json()).then(res => {
			if (res.success == true) {
				this.setState({
					categoryList: res.result
				})
			} else {
				message.error(res.message);
			}
		});

	}
	render() {
		const columns = [{
			title: '物品名称',
			dataIndex: 'materialName',
			key: 'materialName',
			className: 'f-align-center',
		}, {
			title: '类别',
			dataIndex: 'category',
			key: 'category',
			className: 'f-align-center',
		}, {
			title: '代码',
			key: 'materialCode',
			dataIndex: 'materialCode',
			className: 'f-align-center',
		}, {
			title: '规格',
			dataIndex: 'specify',
			key: 'specify',
			className: 'f-align-center'
		}, {
			title: '状态',
			dataIndex: 'isDisable',
			key: 'isDisable',
			className: 'f-align-center',
			render: (text, record) => (
				record.isDisable == '1' ? <a className="f-red">停用</a> : <a className="f-green">启用</a>
			)
		}, {
			title: '单位',
			dataIndex: 'unit',
			key: 'unit',
			className: 'f-align-center',
		}, {
			title: '销售单价',
			dataIndex: 'salePrice',
			key: 'salePrice',
			className: 'f-align-center',
		}, {
			title: '当前库存',
			dataIndex: 'inventory',
			key: 'inventory',
			className: 'f-align-center'
		}];
		const {
			category = '',
				inventory = '',
				materialName = '',
				isDisable = ''
		} = this.state.searchPara;
		return (<div className='courseArranging classListPage wid100'>
					<Breadcrumb className="f-mb3">
					    <Breadcrumb.Item>仓库管理</Breadcrumb.Item>
					    <Breadcrumb.Item>仓库管理</Breadcrumb.Item>
					    <Breadcrumb.Item><span className='f-fz5 f-bold'>库存查询</span></Breadcrumb.Item>
					</Breadcrumb>
					<div className='f-bg-white f-pd4 f-box-shadow1 f-mt5 f-radius1'>
						<SearchFormCreate pageSize={this.state.searchPara.pageSize} setPara={this.setPara.bind(this)} wrappedComponentRef={ (inst) => this.searchForm = inst } stockList={this.state.stockList} categoryList={this.state.categoryList} superStockList={this.getStockList.bind(this)}/>
					</div>
					<div className='f-bg-white f-pd4 f-box-shadow1 f-mt5 f-radius1'>
						<div className='f-pb3 f-flex' style={{justifyContent: 'space-between'}}>
							<h3 className="f-title-blue">库存列表</h3>
							<a className="f-btn-blue" href={hostname()+'/api/materialstock/exportMaterialStockVO?warehouseId='+(this.state.searchPara.warehouseId!=''? this.state.searchPara.warehouseId : this.state.warehouseId)+'&category='+category+'&inventory='+inventory+'&materialName='+materialName+'&isDisable='+isDisable+'&x-auth-token='+store.get('sessionId')}>
								<i className='iconfont icon-daoru1-copy f-mr1'></i>导出
							</a>
						</div>
						{
							this.state.stockListData != null? 
							<Table columns={columns} pagination={ Pagination(this.state.total, this.state.searchPara, this.getStockList.bind(this)) } dataSource={this.state.stockListData} bordered className='f-align-center' scroll={{ x: 1300 }}/>
							: <Loading />
						}
					</div>
				</div>)
	}
}


/*搜索条件*/
class SearchForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			value: ''
		}
	}
	resetForm() {
		let pageSize = this.props.pageSize;
		this.props.form.resetFields();
		this.props.superStockList({
			currentPage: 1,
			pageSize: pageSize,
			warehouseId:''
//			warehouseId: this.props.form.getFieldValue('warehouseId')
		});
		this.props.setPara({
			currentPage: 1,
			pageSize: pageSize,
			warehouseId:''
		});
	}
	searchCondition() {
		let obj = this.props.form.getFieldsValue(),
			pageSize = this.props.pageSize;
		obj.warehouseId = obj.warehouseId ? obj.warehouseId : '';
		obj.category = obj.category ? obj.category : '';
		obj.currentPage = 1;
		obj.pageSize = pageSize;
		obj.isDisable = obj.isDisable ? obj.isDisable : '';
		obj.inventory = obj.inventory ? obj.inventory : '';

		obj = undefinedToEmpty(obj);
		this.props.superStockList(obj);
		this.props.setPara(obj);
	}
	render() {
		const {
			getFieldDecorator
		} = this.props.form;

		return (<Form layout="inline">
					<FormItem style={{ marginBottom:'20px' }} className='f-mr5' label='仓库' wrapperCol={{style:{width:'60%'}}}>
						{getFieldDecorator('warehouseId', {
				            rules: [{
				              required: false,
				              message: '请选择',
				            }],
				            initialValue: undefined
				          })(
				            <Select
							    className='f-vertical-middle'
							    style={{ width: 220 }}
							    placeholder="请选择"
							    optionFilterProp="children"
							    filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
							  >
							    {
							    	this.props.stockList.length? this.props.stockList.map((e, i)=>{
										return (<Option key={i} value={ e.warehouseId }>{ e.warehouseName }</Option>)
							    	}) : ''
							    }
						  </Select>
				          )}
					</FormItem>
					<FormItem style={{ marginBottom:'20px' }} className='f-mr5' label='类别' wrapperCol={{style:{width:'60%'}}}>
						{getFieldDecorator('category', {
				            rules: [{
				              required: false,
				              message: '请选择类别',
				            }],
				            initialValue: undefined
				          })(
				            <Select
							    className='f-vertical-middle'
							    style={{ width: 220 }}
							    placeholder="请选择"
							    optionFilterProp="children"
							    filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
							  >
							    {
							    	this.props.categoryList.length? this.props.categoryList.map((e, i)=>{ 
										return (<Option key={i} value={ e }>{ e }</Option>)
							    	 }) : ''
							    }
						  </Select>
				          )}
					</FormItem>
					<FormItem style={{ marginBottom:'20px' }} className='f-mr5' label='库存数量' wrapperCol={{style:{width:'60%'}}}>
						{getFieldDecorator('inventory', {
				            rules: [{
				              required: false,
				              message: '请选择',
				            }],
				            initialValue: undefined
				          })(
				            <Select
							    className='f-vertical-middle'
							    style={{ width: 220 }}
							    placeholder="请选择"
							    optionFilterProp="children"
							    filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
							  >
							    <Option value="-1">小于0</Option>
							    <Option value="0">等于0</Option>
							    <Option value="1">大于0</Option>
						  </Select>
				          )}
					</FormItem>
					<FormItem style={{ marginBottom:'20px' }} className='f-mr5' label='物品名称' wrapperCol={{style:{width:'200px'}}}>
						{getFieldDecorator('materialName', {
				            rules: [{
				              required: false,
				              message: '请输入物品名称',
				            }],
				            initialValue: ''
				          })(
				            <Input placeholder="请输入物品名称" />
				          )}
					</FormItem>
					<FormItem style={{ marginBottom:'20px' }} className='f-mr5' label='状态' wrapperCol={{style:{width:'60%'}}}>
						{getFieldDecorator('isDisable', {
				            rules: [{
				              required: false,
				              message: '请选择',
				            }],
				            initialValue: undefined
				          })(
				            <Select
							    className='f-vertical-middle'
							    style={{ width: 220 }}
							    placeholder="请选择"
							    optionFilterProp="children"
							    filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
							  >
							    <Option value="0">启用</Option>
							    <Option value="1">停用</Option>
						  </Select>
				          )}
					</FormItem>
					<FormItem style={{width:'100%', textAlign:'center'}}>
						<Button className='f-mr5' type="primary" style={{borderRadius: '4px',width:'110px',height:'40px'}} onClick={ this.searchCondition.bind(this) }>
	        				<i className='iconfont icon-hricon33 f-mr2'></i>
	        				搜索
	        			</Button>
	        			<Button style={{border: '1px solid #999999',borderRadius: '4px',width:'110px',height:'40px'}} onClick={this.resetForm.bind(this)}>
	        				<i className='iconfont icon-reset f-mr2'></i>
	        				重置
	        			</Button>
					</FormItem>
				</Form>)
	}
}

const SearchFormCreate = Form.create()(SearchForm);