import React, {
	Component
} from 'react';
import {
	Form,
	Breadcrumb,
	Table,
	Input,
	Select,
	Button,
	Row,
	Col,
	DatePicker,
	message
} from 'antd';
import {
	fpost
} from '../common/io.js';
import {
	Loading,
	Pagination,
	undefinedToEmpty
} from '../common/g.js';

const dateFormat = 'YYYY-MM-DD';
const FormItem = Form.Item;
const Option = Select.Option;
const {
	Column,
	ColumnGroup
} = Table;


export default class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			changeDataList: null,
			stockList: [],
			categoryList: [],
			warehouseId: '',
			total: 1,
			searchPara: {
				currentPage: 1,
				pageSize: 10
			}
		}
	}
	updateWarehouseId(v) {
		this.setState({
			warehouseId: v
		})
	}
	getChangeDataList(para) {
		fpost('/api/materialstock/pageMaterialStockModifyDetailVO', para).then(res => res.json()).then(res => {
			if (res.success == true) {
				let arr = [];
				res.result.records.map((e, i) => {
					e.key = i;
					arr.push(e);
				});
				this.setState({
					changeDataList: arr,
					total: res.result.total
				})
			} else {
				message.error(res.message);
			}
		})
	}
	setPara(obj) {
		this.setState({
			searchPara: obj
		})
	}
	componentDidMount() {
		let self = this;
		//查询仓库列表
		fpost('/api/warehouse/listWarehouseSimpleVO').then(res => res.json()).then(res => {
			if (res.success == true) {
				this.setState({
					stockList: res.result,
					warehouseId: res.result.length ? res.result[0].warehouseId : ''
				}, function() {
					let searchPara = self.state.searchPara;
					searchPara.warehouseId = res.result.length ? res.result[0].warehouseId : '';
					if (searchPara.warehouseId == '') {
						self.setState({
							changeDataList: []
						})
					} else {
						self.getChangeDataList(searchPara)
					}
				})
			} else {
				message.error(res.message);
			}
		});

		//查询分类列表
		fpost('/api/material/listMaterialCategory').then(res => res.json()).then(res => {
			if (res.success == true) {
				this.setState({
					categoryList: res.result
				})
			} else {
				message.error(res.message);
			}
		});
	}
	render() {
		return (<div className='courseArranging classListPage'>
					<Breadcrumb className="f-mb3">
					    <Breadcrumb.Item>仓库管理</Breadcrumb.Item>
					    <Breadcrumb.Item>仓库管理</Breadcrumb.Item>
					    <Breadcrumb.Item><span className='f-fz5 f-bold'>库存变动表</span></Breadcrumb.Item>
					</Breadcrumb>
					<div className='f-bg-white f-pd4 f-box-shadow1 f-mt5 f-radius1 f-over-hide'>
						<SearchFormCreate pageSize={this.state.searchPara.pageSize} setPara={ this.setPara.bind(this) } stockList={ this.state.stockList } updateChangeList={ this.getChangeDataList.bind(this) } updateWarehouseId={ this.updateWarehouseId.bind(this) } categoryList={ this.state.categoryList } warehouseId={ this.state.warehouseId }/>
					</div>
					<div className='f-bg-white f-pd4 f-box-shadow1 f-mt5 f-radius1'>
						<div className='f-pb3 f-flex' style={{justifyContent: 'space-between'}}>
							<h3 className="f-title-blue">库存变动列表</h3>
						</div>
						{
							this.state.changeDataList != null?
							<Table dataSource={this.state.changeDataList} bordered pagination={ Pagination(this.state.total, this.state.searchPara, this.getChangeDataList.bind(this)) } scroll={{ x: 1300 }}>
							    <Column
							      title="物品名称"
							      dataIndex="materialName"
							      key="materialName"
							      className="f-align-center"
							    />
							    <Column
							      title="类别"
							      dataIndex="category"
							      key="category"
							      className="f-align-center"
							    />
							    <Column
							      title="状态"
							      dataIndex="isDisable"
							      key="isDisable"
							      className="f-align-center"
							    />
							    <Column
							      title="单位"
							      dataIndex="unit"
							      key="unit"
							      className="f-align-center"
							    />
							    <Column
							      title="期初库存"
							      dataIndex="beforeModifyQuantity"
							      key="beforeModifyQuantity"
							      className="f-align-center"
							    />
							    <ColumnGroup title="入库">
							      <Column
							        title="进货"
							        dataIndex="goodsInQuantity"
							        key="goodsInQuantity"
							      	className="f-align-center"
							      />
							      <Column
							        title="调拨入"
							        dataIndex="commitOutQuantity"
							        key="commitOutQuantity"
							      	className="f-align-center"
							      />
							      <Column
							        title="内部员工退领"
							        dataIndex="interStaffReturnQuantity"
							        key="interStaffReturnQuantity"
							      	className="f-align-center"
							      />
							      <Column
							        title="销售退回"
							        dataIndex="saleReturnQuantity"
							        key="saleReturnQuantity"
							      	className="f-align-center"
							      />
							    </ColumnGroup>
							    <ColumnGroup title="出库">
							    	<Column
								        title="退货"
								        dataIndex="goodsOutQuantity"
								        key="goodsOutQuantity"
								      	className="f-align-center"
								    />
							    	<Column
							    	  title="调拨出"
							    	  dataIndex="commitInQuantity"
							    	  key="commitInQuantity"
							      	className="f-align-center"
							    	/>
							    	<Column
							    	  title="内部员工领用"
							    	  dataIndex="interStaffReceiveQuantity"
							    	  key="interStaffReceiveQuantity"
							      	className="f-align-center"
							    	/>
							    	<Column
							    	  title="销售"
							    	  dataIndex="saleReceiveQuantity"
							    	  key="saleReceiveQuantity"
							      	className="f-align-center"
							    	/>
							    </ColumnGroup>
							    <Column
							      title="报损"
							      dataIndex="breakageQuantity"
							      key="breakageQuantity"
							      className="f-align-center"
							    />
							    <Column
							      title="调整"
							      dataIndex="stockSettingQuantity"
							      key="stockSettingQuantity"
							      className="f-align-center"
							    />
							    <Column
							      title="期末存结"
							      dataIndex="afterModifyQuantity"
							      key="afterModifyQuantity"
							      className="f-align-center"
							    />
							  </Table>
							  : <Loading />
						}
					</div>
				</div>)
	}
}


class SearchForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			indeterminate: false,
			checkAll: false,
			startValue: null,
			endValue: null,
			endOpen: false,
		};
	}
	disabledStartDate = (startValue) => {
		const endValue = this.state.endValue;
		if (!startValue || !endValue) {
			return false;
		}
		return startValue.valueOf() > endValue.valueOf();
	}

	disabledEndDate = (endValue) => {
		const startValue = this.state.startValue;
		if (!endValue || !startValue) {
			return false;
		}
		return endValue.valueOf() <= startValue.valueOf();
	}

	onChange = (field, value) => {
		this.setState({
			[field]: value,
		});
	}

	onStartChange = (value) => {
		this.onChange('startValue', value);
	}

	onEndChange = (value) => {
		this.onChange('endValue', value);
	}

	handleStartOpenChange = (open) => {
		if (!open) {
			this.setState({
				endOpen: true
			});
		}
	}
	updateWarehouseId(v) {
		this.props.updateWarehouseId(v);
	}
	handleEndOpenChange = (open) => {
		this.setState({
			endOpen: open
		});
	}
	searchCondition() {
		let obj = this.props.form.getFieldsValue(),
			pageSize = this.props.pageSize;
		obj.currentPage = 1;
		obj.pageSize = pageSize;
		obj.startTime = obj.startTime ? obj.startTime.format(dateFormat) : '';
		obj.endTime = obj.endTime ? obj.endTime.format(dateFormat) : '';
		obj.warehouseId = obj.warehouseId ? obj.warehouseId : '';
		obj.category = obj.category ? obj.category : '';
		obj.isDisable = obj.isDisable ? obj.isDisable : '';

		obj = undefinedToEmpty(obj);

		this.props.updateChangeList(obj);
		this.props.setPara(obj);
	}
	resetForm() {
		let warehouseId = '',
			pageSize = this.props.pageSize;
		this.props.form.resetFields();
		warehouseId = this.props.form.getFieldValue('warehouseId');
		this.props.updateChangeList({
			currentPage: 1,
			pageSize: pageSize,
			warehouseId: warehouseId
		});
		this.props.setPara({
			currentPage: 1,
			pageSize: pageSize,
			warehouseId: warehouseId
		});
	}
	render() {
		const {
			getFieldDecorator
		} = this.props.form;
		const {
			startValue,
			endValue,
			endOpen
		} = this.state;
		return (<Form layout={'inline'}>
					<Row>
						<Col span={8}>
							<FormItem style={{ margin:'0 0 20px', width: '100%' }} label='仓库' labelCol={{span: 5}} wrapperCol={{span:19, style:{display:'inline-block', verticalAlign:'middle'}}}>
								{getFieldDecorator('warehouseId', {
						            rules: [{
						              required: false,
						            }],
						            initialValue: this.props.stockList.length? this.props.stockList[0].warehouseId : undefined
						          })(
						            <Select
						            	style={{width: '100%'}}
						                showSearch
						                placeholder="请选择"
						                onChange={ this.updateWarehouseId.bind(this) }
						                optionFilterProp="children"
						                filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
						              >
									    {
									    	this.props.stockList.length? this.props.stockList.map((e, i)=>{
												return (<Option key={i} value={ e.warehouseId }>{ e.warehouseName }</Option>)
									    	}) : ''
									    }
						              </Select>
						          )}
							</FormItem>
						</Col>
						<Col span={8}>
							<FormItem style={{ margin:'0 0 20px', width: '100%' }} labelCol={{span: 5}} label='类别' wrapperCol={{span: 19, style:{display:'inline-block', verticalAlign:'middle'}}}>
								{getFieldDecorator('category', {
						            rules: [{
						              required: false,
						            }],
						            initialValue: undefined
						          })(
						            <Select
						                showSearch
						                placeholder="请选择"
						                optionFilterProp="children"
						                filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
						              >
									    {
									    	this.props.categoryList.length? this.props.categoryList.map((e, i)=>{ 
												return (<Option key={i} value={ e }>{ e }</Option>)
									    	 }) : ''
									    }
						              </Select>
						          )}
							</FormItem>
						</Col>
						<Col span={8}>
							<FormItem style={{ margin:'0 0 20px', width: '100%' }} label='物品名称' labelCol={{span: 5}} wrapperCol={{span: 19}}>
								{getFieldDecorator('materialName', {
						            rules: [{
						              required: false,
						              message: '请输入物品名称',
						            }],
						            initialValue: ''
						          })(
						            <Input placeholder="请输入物品名称" />
						          )}
							</FormItem>
						</Col>
					</Row>
					<Row>
						<Col span={8}>
							<FormItem style={{ margin:'0 0 20px', width: '100%' }} label='状态' labelCol={{span: 5}} wrapperCol={{span: 19, style:{ display:'inline-block', verticalAlign:'middle'}}}>
							{getFieldDecorator('isDisable', {
					            rules: [{
					              required: false,
					            }],
					            initialValue: undefined
					          })(
					            <Select
					                showSearch
					                placeholder="请选择"
					                optionFilterProp="children"
					                filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
					              >
					                <Option value={'0'}>开启</Option>
					                <Option value={'1'}>停用</Option>
					              </Select>
					          )}
						</FormItem>
						</Col>
						<Col span={8}>
							<Row>
								<Col span={5}>
									<span className='f-right f-custom-label'>选择时段</span>
								</Col>
								<Col span={19}>
									<Row>
										<Col span={11}>
											<FormItem style={{width:'100%'}} wrapperCol={{style:{width:'100%'}}}>
												{
													getFieldDecorator('startTime', { 
														
													})(<DatePicker
												          disabledDate={this.disabledStartDate}
												          format="YYYY-MM-DD"
												          setFieldsValue={startValue}
												          placeholder="开始时间"
												          onChange={this.onStartChange}
												          onOpenChange={this.handleStartOpenChange}
												        />)
												}
											</FormItem>
										</Col>
										<Col className='f-align-center' span={2}>
											<span style={{lineHeight:'36px'}}>--</span>
										</Col>
										<Col span={11}>
											<FormItem style={{width:'100%'}} wrapperCol={{style:{width:'100%'}}}>
												{
													getFieldDecorator('endTime', { 
														
													})(<DatePicker
												          disabledDate={this.disabledEndDate}
												          format="YYYY-MM-DD"
												          setFieldsValue={endValue}
												          placeholder="结束时间"
												          onChange={this.onEndChange}
												          open={endOpen}
												          onOpenChange={this.handleEndOpenChange}
												        />)
												}
											</FormItem>
										</Col>
									</Row>
								</Col>
							</Row>
						</Col>
					</Row>
							
					<FormItem className='f-right'>
						<Button className='f-mr5' type="primary" style={{borderRadius: '4px',width:'110px',height:'40px'}} onClick={ this.searchCondition.bind(this) }>
	        				<i className='iconfont icon-hricon33 f-mr2'></i>
	        				搜索
	        			</Button>
	        			<Button style={{border: '1px solid #999999',borderRadius: '4px',width:'110px',height:'40px'}} onClick={this.resetForm.bind(this)}>
	        				<i className='iconfont icon-reset f-mr2'></i>
	        				重置
	        			</Button>
					</FormItem>
				</Form>)
	}
}

const SearchFormCreate = Form.create()(SearchForm);