import React, {
	Component
} from 'react';
import {
	Form,
	Breadcrumb,
	Table,
	Modal,
	Input,
	Select,
	Radio,
	Button,
	message,
	Popconfirm
} from 'antd';
import {
	fpost,
	fget
} from '../common/io.js';
import {
	Loading,
	Pagination,
	undefinedToEmpty
} from '../common/g.js';

const FormItem = Form.Item;
const {
	TextArea
} = Input;
const Option = Select.Option;
const RadioGroup = Radio.Group;

export default class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			visibleType: false,
			curId: '',
			isAdd: true,
			data: null,
			total: 1,
			editData: {},
			schoolList: [],
			isRequest: false,
			searchPara: {
				currentPage: 1,
				pageSize: 10
			}
		}
		this.removeStock.bind(this);
		this.setStock.bind(this);
	}
	setStock(id) {
		if (id) {
			fpost('/api/warehouse/findWarehouseVOById', {
				warehouseId: id
			}).then((res) => res.json()).then((res) => {
				if (res.success == true) {
					this.setState({
						isAdd: false,
						editData: res.result,
						visibleType: true,
						curId: id
					})
				}
			});
		} else {
			this.setState({
				visibleType: true
			})
		}
	}
	saveData() {

		let obj = this.formRef.props.form.getFieldsValue(),
			self = this,
			isError = false;
		if (!this.state.isAdd) obj.warehouseId = this.state.curId;

		this.formRef.props.form.validateFields(
			(err, fieldsValue) => {
				if (err) {
					message.error('请填写完整!');
					isError = true;
				}
			}
		)

		if (isError) return;

		this.setState({
			isRequest: true
		})

		obj = undefinedToEmpty(obj);

		//提交保存
		fpost('/api/warehouse/saveWarehouse', obj).then(res => res.json()).then(res => {
			if (res.success == true) {
				self.setState({
					visibleType: false,
					editData: {},
					searchPara: {
						currentPage: 1,
						pageSize: 10
					}
				});
				message.success(res.message);
				self.formRef.props.form.resetFields();
				self.getDataList(this.state.searchPara);
			} else {
				message.error(res.message);
			}
			self.setState({
				isRequest: false
			})
		})
	}
	removeStock(id) {
		fpost('/api/warehouse/deleteWarehouse', {
			warehouseId: id
		}).then((res) => res.json()).then((res) => {
			if (res.success == true) {
				message.success(res.message);
				this.getDataList(this.state.searchPara);
			} else {
				message.error(res.message);
			}
		})
	}
	cancelSetStock() {
		this.formRef.props.form.resetFields();
		this.setState({
			visibleType: false,
			editData: []
		})
	}
	searchPage(page, pageSize) {
		this.getDataList(page);
	}
	getDataList(para) {
		//仓库列表
		fpost('/api/warehouse/pageWarehouseVO', para).then((res) => res.json()).then((res) => {

			if (res.success == true) {
				let data = [];
				res.result.records.forEach((e, i) => {
					e.key = i;
					data.push(e)
				})
				this.setState({
					data: data,
					total: res.result.total
				})
			}
		});

		//校区列表
		fget('/api/warehouse/listSchoolAreaNoWarehouse').then((res) => res.json()).then((res) => {
			if (res.success == true) {
				this.setState({
					schoolList: res.result
				})
			}
		});
	}
	componentDidMount() {
		this.getDataList(this.state.searchPara);
	}
	render() {
		const columns = [{
			title: '仓库名称',
			dataIndex: 'warehouseName',
			key: 'warehouseName',
			className: 'f-align-center',
		}, {
			title: '关联校区',
			dataIndex: 'schoolAreaName',
			key: 'schoolAreaName',
			className: 'f-align-center',
		}, {
			title: '描述',
			dataIndex: 'warehouseDesc',
			key: 'warehouseDesc',
			className: 'f-align-center',
		}, {
			title: '操作',
			key: 'action',
			className: 'f-align-center',
			render: (text, record) => (
				<span>
					<Button type="primary" icon="edit" className="f-mr3 f-radius4" onClick={ () => this.setStock(record.warehouseId) }>编辑</Button><Popconfirm 
				            title="确定要删除吗?" 
				            onConfirm={
				              ()=> this.removeStock(record.warehouseId)
				            }
				            okText="确定" 
				            cancelText="取消"
				          ><Button icon="delete" className='f-radius4'>删除</Button>
				        </Popconfirm>
			    </span>
			),
		}];

		return (<div className='courseArranging classListPage wid100'>
					<Breadcrumb className="f-mb3">
					    <Breadcrumb.Item>仓库管理</Breadcrumb.Item>
					    <Breadcrumb.Item>仓库管理</Breadcrumb.Item>
					    <Breadcrumb.Item><span className='f-fz5 f-bold'>仓库设置</span></Breadcrumb.Item>
					</Breadcrumb>
					<div className='f-bg-white f-pd4 f-box-shadow1 f-mt5 f-radius1'>
						<div className='f-pb3 f-flex' style={{justifyContent: 'space-between'}}>
							<h3 className="f-title-blue">仓库设置</h3>
							<a className="f-btn-green" onClick={ () => this.setStock() }>
								<i className='iconfont icon-tianjiajiahaowubiankuang'></i>新增仓库
							</a>
							<Modal title={ this.state.isAdd? '添加仓库' : '编辑仓库' }
								wrapClassName="vertical-center-modal"
								visible={ this.state.visibleType }
								onOk={ this.saveData.bind(this) }
								onCancel={ this.cancelSetStock.bind(this) }
								confirmLoading={ this.state.isRequest }
							>
								<AddFormCreate data={ this.state.editData } wrappedComponentRef={ (inst) => this.formRef = inst } schoolList={ this.state.schoolList }/>
							</Modal>
						</div>
						{
							this.state.data != null? 
							<Table columns={columns} dataSource={this.state.data} bordered pagination={ Pagination(this.state.total, this.state.searchPara, this.getDataList.bind(this)) } className='f-align-center' scroll={{ x: 1300 }}/>
							: <Loading />
						}
						
					</div>
				</div>)
	}
}

const formItemLayout = {
	labelCol: {
		span: 4
	},
	wrapperCol: {
		span: 20
	},
};

/*新增仓库*/
class AddForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			value: '1'
		}

	}
	setRadioValue(e) {
		this.setState({
			value: e.target.value
		})
	}
	render() {
		const {
			getFieldDecorator
		} = this.props.form;
		const {
			warehouseName,
			warehouseDesc,
			schoolAreaName,
			schoolAreaId,
			onlyWhouse
		} = this.props.data;

		return (<Form>
					<FormItem {...formItemLayout} label='仓库名称'>
						{getFieldDecorator('warehouseName', {
				            rules: [{
				              required: true,
				              message: '仓库名称不能为空!',
				              whitespace: true
				            },
				            {
				            	max: 50,
				            	min: 1,
				            	transform: (val) =>{
				            		return val!=undefined? val.replace(/\s+/g,'').replace(/[^\x00-\xff]/g,'01') : '';
				            	},
				            	message: '长度不能超过50（25个汉字）'
				            }
				            ],
				            initialValue: warehouseName || ''
				          })(
				            <Input placeholder='请输入仓库名称'/>
				          )}
					</FormItem>
					<FormItem {...formItemLayout} label='描述'>
						{getFieldDecorator('warehouseDesc', {
				            rules: [{
				              required: false,
				              message: '请输入描述',
				            },
				            {
				            	max: 200,
				            	min: 1,
				            	transform: (val) =>{
				            		return val!=undefined? val.replace(/\s+/g,'').replace(/[^\x00-\xff]/g,'01') : '';
				            	},
				            	message: '长度不能超过200（100个汉字）'
				            }],
				            initialValue: warehouseDesc || ''
				          })(
				            <TextArea rows={4} placeholder="请输入描述" />
				          )}
					</FormItem>
					<FormItem {...formItemLayout} label='独立仓库'>
						{getFieldDecorator('isIndependent', {
				            rules: [{
				              required: false,
				            }],
				            initialValue: onlyWhouse? onlyWhouse : this.state.value
				          })(
				            <RadioGroup onChange={this.setRadioValue.bind(this)}>
						        <Radio value='1'>是</Radio>
						        <Radio value='0'>否</Radio>
					      	</RadioGroup>
				          )}
					</FormItem>
					{
						this.state.value=='0'? <FormItem {...formItemLayout} label='关联校区'>
						{getFieldDecorator('schoolAreaId', {
				            rules: [{
				              required: this.state.value==0? true : false,
				              message: '请选择关联校区',
				            }],
				            initialValue: schoolAreaId
				          })(
				            <Select
							    className='f-vertical-middle'
							    style={{ width: '100%' }}
							    placeholder="请选择"
							    optionFilterProp="children"
							    disabled={ this.props.schoolList.length==0 && !schoolAreaId ? true: false }
							  >
							    { schoolAreaId? <Option value={ schoolAreaId }>{ schoolAreaName }</Option> : '' }
							    {
							    	this.props.schoolList.length? this.props.schoolList.map((e, i) => {
							    		return (<Option key={ i } value={ e.schoolAreaId }>{ e.schoolAreaName }</Option>)
							    	}) : ''
							    }
						  </Select>
				        )}
					</FormItem> : ''
					}
				</Form>)
	}
}

const AddFormCreate = Form.create()(AddForm);