import React, {
	Component
} from 'react';
import {
	Breadcrumb,
	Button,
	Select,
	Table,
	Tabs,
	Input,
	DatePicker,
	Row,
	Col,
	Form,
	Pagination,
	message,
	Dropdown,
	Menu,
	Icon
} from 'antd';
import moment from 'moment';
import store from 'store2';
import $ from 'jquery';
import {
	Loading,
	getUrlParam,
	numberFormate
} from '../../common/g.js';
//使用fetch请求接口数据
import {
	fpost
} from '../../common/io.js'; //同步、异步请求
const dateFormat = 'YYYY-MM-DD HH:mm:ss';
const u = require('../../common/io.js');
const TabPane = Tabs.TabPane;
const Option = Select.Option;
const FormItem = Form.Item;
let classname = {};
let classname1 = {}

//教务教学默认打开的首页
class Main extends Component {
	constructor(props) {
		super(props)
		this.state = {
			startValue: null,
			endValue: null,
			endOpen: false,
			className: null, //班级名称
			pageResult: null,
			currentPage: 1, //当前页码
			pageSize: 10, //每页条数
			total: null, //总条数
			searchState: 0,
			course: [], //课程名称
			classroomList: [], //教室名称
			masterInfo: [], //课程顾问等下拉信息
		}
	}
	disabledStartDate = (startValue) => { //只读的开始日期
		const endValue = this.state.endValue;
		if (!startValue || !endValue) {
			return false;
		}
		return startValue.valueOf() > endValue.valueOf();
	}

	disabledEndDate = (endValue) => { //只读的结束日期
		const startValue = this.state.startValue;
		if (!endValue || !startValue) {
			return false;
		}
		return endValue.valueOf() <= startValue.valueOf();
	}

	onChange = (field, value) => {
		this.setState({
			[field]: value,
		});
	}
	//开始日期改变
	onStartChange = (value) => {
		this.onChange('startValue', value);
	}
	//结束日期改变
	onEndChange = (value) => {
		this.onChange('endValue', value);
	}

	handleStartOpenChange = (open) => {
		if (!open) {
			this.setState({
				endOpen: true
			});
		}
	}

	handleEndOpenChange = (open) => {
		this.setState({
			endOpen: open
		});
	}
	reset() { //高级筛选里的重置
		this.props.form.resetFields();
		this.setState({
			currentPage: 1
		}, function() {
			this._getForm();
		});
	}
	onShowSizeChange(current, pageSize) {
		this.setState({
			currentPage: current,
			pageSize: pageSize
		}, function() {
			if (this.state.searchState == 0) {
				this.jqSearch();
			} else {
				this.advancedSearch()
			}
		})
	}
	_getMaster() { //课程顾问下拉
		const self = this;
		fpost('/api/hr/staff/listStaffByCondForDropDown', {
				type: 3
			})
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					let masterInfo = []; //助教、老师等下拉搜索信息
					for (let i = 0; i < res.result.length; i++) {
						let list = {};
						list.id = res.result[i].id;
						list.name = res.result[i].name + '-' + res.result[i].department + '-' + res.result[i].mobile;
						masterInfo.push(list)
					}
					self.setState({
						masterInfo: masterInfo
					})
				} else {
					message.error(res.message)
				}
			});
	}
	componentDidMount() {
		this.jqSearch();
		this._getCourse(); //获取课程
		// this._getClassroom(); //获取教室
		this._getMaster(); //上课老师
	}
	_getClassroom() { //获取教室
		const self = this;
		fpost('/api/system/schoolarea/listClassRoom ')
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					self.setState({
						classroomList: res.result
					})
				} else {
					message.error(res.message)
				}
			});
	}
	_getCourse() { //获取课程
		const self = this;
		fpost('/api/system/course/listCourse')
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					self.setState({
						course: res.result
					})
				} else {
					message.error(res.message)
				}
			});
	}
	className(e) { //班级名称
		let v = e.target.value;
		this.setState({
			className: v
		});
	}
	_getForm() {
		this.props.form.validateFields(
			(err, fieldsValue) => {
				classname1.beginTime = fieldsValue['beginTime'] ? fieldsValue['beginTime'].format('YYYY-MM-DD HH:mm:ss') : '';
				classname1.endTime = fieldsValue['endTime'] ? fieldsValue['endTime'].format('YYYY-MM-DD HH:mm:ss') : "";
				classname1.pageSize = this.state.pageSize;
				classname1.currentPage = this.state.currentPage;
				classname1.courseId = fieldsValue['courseId'] ? fieldsValue['courseId'] : '';
				classname1.className = fieldsValue['className'] ? fieldsValue['className'] : '';
				classname1.isArrear = fieldsValue['isArrear'] ? fieldsValue['isArrear'] : '';
				classname1.salesFrom = fieldsValue['salesFrom'] ? fieldsValue['salesFrom'] : '';
				classname1.salesId = fieldsValue['salesId'] ? fieldsValue['salesId'] : '';
				classname1.isOriginal = fieldsValue['isOriginal'] ? fieldsValue['isOriginal'] : '';
				classname1.state = fieldsValue['state'] ? fieldsValue['state'] : '';
				this.getList(classname1);
			}
		)
	}
	pageChange(pageNumber) { //列表分页获取当前页码
		this.setState({
			currentPage: pageNumber
		}, function() {
			if (!this.state.searchState) {
				classname['studentName'] = $.trim($('.inputBG').val());
				classname['phone'] = $.trim($('.phone').val());
				classname.pageSize = this.state.pageSize;
				classname.currentPage = this.state.currentPage;
				this.getList(classname);
			} else {
				this._getForm(); //表单内容
			}
		});
	}
	jqSearch() { //精确搜索
		let reg = /^1(3|4|5|7|8)\d{9}$/;
		if ($.trim($('.phone').val()) && !reg.test($.trim($('.phone').val()))) {
			message.error('手机号格式不正确，请重新输入');
			return false;
		}
		this.setState({
			currentPage: 1,
		}, function() {
			classname['studentName'] = $.trim($('.inputBG').val());
			classname['phone'] = $.trim($('.phone').val());
			classname.pageSize = this.state.pageSize;
			classname.currentPage = this.state.currentPage;
			this.getList(classname);
		});
	}
	advancedSearch() { //筛选搜索
		this.setState({
			currentPage: 1
		}, function() {
			this._getForm();
		});
	}
	callback(key) {
		if (key == 2) {
			this.setState({
				searchState: 1
			})
		} else {
			this.setState({
				searchState: 0
			})
		}
	}
	_getPhone(v) { //手机号校验
		if (isNaN(v.target.value) || v.target.value <= 0 || v.target.value.indexOf('.') != -1) {
			v.target.value = v.target.value.substring(0, v.target.value.length - 1)
		}
	}
	getList(fieldsValue) { //获取全部校区信息
		let self = this;
		fpost('/api/reception/student/course/pageRegistrationCourseByCnd', fieldsValue)
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					if (res.result) {
						res.result.records.forEach((item, i) => {
							item.key = i;
							item.receivableAmount = numberFormate(item.receivableAmount)
						});
						self.setState({
							pageResult: res.result.records,
							total: parseInt(res.result.total),
						})
					}
				} else {
					message.error(res.message); //错误提示信息
				}
			});
	}
	render() {
		const columns = [{
			title: '学员姓名（学号）',
			key: '0',
			fixed: 'left',
			width: 180,
			className: 'f-align-center',
			render: (text, record, index) => {
				return (
					<div>
			   			{record.studentName}({record.stuNo})
			   		</div>
				)
			}
		}, {
			title: '头像',
			key: '1',
			width: 180,
			className: 'f-align-center',
			render: (text, record, index) => {
				return (
					<div>
					{record.headImgUrl?
						<img src={record.headImgUrl} style={{width:'50px',height:'50px',borderRadius:'100%'}}/>:''
					}
					</div>
				)
			}
		}, {
			title: '性别',
			className: 'f-align-center',
			dataIndex: 'genderStr',
			key: '2',
			width: 150,
		}, {
			title: '年龄',
			className: 'f-align-center',
			dataIndex: 'age',
			key: '3',
			width: 150,
		}, {
			title: '生日',
			className: 'f-align-center',
			dataIndex: 'birthday',
			key: '4',
			width: 150,
		}, {
			title: '手机号码',
			className: 'f-align-center',
			dataIndex: 'phone',
			key: '5',
			width: 150,
		}, {
			title: '课程名称',
			className: 'f-align-center',
			dataIndex: 'courseName',
			key: '6',
			width: 150,
		}, {
			title: '班级名称',
			className: 'f-align-center',
			key: '7',
			width: 150,
			dataIndex: 'className'
		}, {
			title: '上课老师',
			className: 'f-align-center',
			key: '8',
			width: 150,
			dataIndex: 'teacherName'
		}, {
			title: '学费金额',
			className: 'f-align-center',
			key: '9',
			width: 150,
			dataIndex: 'receivableAmount'
		}, {
			title: '学费状态',
			className: 'f-align-center',
			key: '10',
			width: 150,
			dataIndex: 'isArrearStr'
		}, {
			title: '优惠状态',
			className: 'f-align-center',
			key: '11',
			width: 150,
			dataIndex: 'isOriginalStr'
		}, {
			title: '报班状态',
			className: 'f-align-center',
			key: '12',
			width: 150,
			dataIndex: 'stateStr'
		}, {
			title: '是否续报',
			className: 'f-align-center',
			key: '13',
			width: 150,
			dataIndex: 'isContinueStr'
		}, {
			title: '是否新生',
			className: 'f-align-center',
			key: '14',
			width: 150,
			dataIndex: 'isFreshManStr'
		}, {
			title: '班级状态',
			className: 'f-align-center',
			key: '15',
			width: 150,
			dataIndex: 'classStateStr'
		}, {
			title: '报课来源',
			className: 'f-align-center',
			key: '16',
			width: 150,
			dataIndex: 'salesFromStr'
		}, {
			title: '课程顾问',
			className: 'f-align-center',
			key: '17',
			width: 150,
			dataIndex: 'salesName'
		}, {
			title: '所属校区',
			className: 'f-align-center',
			key: '18',
			width: 150,
			dataIndex: 'schoolAreaName'
		}, {
			title: '操作',
			className: 'f-align-center',
			key: '19',
			width: 200,
			fixed: 'right',
			render: (text, record, index) => {
				return (<div>
		            		<Dropdown overlay={
	            			 <Menu>
						    <Menu.Item>
						      <span onClick={()=>{window.location.href='/front/students/detail?id='+record.id+'&name='+record.studentName+'&stuNo='+record.stuNo}}>课程详情</span>
						    </Menu.Item>
						    <Menu.Item>
						      <span onClick={()=>{window.location.href='/front/students/after?id='+record.id+'&name='+record.studentName+'&stuNo='+record.stuNo+'&courseName='+record.courseName+'&className='+record.className}}>售后沟通</span>
						    </Menu.Item>
						    <Menu.Item>
						      <span onClick={()=>{window.location.href='/front/students/singRecord?id='+record.id+'&registrationCourse=1'}}>上课记录</span>
						    </Menu.Item>
						  </Menu>
	            			} placement="bottomRight">
	            			 <Button type="f-classlist-check" className="f-radius4 f-mr1">
				          查看<Icon type="down" className='f-classlist-down'/>
				        </Button>
					 </Dropdown>
					 <Dropdown overlay={
	            			 <Menu>
	            			 {record.isArrear && record.isArrear == '1'?
						    <Menu.Item>
						      <span onClick={()=>{window.location.href='/front/fillmoney/form?registrationCourseId='+record.id+'&studentId='+record.studentId}}>补欠费</span>
						    </Menu.Item>
						    :null
	            			 }
	            			 {record.state=='1'?
						    <Menu.Item>
						      <span onClick={()=>{window.location.href='/front/signup/step1?isChangeClass=1&registrationCourseId='+record.id+'&id='+record.studentId+'&type=1'}}>转班</span>
						    </Menu.Item>
						    :null
						  }
						    {record.state=='1'?
							    <Menu.Item>
							      <span onClick={()=>{window.location.href='/front/stopclasses/form?registrationCourseId='+record.id+'&studentId='+record.studentId}}>停课</span>
							    </Menu.Item>
							    :null
						    }
						    <Menu.Item>
						      <span onClick={()=>{window.location.href='/front/signup/step1?type=1&id='+record.id}}>续报</span>
						    </Menu.Item>
						    {record.state=='1'?
							    <Menu.Item>
							      <span onClick={()=>{window.location.href='/front/moneyback/form?registrationCourseId='+record.id+'&studentId='+record.studentId}}>退课</span>
							    </Menu.Item>
							    :null
							 }
						  </Menu>
	            			} placement="bottomRight">
	            			 <Button type="primary f-classlist-check" className="f-radius4 f-mr1">
				          操作<Icon type="down" className='f-classlist-down'/>
				        </Button>
					 </Dropdown>
		            </div>);
			}
		}];
		const {
			getFieldDecorator
		} = this.props.form;
		const formItemLayout = {
			labelCol: {
				span: 6
			},
			wrapperCol: {
				span: 18
			}
		};
		return (
			<Form className='classListPage wid100'>
				<Breadcrumb className="f-mb3">
				    <Breadcrumb.Item>前台业务</Breadcrumb.Item>
				    <Breadcrumb.Item>
				   		<span className="f-fz5 f-bold">报课列表</span>
				    </Breadcrumb.Item>
				</Breadcrumb>
				<div className='Topselect f-bg-white f-radius1 f-box-shadow1' style={{width:'100%'}}>
					<Tabs defaultActiveKey="1" style={{textAlign:'center'}} tabPosition='top' animated={false} onChange={this.callback.bind(this)}>
						<TabPane tab={<span><i className='iconfont icon-hricon33 f-mr2'></i>精确查找</span>} key="1" style={{textAlign:'left'}}>
				    			<div style={{padding:'0 0 30px 26px'}}>
				    				<Row>
				    					<Col span={8} className='f-pr3 f-align-left'>
				    						<Col span={4} className='f-align-right'>
				    							<i className='f-fz4 f-black f-h4-lh4'>姓名:&emsp;</i>
				    						</Col>
				    						<Col span={20}>
				    							<Input placeholder="请输入姓名" onChange={this.className.bind(this)} className='inputBG pageInpHeight f-radius1' style={{border:0}}/>
				    						</Col>
				    					</Col>
				    					<Col span={8} className='f-pr3 f-align-left'>
				    						<Col span={6} className='f-align-right'>
				    							<i className='f-fz4 f-black f-h4-lh4'>手机号:&emsp;</i>
				    						</Col>
				    						<Col span={18}>
				    							<Input placeholder="请输入手机号" onChange={this._getPhone.bind(this)} className='phone pageInpHeight f-radius1' style={{border:0}}/>
				    						</Col>
				    					</Col>
				    					<Col span={8} className='f-align-left f-pl4'>
				    						<Button type="primary" icon="search" style={{height:'40px'}} onClick={this.jqSearch.bind(this)}>搜索</Button>
				    					</Col>
				    				</Row>
				    			</div>
				    		</TabPane>
					    <TabPane tab={<span><i className='iconfont icon-shaixuan f-mr2' ></i>高级筛选</span>} key="2">
					    		<Row gutter={40}>
					          <Col span={11}>
					        		<Col span={6} className='f-fz4 f-black f-h4-lh4 f-align-right'>时间段:&emsp;</Col>
					        		<Col span={18} >
						    			 <Col span={11}>
						    			 <FormItem>
						                  {getFieldDecorator('beginTime', {
						                  })(
							    			 	<DatePicker
									          disabledDate={this.disabledStartDate.bind(this)}
									          format="YYYY-MM-DD HH:mm:ss"
									          showTime
									          setFieldsValue={this.state.startValue}
									          placeholder="开始时间"
									          onChange={this.onStartChange.bind(this)}
									          onOpenChange={this.handleStartOpenChange.bind(this)}/>
						    			 	)}
				                			</FormItem>
						    			 </Col>
						    			 <Col span={2} className='f-align-center f-h4-lh4'>--</Col>
						    			  <Col span={11}>
						    			  <FormItem>
						                  {getFieldDecorator('endTime', {
						                  })(
								        <DatePicker
								          disabledDate={this.disabledEndDate.bind(this)}
								          format="YYYY-MM-DD HH:mm:ss"
								          showTime
								          setFieldsValue={this.state.endValue}
								          placeholder="结束时间"
								          onChange={this.onEndChange.bind(this)}
								          open={this.state.endOpen}
								          onOpenChange={this.handleEndOpenChange.bind(this)}/>
								        )}
				                			</FormItem>
							         	</Col>
							        	</Col>
					        		</Col>
					          <Col span={6} >
					          	 <Col span={8} className='f-fz4 f-black f-h4-lh4'>班级名称:&emsp;</Col>
					          	  <Col span={16} >
					          	  	<FormItem {...formItemLayout}>
					                  {getFieldDecorator('className', {
					                  })(
							           <Input placeholder='请输入班级名称' className='pageInpHeight'/> 
							          )}
					        	     	</FormItem>
						           </Col>
					          </Col>
					          <Col span={6} >
					             <Col span={8} className='f-fz4 f-black f-h4-lh4'>课程名称:&emsp;</Col>
					              <Col span={15} >
					              <FormItem {...formItemLayout}>
					                  {getFieldDecorator('courseId', {
					                  })(
							            <Select showSearch allowClear placeholder="请选择" optionFilterProp="children"
					                        filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}>
						                      {this.state.course?
						                        	this.state.course.map((item,i)=> {
						                        		return(<Option key={item.id} value={item.id}>{ item.name }</Option>)
						                        	})
						                        	:null
						                      }
					                      </Select>
							          )}
					        	     	</FormItem>
						            </Col>
					          </Col>
					        </Row>
					        <Row className='' gutter={40}>
					        		<Col span={11}>
					        		<Col span={6} className='f-fz4 f-black f-h4-lh4 f-align-right'>报课来源:&emsp;</Col>
						        		<Col span={18} >
						              <FormItem {...formItemLayout}>
						                  {getFieldDecorator('salesFrom', {
						                  })(
								            <Select showSearch allowClear placeholder="请选择" optionFilterProp="children"
						                        filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}>
													<Option key={1} value='1'>电话邀约</Option>
													<Option key={2} value='2'>地推活动</Option>
													<Option key={3} value='3'>网络数据</Option>
													<Option key={4} value='4'>渠道推广</Option>
													<Option key={5} value='5'>网络推广</Option>
													<Option key={6} value='6'>主动上门</Option>
													<Option key={7} value='7'>陌生来电</Option>
													<Option key={8} value='8'>老带新</Option>
													<Option key={9} value='9'>内部转化</Option>
													<Option key={10} value='10'>试听</Option>
													<Option key={11} value='11'>入学测试</Option>
													<Option key={12} value='12'>公开课</Option>
							                      
						                      </Select>
								          )}
						        	     	</FormItem>
							            </Col>
					        		</Col>
					        		<Col span={6} >
					             <Col span={8} className='f-fz4 f-black f-h4-lh4'>课程顾问:&emsp;</Col>
					              <Col span={16} >
					              	<FormItem {...formItemLayout}>
					                  {getFieldDecorator('salesId', {
					                  })(
							           <Select
						                  	  showSearch
						                  	  allowClear
									          placeholder='请选择'
									          style={{ width: '100%' }}
									          optionFilterProp="children"
	                        					filterOption={
	                        						(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
	                        					}
									        >
												{this.state.masterInfo?
								                        	this.state.masterInfo.map((item,i)=> {
								                        		return(<Option key={item.id} value={item.id}>{ item.name}</Option>)
								                        	})
								                        	:null
							                      }
									        </Select>
						            		)}
									</FormItem>
						            </Col>
					          	</Col>
					          	<Col span={6} >
					             <Col span={8} className='f-fz4 f-black f-h4-lh4'>报班状态:&emsp;</Col>
					              <Col span={15} >
					              	<FormItem {...formItemLayout}>
					                  {getFieldDecorator('state', {
					                  })(
							            <Select
					                  	  showSearch
					                  	  allowClear
								          placeholder='全部状态'
								          style={{ width: '100%' }}
								          optionFilterProp="children"
                        					filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
								        >
											<Option key={1} value='1'>正常报名</Option>
											<Option key={2} value='2'>转出</Option>
											<Option key={3} value='3'>未开始</Option>
											<Option key={4} value='4'>退课</Option>
											<Option key={5} value='5'>结课</Option>
											<Option key={6} value='6'>停课</Option>
								        </Select>
						            )}
									</FormItem>
						            </Col>
					          	</Col>
					        </Row>
					        <Row className='' gutter={40}>
					        		<Col span={11}>
					        		<Col span={6} className='f-fz4 f-black f-h4-lh4 f-align-right'>优惠状态:&emsp;</Col>
						        		<Col span={18} >
						              <FormItem {...formItemLayout}>
						                  {getFieldDecorator('isOriginal', {
						                  })(
								            <Select showSearch allowClear placeholder="全部状态" optionFilterProp="children"
						                        filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}>
							                      <Option key={0} value='0'>优惠</Option>
					                     		  <Option key={1} value='1'>原价</Option>
						                      </Select>
								          )}
						        	     	</FormItem>
							            </Col>
					        		</Col>
					        		<Col span={6} >
					             <Col span={8} className='f-fz4 f-black f-h4-lh4'>学费状态:&emsp;</Col>
					              <Col span={16} >
					              	<FormItem {...formItemLayout}>
					                  {getFieldDecorator('isArrear', {
					                  })(
							           <Select showSearch allowClear placeholder="全部状态" optionFilterProp="children"
				                        filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}>
					                     <Option key={0} value='0'>缴清</Option>
					                     <Option key={1} value='1'>欠费</Option>
				                      </Select> 
						            		)}
									</FormItem>
						            </Col>
					          	</Col>
					        </Row>
					        <Row className='f-pb3'>
					        		<Col span={12} className='f-align-right f-pr4'>
					        			<Button style={{border: '1px solid #999999',borderRadius: '4px',width:'110px',height:'40px'}} onClick={this.reset.bind(this)}>
					        				<i className='iconfont icon-reset f-mr2'></i>重置
					        			</Button>
					        		</Col>
					        		<Col span={12} className='f-align-left'>
					        			<Button type="primary" style={{borderRadius: '4px',width:'110px',height:'40px'}} onClick={this.advancedSearch.bind(this)}>
					        				<i className='iconfont icon-hricon33 f-mr2'></i>搜索
					        			</Button>
					        		</Col>
					        </Row>
					    </TabPane>
					 </Tabs>
				</div>
				<div className='f-bg-white f-pd4 f-box-shadow1 f-mt5 f-radius1 f-mb4'>
					<div className='f-flex' style={{justifyContent: 'space-between'}}>
						<h3 className="f-title-blue f-mb3">学员列表</h3>
					</div>
					<div>
						{this.state.pageResult?
							<div>
								<Table columns={columns} dataSource={this.state.pageResult} scroll={{ x: 3000}} bordered pagination={false}/>
									<div className='f-flex f-mt3' style={{justifyContent:'flex-end'}}>
										<div>
											<Pagination current={this.state.currentPage} total={this.state.total} onShowSizeChange={this.onShowSizeChange.bind(this)} showSizeChanger showQuickJumper onChange={this.pageChange.bind(this)}/>
										</div>
									</div>
							</div>:<Loading/>
						}
					</div>
				</div>
			</Form>
		)
	}
}

const RegistrationCourse = Form.create()(Main);
export {
	RegistrationCourse
}