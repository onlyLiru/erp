import React, {
  Component
} from 'react';
import {
  Table,
} from 'antd';

export default class Main extends Component {
  render() {

    let {
      data,
      loading,
      pagination,
      onChange
    } = this.props;

    return (
      <div className="f-box-shadow2 f-radius1 f-bg-white f-pd4 f-mt5">
        <h3 className="f-mb5">
          <span className="f-title-blue f-mt3">数据列表</span>
        </h3>
        <Table
            columns={ columns }
            bordered
            dataSource={ data }
            scroll={{ x: 1400}}
            pagination={ pagination }
            loading={loading}
            onChange={onChange}
          />
      </div>
    );
  }

}

const columns = [{
  title: '学员姓名（学号）',
  dataIndex: 'name',
  width: '150px',
  className: 'f-align-center',
  fixed: 'left',
  render: (value, row, index) => {
    let {
      name,
      stuNo
    } = row;
    let text = `${name }(${stuNo})`;
    return <div 
      className="f-line1 f-over-hide"
      style={{
        width:'160px'
      }}
      title={text}
    >
      { text }
    </div>
  }
}, {
  title: '头像',
  dataIndex: 'headImgUrl',
  className: 'f-align-center',
  width: '100px',
  render: (value, row, index) => {
    let {
      headImgUrl
    } = row;
    return (<div className="f-list-avatar">
        <img src= {value}/> 
      </div>);
  }
}, {
  title: '性别',
  dataIndex: 'genderStr',
  className: 'f-align-center',
  width: '100px'
}, {
  title: '年龄',
  dataIndex: 'age',
  className: 'f-align-center',
  width: '100px'
}, {
  title: '手机号码',
  className: 'f-align-center',
  dataIndex: 'phone',
  width: '150px'
}, {
  title: '市场来源',
  className: 'f-align-center',
  dataIndex: 'sourceStr',
  width: '150px'
}, {
  title: '市场顾问',
  className: 'f-align-center',
  dataIndex: 'salesName',
  width: '150px'
}, {
  title: '操作人',
  className: 'f-align-center',
  dataIndex: 'operatorName',
  width: '150px'
}, {
  title: '操作时间',
  className: 'f-align-center',
  dataIndex: 'modifyTime',
  width: '200px'
}, {
  title: '操作',
  dataIndex: 'des',
  fixed: 'right',
  width: '150px',
  className: "f-align-center",
  render: (value, row, index) => {
    let {
      name,
      phone
    } = row;
    return (<div>
      <a className="ant-btn f-radius3 f-lh4" href={`/front/studentsaleist?name=${name}&phone=${phone}`}>查看</a>
      <a className="ant-btn ant-btn-primary f-radius3 f-ml2 f-lh4" href={`/front/salegoods?id=${row.id}&name=${name}&phone=${row.phone}`}>售卖</a>
    </div>);
  }
}];