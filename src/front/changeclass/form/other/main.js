import React, {
	Component
} from 'react';
import {
	Col,
	Row,
	Form,
	DatePicker,
} from 'antd';
import moment from 'moment';

const FormItem = Form.Item;

class MainForm extends Component {
	constructor(props) {
		super(props);
		this.state = {

		}
	}
	render() {
		const {
			getFieldDecorator
		} = this.props.form;

		const formItemLayout = {
			labelCol: {
				span: 7
			},
			wrapperCol: {
				span: 17
			}
		};

		return (<Form>
			<div className="f-box-shadow2 f-radius1 f-over-hide f-bg-white f-mt5">
		        <div className="f-pd5 f-right-title-box">
			        <div className="f-bg-blue f-right-title">
	        			<h3>其他</h3>
	        		</div>
	        		<div>
	        			<Row gutter={ 40 }>
	        				<Col span={8}>
	        					<FormItem
									label="转班时间"
									{...formItemLayout}
								>
									{
										getFieldDecorator('registrationTime',{
											rules:[{
												required:true,
												message:'请输入转班时间'
											}]
										})(
											<DatePicker
												style={{width:'100%'}}
										      	format="YYYY-MM-DD HH:mm:ss"
										      	showTime={{ defaultValue: moment('00:00:00', 'HH:mm:ss') }}
										    />
										)
									}
								</FormItem>
	        				</Col>
		    				
		    	        </Row>
	        		</div>
		        </div>
			</div>
		</Form>);
	}
}

const MyForm = Form.create()(MainForm);
export default MyForm;