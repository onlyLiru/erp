import React, {
	Component
} from 'react';
import {
	Row,
	Col,
	InputNumber,
	Select,
} from 'antd';
import {
	changeTwoDecimal
} from '../../../../common/g.js';

const Option = Select.Option;

export default class OutClassInfo extends Component {
	constructor(props) {
		super(props);
		this.state = {
			arrearAmount: 0, // 欠费金额
			classId: '', // 班级id
			className: '', // 班级名称
			classState: '', // 班级状态1:已排课未开课、2:已开课、3:已结课
			courseName: '', // 课程名称
			coursePeriodConsumeVO: { // 课时消耗统计对象
				consumeOriginalAmount: '', // 原价 课时费消耗
				consumePeriod: '', //  消耗课时
				consumeReceivableAmount: '' // 应付 课时消耗
			},
			isArrear: 0, // 是否欠费
			receivableAmount: '', // 应付金额
			remainTuitionAmount: '', // 剩余学费
			studentId: '',
			registrationCourseId: null, // 报班id
			transferBalance: '', // 转出余额
			actualConsumeType: '1', // 实际课消类型  1 原价 2 应付
			actualConsumeAmount: 0, // 实际课消金额

			classStateName: ['-', '已排课未开课', '已开课', '已结课'], //用户展示班级状态
		}
	}
	render() {
		if (!this.state.classId) {
			return null;
		}

		let {
			classId,
			className,
			classState,
			classStateName,
			isArrear,
			arrearAmount,
			courseName,
			receivableAmount,
			remainTuitionAmount,
			coursePeriodConsumeVO,
			actualConsumeType,
			actualConsumeAmount,
			registrationCourseId,
			studentId
		} = this.state;
		actualConsumeType = actualConsumeType ? actualConsumeType : '1';

		let {
			consumeOriginalAmount, // 原价 课时费消耗
			consumePeriod, //  消耗课时
			consumeReceivableAmount // 应付 课时消耗
		} = coursePeriodConsumeVO;

		return (<Row gutter={24}>
			<Col span={24}>
				<div className="f-radius1 f-over-hide f-bg-white f-box-shadow2 ">
					<div className="f-pd5 f-left-title-box">
				        <div className="f-bg-green f-left-title">
		        			<h3>转出班级</h3>
		        		</div>

		        		<div className="f-mb2">
		        			<span className="gradeName f-fz5 f-black f-bold">
		        				{courseName}
	        				</span>
		        			{
		        				isArrear == '1' ?
				        			<span className="f-ml4">
					        			<i className="f-round f-bg-orange f-white f-round-tip f-mr2">欠</i>
					        			<a 
					        				href={`/front/fillmoney/form?registrationCourseId=${registrationCourseId}&studentId=${studentId}`}
					        				className="f-red"
				        				>学员欠费{arrearAmount}元，点此查看</a>
				        			</span>
				        		: null
		        			}
		        			<span className="f-ml5 f-mr2">
		        				{className}
	        				</span>
		        			<span className="f-green f-ml5">
		        				{ classStateName[classState] }
		        			</span>
		        		</div>

		        		<div className="f-lh6">
		        			<span>学费¥{receivableAmount}</span>
		        			<span className="f-ml2 f-mr2">-</span>
		        			<span>已上{consumePeriod}课时</span>
		        			<Select 
		        				className="f-ml2 f-mr2"
		        				size="large"
		        				style={{width:120}}
		        				value={actualConsumeType}
		        				onChange= {
		        					(v)=> {
		        						let actualConsumeAmount = v == '1' ? consumeOriginalAmount : consumeReceivableAmount;
		        						this.setState({
		        							actualConsumeAmount,
		        							actualConsumeType:v
		        						},()=> {
											this._computePrice();
										});
		        					}
		        				}
	        				>
		        				<Option value="1">原价课消</Option>
		        				<Option value="2">优惠课消</Option>
		        			</Select>
		        			<InputNumber
								formatter={
									(value)=> {
										if(/^\d*(\.\d{0,2})?$/.test(value)){
											if(value>receivableAmount) {
												value=receivableAmount
											}
											return `¥ ${value}`
										}else{
											value = parseFloat(value);
											value = Math.round(value * 100) / 100;
											return `¥ ${value}`;
										};
									}
								} 
								style={{ width:'100px' }} 
								size="large"
								min={0}
								max={receivableAmount}
								value={actualConsumeAmount}
								onChange = {
									(value)=> {
										if(/^\d*(\.\d{0,2})?$/.test(value)){
											if(value>receivableAmount) {
												value=receivableAmount
											}
										}else{
											value = parseFloat(value);
											value = Math.round(value * 100) / 100;
										};

										this.setState({
											actualConsumeAmount:value || 0
										},()=> {
											this._computePrice();
										});
									}
								}
							/>
		        			<span className="f-ml2 f-mr2">=</span>
		        			<span>
		        				剩余学费¥{remainTuitionAmount}
		        			</span>
		        		</div>
			        </div>
				</div>
			</Col>
		</Row>);
	}
	componentDidMount() {
		let {
			data
		} = this.props;
		this.setState({
			...data,
			actualConsumeAmount: data.coursePeriodConsumeVO.consumeOriginalAmount, //初始化实际课消金额等于原价课消金额
		}, () => {
			// console.log(data);
			this._computePrice();
		});
	}
	_computePrice() {
		let signData = window.store.session('signData');
		let {
			signType,
			singleIds,
			id,
			discount,
			many,
			registrationCourseId
		} = signData;
		let {
			getData
		} = this.props;
		let {
			remainTuitionAmount, // 剩余学费
			receivableAmount, //学费
			actualConsumeAmount, // 实际课消金额
			coursePeriodConsumeVO, //课消对象
			actualConsumeType,
		} = this.state;
		actualConsumeType = actualConsumeType ? actualConsumeType : '1';
		let {
			consumeOriginalAmount, // 原价 课时费消耗
			consumePeriod, //  消耗课时
			consumeReceivableAmount // 应付 课时消耗
		} = coursePeriodConsumeVO;

		remainTuitionAmount = Number((receivableAmount - actualConsumeAmount).toFixed(2));
		this.setState({
			registrationCourseId,
			remainTuitionAmount,
			actualConsumeType
		}, () => {
			getData(this.state);
		});
	}
}