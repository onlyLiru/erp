import React, {
  Component
} from 'react';
import {
  Table,
} from 'antd';
import {
  Link
} from 'react-router-dom';

export default class Main extends Component {
  render() {

    let {
      data,
      loading,
      pagination,
      onChange
    } = this.props;

    return (
      <div className="f-box-shadow2 f-radius1 f-bg-white f-pd4 f-mt5">
        <h3 className="f-mb5">
          <span className="f-title-blue f-mt3">退费列表</span>
        </h3>
        <Table
            columns={ columns }
            dataSource={ data }
            scroll={{ x: 2450}}
            pagination={ pagination }
            loading={loading}
            onChange={onChange}
          />
      </div>
    );
  }

}

const columns = [{
  title: '学员姓名（学号）',
  dataIndex: 'studentName',
  width: '160px',
  fixed: 'left',
  render: (value, row, index) => {
    let {
      studentName,
      stuNo
    } = row;
    return <div className="f-line1">
      { studentName }({stuNo})
    </div>
  }
}, {
  title: '头像',
  dataIndex: 'headImgUrl',
  className: 'f-align-center',
  width: '80px',
  render: (value, row, index) => {
    let {
      headImgUrl
    } = row;
    return (<div className="f-list-avatar">
        <img src= {value}/> 
      </div>);
  }
}, {
  title: '性别',
  dataIndex: 'genderStr',
  className: 'f-align-center',
  width: '60px'
}, {
  title: '年龄',
  dataIndex: 'age',
  className: 'f-align-center',
  width: '60px'
}, {
  title: '生日',
  dataIndex: 'birthday',
  width: '200px'
}, {
  title: '手机号码',
  dataIndex: 'phone',
  width: '160px'
}, {
  title: '课程名称',
  dataIndex: 'courseName',
  width: '160px'
}, {
  title: '班级名称',
  dataIndex: 'className',
  width: '160px'
}, {
  title: '上课老师',
  dataIndex: 'teacherName',
  width: '100px'
}, {
  title: '学费金额',
  dataIndex: 'receivableAmount',
  width: '100px'
}, {
  title: '学费状态',
  dataIndex: 'isArrearStr',
  className: 'f-align-center',
  width: '100px'
}, {
  title: '优惠状态',
  dataIndex: 'isOriginalStr',
  className: 'f-align-center',
  width: '100px'
}, {
  title: '报班状态',
  dataIndex: 'stateStr',
  className: 'f-align-center',
  width: '100px'
}, {
  title: '报名类型',
  dataIndex: 'type',
  className: 'f-align-center',
  width: '150px',
  render: (value, row, index) => {
    let arr = ['', '单报', '连报', '课程包']
    let {
      currentPeriod,
      totalPeriod
    } = row;
    return (<div>
          { arr[value] }
          { value == 2 ? <span className="f-ml2">({ currentPeriod }/ { totalPeriod })</span> : null }
        </div>);
  }
}, {
  title: '是否续报',
  dataIndex: 'isContinueStr',
  className: 'f-align-center',
  width: '100px'
}, {
  title: '是否新生',
  dataIndex: 'isFreshManStr',
  className: 'f-align-center',
  width: '100px'
}, {
  title: '班级状态',
  dataIndex: 'classStateStr',
  width: '160px'
}, {
  title: '市场来源',
  dataIndex: 'salesFromStr',
  width: '100px'
}, {
  title: '市场顾问',
  dataIndex: 'salesName',
  width: '100px'
}, {
  title: '所属校区',
  dataIndex: 'schoolAreaName',
  width: '100px'
}, {
  title: '操作',
  dataIndex: 'des',
  fixed: 'right',
  width: '100px',
  className: "f-align-center",
  render: (value, row, index) => {
    let {
      id, //报课id
      studentId //学员id
    } = row;

    return (<div>
      <Link className="ant-btn f-radius3 f-ml2 f-white f-bg-green f-lh4" to={`/front/returnmoney/form?registrationCourseId=${id}&studentId=${studentId}`}>退差价</Link>
    </div>);
  }
}];