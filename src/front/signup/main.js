import React, {
	Component
} from 'react';
import {
	Route
} from 'react-router-dom';

import {
	default as Step1
} from './step1/main';
import {
	default as Step2
} from './step2/main';

let PARENTPATH;

export default class Main extends Component {
	constructor(props) {
		super(props);
		const {
			match
		} = props;
		PARENTPATH = match.path;
	}
	render() {
		return (<section>
			<Route path={ PARENTPATH } exact component={ Step1 } />
			<Route path={`${PARENTPATH }/step1`} exact component={ Step1 } />
			<Route path={`${PARENTPATH }/step2`} exact component={ Step2 } />
		</section>);
	}
}