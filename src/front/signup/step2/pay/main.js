import React, {
	Component
} from 'react';
import {
	Input,
	Col,
	Row,
	Form,
	DatePicker,
	Select,
	Radio,
} from 'antd';

import PayTypes from '../../../../common/payTypes.js';

export default class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {

		}
	}
	render() {
		return (<div 
				className="f-box-shadow2 f-radius1 f-over-hide f-bg-white f-mt5"
				style={{
					paddingLeft:'24px'
				}}
			>
			        <div className="f-pd5 f-right-title-box">
				        <div className="f-bg-green f-right-title">
		        			<h3>费用信息</h3>
		        		</div>
		        		<Row gutter={16} className="f-mb5">
		        			<Col span={2} className="f-align-right">总计 :</Col>
		        			<Col span={20}><i className="f-fz5 f-red" id="J-total">0</i></Col>
		        		</Row>
		        		<Row gutter={16}>
		        			<Col span={2} className="f-align-right f-lh6">支付方式 :</Col>
		        			<Col span={20}>
		        				<PayTypes 
		        					onInput = { this.props.onInput }
		        				/>
		        			</Col>
		        		</Row>
			        </div>
				</div>);
	}
}