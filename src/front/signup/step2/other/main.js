import React, {
	Component
} from 'react';
import {
	Col,
	Row,
	Form,
	DatePicker,
	Select,
	Radio
} from 'antd';
import moment from 'moment';
import {
	default as SelectPerson
} from '../../../../common/selectPerson.js';
import {
	courseFrom
} from '../../../../common/staticData.js';

const FormItem = Form.Item;
const Option = Select.Option;
const RadioGroup = Radio.Group;

class MainForm extends Component {
	constructor(props) {
		super(props);
		let signData = window.store.session('signData');
		let {
			school
		} = signData;
		this.state = {
			salesFrom: null,
			schoolAreaId: school
		}
	}
	render() {
		const {
			getFieldDecorator
		} = this.props.form;
		let {
			salesFrom
		} = this.state;

		const formItemLayout = {
			labelCol: {
				span: 7
			},
			wrapperCol: {
				span: 17
			}
		};

		return (<Form>
			<div className="f-box-shadow2 f-radius1 f-over-hide f-bg-white f-mt5">
		        <div className="f-pd5 f-right-title-box">
			        <div className="f-bg-blue f-right-title">
	        			<h3>其他</h3>
	        		</div>
	        		<div>
	        			<Row gutter={ 40 } type="flex">
	        				<Col span={8}>
	        					<FormItem
									label="报名时间"
									{...formItemLayout}
								>
									{
										getFieldDecorator('registrationTime',{
											rules:[{
												required:true,
												message:'请填写报名时间'
											}],
											initialValue:moment()
										})(
											<DatePicker
												style={{width:'100%'}}
										      	format="YYYY-MM-DD HH:mm:ss"
										      	showTime={{ defaultValue: moment('00:00:00', 'HH:mm:ss') }}
										    />
										)
									}
								</FormItem>
	        				</Col>
		    				<Col span={8}>
	        					<FormItem
									label="课程顾问"
									{...formItemLayout}
								>
									{
										getFieldDecorator('salesId', {
											rules: [{  
												required:true,
												message:'请选择销售员'
											}]
										})(
											<SelectPerson
												url='/api/hr/staff/listStaffByCondForDropDown'
												data={
													{ 
														type:1,
														schoolAreaId:this.state.schoolAreaId
													}
												}
												onSelect={
													(v) => {
														this.props.form.setFieldsValue({
															salesId: v
														});
													}
												}
											/>
										)
									}
								</FormItem>
	        				</Col>
	        				<Col span={8}>
	        					<FormItem
									label="是否新生"
									{...formItemLayout}
								>
									{
										getFieldDecorator('isNewStudent', {
											rules: [{ 
												required: true, 
												message: '请选择是否新生' 
											}]
										})(
											<Select
												placeholder="请选择"
											>
										        <Option value='1'>是</Option>
										        <Option value='0'>否</Option>
										     </Select>
										)
									}
								</FormItem>
	        				</Col>
	        				<Col span={8}>
	        					<FormItem
									label="课程来源"
									{...formItemLayout}
								>
									{
										getFieldDecorator('salesFrom', {
											rules: [{  
												required:true,
												message: '请选择来源' 
											}]
										})(
											<Select
												size="large"
												allowClear
			                					placeholder="请选择来源"
			                					onChange={
			                						(v)=> {
			                							this.setState({
			                								salesFrom:v
			                							});
			                						}
			                					}
											>
												{
													courseFrom.map((d,i)=> {
														return(<Option 
																key={i} 
																value={d.value}
															>{d.label}</Option>);
													})
												}
											</Select>
										)
									}
								</FormItem>
	        				</Col>
	        				{
	        					salesFrom && 
	        					(
	        						salesFrom == '10'
	        						|| salesFrom == '11'
	        						|| salesFrom == '12'
	        					) ?
				    				<Col span={8}>
			        					<FormItem
											label="上课老师"
											{...formItemLayout}
										>
											{
												getFieldDecorator('teacherId', {
													rules: [{  
														required:true,
														message:'请选择上课老师'
													}]
												})(
													<SelectPerson 
														onSelect={
															(v) => {
																this.props.form.setFieldsValue({
																	teacherId: v
																});
															}
														}
													/>
												)
											}
										</FormItem>
			        				</Col>
			        			: null
	        				}
		    	        </Row>
	        		</div>
		        </div>
			</div>
		</Form>);
	}
}

const MyForm = Form.create()(MainForm);
export default MyForm;