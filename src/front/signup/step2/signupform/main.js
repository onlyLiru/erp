import React, {
	Component
} from 'react';
import {
	Input,
	InputNumber,
	Button,
	Icon,
	Col,
	Row,
	Card,
	Modal,
	Form,
	DatePicker,
	Select,
	Upload,
	message,
	Radio,
	Tag,
	Checkbox
} from 'antd';
import store from 'store2';

import SelectClassRoom from '../../../../common/selectClassRoom.js';
import SelectGoods from '../../../../common/selectGoods.js';
import SelectDiscounts from '../../../../common/selectDiscounts.js';
import Remark from '../../../../common/remark.js';
import {
	fpost,
	fget
} from '../../../../common/io.js';
import {
	changeTwoDecimal
} from '../../../../common/g.js';
import {
	default as NumberInput
} from '../../../../common/numberInput.js';

let SESSION = store.session();

const RangePicker = DatePicker.RangePicker;
const FormItem = Form.Item;
const Option = Select.Option;
const RadioGroup = Radio.Group;
const {
	TextArea
} = Input;

class MainForm extends Component {
	constructor(props) {
		super(props);

		let signData = window.store.session('signData');
		let {
			id,
			type, //1,是学员,2潜客
			signType, //1,单报，2，连报
			school
		} = signData;

		this.state = {
			schoolAreaId: school, //所选校区
			type: signType, //单报还是连报1单,2连
			isContinue: type, //是否是续报，type是1就是学员，也就是续报
			key: 0, //连报时候第几期

			courseName: '',
			courseId: '', //课程id
			regularPrice: '', //课程上的费用
			receivableAmount: 0, //实际学费

			visibleClassRoomModal: false, //是否显示选择班级弹窗
			classGrade: null, //已选择班级
			myClassName: '', //班级名称
			insertClassInfo: null, //插班列表
			insertClassId: '', //已选择插班id
			selectedInsertClass: null, //已经选择的插班信息
			insertPeriodPrice: 0, //已选择插班的价格
			insertCoursePeriod: 0, //插班课时

			visibleSelectGoods: false, //是否显示选择物品弹窗
			goodsData: [], //已选择商品,
			goodsDataKeys: [], //默认商品id，用于默认选中
			courseMaterialVOList: [], //初始化接口中获取到的物品信息
			goodsTotalPrice: 0, //物品费用

			preferentialAmount: 0, //总优惠
			visibleSelectDiscounts: false, //是否显示选择优惠弹窗
			couponData: null, //已选择优惠
			remark: null, //备注信息

			studentPoolBalance: this.props.studentPoolBalance, //学生账户余额
			arrearAmount: 0, //欠费金额
			isArrear: 0, //是否欠费
			isOriginal: 0, //是否原价
			useBalance: 0, //余额抵用
			actualAmount: 0, //实收

			/*给服务端封装数据*/
			registrationCourseReduceList: [], //封装优惠信息给后端传参数用
			registrationMaterialList: [], //物品信息
			internalRemark: '', //内部备注
			foreignRemark: '', //外部备注

			thisCourseTotalPrice: 0, //本课程的学费+物品总费用
		}
	}
	render() {

		const {
			getFieldDecorator
		} = this.props.form;

		let {
			type,
			schoolAreaId,
			thisCourseTotalPrice,

			key,
			courseName, //课程名称
			courseId,

			visibleSelectGoods, //是否显示选择物品弹窗
			goodsData, //课程中包含的物品信息
			goodsTotalPrice, //物品费用总和

			studentPoolBalance, //账户余额

			visibleClassRoomModal, //是否显示选择班级弹窗
			classGrade, //班级信息
			myClassName, //班级名称
			originalPrice = 0, //班级中课程的价格-原价
			receivableAmount, //实际学费
			insertClassInfo, //插班列表

			visibleSelectDiscounts, //是否显示选择优惠弹窗
			couponData,

			preferentialAmount, //总优惠,
			arrearAmount, //欠费
			isArrear, //是否欠费
			useBalance, //余额抵用
			actualAmount, //实收
		} = this.state;

		let {
			modifyMaterialPrice
		} = SESSION

		return (<Form
				style={{
					paddingLeft:'14px'
				}}
			>
			<Row gutter={40} type="flex">
				<Col span={19}>
					<Row gutter={24}>
						<Col span={3} className="f-align-right f-lh7 f-black">课程名称:</Col>
						<Col span={20}>
							<Row gutter={24}>
								<Col span={6} className="f-lh7">{courseName}</Col>
								{
									key !=0 && type=='2' ?
										<span className="f-lh7 f-pale">尚未排班</span>
									:
									<Col span={18}>
			        					<FormItem
											label="选择班级"
											labelCol= {
												{ span:4 }
											}
											wrapperCol= {
												{ span:12 }
											}
										>	
											{
												getFieldDecorator('classId',{
													rules: [{  
														required:true,
														message:'请选择班级'
													}],
													initialValue:myClassName ? myClassName : ''
												})(
													<Input
														readOnly
														onClick={
															()=> {
																this.setState({
																	visibleClassRoomModal:true
																});
															}
														}
														placeholder="请输选择班级"
													/>
												)
											}
										</FormItem>
									</Col>
								}
							</Row>
						</Col>
					</Row>
					{
						(classGrade || key!=0) ?
							<Row gutter={24}>
								<Col span={3} className="f-align-right f-lh7 f-black">课程费用:</Col>
								<Col span={20}>
									<div className="f-lh7 f-mb5">
										<span className="f-mr5">原价：¥{originalPrice}</span>
										<span className="f-mr5">学费：¥{changeTwoDecimal(receivableAmount)}</span>
										{
											studentPoolBalance ?
												<span className="f-mr5">
													<span className="f-mr2">使用余额:</span>
		    										<NumberInput
		    											size="large"
		    											style={{
		    												width:'100px'
		    											}}
		    											value={
		    												{ 
		    													number:useBalance,
		    													max:receivableAmount < studentPoolBalance ? receivableAmount : studentPoolBalance,
		    												}
		    											}
		    											onChange = { this._changeUseBalance.bind(this) }
													/>
													（可用¥{studentPoolBalance}）
												</span>
											: null
										}
			        					<FormItem
											label="实收"
											labelCol= {
												{ span:2 }
											}
											wrapperCol= {
												{ span:12 }
											}
										>	
											{
												getFieldDecorator('actualAmount',{
													rules: [{  
														required:true,
														validator: this._checkActualAmount.bind(this) 
													}],
													initialValue:{
    													number:actualAmount,
    													max:receivableAmount,
    												}
												})(
													<NumberInput
		    											size="large"
		    											style={{
		    												width:'120px'
		    											}}
													/>
												)
											}
										</FormItem>
									</div>
									<div className="f-lh7 f-mb5">
										<Row>
											<Col span={3}>优惠信息:</Col>
											<Col span={21}>
												{
													(couponData && couponData.coupon) ? 
														<span>
															{
																couponData.discount ?
																	<Tag 
																		color="orange" 
																		className="f-lh7 discounts-tag"
																	> 折扣: { couponData.discount } </Tag>
																: null
															}
															{
																couponData.cash ?
																	<Tag 
																		color="orange" 
																		className="f-lh7 discounts-tag"
																	> 现金优惠: ¥{ couponData.cash } </Tag>
																: null
															}
															{ couponData.coupon.map((d,i)=> {
																return (<Tag 
																		key={i}
																		color="orange" 
																		className="f-lh7 discounts-tag"
																	>
																		{d.name}：¥{d.amount}
																	</Tag>)
															}) }
														</span>
													: null
												}
												<Button 
													className="f-mb3" 
													size="large" 
													type="primary"
													onClick={ ()=> {
														this.setState({
															visibleSelectDiscounts:true
														});
													} }
												>选择优惠</Button>
											</Col>
										</Row>
										{
											insertClassInfo && insertClassInfo.length ?
												<div className="f-mt4">
													<span className="f-mr2">插班</span>
													<Select
													    showSearch
													    size="large"
													    style={{ width: 480 }}
													    placeholder="选择插班"
													    optionFilterProp="children"
													    onChange={this._getInsertClassInfo.bind(this)}
													>
														<Option value=''>请选择</Option>
														{
															insertClassInfo.map((d,i)=> {
																let {
																	id,
																	insertPeriod,
																	classTimes,
																	insertPeriodPrice,
																	classDate
																} = d;
													    		return <Option 
													    			value={id}
													    			key={i}
												    			>
												    				<span className="f-mr2">课时:{ insertPeriod } </span>
												    				<span className="f-mr2">课次:{ classTimes }</span>
												    				<span className="f-mr2">费用:¥{ insertPeriodPrice }</span>
													    			上课日期: { classDate }
												    			</Option>
															})
														}
												  	</Select>
												</div>
											: null
										}
									</div>
								</Col>
							</Row>
						: null
					}
					<Row gutter={24} className="f-mb5">
						<Col span={3} className="f-align-right f-lh7 f-black">物品信息:</Col>
						<Col span={20}>
							{
								(goodsData && goodsData.length) ?
									<ul className="f-lh7 my-goods">
										{
											goodsData.map((d,i)=> {
												let {
													count,
													salePrice
												} = d;
												return (<li key={i}>
													<span className="f-mr5">{d.materialName}</span>
													<span className="f-mr5">
														{
															modifyMaterialPrice ?
																<InputNumber 
																	formatter={
																		(value)=> {
																			if(/^\d*(\.\d{0,2})?$/.test(value)){
																				return `¥ ${value}`
																			}else{
																				value = parseFloat(value);
																				value = Math.round(value * 100) / 100;
																				return `¥ ${value}`;
																			};
																		}
																	} 
																	style={{ width:'100px' }} 
																	size="large" 
																	value={salePrice}
																	defaultValue={0}
																	min={0}
																	onChange={ (e)=> this._onGoodsPriceChange(e,d) }
																/>
															: <span className="f-mr5">{`¥ ${salePrice}`}</span>
														}
													</span>
													<span className="f-mr5">
														<InputNumber 
															formatter={
																(value) => {
																	value = parseInt(value);
																	return `✖️ ${value}`
																}
															} 
															style={{ width:'100px' }} 
															size="large" 
															value={count}
															defaultValue={1}
															min={1}
															onChange={ (e)=> this._onGoodsUnitChange(e,d) }
														/>
													</span>
													<span>
														<Checkbox 
															checked={d.isTakeAway}
															onChange={ (e)=> this._isGet(e,d) }
														>已领取</Checkbox>
													</span>
													<Icon 
														onClick={  
															(e)=> this._removeGoods(e,d)
														}
														type="delete" 
														className="f-fz5 f-ml3 f-pointer" 
													/> 
												</li>);
											})
										}
									</ul>
								: null
							}
							<Button
								className="f-mt1"
								onClick={ ()=> {
									this.setState({
										visibleSelectGoods:true
									});
								} }
							>添加物品</Button>
						</Col>
					</Row>
					{/*备注*/}
					<div style={{
						position:'relative',
						left:'14px'
					}}>
						<Remark
							onInput= { this._getRemark.bind(this) }
						/>
					</div>

				</Col>
				<Col span={5}>
					<div className="price-tip">
						<dl>
							{
								type == '2' ?
									<dt>{`第${key+1}期费用`}</dt>
								: 
									<dt>{courseName}</dt>
							}
							<dd>
								<span className="label">
									<i className="operator"/>
									原价
								</span>
								<span className="f-red">¥{changeTwoDecimal(originalPrice)}</span>
							</dd>
							<dd>
								<span className="label">
									<i className="operator">-</i>
									优惠
								</span>
								<span className="f-red">¥{changeTwoDecimal(preferentialAmount)}</span>
							</dd>
							<dd>
								<span className="label">
									<i className="operator">-</i>
									欠费
								</span>
								<span className="f-red">¥{changeTwoDecimal(arrearAmount)}</span>
							</dd>
							<dd>
								<span className="label">
									<i className="operator">-</i>
									余额抵用
								</span>
								<span className="f-red">¥{changeTwoDecimal(useBalance)}</span>
							</dd>
							<dd className="f-line1">
								<span className="label">
									<i className="operator">+</i>
									物品费用
								</span>
								<span className="f-red">¥{goodsTotalPrice}</span>
							</dd>
							<dd className="f-line1">
									<i className="operator">=</i>
									<i className="f-fz5 f-red">¥{ changeTwoDecimal(thisCourseTotalPrice) }</i>
							</dd>
						</dl>
					</div>
				</Col>
			</Row>
			{/*选择班级*/}
			{
				visibleClassRoomModal ? 
					<SelectClassRoom 
						visible={ visibleClassRoomModal }
						filterRegistrationFull={1}
						schoolAreaId={schoolAreaId}
						courseId={courseId}
						handleOk= { this._selectClassRoomOk.bind(this) }
						handleCancel= { this._selectClassRoomCancel.bind(this) }
					/>
				: null
			}
			{/*选择物品*/}
			{
				visibleSelectGoods ?
					<SelectGoods 
						visible={ visibleSelectGoods }
						handleOk= { this._selectGoodsOk.bind(this) }
						handleCancel= { this._selectGoodsCancel.bind(this) }
						// goodsDataKeys= { this.state.goodsDataKeys }
						schoolAreaId={schoolAreaId}
					/>
				: null
			}
			{/*选择优惠*/}
			{
				visibleSelectDiscounts ? 
					<SelectDiscounts
						visible={ visibleSelectDiscounts }
						handleOk= { this._selectDiscountsOk.bind(this) }
						handleCancel= { this._selectDiscountsCancel.bind(this) }
						courseId={ courseId }
						defaultData={ couponData }
					/>
				: null
			}
			<div className="f-border f-mt5 f-mb5" />
		</Form>);
	}
	componentDidMount() {
		let courseData = this.props.data;
		// console.log('courseData:', courseData);

		let {
			key,
			id, //课程id
			name, //课程名称
			regularPrice, //课程上的费用
			courseMaterialVOList, //课程中包含的物品信息
		} = courseData;

		this.setState({
			key,
			courseName: name,
			courseId: id,
			originalPrice: regularPrice,
			receivableAmount: regularPrice,
			courseMaterialVOList,
		}, () => {
			this._showCourseGoods(); //展示默认物品
		});
	}
	componentDidUpdate() {
		let {
			getData
		} = this.props;
		getData(this.state);
		// console.log(this.state);
	}
	_changeUseBalance(v) { //校验使用余额
		let {
			number
		} = v;
		this.setState({
			useBalance: Number(number)
		}, () => {
			this._computePrice();
		});
	}
	_checkActualAmount(rule, value, callback) { //校验实收
		let {
			number
		} = value;
		let {
			receivableAmount,
			actualAmount,
			useBalance
		} = this.state;
		receivableAmount = changeTwoDecimal(receivableAmount);
		actualAmount = changeTwoDecimal(actualAmount);
		useBalance = changeTwoDecimal(useBalance);
		let max = changeTwoDecimal(receivableAmount - useBalance);
		if (number === '') {
			callback('请输入实收金额!');
			return;
		}

		number = changeTwoDecimal(number);

		if (number >= 0 && number <= max) {
			this.setState({
				actualAmount: number
			}, () => {
				this._computePrice();
			});

			callback();
		} else {
			callback('请输入正确的实收金额!');
		}
	}
	_selectClassRoomCancel() { //取消选择班级
		this.setState({
			visibleClassRoomModal: false
		});
	}
	_selectClassRoomOk(v) { //已选择班级信息
		// console.log('已选择班级信息', v);
		if (!v) {
			return;
		}
		let classGrade = v;

		let {
			id,
			name,
			salePrice
		} = classGrade;

		this.setState({
			visibleClassRoomModal: false,
			classGrade: v,
			myClassName: name,
			originalPrice: salePrice,
			receivableAmount: salePrice,
			classId: id
		}, () => {
			this._getInsertInfo();
			this._computePrice();
		});

		this.props.form.setFieldsValue({
			classId: name
		});
	}
	_getInsertInfo() { //获取插班信息，没有数据代表不能插班
		let {
			classId
		} = this.state;

		let param = {
			classId
		}

		fget(`/api/educational/class/times/detail/list/notclass?classId=${classId}`)
			.then(res => res.json())
			.then((res) => {
				if (!res.success) {
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res.result);
			})
			.then((result) => {
				this.setState({
					insertClassInfo: result
				});
			}, () => {
				this._computePrice();
			})
			.catch((err) => {
				console.log(err);
			});
	}
	_getInsertClassInfo(v) { //获取用户选择的插班信息
		console.log('插班:', v);
		let insertClassId = v;
		let selectedInsertClass = null;
		let insertPeriodPrice = 0;
		let insertCoursePeriod = 0;
		let {
			insertClassInfo,
			originalPrice,
			isInsert,
			registrationCourseReduceList
		} = this.state;

		registrationCourseReduceList = registrationCourseReduceList.filter((d, i) => {
			return d.type != 4;
		});

		if (insertClassId) {
			selectedInsertClass = insertClassInfo.filter((d, i) => {
				return d.id == insertClassId
			});
			selectedInsertClass = selectedInsertClass[0];
			insertPeriodPrice = selectedInsertClass.insertPeriodPrice;
			insertCoursePeriod = selectedInsertClass.insertPeriod;
			isInsert = 1;
			registrationCourseReduceList.push({
				amount: insertPeriodPrice,
				type: 4
			});
		} else {
			registrationCourseReduceList = registrationCourseReduceList.filter((d, i) => {
				return d.type != 4;
			});
		}

		// console.log(registrationCourseReduceList);

		this.props.form.setFieldsValue({
			actualAmount: {
				number: 0
			}
		});

		this.setState({
			insertClassId,
			insertPeriodPrice,
			selectedInsertClass,
			insertCoursePeriod,
			isInsert,
			registrationCourseReduceList
		}, () => {
			// console.log(registrationCourseReduceList);
			this._computePrice();
		});
	}
	_computePrice() { //计算价格
		let {
			thisCourseTotalPrice, //本课程的学费+物品总费用
			originalPrice, //班级中课程的价格-原价
			goodsTotalPrice, //物品费用总和
			preferentialAmount, //总优惠,
			arrearAmount, //欠费
			isOriginal, //是否原价
			receivableAmount, //实际学费
			useBalance, //余额抵用
			actualAmount, //实收
			insertPeriodPrice, //已选择插班的价格
			couponData,
		} = this.state;
		thisCourseTotalPrice = changeTwoDecimal(thisCourseTotalPrice);
		originalPrice = changeTwoDecimal(originalPrice);
		goodsTotalPrice = changeTwoDecimal(goodsTotalPrice);
		preferentialAmount = changeTwoDecimal(preferentialAmount);
		arrearAmount = changeTwoDecimal(arrearAmount);
		receivableAmount = changeTwoDecimal(receivableAmount);
		useBalance = changeTwoDecimal(useBalance);
		actualAmount = changeTwoDecimal(actualAmount);
		insertPeriodPrice = changeTwoDecimal(insertPeriodPrice);

		let {
			discount,
			cash,
			coupon
		} = couponData || '';
		discount = changeTwoDecimal(discount);
		cash = changeTwoDecimal(cash);

		//如果有插班先算插班
		receivableAmount = changeTwoDecimal(originalPrice - insertPeriodPrice);
		if (discount) { //如果有折扣
			receivableAmount = changeTwoDecimal(receivableAmount * discount);
		};
		if (cash) { //如果有现金优惠
			receivableAmount = changeTwoDecimal(receivableAmount - cash);
		}
		if (coupon && coupon.length) { //如果有其他优惠
			coupon.forEach((d, i) => {
				let {
					amount
				} = d;
				amount = changeTwoDecimal(amount);
				receivableAmount = changeTwoDecimal(receivableAmount - amount);
			});
		}

		if (actualAmount > changeTwoDecimal(receivableAmount - useBalance)) {
			actualAmount = changeTwoDecimal(receivableAmount - useBalance); //计算出实收学费-余额抵用
		}

		if (actualAmount > receivableAmount) {
			actualAmount = receivableAmount;
		};


		preferentialAmount = changeTwoDecimal(originalPrice - receivableAmount); //计算出总优惠金额

		arrearAmount = changeTwoDecimal(changeTwoDecimal(receivableAmount - actualAmount) - useBalance); //计算出欠费金额

		thisCourseTotalPrice = changeTwoDecimal(actualAmount + goodsTotalPrice); //计算出本课程的学费+物品费用=总费用
		this.setState({
			thisCourseTotalPrice,
			receivableAmount,
			preferentialAmount,
			arrearAmount,
			actualAmount,
			isArrear: arrearAmount ? 1 : 0,
			isOriginal: preferentialAmount ? 0 : 1,
		}, () => {
			// console.log(this.state);
		});
		console.log(actualAmount);
		// this.props.form.setFieldsValue({
		// 	actualAmount: {
		// 		number: actualAmount
		// 	}
		// });
	}
	_showCourseGoods() { //展示默认物品信息
		let {
			goodsData,
			goodsDataKeys,
			courseMaterialVOList,
		} = this.state;

		if (!courseMaterialVOList || !courseMaterialVOList.length) {
			return;
		}

		courseMaterialVOList.map((d, i) => {
			let {
				salePrice
			} = d;
			d.originalPrice = salePrice; //原价-报名接口需要的字段
			d.salesPrice = salePrice; //原价-报名接口需要的字段
			d.count = 1;
			d.isTakeAway = 1;
			goodsDataKeys.push(d.materialId);
		});

		goodsData = [
			...goodsData,
			...courseMaterialVOList
		]

		this.setState({
			goodsData,
		}, () => {
			// console.log(goodsData);
			this._getGoodsTotalPrice();
		});
	}
	_selectGoodsOk(v) { //已选择物品
		console.log(v);
		let {
			goodsData,
			goodsDataKeys,
		} = this.state;

		if (!v || !v.length) {
			message.error('请选择物品');
			return;
		}

		v.forEach((d) => {
			let {
				materialId,
				salePrice
			} = d;
			/*去重start*/
			let oldG = goodsData.filter(
				(oldD, i) => {
					return oldD.materialId == materialId;
				}
			);
			// console.log(oldG);
			if (!oldG || !oldG.length) { //看是否默认商品中已经有了这个商品，有得话就去重
				// console.log('hasSaveGoods?:',oldG);
				goodsDataKeys.push(materialId);
				d.count = 1;
				d.isTakeAway = 1;
				goodsData.push(d);
			}
			/*去重end*/
		});

		this.setState({
			goodsData,
			goodsDataKeys,
			visibleSelectGoods: false
		}, () => {
			// console.log('已选择物品:', this.state.goodsData);
			this._getGoodsTotalPrice();
		});
	}
	_selectGoodsCancel() { //取消选择物品
		this.setState({
			visibleSelectGoods: false
		});
	}
	_onGoodsPriceChange(v, d) { //当改变物品价格
		let {
			goodsData
		} = this.state;
		let curPrice = v;
		let materialId = d.materialId;
		let index;

		if (!curPrice || isNaN(curPrice) || curPrice < 0) {
			goodsData.forEach((data, i) => {
				if (data.materialId == d.materialId) {
					data.salePrice = 0;
				}
			});
			this.setState({
				goodsData
			}, () => {
				this._getGoodsTotalPrice();
			});
			return;
		}

		index = goodsData.findIndex((obj) => {
			return obj.materialId == materialId;
		});

		goodsData[index].salePrice = curPrice;
		goodsData[index].salesPrice = curPrice;

		this.setState({
			goodsData
		}, () => {
			// console.log('当改变物品价格', this.state.goodsData)
			this._getGoodsTotalPrice();
		});
	}
	_removeGoods(e, d) { //删除物品(挪到弹窗里面操作，取消checkbox选择即可)
		let {
			goodsData,
			goodsDataKeys
		} = this.state;
		let materialId = d.materialId;
		goodsData = goodsData.filter((obj) => obj.materialId != materialId);
		goodsDataKeys = goodsDataKeys.filter((id) => id != materialId);
		this.setState({
			goodsData,
			goodsDataKeys
		}, () => {
			// console.log(goodsData);
			this._getGoodsTotalPrice();
		});
	}
	_onGoodsUnitChange(v, d) { //改变物品数量
		let curCount = v;
		let {
			goodsData
		} = this.state;
		let materialId = d.materialId;
		let index;

		if (!curCount || isNaN(curCount)) {
			goodsData.forEach((data, i) => {
				if (data.materialId == d.materialId) {
					data.count = 0;
				}
			});
			this.setState({
				goodsData
			}, () => {
				this._getGoodsTotalPrice();
			});
			return;
		}

		index = goodsData.findIndex((obj) => {
			return obj.materialId == materialId;
		});

		goodsData[index].count = curCount;

		this.setState({
			goodsData
		}, () => {
			// console.log('数量改变后', this.state.goodsData);
			this._getGoodsTotalPrice();
		});
	}
	_isGet(e, d) {
		let isTakeAway = e.target.checked ? 1 : 0;
		let materialId = d.materialId;
		let {
			goodsData
		} = this.state;

		let index = goodsData.findIndex((obj) => {
			return obj.materialId == materialId;
		});

		goodsData[index].isTakeAway = isTakeAway;

		this.setState({
			goodsData
		}, () => {
			this._getGoodsTotalPrice();
			console.log('取消已领取后', this.state.goodsData);
		});
	}
	_getGoodsTotalPrice() { //计算物品总价格
		let {
			registrationMaterialList,
			goodsData
		} = this.state;
		let goodsTotalPrice = 0;
		goodsData.forEach((d, i) => {
			let {
				salePrice,
				count
			} = d;
			d.quantity = count; //报名接口需要的数量字段quantity
			goodsTotalPrice += Number(salePrice) * Number(count);
		});

		goodsTotalPrice = changeTwoDecimal(goodsTotalPrice);

		registrationMaterialList = goodsData;

		this.setState({
			goodsTotalPrice,
			registrationMaterialList
		}, () => {
			this._computePrice();
		});
	}
	_selectDiscountsOk(v) { //确定选择优惠
		// console.log(v);
		let {
			registrationCourseReduceList
		} = v;
		let oldRegistrationCourseReduceList = this.state.registrationCourseReduceList.filter((d, i) => {
			return d.type == 4
		});

		registrationCourseReduceList = [
			...registrationCourseReduceList,
			...oldRegistrationCourseReduceList
		]

		this.props.form.setFieldsValue({
			actualAmount: {
				number: 0
			}
		});

		this.setState({
			couponData: v,
			visibleSelectDiscounts: false,
			registrationCourseReduceList
		}, () => {
			this._computePrice();
			// console.log(this.state.registrationCourseReduceList);
		});
	}
	_selectDiscountsCancel() { //取消选择优惠
		this.setState({
			visibleSelectDiscounts: false
		});
	}
	_getRemark(v) {
		this.setState({
			remark: v,
			internalRemark: v.in,
			foreignRemark: v.out
		}, () => {
			// console.log('备注:', this.state.remark);
		});
	}
}


const MyForm = Form.create()(MainForm);
export default MyForm;