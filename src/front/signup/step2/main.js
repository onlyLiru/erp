import React, {
	Component
} from 'react';
import {
	Button,
	Breadcrumb,
	message
} from 'antd';
import moment from 'moment';

import {
	default as ViewInfo
} from './viewinfo/main.js';
import {
	default as BasickForm
} from './basicform/main.js';
import {
	default as SignUpForm
} from './signupform/main.js';
import {
	default as PayForm
} from './pay/main.js';
import {
	default as OtherForm
} from './other/main.js';

import {
	fpost,
	fpostArray
} from '../../../common/io.js';
import printJS from 'print-js';

import './main.less';


/*封装给服务端*/
let registrationClassCourseVOList = []; //报班对象

export default class Main extends Component {
	constructor(props) {
		super(props);

		let signData = window.store.session('signData');
		let {
			id, //潜客id，或者学员id，根据type区分
			type, //1,是学员,2潜客
			signType, //1,单报，2，连报
			school //校区id
		} = signData;

		this.state = {
			type,
			result: null,
			registrationVO: {
				amountType: 0, //报名时没用，转班时候有用，这里写死0
				refundsType: 0, //报名时没用，转班时候有用，这里写死0
				schoolAreaId: school, //所选校区
				type: signType, //单报还是连报
				isContinue: type, //是否是续报
			}, //报班对象
			studentVO: {
				registrationType: type, //1,是学员,2潜客
			}, //基本信息
		}
	}
	render() {
		let {
			type,
			result,
		} = this.state;

		return (<section>
			<Breadcrumb className="f-mb3">
			    <Breadcrumb.Item>前台业务</Breadcrumb.Item>
			    <Breadcrumb.Item>
			   		<span className="f-fz5 f-bold">报名</span>
			    </Breadcrumb.Item>
			</Breadcrumb>

			<section>
				<div className="f-clear f-mb3">
					<span className="f-title-blue f-mt2">基本信息</span>
				</div>

				{
					type=='1' ?
						<ViewInfo 
							data={result}
						/>
					:
						<BasickForm 
							ref = {form => this.basicForm = form}
							data={result}
						/>
				}


				<div className="f-clear f-mb3 f-mt5">
					<span className="f-title-blue">报名信息</span>
				</div>

				<div className="f-box-shadow2 f-radius1 f-over-hide f-bg-white">
					<div className="f-pd5 f-right-title-box">
						<div className="f-bg-yellow f-right-title">
							<h3>课程／班级信息</h3>
						</div>
						{
							result && result.courseVOList ?
								<div>
									{
										result.courseVOList.map((d,i)=> {
											let studentPoolBalance =(result.studentVO && result.studentVO.balanceAmount) || '';//账户余额

											return <SignUpForm
												ref={form => this[`classForm${i}`] = form}
												key={i}
												data={d}
												getData={this._getCourseData.bind(this)}
												studentPoolBalance={studentPoolBalance}
											/>
										})
									}
								</div>
							: null
						}
		        	</div>
				</div>

				<PayForm 
					onInput = { this._getPayData.bind(this) }
				/>

				<OtherForm 
					ref= { form => this.otherForm= form } 
				/>

				<div className="f-align-right f-mt5">
					<Button 
						size="large" 
						className="f-mr5"
						onClick={
							()=> {
								window.history.back();
							}
						}
					>
						取消
					</Button>
					<Button 
						size="large" 
						type="primary" 
						onClick= {this._getBasicData.bind(this)}
					>
						保存
					</Button>
				</div>

	      	</section>
	      	
		</section>);
	}
	componentDidMount() {
		this._init();
	}
	_getCourseData(data) {
		let totalActualAmount = 0;

		registrationClassCourseVOList = registrationClassCourseVOList.filter((d, i) => {
			return data.key != d.key
		});
		registrationClassCourseVOList.push(data);


		/*计算课程总实收totalActualAmount*/
		registrationClassCourseVOList.forEach((d, i) => {
			let {
				thisCourseTotalPrice
			} = d;
			totalActualAmount += Number(thisCourseTotalPrice);
		});
		totalActualAmount = totalActualAmount.toFixed(2);

		document.querySelector('#J-total').innerText = totalActualAmount;
		// console.log(registrationClassCourseVOList);
	}
	_getBasicData(e) { //获取基本信息
		e.preventDefault();
		let {
			type,
			studentVO
		} = this.state;

		if (type) { //如果是潜客或者学员就用初始化的学员信息，什么都不是的情况才需要获取表单个人信息
			this._getClassData();
			return;
		};
		/*基本信息表单*/
		this.basicForm.validateFields((err, values) => {
			if (!err) {
				let {
					motherPhone,
					fatherPhone,
					otherPhone,
					birthday,
					citySelect
				} = values;
				birthday = birthday ? moment(birthday).format('YYYY-MM-DD') : '';
				let provinceId = citySelect[0];
				let cityId = citySelect[1];
				let districtId = citySelect[2];

				values.birthday = birthday;
				values.provinceId = provinceId;
				values.cityId = cityId;
				values.districtId = districtId;

				if (!motherPhone && !fatherPhone && !otherPhone) {
					message.error('至少要有一个电话号码!');
					return;
				}
				this.setState({
					studentVO: {
						...studentVO,
						...values
					}
				}, () => {
					this._getClassData();
					console.log('基本信息: ', this.state);
				});
			}
		});
	}
	_getClassData() { //获取报班信息
		let {
			registrationVO
		} = this.state;
		let canSave = true;
		/*报名信息表单*/
		registrationClassCourseVOList.forEach((d, i) => {
			this[`classForm${i}`].validateFields((err, values) => {
				if (err) {
					canSave = false;
				}
			});
		});

		if (!canSave) {
			// alert('error');
			return;
		}

		registrationVO.registrationClassCourseVOList = registrationClassCourseVOList;

		this.setState({
			registrationVO
		}, () => {
			this._getOtherData();
			console.log('报名信息: ', this.state);
		});
	}
	_getOtherData() {
		let {
			registrationVO
		} = this.state;
		/*其他信息表单*/
		this.otherForm.validateFields((err, values) => {
			if (!err) {
				let {
					registrationTime,
					salesId,
					salesFrom,
					isNewStudent,
					teacherId
				} = values;
				registrationTime = moment(registrationTime).format('YYYY-MM-DD HH:mm:ss');
				values.registrationTime = registrationTime;

				registrationVO.registrationTime = registrationTime;
				registrationVO.salesId = salesId;
				registrationVO.salesFrom = salesFrom;
				registrationVO.isNewStudent = isNewStudent;
				registrationVO.teacherId = teacherId;

				this.setState({
					registrationVO
				}, () => {
					console.log('其他信息: ', this.state);
					this._save();
				});
			}
		});
	}
	_save() {
		// console.log(JSON.stringify(this.state));
		let url = '/api/reception/registration/add';
		fpostArray(url, this.state)
			.then(res => res.json())
			.then((res) => {
				if (!res.success) {
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res.result);
			})
			.then((result) => {
				// console.log(result);
				let signData = window.store.session('signData');
				let {
					id, //潜客id，或者学员id，根据type区分
					type, //1,是学员,2潜客
					signType, //1,单报，2，连报
					school //校区id
				} = signData;
				message.success('恭喜您,报名成功!');
				let registrationId = result.id;
				let schoolAreaId = result.schoolAreaId;
				let url = "/front/students";
				if (signType == '1') {
					url = `/print?registrationId=${registrationId}&schoolAreaId=${schoolAreaId}&type=3`;
				} else if (signType == '2') {
					url = `/print?registrationId=${registrationId}&schoolAreaId=${schoolAreaId}&type=4`;
				}
				window.location.href = url;
				// window.location.href = "/front/students";
			})
			.catch((err) => {
				console.log(err);
			});
	}
	_init() {
		let signData = window.store.session('signData');
		if (!signData) {
			return;
		}
		let {
			type,
			signType,
			singleIds,
			id,
			discount,
			many
		} = signData;
		let {
			studentVO
		} = this.state;

		let manyIds = many && many.courseIds;
		let courseIds = signType == '1' ? singleIds : manyIds;
		courseIds = courseIds.join(',');

		console.log('signData:', signData);
		let param = {
			courseIds,
			id,
			type
		}

		fpost('/api/reception/registration/add/init', param)
			.then(res => res.json())
			.then((res) => {
				if (!res.success) {
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res.result);
			})
			.then((result) => {
				let {
					courseVOList
				} = result;
				courseVOList.map((d, i) => {
					d.key = i;
				});
				this.setState({
					result,
					studentVO: {
						...result.studentVO,
						...studentVO
					}
				});
			})
			.catch((err) => {
				console.log(err);
			});
	}
	_getPayData(v) { //支付信息
		let {
			registrationVO
		} = this.state;
		let registrationPayTypeVOList;
		registrationPayTypeVOList = (
			v.trades &&
			v.trades[0] &&
			v.trades[0].tradeAmount.length
		) ? v.trades : [];

		registrationVO.registrationPayTypeVOList = registrationPayTypeVOList;

		this.setState({
			registrationVO
		}, () => {
			console.log('支付信息', this.state);
		});
	}
}