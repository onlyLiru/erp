import React, {
	Component
} from 'react';
import {
	Tabs,
	Button,
	Col,
	Row,
	Breadcrumb,
	Input,
	message
} from 'antd';
import {
	default as SignUpSingle
} from './single/main.js';
import {
	default as SignUpMany
} from './many/main.js';
import SelectSchool from '../../../common/selectSchool.js';
import {
	getUrlParam
} from '../../../common/g.js';
import './main.less';

const TabPane = Tabs.TabPane;


export default class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			id: getUrlParam('id') || '', //是学员就是学员id，是潜客就是潜客id，空就是直接报名
			type: getUrlParam('type') || '', //1是学员，2是潜客空是直接报名
			signType: "1", //1是单报，2是联报
			single: null, //单报数据
			many: null, //连报数据
			discount: null, //折扣
			school: null, //校区
			isChangeClass: getUrlParam('isChangeClass') || '', //是否是转班,1是,其他不是
			registrationCourseId: getUrlParam('registrationCourseId') || '', //转出班的id
		}
	}
	render() {
		let {
			isChangeClass
		} = this.state;

		return (<section>
			<Breadcrumb className="f-mb3">
			    <Breadcrumb.Item>前台业务</Breadcrumb.Item>
			    <Breadcrumb.Item>
			   		<span className="f-fz5 f-bold">报名</span>
			    </Breadcrumb.Item>
			</Breadcrumb>

			<Row gutter={16} type="flex" style={{minHeight:'600px'}}>
				<Col span={6}>
					<header className="f-bg-blue f-relative" style={{height:'100%'}}>
						<h3 className="f-align-center f-white f-lh7">课程报名流程</h3>
						<div className="f-relative f-pd4 ">
							<img style={{width:'100%'}} src="http://img.fancyedu.com/sys/ic/operation/1515723370065_sm.png" />
						</div>
						<img style={{width:'100%',position:'absolute',bottom:'0'}} src="http://img.fancyedu.com/sys/ic/operation/1515246332465_b.png" />
					</header>
				</Col>
				<Col span={18}>
					<header className="f-box-shadow2 f-radius1 f-bg-white f-pd4 f-relative mybox">
						<Tabs 
							tabBarExtraContent={ 
								<div>
									<span className="ant-form-item-required f-mr2">校区:</span>
									<SelectSchool 
										onSelect={ 
											this._selectSchool.bind(this) 
										} 
									/>
								</div>
							}
							defaultActiveKey={this.state.signType}
							onChange={
								(key)=> {
									this.setState({
										signType:key
									});
								}
							}
						>
						    <TabPane tab="选择课程" key="1">
						    	<SignUpSingle 
						    		getData={ this._getSingleData.bind(this) }
						    	/>
						    </TabPane>
						    {
						    	isChangeClass != '1' ?
								    <TabPane tab="连报" key="2">
								    	<SignUpMany 
								    		getData={ this._getManyData.bind(this) }
								    	/>
								    </TabPane>
								: null
						    }
						</Tabs>
						
						<div className="bottom-box">
							{
								this.state.signType=='2-dev' ?
									<div className="f-mb5">
										<span className="f-mr2">折扣:</span> 
										<Input 
											onBlur={ this._discount.bind(this) }
											style={{width:'220px'}} 
											placeholder="请输入0.01至0.99区间的数字" 
										/>
									</div>
								: null
								
							}
							<Button
								className="f-mr2"
								size="large"
								onClick={
									()=> window.history.back()
								}
							>
								取消
							</Button>
							<Button
								type="primary"
								size="large"
								onClick={ this._nextStep.bind(this) }
							>
								下一步
							</Button>
						</div>

					</header>
				</Col>
			</Row>
		</section>);
	}
	_selectSchool(v) {
		console.log(v);
		this.setState({
			school: v
		});
	}
	_getSingleData(d) {
		let {
			single
		} = this.state;
		let courseIds = [];

		if (d && d.length) {
			d.forEach((data, index) => {
				courseIds.push(data.courseData.id);
			});
		}
		this.setState({
			single: d,
			singleIds: courseIds
		}, () => {
			// console.log('单报', d);
		});
	}
	_getManyData(d) {
		console.log('连报', d);
		this.setState({
			many: d
		});
	}
	_discount(e) {
		let v = e.target.value;
		this.setState({
			discount: v
		});
	}
	_nextStep() {
		console.log(this.state);
		let {
			signType,
			single,
			many,
			school,
			isChangeClass
		} = this.state;

		if (!school) {
			message.error('请选择校区');
			return;
		}

		window.store.session('signData', this.state);

		if (signType == '1') { //单报
			if (!single || !single.length) {
				message.error('请选择课程');
				return;
			}

			message.info('可以继续了-单报');
			// console.log(this.state);
			if (isChangeClass == '1') { //如果是转班则去转班页面，否则去报名第二步
				window.location.href = "/front/changeclass/form";
			} else {
				window.location.href = "/front/signup/step2";
			}
		} else if (signType == '2') { //连报
			if (!many || !many.courseIds || !many.courseIds.length) {
				message.error('请选择课程');
				return;
			}

			message.info('可以继续了-连报');
			window.location.href = "/front/signup/step2";
		}
	}
}