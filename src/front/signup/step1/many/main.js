import React, {
	Component
} from 'react';
import {
	Col,
	Row,
} from 'antd';
import SelectCourseType from '../../../../common/selectCourseType.js';
import DynmicForm from '../../../../common/dynamicForm.js';

export default class SignUpSingle extends Component {
	constructor(props) {
		super(props);

		this.state = {
			courseTypeId: null,
			courseIds: []
		}
	}
	render() {
		let {
			courseTypeId
		} = this.state;

		return (
			<div className="f-relative" style={{paddingBottom:'60px'}}>
				<Row gutter={24} className="f-mb5">
					<Col span={4} offset={2} className="f-align-right f-lh6">
						选择课程大类: 
					</Col>
					<Col span={12}>
						<SelectCourseType 
							width="300px"
							onSelect= {
								(v,name)=> {
									this._typeChange(v,name);
								}
							}
						/>
					</Col>
				</Row>
				{
					courseTypeId ?
						<Row gutter={24}>
							<Col  offset={2} span={4} className="f-align-right f-lh6">
								选择课程:
							</Col>
							<Col span={12}>
								<DynmicForm
									ref = { (form)=> this.dynmicForm = form  }
									courseTypeId={courseTypeId}
									onSelect= { this._selectCourse.bind(this) }
								/>
							</Col>
						</Row>
					: null
				}
			</div>
		);
	}
	_typeChange(v, name) {
		let {
			getData
		} = this.props;
		// console.log('courseTypeId', v);
		// console.log('courseTypeName', name);
		this.setState({
			courseTypeId: null,
			courseIds: null
		}, () => {
			this.setState({
				courseTypeId: v
			}, () => {
				getData(this.state);
			});
		});
	}
	_selectCourse(v) {
		let {
			getData
		} = this.props;
		let {
			courseIds
		} = this.state;

		courseIds = v;

		// console.log('courseIds', courseIds);

		this.setState({
			courseIds: courseIds
		}, () => {
			getData(this.state);
		});
	}
}