import React, {
	Component
} from 'react';
import {
	Button,
	Modal,
	Form,
	Icon
} from 'antd';
import SelectCourseType from '../../../../common/selectCourseType.js';
import SelectCourses from '../../../../common/selectCourses.js';
import {
	getUrlParam
} from '../../../../common/g.js';

const FormItem = Form.Item;

export default class SignUpSingle extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			singleData: [],
			visible: false
		}
	}
	render() {
		let {
			singleData,
			visible
		} = this.state;

		let Html = () => {
			if (singleData && singleData.length > 0) {
				return singleData.map((d, i) => {
					let {
						typeData,
						courseData
					} = d;

					return (<tr key={i}>
						<td>{typeData.name}</td>
						<td>{courseData.name}</td>
						<td>
							<Icon 
								type="delete"
								className="f-pointer" 
								onClick={ 
									()=> {
										this._remove.bind(this)(d)
									}
								}
							/>
						</td>
					</tr>);
				})
			} else {
				return (<tr>
					<td colSpan="3" className="f-pd5">
						您还没有添加课程
						<a 
						onClick={ this._onAdd.bind(this) }
						className="f-pd5">点击添加课程</a>
					</td>
				</tr>)
			}
		}

		return (
			<div>
				<div className="f-align-right f-mb2">
					<Button  
						size="large" 
						onClick={ this._onAdd.bind(this) }
					>添加课程</Button>
				</div>
				<table className="info-classes-table">
					<tbody>
						<tr>
							<th>课程大类名称</th>
							<th>课程名称</th>
							<th>操作</th>
						</tr>
						<Html />
					</tbody>
				</table>

				{
					visible ?
						<AddFormCreate
							ref = { (form)=> this.form=form }
							visible={ this.state.visible }
							onCreate={this._onValidator.bind(this)}
							onCancel= { this._onCancel.bind(this) }
						/>
					: null
				}

			</div>
		);
	}
	shouldComponentUpdate() {
		return true;
	}
	_onValidator() {
		const form = this.form;
		let isChangeClass = getUrlParam('isChangeClass');

		form.validateFields((err, values) => {
			if (err) {
				return;
			}

			let {
				singleData
			} = this.state;
			/*过滤掉重复的数据*/
			singleData = singleData.filter((d, i) => {
				return d.courseData.id != values.courseData.id
			});
			// console.log('Received values of form: ', values);
			// console.log('singleData: ', singleData);

			if (isChangeClass == 1) { //如果是转班，就只能选择一个课程
				singleData = [];
			};
			singleData.push(values);


			form.resetFields();

			this.setState({
				visible: false,
				singleData
			}, () => {
				/*把单包的数据提交给父组件*/
				this.props.getData(this.state.singleData);
			});
		});
	}
	_onAdd() {
		this.setState({
			visible: true
		});
	}
	_onCancel() {
		this.setState({
			visible: false
		});
	}
	_remove(obj) {
		let {
			singleData
		} = this.state;

		singleData = singleData.filter((d) => {
			return d.courseData.id != obj.courseData.id
		});
		console.log(singleData);
		this.setState({
			singleData
		}, () => {
			/*把单包的数据提交给父组件*/
			this.props.getData(this.state.singleData);
		});
	}
}


class AddForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			courseTypeId: null,
			courseIds: null
		}
	}
	render() {
		const {
			visible,
			onCancel,
			onCreate,
			form
		} = this.props;
		const {
			getFieldDecorator
		} = form;
		let {
			courseTypeId,
			courseIds
		} = this.state;

		return (
			<Modal
		        visible={visible}
		        title="选择课程"
		        okText="确定"
		        onCancel={ onCancel }
		        onOk={ onCreate }
		        width= '660px'
		    >
	        <Form layout="inline">
				<FormItem label="选择课程大类">
					{getFieldDecorator('typeData', {
						rules: [{ 
							required: true,
							message:'请选择课程大类'
						}],
					})(
						<SelectCourseType 
							width='200px'
							onSelect= {
								(id,name)=> {
									this._typeChange(id,name);
								}
							}
						/>
					)}
				</FormItem>
				{
					courseTypeId ?
						<FormItem label="选择课程">
							{getFieldDecorator('courseData', {
								rules: [{ 
									required: true,
									message:'请选择课程'
								}],
							})(
								<SelectCourses 
									width='200px'
									courseTypeId={courseTypeId}
									onSelect= {
										(id,name)=> {
											this.props.form.setFieldsValue({
												courseData:{id,name}
											});
										}
									}
								/>
							)}
						</FormItem>
					: null
				}
	        </Form>
	      </Modal>
		);
	}
	_typeChange(id, name) {
		let typeData = null;

		this.props.form.setFieldsValue({
			typeData
		});

		this.setState({
			courseTypeId: null,
			courseIds: null
		}, () => {
			setTimeout(() => {
				this.setState({
					courseTypeId: id
				});
			}, 100);

			if (id && name) {
				typeData = {
					id,
					name
				};

			} else {
				typeData = null
			}
			this.props.form.setFieldsValue({
				typeData
			});
		});
	}

}
const AddFormCreate = Form.create()(AddForm);