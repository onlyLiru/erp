import React, {
  Component
} from 'react';
import {
  Table,
  Icon,
} from 'antd';
import {
  Link
} from 'react-router-dom';

export default class Main extends Component {
  render() {

    let {
      data,
      loading,
      pagination,
      onChange
    } = this.props;

    return (
      <div className="f-box-shadow2 f-radius1 f-bg-white f-pd4 f-mt5">
        <h3 className="f-mb5">
          <span className="f-title-blue f-mt3">新生列表</span>
          <a
            href="/front/signup/step1"
            className="f-btn-green f-right">
            <Icon className="f-fz7 f-vertical-middle f-bold" type="plus" />新增报名
          </a>
        </h3>
        {
          data && data.length ?
            <Table
                columns={ columns }
                bordered
                dataSource={ data }
                scroll={{ x: 1626}}
                pagination={ false }
                loading={loading}
                onChange={onChange}
            />
          : 
            <div className="f-align-center">
              <h3 className="f-pale f-mb3">查不到学员信息</h3>
              <a
                href="/front/signup/step1"
                className="f-btn-green"
              >前往新生报名</a>
            </div>
        }
        
      </div>
    );
  }

}

const columns = [{
  title: '姓名（学号）',
  dataIndex: 'name',
  className: 'f-align-center',
  width: '160px',
  fixed: 'left',
  render: (value, row, index) => {
    let {
      name,
      stuNo
    } = row;
    let text = name;
    if (stuNo) {
      text = `${name} (${stuNo})`;
    }
    return <div 
      className="f-line1 f-over-hide"
      style={{
        width:'160px'
      }}
      title={text}
    >
      {text}
    </div>
  }
}, {
  title: '头像',
  dataIndex: 'headImgUrl',
  className: 'f-align-center',
  width: '76px',
  render: (value, row, index) => {
    let {
      headImgUrl
    } = row;
    return (<div className="f-list-avatar">
        <img src= {value}/> 
      </div>);
  }
}, {
  title: '性别',
  dataIndex: 'genderStr',
  className: 'f-align-center',
  width: '60px'
}, {
  title: '年龄',
  dataIndex: 'age',
  className: 'f-align-center',
  width: '60px'
}, {
  title: '生日',
  className: 'f-align-center',
  dataIndex: 'birthday',
  width: '200px'
}, {
  title: '手机号码',
  className: 'f-align-center',
  dataIndex: 'phone',
  width: '160px'
}, {
  title: '市场来源',
  className: 'f-align-center',
  dataIndex: 'sourceStr',
  width: '100px'
}, {
  title: '市场顾问(工号)',
  className: 'f-align-center',
  dataIndex: 'salesName',
  width: '150px',
}, {
  title: '操作人',
  className: 'f-align-center',
  dataIndex: 'operatorName',
  width: '100px'
}, {
  title: '操作时间',
  className: 'f-align-center',
  dataIndex: 'modifyTime',
  width: '200px'
}, {
  title: '操作',
  dataIndex: 'des',
  fixed: 'right',
  width: '200px',
  className: "f-align-center",
  render: (value, row, index) => {
    let {
      isStudent,
      id
    } = row;

    let txt = isStudent == 1 ? "续报" : "办报名"
    let url = isStudent == 1 ? `/front/students/courses?studentId=${id}` : `/market/client/viewCommunication?potentialStudentId=${id}`
    let txt1 = isStudent == 1 ? "课程信息" : "沟通记录"
    let url1 = isStudent == 1 ? `/front/signup/step1?type=1&id=${id}` : `/front/signup/step1?type=2&id=${id}`

    return (<div>
      <Link className="ant-btn f-radius3 f-ml2 f-white f-bg-green f-lh4" to={ url }>{txt1}</Link>
      <Link className="ant-btn f-radius3 f-ml2 f-white f-bg-green f-lh4" to={ url1 }>{txt}</Link>
    </div>);
  }
}];