import React, {
	Component
} from 'react';
import {
	message,
	Input,
	InputNumber,
	Button,
	Col,
	Row,
	Breadcrumb,
	Modal,
	Form,
	DatePicker,
	Select,
	Checkbox
} from 'antd';
import store from 'store2';

import moment from 'moment';
import {
	default as SelectPerson
} from '../../common/selectPerson.js';
import PayTypes from '../../common/payTypes.js';
import SelectSchool from '../../common/selectSchool.js';
import SelectGoods from '../../common/selectGoods.js';
import {
	fpostArray
} from '../../common/io.js';
import {
	getUnicodeParam
} from '../../common/g.js';

const FormItem = Form.Item;
const {
	TextArea
} = Input;

let SESSION = store.session();

export default class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			saveing: false,
			visibleSelectGoods: false, //是否显示选择物品弹窗
			goodsData: [], //已选择商品,
			goodsDataKeys: [], //默认商品id，用于默认选中

			isExternal: getUnicodeParam('id') ? '0' : '1', //是否外部订单(0-内部订单，1-外部订单)
			studentId: getUnicodeParam('id') || '', //学员ID
			totalAmount: 0, //实付总额
			receiver: getUnicodeParam('name'), //购买人姓名
			receivePhone: getUnicodeParam('phone'), //手机号码
			buyTime: null, //购买时间
			salesId: null, //销售员ID
			schoolAreaId: null, //校区ID
			trades: null, //支付方式
			tradesToal: 0, //输入金额的总数
			orderDetails: [], //订单详情
		}
	}
	render() {
		let {
			saveing,
			visibleSelectGoods,
			goodsData,
			schoolAreaId,
			trades,
			orderDetails,
			totalAmount
		} = this.state;

		totalAmount = totalAmount.toFixed(2);

		let {
			modifyMaterialPrice
		} = SESSION

		return (<div>

			<Breadcrumb className="f-mb3">
			    <Breadcrumb.Item>前台业务</Breadcrumb.Item>
			    <Breadcrumb.Item>
			   		<span className="f-fz5 f-bold">物品售卖</span>
			    </Breadcrumb.Item>
			</Breadcrumb>

			<div className="f-box-shadow2 f-radius1 f-over-hide f-bg-white f-mt5">
		        <div className="f-pd5 f-right-title-box">
			        <div className="f-bg-blue f-right-title">
	        			<h3>购买信息</h3>
	        		</div>
	        		<div className="f-pd3" />
					<MyForm
						ref={form => this.form=form}
						getSelectPerson= { 
							this._getSelectPerson.bind(this) 
						}
					/>
					<div className="f-pd3" />
		        </div>
			</div>

			<div className="f-box-shadow2 f-radius1 f-over-hide f-bg-white f-mt5">
		        <div style={{minHeight:'160px'}} className="f-pd5 f-right-title-box">
			        <div className="f-bg-blue f-right-title">
	        			<h3>物品信息</h3>
	        		</div>

        			<span className="f-mr5">校区:</span> 
					<SelectSchool 
						onSelect={ this._selectSchool.bind(this) } 
					/>

					<Button
						className="f-ml5"
						size="large"
						disabled={ schoolAreaId ? false : true}
						type="primary"
						onClick={ ()=> {
							this.setState({
								visibleSelectGoods:true
							});
						} }
					>添加物品</Button>
					{
						!schoolAreaId ?
							<span className="f-ml2 f-pale">请先选择校区</span>
						: null
					}

					<div className="f-pt5">
						{
							(goodsData && goodsData.length) ?
								<ul className="f-lh7 my-goods">
									{
										goodsData.map((d,i)=> {
											return (<li key={i}>
												<span className="f-mr5">{d.materialName}</span>
												{
													modifyMaterialPrice ?
														<span className="f-mr5">
															<InputNumber 
																formatter={
																	(value)=> {
																		if(/^\d*(\.\d{0,2})?$/.test(value)){
																			return `¥ ${value}`
																		}else{
																			value = parseFloat(value);
																			value = Math.round(value * 100) / 100;
																			return `¥ ${value}`;
																		};
																	}
																} 
																style={{ width:'100px' }} 
																size="large" 
																value={d.salePrice}
																min={0}
																onChange={ (v)=> this._onGoodsPriceChange(v,d) }
															/>
														</span>
													: <span className="f-mr5">{`¥ ${d.salePrice}`}</span>
												}
												<span className="f-mr5">
													<InputNumber 
														formatter={
															(value) => {
																value = value ? parseInt(value) : 0;
																return `✖️ ${value}`
															}
														} 
														style={{ width:'100px' }} 
														size="large" 
														value={d.count}
														defaultValue={1}
														min={1}
														onChange={ (v)=> this._onGoodsCountChange(v,d) }
													/>
												</span>
												<span>
													<Checkbox 
														checked={d.isTaken}
														onChange={ (e)=> this._isGet(e,d) }
													>已领取</Checkbox>
												</span>
											</li>);
										})
									}
								</ul>
							: null
						}
					</div>

					{/*选择物品*/}
					{
						visibleSelectGoods ?
							<SelectGoods 
								visible={ this.state.visibleSelectGoods }
								handleOk= { this._selectGoodsOk.bind(this) }
								handleCancel= { this._selectGoodsCancel.bind(this) }
								goodsDataKeys= { this.state.goodsDataKeys }
								id= { schoolAreaId }
							/>
						: null
					}

		        </div>
			</div>

			<div className="f-box-shadow2 f-radius1 f-over-hide f-bg-white f-mt5">
		        <div className="f-pd5 f-right-title-box">
			        <div className="f-bg-green f-right-title">
	        			<h3>费用信息</h3>
	        		</div>
	        		<Row gutter={16} className="f-mb5 f-lh6">
	        			<Col span={2} className="f-align-right">总计: </Col>
	        			<Col span={20}><i className="f-fz5 f-red">¥{totalAmount}</i></Col>
	        		</Row>
	        		<Row gutter={16}>
	        			<Col span={2} className="f-align-right f-lh6">支付方式: </Col>
	        			<Col span={20}>
	        				<PayTypes 
	        					onInput = { this._getPayInfo.bind(this) }
	        				/>
	        			</Col>
	        		</Row>
		        </div>
			</div>

			<div className="f-align-right f-mt5">
				<Button size="large" 
					className="f-mr5"
					onClick={
						()=> {
							window.location.href ="/front/salelist";
						}
					}
					>
					取消
				</Button>
				<Button
					onClick= { this._onSubmit.bind(this) }
					type="primary"
					loading={saveing}
				>
					保存
				</Button>
			</div>

		</div>);
	}
	_onSubmit(e) {
		let state = this.state;
		e.preventDefault();
		this.form.validateFields((err, values) => {
			if (!err) {
				console.log('Received values of form: ', values);
				values.buyTime = moment(values.buyTime).format('YYYY-MM-DD HH:mm:ss');
				state = {
					...state,
					...values
				};
				this.setState(state, () => {
					this._formatGoods();
				});
			}
		});
	}
	_formatGoods() {
		let {
			goodsData
		} = this.state;
		let orderDetails = [];
		let totalAmount = 0;

		goodsData.forEach((d, i) => {
			let entityId = d.materialId;
			let entityName = d.materialName;
			let unitPrice = d.salePrice;
			let qty = d.count;
			let totalAmount = unitPrice * qty;
			let isTaken = d.isTaken;

			orderDetails.push({
				entityId,
				entityName,
				unitPrice,
				unitPrice,
				qty,
				totalAmount,
				isTaken
			});
		});

		orderDetails.forEach((d, i) => {
			totalAmount += Number(d.totalAmount);
		});

		this.setState({
			totalAmount,
			orderDetails
		}, () => {
			this._confirm();
		});
	}
	_confirm() {
		let param = this.state;
		let {
			orderDetails,
			trades,
			tradesToal,
			totalAmount,
			receivePhone,
			receiver,
			isExternal
		} = param;

		console.log(param);
		if (!orderDetails.length) {
			Modal.error({
				title: '温馨提醒',
				content: '请选择物品',
			});
			return;
		} else if (!trades) {
			Modal.error({
				title: '温馨提醒',
				content: '请选择支付方式',
			});
			return;
		}
		// else if(tradesToal !== totalAmount) {
		// 	Modal.error({
		// 		title: '温馨提醒',
		// 		content: '请选择支付方式',
		// 	});
		// 	return;
		// }

		this.setState({
			saveing: true
		});
		fpostArray('/api/reception/order/saveOrder', param)
			.then(res => res.json())
			.then((res) => {
				if (!res.success) {
					this.setState({
						saveing: false
					});
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res);
			})
			.then((res) => {
				this.setState({
					saveing: false
				});
				// console.log(res);
				// alert('ok');
				// window.history.back();

				let {
					orderInfoId,
					showPrint
				} = res.result;

				if (showPrint) {
					window.location.href = `/print?orderInfoId=${orderInfoId}&type=2`
				} else {
					if (isExternal == '1') {
						window.location.href = `/front/clientsaleist?name=${receiver}&phone=${receivePhone}`
					} else {
						window.location.href = `/front/studentsaleist?name=${receiver}&phone=${receivePhone}`
					}
				}

			})
			.catch((err) => {
				this.setState({
					saveing: false
				});
				console.log(err);
			});
	}
	_getSelectPerson(v) { //获取选择的销售员
		v = v ? v : null;
		this.form.setFieldsValue({
			salesId: v
		});
	}
	_getPayInfo(v) {
		// console.log('支付信息', v);
		this.setState({
			trades: v.trades,
			tradesToal: v.total
		}, () => {
			console.log(this.state);
		});
	}
	_selectSchool(v) {
		// console.log(v);
		this.setState({
			schoolAreaId: v
		});
	}
	_getTotalAmount() {
		let {
			goodsData
		} = this.state;
		let totalAmount = 0;
		goodsData.forEach((d) => {
			let {
				salePrice,
				count = 1
			} = d;
			totalAmount += salePrice * count;
		});

		this.setState({
			totalAmount
		});
	}
	_selectGoodsOk(v) { //已选择物品
		if (!v || !v.length) {
			this.setState({
				goodsData: [],
				visibleSelectGoods: false
			});
			return;
		}
		let goodsDataKeys = [];
		v.forEach((d) => {
			goodsDataKeys.push(d.materialId);
			d.count = 1;
			d.isTaken = 1;
		});


		this.setState({
			goodsData: v,
			goodsDataKeys,
			visibleSelectGoods: false
		}, () => {
			this._getTotalAmount();
			console.log('已选择物品:', this.state.goodsData);
		});
	}
	_selectGoodsCancel() { //取消选择物品
		this.setState({
			visibleSelectGoods: false
		});
	}
	_onGoodsPriceChange(v, d) { //当改变物品价格
		if (/^\d*(\.\d{0,2})?$/.test(v)) {
			v = v;
		} else {
			v = parseFloat(v);
			v = Math.round(v * 100) / 100;
		};

		console.log(v);
		let curPrice = v;
		if (!curPrice || isNaN(curPrice) || curPrice < 0) {
			message.error('价格输入有误');
			return;
		};
		let materialId = d.materialId;
		let {
			goodsData
		} = this.state;
		let index = goodsData.findIndex((obj) => {
			return obj.materialId == materialId;
		});

		goodsData[index].salePrice = curPrice;

		this.setState({
			goodsData
		}, () => {
			this._getTotalAmount();
			console.log('当改变物品价格', this.state.goodsData)
		});

	}
	_onGoodsCountChange(v, d) { //改变物品数量
		v = v ? parseInt(v) : 0;

		let curCount = v;
		if (!curCount || isNaN(curCount) || curCount < 0) {
			message.error('数量输入有误');
			return;
		};
		let materialId = d.materialId;
		let {
			goodsData
		} = this.state;
		let index = goodsData.findIndex((obj) => {
			return obj.materialId == materialId;
		});

		goodsData[index].count = curCount;

		this.setState({
			goodsData
		}, () => {
			this._getTotalAmount();
			console.log('数量改变后', this.state.goodsData);
		});
	}
	_isGet(e, d) {
		let isTaken = e.target.checked ? 1 : 0;
		let materialId = d.materialId;
		let {
			goodsData
		} = this.state;

		let index = goodsData.findIndex((obj) => {
			return obj.materialId == materialId;
		});

		goodsData[index].isTaken = isTaken;

		this.setState({
			goodsData
		}, () => {
			this._getTotalAmount();
			console.log('取消已领取后', this.state.goodsData);
		});
	}
}

/*搜索表单*/
class MainForm extends React.Component {
	render() {
		const {
			getFieldDecorator
		} = this.props.form;

		let name = getUnicodeParam('name') || '';
		let phone = getUnicodeParam('phone') || '';

		const formItemLayout = {
			labelCol: {
				span: 5
			},
			wrapperCol: {
				span: 18
			}
		};

		return (
			<Form>
				<Row>
					<Col span = {12} >
						<FormItem
							label="购买人姓名"
							{...formItemLayout}
						>
							{
								getFieldDecorator('receiver', { 
									rules: [{
										required: true, 
										message: '请输入姓名' 
									}],
									initialValue:name
								})
								(	
									<Input placeholder="请输入姓名" />
								)
							}
						</FormItem>
						<FormItem
							label="手机号码"
							{...formItemLayout}
						>
							{
								getFieldDecorator('receivePhone', { 
									rules: [{
										required: true, 
										message: '请输入手机号码',
									},{
										pattern: /(^1[0-9]\d{4,9}$)|(^(\d{2,4}-?)?\d{7,8}$)/,
										message:'电话号码格式有误'
									}],
									initialValue:phone
								})
								(
									<Input placeholder="请输入手机号码" />
								)
							}
						</FormItem>
					</Col>
					<Col span = {12} >
						<FormItem
							label="购买时间"
							{...formItemLayout}
						>
							{
								getFieldDecorator('buyTime', { 
									rules: [{
										required: true, 
										message: '请输入购买时间' 
									}],
									initialValue:moment()
								})
								(
									<DatePicker
										style={{width:'100%'}}
							      		format="YYYY-MM-DD HH:mm:ss"
							      		showTime={{ defaultValue: moment('00:00:00', 'HH:mm:ss') }}
								    />
								)
							}
						</FormItem>

						<FormItem
							label="销售员"
							{...formItemLayout}
						>
							{
								getFieldDecorator('salesId', {
									rules: [{  
										required:true,
										message:'请选择销售员'
									}]
								})(
									<SelectPerson
										url='/api/hr/staff/listStaffByCondForDropDown'
										data={
											{ 
												type:1
											}
										} 
										onSelect={ this.props.getSelectPerson }
										width='100%'
									/>
								)
							}
						</FormItem>
					</Col>
					<Col span ={12}>
						<FormItem
						    label="对内备注"
						    {...formItemLayout}
						>
						    {
						        getFieldDecorator('internalRemark', { 
						            rules: [{
						                required: false, 
						            }] 
						        })
						        (
						            <TextArea 
						            	rows={2} 
						            />
						        )
						    }
						</FormItem>
					</Col>
					<Col span ={12}>
						<FormItem
						    label="对外备注"
						    {...formItemLayout}
						>
						    {
						        getFieldDecorator('foreignRemark', { 
						            rules: [{
						                required: false, 
						            }] 
						        })
						        (
						            <TextArea 
						            	rows={2} 
						            />
						        )
						    }
						</FormItem>
					</Col>
				</Row>
	      	</Form>
		);
	}
}

const MyForm = Form.create()(MainForm);