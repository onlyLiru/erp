import React, {
	Component
} from 'react';
import {
	Breadcrumb,
	Table,
	Row,
	Col,
	Form,
	message
} from 'antd';
import {
	fpost
} from '../../../common/io.js';
import {
	getUrlParam
} from '../../../common/g.js';
import $ from 'jquery';
const u = require('../../../common/io.js');
export class Main extends Component {
	constructor(props) {
		super(props)
		this.state = {
			currentPage: 1,
			pageSize: 10,
			pageResult: null, //分页返回的列表总信息
			total: null,
			infoVO: {}, //头部信息
			studentSignUpList: [], //列表信息
		}
	}
	componentDidMount() {
		this._getDetail(); //获取页面详情
		if (getUrlParam('registrationCourse')) {
			$('.courseList').html('报课列表');
		}
	}
	_getDetail() {
		const self = this;
		fpost('/api/reception/student/course/getSignUpInfoByRegCourseId', {
				id: getUrlParam('id'),
			})
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					res.result.studentSignUpList.forEach((item, i) => {
						item.key = i;
					});
					self.setState({
						infoVO: res.result.infoVO,
						studentSignUpList: res.result.studentSignUpList
					});
					let html = '';
					let str = $('.truncate').html().substring(0, 10);
					if ($('.truncate').html().length > 10) {
						html = str + '...';
					} else {
						html = str;
					}
					$('.truncate').html(html);
				} else {
					message.error(res.message); //错误提示信息
				}
			});
	}
	render() {
		const columns = [{
			title: '序号',
			key: '0',
			width: 150,
			fixed: 'left',
			className: 'f-align-center',
			render: (text, record, index) => {
				return (
					<div>{index+1}</div>
				)
			}
		}, {
			title: '课次',
			key: '1',
			width: 150,
			dataIndex: 'classTimes',
			className: 'f-align-center',
		}, {
			title: '课次状态',
			key: '2',
			width: 150,
			className: 'f-align-center',
			render: (text, record, index) => {
				return (
					<div>
						{record.timesDetailStatus=='未记上课'?<i style={{color:'#FF3750'}}>未记上课</i>:record.timesDetailStatus}
					</div>
				)
			}
		}, {
			title: '上课时间',
			key: '3',
			width: 180,
			className: 'f-align-center',
			dataIndex: 'classStartEndTime'
		}, {
			title: '课时',
			key: '4',
			width: 150,
			dataIndex: 'period',
			className: 'f-align-center',
		}, {
			title: '教室',
			key: '5',
			className: 'f-align-center',
			width: 150,
			dataIndex: 'classRoomName'
		}, {
			title: '上课老师',
			dataIndex: 'teacherName',
			key: '6',
			width: 150,
			className: 'f-align-center',
		}, {
			title: '考勤情况',
			key: '7',
			width: 150,
			className: 'f-align-center',
			render: (text, record, index) => {
				return (
					<div>
						{record.signUpStatusTxt==1?<i style={{color:'#41A99D'}}>上课</i>:''}
						{record.signUpStatusTxt==2?<i style={{color:'#FF8500'}}>请假</i>:''}
						{record.signUpStatusTxt==4?<i style={{color:'#2187FF'}}>补课</i>:''}
						{record.signUpStatusTxt==3?<i style={{color:'#FF3750'}}>旷课</i>:''}
					</div>
				)
			}
		}, {
			title: '纪律专注',
			dataIndex: 'disciplineFocus',
			key: '8',
			width: 150,
			className: 'f-align-center',
		}, {
			title: '活跃参与',
			dataIndex: 'active',
			key: '9',
			width: 150,
			className: 'f-align-center',
		}, {
			title: '上课内容',
			dataIndex: 'classContent',
			key: '10',
			width: 150,
			className: 'f-align-center',
		}, {
			title: '课后作业',
			dataIndex: 'homework',
			key: '11',
			width: 150,
			className: 'f-align-center',
		}, {
			title: '备注',
			key: '15',
			fixed: 'right',
			className: 'f-align-center',
			width: 150,
			dataIndex: 'remark'
		}];
		return (<div className='courseArranging classListPage'>
				<Breadcrumb className="f-mb3">
				    <Breadcrumb.Item>前台业务</Breadcrumb.Item>
				    <Breadcrumb.Item>学员管理</Breadcrumb.Item>
				    <Breadcrumb.Item>学员列表</Breadcrumb.Item>
				    <Breadcrumb.Item>课程列表</Breadcrumb.Item>
				    <Breadcrumb.Item>
				   		<span className="f-fz5 f-bold">上课记录</span>
				    </Breadcrumb.Item>
				</Breadcrumb>
				<div className='f-bg-white f-pd4 f-box-shadow1 f-mt4 f-radius1 f-mb4'>
					<Row className='f-mb5'>
						<Col span={4} className='f-align-left'>
							<div className='f-mb1 f-fz2 f-gray'>班级名称</div>
							<div className='f-fz4 f-black f-bold truncate' title={this.state.infoVO.className}>{this.state.infoVO.className}</div>
						</Col>
						<Col span={4} className='f-align-left'>
							<div className='f-mb1 f-fz2 f-gray'>校区</div>
							<div className='f-fz4 f-black f-bold'>{this.state.infoVO.schoolAreaName}</div>
						</Col>
						<Col span={4} className='f-align-left'>
							<div className='f-mb1 f-fz2 f-gray'>上课老师</div>
							<div className='f-fz4 f-black f-bold'>{this.state.infoVO.teacherName}</div>
						</Col>
						<Col span={4} className='f-align-left'>
							<div className='f-mb1 f-fz2 f-gray'>课次</div>
							<div className='f-fz4 f-black f-bold'>
								{this.state.infoVO.courseTimes}课次
							</div>
						</Col>
						<Col span={4} className='f-align-left'>
							<div className='f-mb1 f-fz2 f-gray'>班级状态</div>
							<div className='f-fz4 f-bold' style={{color:'#00C9B9'}}>{this.state.infoVO.classStateStr?this.state.infoVO.classStateStr:'暂无'}</div>
						</Col>
					</Row>
				</div>
				<div className='f-bg-white f-pd4 f-box-shadow1 f-radius1 f-mb6'>
					<div className='f-flex' style={{justifyContent: 'space-between'}}>
						<h3 className="f-title-blue f-mb3">排课信息</h3>
					</div>
					<Table columns={columns} dataSource={this.state.studentSignUpList} scroll={{ x: 1970}} bordered pagination={false}/>
				</div>
		</div>);
	}
}
const SingRecord = Form.create()(Main);
export {
	SingRecord
}