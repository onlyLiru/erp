import React, {
	Component
} from 'react';
import {
	Breadcrumb,
} from 'antd';
import {
	default as Mylist
} from './list.js';
import './main.less';

export default class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {

		}
		console.log(props);
	}
	render() {
		return (<section>
			<Breadcrumb className="f-mb3">
			    <Breadcrumb.Item>前台业务</Breadcrumb.Item>
			    <Breadcrumb.Item>
			   		<span className="f-fz5 f-bold">学员王小仙的售前沟通日志</span>
			    </Breadcrumb.Item>
			</Breadcrumb>

			<Mylist />

		</section>);
	}
	componentDidMount() {

	}
}