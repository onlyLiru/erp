import React, {
	Component
} from 'react';
import {
	Table,
	Icon,
} from 'antd';

export default class Main extends Component {
	render() {
		let {
			onEdit,
			loading,
			data,
			pagination,
			onChange
		} = this.props;


		const columns = [{
			title: '沟通时间',
			width: '150px',
			className: 'f-align-center',
			dataIndex: 'connectTime'
		}, {
			title: '沟通方式',
			width: '150px',
			className: 'f-align-center',
			dataIndex: 'connectWayStr'
		}, {
			title: '问题反馈',
			className: 'f-align-center',
			width: '400px',
			dataIndex: 'question'
		}, {
			title: '处理结果',
			className: 'f-align-center',
			dataIndex: 'answer'
		}, {
			title: '操作',
			width: '50px',
			dataIndex: 'dess',
			className: "f-align-center",
			render: (value, row, index) => {
				return (<div>
                    <Icon 
                        className="f-pointer" 
                        type="edit" 
                        onClick={ ()=> onEdit(row) }
                    />
                </div>);
			}
		}];

		return (
			<Table
                columns={ columns }
                dataSource={ data }
                pagination={ pagination }
                loading={loading}
                onChange={onChange}
                bordered
            />
		);
	}
}