import React, {
	Component
} from 'react';
import {
	Icon,
	Breadcrumb,
	message,
	Row,
	Col
} from 'antd';
import {
	default as MyList
} from './list.js';
import {
	default as AddForm
} from './add.js';
import {
	fpost
} from '../../../common/io.js';
import {
	getUrlParam,
	getUnicodeParam
} from '../../../common/g.js';

export default class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			courseId: getUrlParam('id'),
			courseName: getUnicodeParam('courseName'),
			className: getUnicodeParam('className'),

			visible: false,
			visible: false,
			loading: false,
			result: null,
			defaultData: null,

			param: {
				registrationCourseId: getUrlParam('id'),
			},
			pagination: {
				showSizeChanger: true,
				showQuickJumper: true,
				total: 1,
			},
			pageSize: 10,
			currentPage: 1,
			total: 1,
		}
	}
	render() {
		let {
			visible,
			result,
			loading,
			pagination,
			defaultData,
			courseName,
			className
		} = this.state;

		return (<section>
			<Breadcrumb className="f-mb3">
			    <Breadcrumb.Item>前台业务</Breadcrumb.Item>
			    <Breadcrumb.Item>学员管理</Breadcrumb.Item>
			    <Breadcrumb.Item>学员列表</Breadcrumb.Item>
			    <Breadcrumb.Item>课程列表</Breadcrumb.Item>
			    <Breadcrumb.Item>
			   		<span className="f-fz5 f-bold">{`${getUnicodeParam('name')}的售后沟通日志`}</span>
			    </Breadcrumb.Item>
			</Breadcrumb>

			<div className="f-box-shadow2 f-radius1 f-bg-white f-pd4 f-mt5 f-over-hide">
				<Row gutter={24} type="flex">
					<Col>
						<p className="f-mb2">课程名称	</p>
						<h3>{courseName}</h3>
					</Col>
					<Col>
						<p className="f-mb2">班级名称	</p>
						<h3>{className}</h3>
					</Col>
				</Row>
			</div>

			<div className="f-box-shadow2 f-radius1 f-bg-white f-pd4 f-mt5">
				<h3 className="f-mb5">
					<span className="f-title-blue f-mt3">售后沟通日志</span>
					<a
						onClick={ 
							this._visibleAddModal.bind(this)
						}
						className="f-btn-green f-right"
					>
						<Icon 
							className="f-fz7 f-vertical-middle f-bold" 
							type="plus" 
						/>
						新增售后沟通
					</a>
				</h3>
				<MyList 
					onEdit={this._edit.bind(this)}
					onDelete={this._delete.bind(this)}
					data={result}
			        loading={loading}
			        onChange={this._getList.bind(this)}
			        pagination={pagination}
				/>
			</div>


			{
				visible ? 
					<AddForm
						ref={ form => this.addForm = form }
						visible={ visible }
						onSaveOk={ this._saveOk.bind(this)}
						onCancel={ this._cancel.bind(this)}
						registrationCourseId={this.state.courseId}
						defaultData={ defaultData }
					/>
				: null
			}

		</section>);
	}
	componentDidMount() {
		this._getList();
	}
	_saveOk() {
		this._cancel();
		this._getList();
	}
	_cancel() {
		this.setState({
			visible: false,
			defaultData: null
		});
	}
	_visibleAddModal() {
		this.setState({
			visible: true
		});
	}
	_delete(param) {
		fpost('/api/system/auth/deleteRole', param)
			.then(res => res.json())
			.then((res) => {
				if (!res.success) {
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res);
			})
			.then((res) => {
				this._getList();
			})
			.catch((err) => {
				console.log(err);
			});
	}
	_edit(param) {
		let {
			id
		} = param;

		this.setState({
			loading: true
		});

		fpost('/api/reception/student/connect/findStudentConnect', {
				id
			})
			.then(res => res.json())
			.then((res) => {
				if (!res.success) {
					this.setState({
						loading: false
					});
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res);
			})
			.then((res) => {
				this.setState({
					visible: true,
					loading: false,
					defaultData: res.result
				});
			})
			.catch((err) => {
				this.setState({
					loading: false
				});
				console.log(err);
			});
	}
	_getList(pager = {}) {
		this.setState({
			loading: true
		});
		let {
			pageSize,
			currentPage,
			pagination,
			param
		} = this.state;
		pageSize = pager.pageSize || pageSize;
		currentPage = pager.current || currentPage;

		param.pageSize = pageSize;
		param.currentPage = currentPage;

		fpost('/api/reception/student/connect/pageStudentConnectByRegCourseId', param)
			.then(res => res.json())
			.then((res) => {
				if (!res.success || !res.result || !res.result.records) {
					this.setState({
						loading: false
					});
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res.result);
			})
			.then((result) => {
				let data = result.records;
				data.forEach((d) => {
					d.key = d.id
				});

				let total = Number(result.total);

				this.setState({
					result: data,
					pagination: {
						...pagination,
						total: total,
						currentPage: currentPage,
						pageSize: pageSize
					},
					currentPage,
					pageSize,
					total,
					loading: false
				});
			})
			.catch((err) => {
				console.log(err);
			});
	}
}