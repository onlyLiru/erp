import React, {
	Component
} from 'react';
import {
	Form,
	Select,
	Input,
	DatePicker,
	Modal,
	message
} from 'antd';
import moment from 'moment';

import {
	fpost
} from '../../../common/io.js';
import {
	communicationList
} from '../../../common/staticData.js';

const FormItem = Form.Item;
const Option = Select.Option;

class MainForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			saveing: false
		}
	}
	render() {
		const {
			getFieldDecorator
		} = this.props.form;
		let {
			visible,
			onCancel,
			defaultData,
			registrationCourseId
		} = this.props;

		let {
			saveing
		} = this.state;

		const formItemLayout = {
			labelCol: {
				span: 5
			},
			wrapperCol: {
				span: 18
			}
		};
		let modalTitle = defaultData ? '编辑' : '新增';
		return (<Modal title={`${modalTitle}售后沟通`}
				visible={ visible }
				onOk={ this._ok.bind(this) }
				confirmLoading={ saveing }
				onCancel={ onCancel }
			>
				<Form>
					<FormItem
						label="沟通时间"
						{...formItemLayout}
					>
						{
							getFieldDecorator('connectTime', {
								rules: [{ 
									required: true, 
									message: '请填写沟通时间' 
								}]
							})(
								<DatePicker
									style={{width:'100%'}}
									showTime
									format="YYYY-MM-DD HH:mm:ss"
									placeholder="请填写沟通时间"
							    />
							)
						}
					</FormItem>

					<FormItem
						label="沟通方式"
						{...formItemLayout}
					>
						{
							getFieldDecorator('connectWay', {
								rules: [{  
									required:true,
									message: '请选择沟通方式' 
								}]
							})(
								<Select
									size="large"
									allowClear
                					placeholder="请选择沟通方式"
								>
									{
										communicationList.map((d,i)=> {
											return(<Option 
													key={i} 
													value={d.value}
												>{d.label}</Option>);
										})
									}
								</Select>
							)
						}
					</FormItem>

					<FormItem
						label="问题反馈"
						{...formItemLayout}
					>
						{
							getFieldDecorator('question', {
								rules: [{ 
									required: true, 
									message: '请输入问题反馈' 
								}]
							})(
								<Input.TextArea placeholder="请输入问题反馈" />
							)
						}
					</FormItem>

					<FormItem
						label="处理结果"
						{...formItemLayout}
					>
						{
							getFieldDecorator('answer', {
								rules: [{ 
									required: true, 
									message: '请输入处理结果' 
								}]
							})(
								<Input.TextArea placeholder="请输入处理结果" />
							)
						}
					</FormItem>
					{
						defaultData && defaultData.id ?
							<span className="f-hide">
								{
									getFieldDecorator('id')
									(
										<Input />
									)
								}
							</span>
						: null
					}
					{
						registrationCourseId ?
							<span className="f-hide">
								{
									getFieldDecorator('registrationCourseId',{
										initialValue:registrationCourseId
									})
									(
										<Input />
									)
								}
							</span>
						: null
					}
		      	</Form>
		</Modal>);
	}
	componentDidMount() {
		let {
			defaultData
		} = this.props;

		console.log(defaultData)

		if (defaultData) { //如果是编辑，设置默认值
			let {
				id,
				connectTime,
				connectWay,
				question,
				answer
			} = defaultData;

			connectTime = moment(connectTime, 'YYYY-MM-DD HH:mm:ss');

			this.props.form.setFieldsValue({
				id,
				connectTime,
				connectWay,
				question,
				answer
			});
		}

	}
	_ok(e) {
		e.preventDefault();
		this.props.form.validateFields((err, values) => {
			if (!err) {
				let {
					connectTime
				} = values;

				connectTime = moment(connectTime).format('YYYY-MM-DD HH:mm:ss')
				values.connectTime = connectTime;
				console.log(values);
				this._save(values);
			}
		});
	}
	_save(values) {
		this.setState({
			saveing: true
		});

		let {
			onSaveOk
		} = this.props;

		fpost('/api/reception/student/connect/saveStudentConnect', values)
			.then(res => res.json())
			.then((res) => {
				if (!res.success) {
					this.setState({
						saveing: false
					});
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res);
			})
			.then((res) => {
				this.setState({
					saveing: false
				}, () => {
					onSaveOk();
				});
			})
			.catch((err) => {
				this.setState({
					saveing: false
				});
				console.log(err);
			});
	}
}

const MyForm = Form.create()(MainForm);

export default MyForm;