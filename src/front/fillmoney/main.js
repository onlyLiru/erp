import React, {
	Component
} from 'react';
import {
	Route
} from 'react-router-dom';

import {
	default as Home
} from './index/main';
import {
	default as FormComPage
} from './form/main';
import './main.less';

let PARENTPATH;

export default class Main extends Component {
	constructor(props) {
		super(props);
		const {
			match
		} = props;
		PARENTPATH = match.path;
	}
	render() {
		return (<section>
			<Route path={ PARENTPATH } exact component={ Home } />
			<Route path={`${PARENTPATH }/index`} exact component={ Home } />
			<Route path={`${PARENTPATH }/form`} exact component={ FormComPage } />
		</section>);
	}
}