import React, {
	Component
} from 'react';
import {
	Cascader,
	message
} from 'antd';
import {
	fpost
} from './io.js';

export default class SelectCity extends Component {
	constructor(props) {
		super(props);

		this.state = {
			options: []
		}
	}
	render() {
		let {
			options
		} = this.state;


		let {
			initialValueData
		} = this.props;

		if (!options || !options.length) {
			return null;
		}

		return (
			<Cascader
				size="large"
				style={{background:'#f3f3f8'}}
			    options={ options }
			    onChange={ this.onChange.bind(this) }
			    placeholder="请选择省,市,区"
			    showSearch
			    defaultValue= { initialValueData }
			/>
		);
	}
	componentDidMount() {
		this._getList();
	}
	_getList() {
		fpost('/api/common/area/areaTree')
			.then(res => res.json())
			.then((res) => {
				if (!res.success || !res.result) {
					message.error(res.message || '系统错误');
					this.setState({
						loading: true
					});
					return;
				};
				return (res.result);
			})
			.then((result) => {
				if (result && result.length) {
					result.forEach((d) => {
						d.key = d.id
					});
				}
				// console.log(result);
				this.setState({
					options: result
				});
			});
	}
	onChange(value, selectedOptions) {
		let {
			onSelect
		} = this.props;
		// console.log(value);
		onSelect(value);
	}
}