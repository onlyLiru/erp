import React, {
    Component
} from 'react';
import {
    Menu,
    Icon,
    Row,
    Col,
    Avatar,
    Select,
    Checkbox,
    Input,
    message,
    Button
} from 'antd';
import $ from 'jquery';
import store from 'store2';
import {
    fpost
} from './io.js';
const Option = Select.Option;
const CheckboxGroup = Checkbox.Group;

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedKeys: store.session('tMenu') && store.session('tMenu').selectedKeys,
        }
    }

    render() {
        let menuTree = store.get('userInfo') && store.get('userInfo').menuTree;
        let name = store.get('userInfo') && store.get('userInfo').currentUser && store.get('userInfo').currentUser.name || '客';
        // name = name.slice(0, 1);
        if (name.length > 2) {
            if (/^[a-z]+$/.test(name)) {
                name = name.substring(name.length - 4, name.length); //英文名字过长取名字后四位进行展示
            } else {
                name = name.substring(name.length - 2, name.length); //中文名字过长取名字后两位进行展示
            }
        } else {
            name = name;
        }
        // console.log(menuTree);
        let {
            selectedKeys
        } = this.state;

        if (!menuTree) {
            return null;
        }

        return (
            <div className="f-header">
                <Row>
                    <Col span={18}>
                        <h1 className="logo f-left f-white f-align-center">
                            <img src="http://img.fancyedu.com/sys/ic/operation/1515725441853_lg.png" />
                            e校通
                        </h1>
                        <Menu
                            theme="dark"
                            mode="horizontal"
                            defaultSelectedKeys={[]}
                            selectedKeys={[selectedKeys]}
                            onClick={ this._selectMenu.bind(this) }
                            className="top-menu"
                        >
                            {
                                menuTree.map((d,i)=> {
                                    let {
                                        path,
                                        key,
                                        title
                                    } = d;
                                    return (<Menu.Item className="f-align-center" key={key}>
                                        <a href={path}>
                                            { key == 'qtyw' ? <i className="iconfont icon-yewu" style={{fontSize:'28px'}}/> : null }
                                            { key == 'jwjx' ? <i className="iconfont icon-jiaoxuepingliang" style={{fontSize:'29px'}}/> : null }
                                            { key == 'cwtj' ? <i className="iconfont icon-caiwu" style={{fontSize:'29px'}}/> : null }
                                            { key == 'xtsz' ? <i className="iconfont icon-xitongshezhi" style={{fontSize:'26px'}}/> : null }
                                            { key == 'scyy' ? <i className="iconfont icon-iconfontjingqingfenxi" style={{fontSize:'34px'}}/> : null }
                                            { key == 'rsgl' ? <i className="iconfont icon-renshiguanli" /> : null }
                                            { key == 'ckgl' ? <i className="iconfont icon--" /> : null }
                                            { key == 'shfw' ? <i className="iconfont icon-shouhou" /> : null }
                                            { key == 'jfpt' ? <i className="iconfont icon-mingshifudao" /> : null }
                                            {title}
                                        </a>
                                    </Menu.Item>);
                                })
                            }
                        </Menu>
                    </Col>
                    <Col span={6} className="f-align-right f-pr5 f-white f-header-right">
                        <div className="f-clearfix f- f-relative">
                            <SchoolAreaCom />
                            <div 
                                className="f-right f-relative f-align-left" 
                                style={{
                                    width:'140px',
                                    paddingLeft:'70px',
                                }}
                            >
                                <div 
                                    style={{
                                        width:'60px',
                                        height:'60px',
                                        position:'absolute',
                                        top:'16px',
                                        left:'0'
                                    }}
                                    className="f-align-center"
                                >
                                    <Avatar
                                        style={{
                                            width:'46px',
                                            height:'46px',
                                            paddingTop:'3px',
                                            background:'parment'
                                        }}
                                        className="f-round" 
                                        size="large"
                                    >{name}</Avatar>
                                </div>
                                <div className="f-line1">
                                    <span
                                        onClick={this._loginOut.bind(this)}
                                        className='logout f-pointer'
                                    >退出</span>
                                </div>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }
    _selectMenu({
        item,
        key,
        selectedKeys
    }) {
        this.setState({
            selectedKeys: key
        });
    }
    componentDidMount() {

    }
    _loginOut() {
        fpost('/logout')
            .then((res) => {
                return res.json();
            })
            .then((res) => {
                if (res.success == true) {
                    message.success('退出成功');
                    store.set('sessionId', null);
                    setTimeout(
                        window.location.href = '/login', 2000
                    );
                } else {
                    message.error(res.message)
                }
            });
    }
}
/*右上角校区*/
class SchoolAreaCom extends Component {
    constructor(props) {
        super(props)
        this.state = {
            result: [],
            selectedIds: [],
            options: [],
            indeterminate: true,
            checkAll: false,
        }
    }
    render() {
        let {
            myOptions,
            options,
            selectedIds
        } = this.state;

        let myName;
        if (selectedIds && selectedIds.length > 1) {
            myName = '多个校区';
        }
        // else if (myOptions && myOptions.length == 1) {
        //     myName = myOptions[0].label
        // };
        if (selectedIds && selectedIds.length == 1) {
            var theSchool = myOptions.find((d, i) => {
                return d.value == selectedIds[0];
            });
            myName = theSchool && theSchool.label || '暂无校区';
        }

        return (<div 
            className="f-left f-align-left f-line1 " 
            style={{width:'160px'}}
            id="J-top-school"
        >
            {
                (selectedIds && selectedIds.length >0) ?
                <span>
                    <Icon className="f-fz5 f-mr2" type="environment-o" />
                    <span 
                        className="f-relative" 
                        style={{top:'-2px'}}
                    >{myName}</span>
                </span>
                : <span>请选择校区</span>
            }
            <div 
                className="top-schools f-hide"
            >
                <Input 
                    style={{
                        borderColor:'#fff',
                        backgroundColor:'transparent',
                        color:'#fff'
                    }} 
                    className="f-mb3" 
                    placeholder="请输入校园名称"
                    onChange={this._search.bind(this)}
                />
                <div style={{maxHeight:'300px',minHeight:'30px',overflowY:'auto'}} >
                    { 
                        options && options.length ?
                        <CheckboxGroup 
                            options={options} 
                            value={selectedIds} 
                            onChange={this._onChange.bind(this)} 
                        />
                        : <span className="f-pale">没有找到...</span>
                    }
                </div>
                <div className="f-clear">
                    <Checkbox 
                        style={{
                            position:'relative',
                            top:'16px'
                        }}
                        indeterminate={this.state.indeterminate}
                        onChange={this._onCheckAllChange.bind(this)}
                        checked={this.state.checkAll}
                    >
                        全选
                    </Checkbox>
                    <Button
                        type="primary"
                        className="f-mt3 f-right"
                        onClick={ this._saveSelectSchool.bind(this) }
                    >确定</Button>
                </div>
            </div>
        </div>);
    }
    componentDidMount() {
        /*获取右上角校区信息*/
        this._getSchool();

        $('#J-top-school').hover(() => {
            $('.top-schools').removeClass('f-hide');
        }, () => {
            $('.top-schools').addClass('f-hide');
        });
    }
    _getSchool() {
        let url = '/api/system/schoolarea/findUserSchoolAreaInfo';
        fpost(url)
            .then(res => res.json())
            .then((res) => {
                if (!res.success || !res.result) {
                    message.error(res.message || '系统错误');
                    throw new Error(res.message || '系统错误');
                };
                return (res.result);
            })
            .then((result) => {
                this.setState({
                    result
                }, () => {
                    this._initData();
                });
            })
            .catch((err) => {
                console.log(err);
            });
    }
    _initData() {
        let {
            result
        } = this.state;
        let {
            userSchoolArea,
            selectedIds
        } = result;
        let options = [];
        selectedIds = selectedIds.split(',');

        userSchoolArea.map((d, i) => {
            let {
                name,
                id
            } = d;
            options.push({
                label: name,
                value: id
            });
        });
        this.setState({
            options,
            myOptions: options,
            selectedIds,
            checkAll: selectedIds.length == options.length,
            indeterminate: !(selectedIds.length == options.length)
        });
    }
    _search(e) {
        let value = e.target.value;
        let {
            options,
            myOptions
        } = this.state;


        if (value) {
            value = value.toLowerCase();
            options = myOptions.filter((d, i) => {
                let {
                    label
                } = d;
                label = label.toLowerCase();
                console.log(value);
                return label.includes(value) || label.includes(value) || label.includes(value)
            });
        } else {
            options = myOptions;
        }

        this.setState({
            options
        });
    }
    _onCheckAllChange(e) {
        let {
            options
        } = this.state;
        let selectedIds = [];
        options.map((d, i) => {
            selectedIds.push(d.value);
        });

        selectedIds = e.target.checked ? selectedIds : [];

        this.setState({
            selectedIds: selectedIds,
            indeterminate: false,
            checkAll: e.target.checked,
        }, () => {
            console.log(e.target.checked);
            console.log(this.state.selectedIds);
        });
    }
    _onChange(checkedValues) {
        let {
            options,
            selectedIds
        } = this.state;

        this.setState({
            selectedIds: checkedValues,
            indeterminate: !!checkedValues.length && (checkedValues.length < options.length),
            checkAll: checkedValues.length === options.length,
        }, () => {
            // console.log(this.state.selectedIds);
        });
    }
    _saveSelectSchool() {
        let {
            selectedIds
        } = this.state;

        selectedIds = selectedIds.join(',');

        let url = '/api/system/schoolarea/selectUserSchoolArea';
        fpost(url, {
                selectedIds
            })
            .then(res => res.json())
            .then((res) => {
                if (!res.success) {
                    message.error(res.message || '系统错误');
                    throw new Error(res.message || '系统错误');
                };
                return (res.result);
            })
            .then((result) => {
                // store.session.set('schoolAreas', selectedIds);
                window.location.reload();
            })
            .catch((err) => {
                console.log(err);
            });
    }
}