import React, {
    Component
} from 'react';
import {
    Select,
    Spin
} from 'antd';
import {
    fpost
} from './io.js';
const Option = Select.Option;

export default class SelectCourseType extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fetching: false,
            result: []
        }
    }
    render() {
        const {
            fetching,
            result
        } = this.state;
        let {
            initialValueData = '',
                width = '100%'
        } = this.props;

        if (!result.length) {
            return <Spin size="small" />;
        }

        return (
            <Select
                size="large"
                placeholder="请选择课程大类"
                style={{ width: width }}
                onChange={ this._handleChange.bind(this) }
                defaultValue= { initialValueData }
            >
                <Option key='' value=''>请选择</Option>
                {
                    result.map((d,i)=> {
                        return (<Option key={i} value={d.id}>{d.name}</Option>);
                    })
                }
            </Select>
        );
    }
    componentDidMount() {
        this._getData();
    }
    _handleChange(v) {
        let {
            result
        } = this.state;
        let myName;

        result.forEach((d, i) => {
            let {
                id,
                name
            } = d;

            if (v == id) {
                myName = name;
            }
        });

        // console.log(v);
        // console.log(myName);

        this.props.onSelect(v, myName);
    }
    _getData() {
        fpost('/api/system/course/listSubjectCourse')
            .then((res) => res.json())
            .then((res) => {
                if (!res.success || !res.result) {
                    // message.error(res.message || '系统错误');
                    throw new Error(res.message || '系统错误');
                };
                return (res.result);
            })
            .then((result) => {
                this.setState({
                    result
                });
            })
            .catch((err) => {
                console.log(err);
            });
    }
}