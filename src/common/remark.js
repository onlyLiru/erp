import React, {
	Component
} from 'react';
import {
	Row,
	Col,
	Input
} from 'antd';

const {
	TextArea
} = Input;

export default class Remark extends Component {
	constructor(props) {
		super(props);
		this.state = { in: '',
			out: '',
			internalRemark: '',
			foreignRemark: '',
		}
	}
	render() {

		return (<div>
			<Row className="f-mb5">
				<Col span={12}>
					<Row gutter={20}>
						<Col span={5} className="f-align-right f-lh7 f-black">对内备注:</Col>
						<Col span={19}>
							<TextArea
								value={this.state.in}
								onChange={ this._handleChangeIn.bind(this) }
								rows={4} 
							/>
						</Col>
					</Row>
				</Col>
				<Col span={12}>
					<Row gutter={20}>
						<Col span={5} className="f-align-right f-lh7 f-black">对外备注:</Col>
						<Col span={19}>
							<TextArea 
								value={ this.state.out }
								onChange={ this._handleChangeOut.bind(this) }
								rows={4} 
							/>
						</Col>
					</Row>
				</Col>
			</Row>
		</div>);
	}
	_handleChangeIn(e) {
		let v = e.target.value;
		let {
			onInput
		} = this.props;
		this.setState({ in: v,
			internalRemark: v
		}, () => {
			// console.log(this.state);
			onInput(this.state);
		});
	}
	_handleChangeOut(e) {
		let v = e.target.value;
		let {
			onInput
		} = this.props;
		this.setState({
			out: v,
			foreignRemark: v
		}, () => {
			// console.log(this.state);
			onInput(this.state);
		});
	}
}