export const fromData = [{ //来源
	label: '电话邀约',
	value: '1'
}, {
	label: '地推活动',
	value: '2'
}, {
	label: '网络数据',
	value: '3'
}, {
	label: '渠道推广',
	value: '4'
}, {
	label: '网络推广',
	value: '5'
}, {
	label: '主动上门',
	value: '6'
}, {
	label: '陌生来电',
	value: '7'
}, {
	label: '老带新',
	value: '8'
}, {
	label: '内部转化',
	value: '9'
}]

export const courseFrom = [{ //来源
	label: '电话邀约',
	value: '1'
}, {
	label: '地推活动',
	value: '2'
}, {
	label: '网络数据',
	value: '3'
}, {
	label: '渠道推广',
	value: '4'
}, {
	label: '网络推广',
	value: '5'
}, {
	label: '主动上门',
	value: '6'
}, {
	label: '陌生来电',
	value: '7'
}, {
	label: '老带新',
	value: '8'
}, {
	label: '内部转化',
	value: '9'
}, {
	label: '试听',
	value: '10'
}, {
	label: '测试',
	value: '11'
}, {
	label: '公开课',
	value: '12'
}]

export const schoolType = [{ //学校类型
	label: '直营',
	value: '1'
}, {
	label: '加盟',
	value: '2'
}, {
	label: '合作',
	value: '3'
}, {
	label: '代理',
	value: '4'
}]

export const discountType = [{ //优惠类型
	label: '时间段',
	value: '1'
}, {
	label: '促销活动',
	value: '2'
}, {
	label: '优惠券',
	value: '3'
}]

export const gradeList = [{ //年纪
	label: '托班',
	value: '1'
}, {
	label: '幼儿园小班',
	value: '2'
}, {
	label: '幼儿园中班',
	value: '3'
}, {
	label: '幼儿园大班',
	value: '4'
}, {
	label: '一年级',
	value: '5'
}, {
	label: '二年级',
	value: '6'
}, {
	label: '三年级',
	value: '7'
}, {
	label: '四年级',
	value: '8'
}, {
	label: '五年级',
	value: '9'
}, {
	label: '六年级',
	value: '10'
}, {
	label: '初一',
	value: '11'
}, {
	label: '初二',
	value: '12'
}, {
	label: '初三',
	value: '13'
}, {
	label: '高一',
	value: '14'
}, {
	label: '高二',
	value: '15'
}, {
	label: '高三',
	value: '16'
}, {
	label: '大学',
	value: '17'
}, {
	label: '成人',
	value: '18'
}, {
	label: '其他',
	value: '19'
}]

export const communicationList = [{ //沟通方式
	label: '聊天工具',
	value: '1'
}, {
	label: '电话',
	value: '2'
}, {
	label: '面对面沟通',
	value: '3'
}]

export const payTypes = [{ //支付方式
	label: '现金',
	value: '1'
}, {
	label: '微信',
	value: '2'
}, {
	label: '支付宝',
	value: '3'
}, {
	label: '刷卡',
	value: '4'
}, {
	label: '转账',
	value: '5'
}, {
	label: '支票',
	value: '6'
}]