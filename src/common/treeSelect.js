import React, {
	Component
} from 'react';
import {
	Tree,
	message
} from 'antd';

import {
	fpost
} from './io.js';

const TreeNode = Tree.TreeNode;

export default class TreeSelectComponent extends Component {
	state = {
		treeData: null,

		expandedKeys: this.props.initialValueData || [],
		autoExpandParent: true,
		checkedKeys: this.props.initialValueData || [],
		selectedKeys: [],
	}
	onExpand = (expandedKeys) => {
		console.log('onExpand', arguments);
		// if not set autoExpandParent to false, if children expanded, parent can not collapse.
		// or, you can remove all expanded children keys.
		this.setState({
			expandedKeys,
			autoExpandParent: false,
		});
	}
	onCheck = (checkedKeys, info) => {
		// console.log('onCheck', info);
		let keys = [...checkedKeys, ...info.halfCheckedKeys]
		// console.log('checkedKeys', checkedKeys);
		// console.log('onCheck', keys);
		this.setState({
			checkedKeys
		}, () => {
			this.props.onSelect(keys);
		});
	}
	onSelect = (selectedKeys, info) => {
		// console.log('onSelect', selectedKeys);
		// this.setState({
		// 	selectedKeys
		// });
	}
	renderTreeNodes = (data) => {
		return data.map((item) => {
			if (item.children) {
				return (
					<TreeNode title={item.title} key={item.key} dataRef={item}>
						{this.renderTreeNodes(item.children)}
					</TreeNode>
				);
			}
			return <TreeNode {...item} />;
		});
	}
	render() {
		let {
			treeData
		} = this.state;
		return (
			<div style={{ marginTop:'-8px' }} >
				{
					treeData ?
						<Tree
							checkable
							onExpand={this.onExpand}
							expandedKeys={this.state.expandedKeys}
							autoExpandParent={this.state.autoExpandParent}
							onCheck={this.onCheck}
							checkedKeys={this.state.checkedKeys}
							onSelect={this.onSelect}
							selectedKeys={this.state.selectedKeys}
						>
							{this.renderTreeNodes(treeData)}
						</Tree>
					: null
				}
			</div>
		);
	}
	componentDidMount() {
		this._getList();
	}
	_getList() {
		let {
			url = '/api/system/auth/treeMenu'
		} = this.props;

		fpost(url)
			.then(res => res.json())
			.then((res) => {
				if (!res.success || !res.result) {
					this.setState({
						loading: false
					});
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res.result);
			})
			.then((result) => {

				this.setState({
					treeData: result
				});
			})
			.catch((err) => {
				console.log(err);
			});
	}
}