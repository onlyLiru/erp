import store from 'store2';
require('es6-promise').polyfill();
require('isomorphic-fetch');

let host;
//  host = 'http://192.168.1.176:8080'; //俊宇
// host = 'http://192.168.1.66:8080'; //苗林
//  host = 'http://192.168.1.21:8080'; //磊磊
// host = 'http://192.168.1.129:8080'; //史亭
// host = 'http://192.168.1.210:8080'; //祥子
// host = 'http://192.168.1.69:8080'; //帅哥
// host = 'http://192.168.1.160:8080'; //首才
// host = 'http://47.97.20.19:26201'; //测试环境
host = 'http://47.97.20.19:26101'; //生产环境
//  host = 'http://10.81.44.164:26203'; //demo(solo)环境


let hostName = window.location.host;
if (hostName == 'erp.fancyedu.com') {
    host = 'http://47.97.20.19:26101'; //生产环境
} else if (hostName == 'test.erp.fancyedu.com') {
    host = 'http://47.97.20.19:26201'; //测试环境
} else if (hostName == 'demo.erp.fancyedu.com') {
    host = 'http://47.97.20.19:26203'; //demo环境
}


export {
    host
};
let sessionId = store.get('sessionId');

export function fget(url) {
    var result = fetch(host + url, {
        method: 'GET',
        credentials: 'include',
        headers: {
            'Accept': 'application/json, text/plain, */*',
            'x-auth-token': sessionId //登录功能放开后需要加上的
        },
    });

    return result;
}

export function hostname() {
    return host;
}

// 将对象拼接成 key1=val1&key2=val2&key3=val3 的字符串形式
function obj2params(obj) {
    var result = '';
    var item;
    for (item in obj) {
        result += '&' + item + '=' + encodeURIComponent(obj[item]);
    }

    if (result) {
        result = result.slice(1);
    }
    return result;
}

// 发送 post 请求
export function fpost(url, paramsObj) {
    var result = fetch(host + url, {
        method: 'POST',
        credentials: 'include',
        headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/x-www-form-urlencoded',
            'x-auth-token': sessionId, //登录功能放开后需要加上的
        },
        body: obj2params(paramsObj)
    });

    return result;
}


// 登录、忘记密码发送 post 请求（headers不需要传x-auth-token参数）
export function fpostLogin(url, paramsObj) {
    var result = fetch(host + url, {
        method: 'POST',
        credentials: 'include',
        headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: obj2params(paramsObj)
    });

    return result;
}
// 发送 post 请求
export function fpostArray(url, paramsObj) {
    var result = fetch(host + url, {
        method: 'POST',
        credentials: 'include',
        headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/json',
            'x-auth-token': sessionId, //登录功能放开后需要加上的
        },
        body: JSON.stringify(paramsObj)
    });

    return result;
}