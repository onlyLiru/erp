import React, { Component } from 'react';
import {
	TimePicker
} from 'antd';
import moment from 'moment';

export default class RangeTime extends Component {
	constructor(props) {
		super(props);
		let {
			initialValueData
		} = this.props;
		let startTime = (initialValueData && initialValueData.startTime) || null;
		let endTime = (initialValueData && initialValueData.endTime) || null;
		this.state={
			startTime:startTime,
			endTime:endTime
		}
	}
	render() {
		let {
			startTime,
			endTime
		} = this.state

		return(<span>
			<TimePicker 
				placeholder="开始时间"
				size="large"
				value={
					startTime ?
						moment(startTime, 'HH:mm:ss')
					: null
				}
				onChange={ this._startChange.bind(this) }
			/>
			<span className="f-ml2 f-mr2">~</span>
			<TimePicker 
				placeholder="结束时间"
				size="large"
				value={
					endTime ?
						moment(endTime, 'HH:mm:ss')
					: null
				}
				onChange= { this._endChange.bind(this) }
			/>
		</span>);
	}
	_startChange(startMoment,startTime) {
		this.setState({
			startTime
		},()=> this._onChange());
	}
	_endChange(endMoment,endTime) {
		this.setState({
			endTime
		},()=> this._onChange());
	}
	_onChange() {
		let {
			onSelect
		} = this.props;
		// console.log(this.state);
		onSelect(this.state);
	}
}