import React, {
	Component
} from 'react';
import {
	Layout,
	Alert,
	BackTop
} from 'antd';
import {
	default as HeaderContent
} from './header.js';
import {
	default as SiderContent
} from './sider.js';
import {
	default as FooterContent
} from './footer.js';

const {
	Header,
	Content,
	Footer,
	Sider
} = Layout;

export default class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {

		}
	}
	render() {
		let Main = this.props.main;

		return (<Layout className="main-layout">
			<Header>
				<HeaderContent />
			</Header>
			<Layout>
				<Sider className="f-box-shadow1 f-left-sider">
					<SiderContent />
				</Sider>
				<Layout className="f-main" style={ { marginLeft: '190px',overflowX:'scroll' } }>
					<Content className="f-main-content">
						<div className="f-mb3">
							<Alert
								message="温馨提醒:为了带给您更好的操作体验，请尽量使用chrome浏览器访问本网站！"
								type="warning"
								closable
						    />
					    </div>
						<Main />
						<BackTop />
					</Content>
					<Footer className="f-footer">
						<FooterContent />
					</Footer>
				</Layout>
			</Layout>
	    </Layout>)
	}
}