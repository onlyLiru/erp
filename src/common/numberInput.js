import React, {
	Component
} from 'react';
import {
	Input,
} from 'antd';

export default class Main extends Component {
	constructor(props) {
		super(props);

		const value = this.props.value || {};
		let {
			number,
			currency,
			min,
			max
		} = value;

		// console.log(value);
		this.state = {
			number: number || '',
			currency: currency || 'rmb',
			min: min || 0,
			max: max || 999999999.99
		};
	}
	componentWillReceiveProps(nextProps) {
		if ('value' in nextProps) {
			const value = nextProps.value;
			this.setState(value);
		}
	}
	handleNumberChange = (e) => {
		let {
			currency
		} = this.state;
		// const number = parseInt(e.target.value || 0, 10);
		let v = e.target.value;

		if (currency == 'rmb') {
			if (!(/^(\d*)(\.?)(\d{1,2})?$/).test(v)) {
				return;
			};
		}
		if (currency == 'count') { //如果是数量转换为整数
			v = parseInt(v || 1, 10);
		}

		const number = v;
		let {
			min,
			max
		} = this.state;

		if (isNaN(number) || number < min || number > max) {
			return;
		}
		if (!('value' in this.props)) {
			this.setState({
				number
			});
		}
		this.triggerChange({
			number
		});
	}
	triggerChange = (changedValue) => {
		// Should provide an event to pass value to Form.
		const onChange = this.props.onChange;
		if (onChange) {
			onChange(Object.assign({}, this.state, changedValue));
		}
	}
	render() {
		const {
			size,
			style,
			placeholder = '请输入'
		} = this.props;
		let {
			number,
			currency
		} = this.state;
		let prefix = 'x';
		if (currency == 'rmb') {
			prefix = '¥';
		} else if (currency == 'count') {
			prefix = "x";
		}
		return (
			<Input
	          type="text"
	          prefix={ prefix }
	          size={size}
	          value={number}
	          placeholder={placeholder}
	          onChange={this.handleNumberChange}
	          style={style}
	        />
		);
	}
}