import React, {
	Component
} from 'react';
import {
	Form,
	Icon,
	Button,
	Select
} from 'antd';
import {
	fpost
} from './io.js';
const FormItem = Form.Item;
const Option = Select.Option;

let uuid = 0;
class DynamicFieldSet extends Component {
	constructor(props) {
		super(props);
		this.state = {
			courseTypeId: this.props.courseTypeId || '',
			courseIds: [],
			result: [],
		}
	}
	remove(k) {
		const {
			form
		} = this.props;

		// can use data-binding to get
		let keys = form.getFieldValue('keys');
		// We need at least one passenger
		if (keys.length === 1) {
			return;
		}
		keys = keys.filter(key => key !== k);
		// can use data-binding to set
		form.setFieldsValue({
			keys: keys,
		});

		this._getCourseData();
	}

	add() {
		uuid++;
		const {
			form
		} = this.props;
		// can use data-binding to get
		const keys = form.getFieldValue('keys');
		const nextKeys = keys.concat(uuid);
		// can use data-binding to set
		// important! notify form to detect changes
		form.setFieldsValue({
			keys: nextKeys,
		});
	}

	render() {
		const {
			getFieldDecorator,
			getFieldValue
		} = this.props.form;
		const formItemLayout = {
			labelCol: {
				xs: {
					span: 24
				},
				sm: {
					span: 4
				},
			},
			wrapperCol: {
				xs: {
					span: 24
				},
				sm: {
					span: 20
				},
			},
		};
		let {
			result
		} = this.state;

		getFieldDecorator('keys', {
			initialValue: []
		});

		const keys = getFieldValue('keys');
		const formItems = keys.map((k, index) => {
			return (
				<FormItem
					{ ...formItemLayout }
					required={false}
					key={k}
				>
				{getFieldDecorator(`names-${k}`, {
					validateTrigger: ['onChange', 'onBlur'],
					rules: [{
						required: false,
						whitespace: true,
						message: "请选择课程",
					}],

				})(
					<Select
						style={{ width: '300px', marginRight: 8 }} 
					    size="large"
					    placeholder="请选择课程"
					    allowClear
					    optionFilterProp="children"
					    onChange={ this._getCourseData.bind(this) }
					>
					    {
					        result.map((d,i)=> {
					            return (<Option key={i} value={d.id}>{d.name}</Option>);
					        })
					    }
					</Select>
				)}
					{
						keys.length > 1 ? 
							<Icon
								className="dynamic-delete-button"
								type="minus-circle-o"
								disabled={keys.length === 1}
								onClick={() => this.remove(k)}
							/>
						: null
					}
				</FormItem>
			);
		});
		return (
			<Form onSubmit={this.handleSubmit}>
				{ formItems }
				<FormItem {...formItemLayout}>
					<Button 
						type="dashed" 
						onClick={ this.add.bind(this) } 
						style={{ width: '300px' }}
					>
						<Icon type="plus" /> 添加课程
					</Button>
				</FormItem>
			</Form>
		);
	}
	componentDidMount() {
		this._getData();
	}
	_getCourseData() {
		let timer;
		let courseIds = [];
		let values;
		if (timer) {
			clearTimeout(timer);
		}

		timer = setTimeout(() => {
			values = this.props.form.getFieldsValue();
			for (let d in values) {
				let data = values[d];
				if (data && typeof data == 'string') {
					courseIds.push(data);
				}
			}
			// console.log(courseIds);
			this.props.onSelect(courseIds);
		}, 200);
	}
	_getData() {
		let {
			courseTypeId = ''
		} = this.props;

		fpost('/api/system/course/listCourse', {
				subjectId: courseTypeId
			})
			.then((res) => res.json())
			.then((res) => {
				if (!res.success || !res.result) {
					// message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res.result);
			})
			.then((result) => {
				this.setState({
					result
				});
			})
			.catch((err) => {
				console.log(err);
			});

	}
}

const WrappedDynamicFieldSet = Form.create()(DynamicFieldSet);
export default WrappedDynamicFieldSet