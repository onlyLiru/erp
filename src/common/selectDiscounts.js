import React, {
	Component
} from 'react';
import {
	Modal,
	Form,
	Select
} from 'antd';
import {
	fpost
} from './io.js';
import {
	default as NumberInput
} from './numberInput.js';

const FormItem = Form.Item;
const Option = Select.Option;

export default class SelectDiscounts extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isLoading: false, //是否正在获取优惠数据
			discountData: null, //获取的优惠数据
			selectData: [], //已经选择的优惠
			registrationCourseReduceList: [], //封装优惠信息，报名时给后端传参数
		}
	}
	render() {
		let {
			handleCancel
		} = this.props;

		let {
			discountData
		} = this.state;

		let {
			defaultData //默认数据
		} = this.props;

		let {
			coupon
		} = defaultData || [];

		// console.log('默认优惠信息', defaultData);

		return (<Modal
			title="选择优惠"
			width='400px'
			visible={ this.props.visible }
			onOk={ this._submit.bind(this) }
			onCancel={ handleCancel }
			>
				<MyFormCreate 
					defaultData={defaultData}
					discountData={ discountData }
					ref={ (form)=> this.form=form }
				/>
        </Modal>);
	}
	componentDidMount() {
		this._getData();
	}
	_getData() { //获取列表数据
		this.setState({
			isLoading: true
		});

		let {
			courseId
		} = this.props;

		fpost('/api/system/preferential/listPreferentialByCourseId', {
				courseId
			})
			.then(res => res.json())
			.then((res) => {
				if (!res.success || !res.result || res.result.length <= 0) {
					return;
				};
				return (res.result);
			})
			.then((result) => {
				this.setState({
					discountData: result
				});
			})
			.catch((err) => {
				console.log(err);
			});
	}
	_submit() {
		const form = this.form;
		let registrationCourseReduceList = [];
		let {
			handleOk
		} = this.props;
		let {
			discountData,
			selectData
		} = this.state;
		let curData;

		form.validateFields((err, values) => {
			if (err) {
				return;
			}
			let {
				couponData
			} = values;
			values.cash = values.cash && values.cash.number || '';
			values.discount = values.discount && values.discount.number || '';
			let {
				cash,
				discount
			} = values;

			if (couponData && couponData.length) {
				couponData.map((id, i) => {
					curData = discountData.filter(data => data.id == id);
					selectData.push(curData[0]);
				})
			}
			/*报名需要的优惠信息封装begin*/
			if (cash) {
				registrationCourseReduceList.push({
					amount: cash,
					type: 3
				});
			}
			if (discount) {
				registrationCourseReduceList.push({
					amount: discount,
					type: 1
				});
			}
			let selectDataSignup = selectData;
			selectDataSignup.forEach((d, i) => {
				d.type = '2';
			});

			registrationCourseReduceList = [...registrationCourseReduceList, ...selectDataSignup];
			console.log(registrationCourseReduceList);

			/*报名需要的优惠信息封装end*/

			let allData = {
				coupon: selectData,
				...values,
				registrationCourseReduceList
			};
			handleOk(allData);
		});
	}
}


class MyForm extends Component {
	constructor(props) {
		super(props);
		this.state = {

		}
	}
	render() {
		const {
			getFieldDecorator
		} = this.props.form;

		let formItemLayout = {
			labelCol: {
				span: 5
			},
			wrapperCol: {
				span: 19
			}
		}

		let {
			defaultData,
			discountData
		} = this.props;

		let cash = defaultData && defaultData.cash;
		let discount = defaultData && defaultData.discount;
		let couponData = (defaultData && defaultData.couponData) ? defaultData.couponData : [];

		return (<div>
			<Form>
				<FormItem 
					label="按折扣"
					{ ...formItemLayout }
				>
					{
						getFieldDecorator('discount', {
							rules: [{ 
								required:false,
								message: '请输入0.01至0.99区间的数值',
								validator: this._discountNumberValidator
							}],
							initialValue: { 
								number: discount, 
								currency: 'rmb',
								min:0,
                            	max:0.99
							},
						})(
							<NumberInput
								placeholder="请输入0.01至0.99区间的数值"
								style={{
									width:'100%'
								}}
							/>
						)
					}
                </FormItem>
				<FormItem 
					label="现金减免"
					{ ...formItemLayout }
				>
					{getFieldDecorator('cash',{
						initialValue: { 
							number: cash
						}
					})(
						<NumberInput
							style={{
								width:'100%'
							}}
						/>
					)}
				</FormItem>
				{
					discountData && discountData.length ?
						<FormItem 
							label="优惠"
							{ ...formItemLayout }
						>
							{getFieldDecorator('couponData',{
								initialValue:couponData || []
							})(
								<Select
									mode="multiple"
								    showSearch
								    size="large"
								    placeholder="选择优惠"
								    optionFilterProp="children"
								>
									{
										discountData.map((d,i)=> {
											let {
												id,
												amount,
												name
											} = d;
											
											return  <Option 
								    			value={id}
								    			key={i}
							    			>
							    				{name} 
							    				<span className="f-red f-ml3">{ amount }</span>
							    			</Option>
										})
									}
								</Select>
							)}
						</FormItem>
					: null
				}
	        </Form>
		</div>);
	}
	_discountNumberValidator(rule, value, callback) { //校验折扣输入
		let number = value.number;
		if (!number) {
			callback();
		};

		if (isNaN(number) || number < 0.01 || number > 0.99) {
			callback(new Error(rule.message));
		} else {
			callback();
		}
	}
}
let MyFormCreate = Form.create()(MyForm);