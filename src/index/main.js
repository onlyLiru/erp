import React, {
	Component
} from 'react';
import {
	Layout
} from 'antd';
import {
	default as HeaderContent
} from '../common/header.js';
import './main.less';
const {
	Header
} = Layout;

export class Index extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}
	render() {
		return (
			<div className="main-layout">
				<Header>
					<HeaderContent />
				</Header>
				<div className='f-bg-white'
					style={{width:'100%',
						height:'100vh',
						paddingTop:'87px',
						background: 'url("http://img.fancyedu.com/sys/ic/operation/1513677115306_bg.png") 0% 0% / 100% no-repeat',
						backgroundSize:' 100% auto',	
					}}
				>
					<div className='f-fz5 f-align-center'
						style={{fontFamily:'PingFangSC-Thin',color:'#007CFF',letterSpacing: '4.9px',marginBottom:'58px',marginTop:'100px'}}
						>欢迎使用e校通学校管理系统
					</div>
					<div className='f-align-center'>
						<img src='http://img.fancyedu.com/sys/ic/operation/1515484202302_newImg.png' style={{width:'530px'}}/>
					</div>
				</div>
				
		    </div>
		);
	}
}