import React, {
	Component
} from 'react';
import $ from 'jquery';
import {
	Input,
	Button,
	Icon,
	Col,
	Row,
	Form,
	DatePicker,
	Select,
	message
} from 'antd';
import RangeDate from '../../../common/rangeDate.js';
import {
	default as SelectPerson
} from '../../../common/selectPerson.js';
import SelectCourses from '../../../common/selectCourses.js';
import {
	undefinedToEmpty
} from '../../../common/g.js';
import {
	fpost,
	host
} from '../../../common/io.js';
const FormItem = Form.Item;
const Option = Select.Option;

class MainForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isAdvancedSearch: false,
			classroomList: [], //获取教室
		}
	}
	render() {
		const {
			getFieldDecorator
		} = this.props.form;

		const {
			isAdvancedSearch,
			classroomList
		} = this.state;

		const formItemLayout = {
			labelCol: {
				span: 6
			},
			wrapperCol: {
				span: 18
			}
		};

		return (<header className="f-box-shadow2 f-radius1 f-bg-white f-pd4">
			<ul id="J-searchTab" className="f-pale f-pointer searchTab">
				<li data-type={0} className="f-blue cur"><Icon type="search" className="f-mr1 f-fz4" />精确查找</li>
				<li className="f-ml5 f-mr5">|</li>
				<li  data-type={1}><Icon type="filter" className="f-mr1 f-fz4" />高级筛选</li>
			</ul>
			<Form>
				{
					!isAdvancedSearch ? //精确搜索
						<section>
							<Row gutter={ 40 }>
	            				<Col span={8}>
	            					<FormItem
										label="班级名称"
										{...formItemLayout}
									>
										{
											getFieldDecorator('className', { 
												rules: [{
													required: false, 
													message: '请输入班级名称' 
												}] 
											})
											(
												<Input placeholder="请输入班级名称" />
											)
										}
									</FormItem>
	            				</Col>
	            				<Col span={ 8 }>
	        						<FormItem>
										<Button 
											type="primary"
											htmlType="submit"
											size="large" 
											icon="search" 
											onClick={ this._search.bind(this) }
										>搜索</Button>
							        </FormItem>
	        					</Col>
	            	        </Row>

				        </section>
				    ://高级搜索
				    <section>
				    	<Row gutter={ 40 }>
				    		<Col span={ 10 }>
            					<FormItem
									label="班级名称"
									{...formItemLayout}
								>
									{
										getFieldDecorator('className', { 
											rules: [{
												required: false, 
												message: '请输入班级名称' 
											}] 
										})
										(
											<Input placeholder="请输入班级名称" />
										)
									}
								</FormItem>
				    		</Col>
				    		<Col span={ 7 }>
            					<FormItem
									label="学员"
									{...formItemLayout}
								>
									{
										getFieldDecorator('studentName', { 
											rules: [{
												required: false, 
												message: '请输入学员名称' 
											}] 
										})
										(
											<Input placeholder="请输入学员名称" />
										)
									}
								</FormItem>
				    		</Col>
		    				<Col span={ 7 }>
								<FormItem
									label="课程名称"
									{...formItemLayout}
								>
									{
										getFieldDecorator('courseId', {
											rules: [{  
												message: '请选择' 
											}]
										})(
											<SelectCourses 
												onSelect= {
													(v)=> {
														this.props.form.setFieldsValue({
															courseId:v
														});
													}
												}
											/>
										)
									}
								</FormItem>
							</Col>
				    	</Row>
		    			<Row gutter={ 40 }>
		    				<Col span={10}>
		    					<FormItem
									label="补课日期"
									{...formItemLayout}
								>
									{
										getFieldDecorator('rangeTime',{
											initialValue:''
										})(
											<RangeDate 
												onSelect={
													(d)=> {
														this.props.form.setFieldsValue({
															rangeTime:d,
															startDate:d.startString,
															endDate:d.endString,
														});
													}
												}
												showTime={true}
											/>
										)
									}
								</FormItem>
								<span className="f-hide">
							        {
										getFieldDecorator('startDate',{
											initialValue:''
										})(
											<Input />
										)
									}
									{
										getFieldDecorator('endDate',{
											initialValue:''
										})(
											<Input />
										)
									}
								</span>
		    				</Col>
		    				{
		    					classroomList ?
				    				<Col span={ 7 }>
										<FormItem
											label="补课教室"
											{...formItemLayout}
										>
											{
												getFieldDecorator('classRoomId', {
													rules: [{  
														message: '请选择补课教室' 
													}]
												})(
													<Select
														size="large"
														allowClear
					                					placeholder="请选择补课教室"
													>
														{
															classroomList.map((d,i)=> {
																return(<Option 
																		key={i} 
																		value={d.id}
																	>{d.name}</Option>);
															})
														}
													</Select>
												)
											}
										</FormItem>
									</Col>
								: null
		    				}
							<Col span={ 7 }>
								<FormItem
									label="补课老师"
									{...formItemLayout}
								>
									{
										getFieldDecorator('teacherId', {
											rules: [{  
												message: '请选择' 
											}]
										})(
											<SelectPerson
												url='/api/hr/staff/listStaffByCondForDropDown'
												data={
													{ 
														type:4
													}
												} 
												onSelect={ 
													(v)=> {
														v = v ? v : null;
														this.props.form.setFieldsValue({
															teacherId: v
														});
													} 
												}
											/>
										)
									}
								</FormItem>
							</Col>
		    	        </Row>
		    	        <div className="f-align-center">
		    	        	<Button 
		    	        		size="large" 
		    	        		className="f-mr4" 
		    	        		onClick={ this._reset.bind(this) }
		    	        	>重置</Button>
		    	        	<Button 
		    	        		type="primary"
		    	        		size="large" 
		    	        		icon="search" 
		    	        		onClick={ this._search.bind(this) }
		    	        	>查询</Button>
		    	        </div>
	    	        </section>
				}
	      	</Form>
		</header>);
	}
	componentDidMount() {
		this._changeSearchType();
		this._getClassroom();
	}
	_changeSearchType() {
		const self = this;
		$('#J-searchTab li').on('click', function(e) {
			let type = $(this).attr('data-type');
			let isAdvancedSearch = type == 1 ? true : false;

			$(this).addClass('cur f-blue').siblings().removeClass('cur f-blue');

			self.setState({
				isAdvancedSearch
			});
		});
	}
	_search(e) { //搜索
		let {
			onSearch
		} = this.props;

		this.props.form.validateFields((err, values) => {
			if (!err) {
				/*如果值为undefined则替换为空*/
				values = undefinedToEmpty(values);

				onSearch(values);
			}
		});
	}
	_reset() {
		this.props.form.resetFields();
		this.setState({
			isAdvancedSearch: false
		}, () => {
			this.setState({
				isAdvancedSearch: true
			}, () => {
				this._search();
			})
		});
	}
	_getClassroom() { //获取教室
		const self = this;
		fpost('/api/system/schoolarea/listClassRoom ')
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					self.setState({
						classroomList: res.result
					})
				} else {
					message.error(res.message)
				}
			});
	}
}

const MyForm = Form.create()(MainForm);

export default MyForm;