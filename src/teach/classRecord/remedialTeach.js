import React, {
	Component
} from 'react';
import {
	Breadcrumb,
	Button,
	Select,
	Table,
	Modal,
	Tabs,
	Input,
	DatePicker,
	Row,
	Col,
	Form,
	Pagination,
	message
} from 'antd';
import '../main.less';
import moment from 'moment';
import $ from 'jquery';
import {
	Loading
} from '../../common/g.js';
//使用fetch请求接口数据
import {
	fpost
} from '../../common/io.js'; //同步、异步请求
const dateFormat = 'YYYY-MM-DD HH:mm:ss';
const u = require('../../common/io.js');
const TabPane = Tabs.TabPane;
const Option = Select.Option;
const FormItem = Form.Item;
let classname = {};
let classname1 = {}

//教务教学默认打开的首页
class Main extends Component {
	constructor(props) {
		super(props)
		this.state = {
			teachersData: [],
			size: 'large',
			visible: false,
			startValue: null,
			endValue: null,
			endOpen: false,
			className: null, //班级名称
			pageResult: null,
			currentPage: 1, //当前页码
			pageSize: 10, //每页条数
			total: null, //总条数
			searchState: 0,
			course: [], //课程名称
			classroomList: [], //教室名称
			schoolGroupId: '', //教育集团
			classSignUpId: '', //记上课id
			classTimesDetailId: '', //排课明细
			studentSignUpDetailId: '', //记上课明细
			studentId: '', //学员id
			masterList: [], //班主任列表
			masterTeachers: [], //补课老师
			initmakeUpTime: null, //初始化补课时间
			initroomId: '', //初始化教室id
			initteacherId: '', //初始化老师id
			initState: '1', //初始化
			dataId: '',
			confirmLoading: false,
		}
		this.showModal = this.showModal.bind(this);
	}
	showModal(data) {
		if (data.signUpStatus == '1' || data.signUpStatus == '4') {
			this._init(data); //弹框初始化
			this.setState({
				initState: '2' //修改
			})
		}
		this.setState({
			visible: true, //确定对话框的展示
			schoolGroupId: data.schoolGroupId,
			classSignUpId: data.classSignUpId,
			classTimesDetailId: data.classTimesDetailId,
			studentSignUpDetailId: data.studentSignUpDetailId,
			studentId: data.studentId,
		}, () => {
			this._getTeachers(data.schoolAreaId);
		});
	}
	_init(data) {
		const self = this;
		fpost('/api/educational/class/makeUp/history/update/init', {
				classMakeUpHistoryId: data.id
			})
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					self.setState({
						initmakeUpTime: res.result.makeUpTime,
						initroomId: res.result.roomId,
						initteacherId: res.result.teacherId,
						dataId: data.id
					})
				} else {
					message.error(res.message)
				}
			});

	}
	handleOk = (e) => { //对话框里的确定按钮对应事件
		this.props.form.validateFields(
			(err, fieldsValue) => {
				const data = {}
				const data1 = {}
				if (!err) {
					fieldsValue['makeUpTime'] = fieldsValue['makeUpTime'] ? fieldsValue['makeUpTime'].format('YYYY-MM-DD HH:mm:ss') : '';
					fieldsValue['roomId'] = fieldsValue['roomId'] ? fieldsValue['roomId'] : "";
					fieldsValue['teacherId1'] = fieldsValue['teacherId1'] ? fieldsValue['teacherId1'] : "";
					data.makeUpTime = fieldsValue['makeUpTime'];
					data.roomId = fieldsValue['roomId'];
					data.teacherId1 = fieldsValue['teacherId1'];

					data1.teacherId = fieldsValue['teacherId1'];
					data1.makeUpTime = fieldsValue['makeUpTime'];
					data1.roomId = fieldsValue['roomId'];
					data1.id = this.state.dataId;
					setTimeout(() => {
						if (this.state.initState == '1') {
							this._modalSave(data);
						} else {
							this._saveM(data1);
						}
					}, 500);

				} else {
					message.error('您还有未完善的信息，请先完善信息')
				}
			}
		)
	}
	_saveM(data) {
		let self = this;
		fpost('/api/educational/class/makeUp/history/update', data)
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					message.success('修改成功');
					self.setState({
						visible: false,
						confirmLoading: true
					}, function() {
						window.location.reload();
					});
					//				
				} else {
					self.setState({
						confirmLoading: false
					});
					message.error(res.message)
				}
			});
	}
	_modalSave(data) {
		let self = this;
		fpost('/api/educational/class/makeUp/history/add', {
				schoolGroupId: self.state.schoolGroupId ? self.state.schoolGroupId : '',
				classSignUpId: self.state.classSignUpId,
				studentId: self.state.studentId,
				classTimesDetailId: self.state.classTimesDetailId ? self.state.classTimesDetailId : '',
				studentSignUpDetailId: self.state.studentSignUpDetailId,
				makeUpTime: data.makeUpTime,
				roomId: data.roomId,
				teacherId: data.teacherId1,
			})
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					self.setState({
						confirmLoading: true
					}, function() {
						window.location.reload();
					});
				} else {
					self.setState({
						confirmLoading: false
					})
					message.error(res.message)
				}
			});
	}
	handleCancel = (e) => { //对话框里的取消按钮对应的事件
		this.props.form.resetFields(); //重置
		this.setState({
			visible: false,
			confirmLoading: false
		});
	}
	disabledStartDate = (startValue) => { //只读的开始日期
		const endValue = this.state.endValue;
		if (!startValue || !endValue) {
			return false;
		}
		return startValue.valueOf() > endValue.valueOf();
	}

	disabledEndDate = (endValue) => { //只读的结束日期
		const startValue = this.state.startValue;
		if (!endValue || !startValue) {
			return false;
		}
		return endValue.valueOf() <= startValue.valueOf();
	}

	onChange = (field, value) => {
		this.setState({
			[field]: value,
		});
	}
	//开始日期改变
	onStartChange = (value) => {
		this.onChange('startValue', value);
	}
	//结束日期改变
	onEndChange = (value) => {
		this.onChange('endValue', value);
	}

	handleStartOpenChange = (open) => {
		if (!open) {
			this.setState({
				endOpen: true
			});
		}
	}

	handleEndOpenChange = (open) => {
		this.setState({
			endOpen: open
		});
	}
	reset() { //高级筛选里的重置
		this.props.form.resetFields();
		this.setState({
			currentPage: 1
		}, function() {
			this._getForm();
		});
	}
	onShowSizeChange(current, pageSize) {
		this.setState({
			currentPage: current,
			pageSize: pageSize
		}, function() {
			if (this.state.searchState == 0) {
				this.jqSearch();
			} else {
				this.advancedSearch()
			}
		})
	}
	componentDidMount() {
		this.jqSearch();
		this._getCourse(); //获取课程
		this._getClassroom(); //获取教室
		this._getMaster(); //上课老师
	}
	_getMaster() { //获取班主任下拉
		const self = this;
		fpost('/api/hr/staff/listStaffByCondForDropDown', {
				type: 4
			})
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					self.setState({
						masterList: res.result
					})
				} else {
					message.error(res.message)
				}
			});
	}
	_getTeachers(id) { //获取补课老师
		const self = this;
		fpost('/api/hr/staff/listStaffByCondForDropDown', {
				type: 6,
				schoolAreaId: id
			})
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					self.setState({
						masterTeachers: res.result
					})
				} else {
					message.error(res.message)
				}
			});
	}
	_getClassroom() { //获取教室
		const self = this;
		fpost('/api/system/schoolarea/listClassRoom ')
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					self.setState({
						classroomList: res.result
					})
				} else {
					message.error(res.message)
				}
			});
	}
	_getCourse() { //获取课程
		const self = this;
		fpost('/api/system/course/listCourse')
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					self.setState({
						course: res.result
					})
				} else {
					message.error(res.message)
				}
			});
	}
	className(e) { //班级名称
		let v = e.target.value;
		this.setState({
			className: v
		});
	}
	_getForm() {
		this.props.form.validateFields(
			(err, fieldsValue) => {
				fieldsValue['startDate'] = fieldsValue['startDate'] ? fieldsValue['startDate'].format('YYYY-MM-DD') : '';
				fieldsValue['endDate'] = fieldsValue['endDate'] ? fieldsValue['endDate'].format('YYYY-MM-DD') : "";
				fieldsValue['pageSize'] = this.state.pageSize;
				fieldsValue['classRoomId'] = fieldsValue['classRoomId'] ? fieldsValue['classRoomId'] : '';
				fieldsValue['teacherId'] = fieldsValue['teacherId'] ? fieldsValue['teacherId'] : '';
				fieldsValue['studentName'] = fieldsValue['studentName'] ? fieldsValue['studentName'] : '';
				fieldsValue['className'] = fieldsValue['className'] ? fieldsValue['className'] : '';
				fieldsValue['courseId'] = fieldsValue['courseId'] ? fieldsValue['courseId'] : '';
				fieldsValue['currentPage'] = this.state.currentPage;
				classname1.startDate = fieldsValue['startDate'];
				classname1.endDate = fieldsValue['endDate'];
				classname1.pageSize = this.state.pageSize;
				classname1.classRoomId = fieldsValue['classRoomId'];
				classname1.teacherId = fieldsValue['teacherId'];
				classname1.studentName = fieldsValue['studentName'];
				classname1.className = fieldsValue['className'];
				classname1.courseId = fieldsValue['courseId'];
				classname1.currentPage = this.state.currentPage;
				this.getList(classname1);
			}
		)
	}
	pageChange(pageNumber) { //列表分页获取当前页码
		this.setState({
			currentPage: pageNumber
		}, function() {
			if (!this.state.searchState) {
				classname['className'] = $.trim($('.inputBG').val());
				classname.pageSize = this.state.pageSize;
				classname.currentPage = this.state.currentPage;
				this.getList(classname);
			} else {
				this._getForm(); //表单内容
			}
		});
	}
	jqSearch() { //精确搜索
		this.setState({
			currentPage: 1,
		}, function() {
			classname['className'] = $.trim($('.inputBG').val());
			classname.pageSize = this.state.pageSize;
			classname.currentPage = this.state.currentPage;
			this.getList(classname);
		});
	}
	advancedSearch() { //筛选搜索
		this.setState({
			currentPage: 1
		}, function() {
			this._getForm();
		});
	}
	callback(key) {
		if (key == 2) {
			this.setState({
				searchState: 1
			})
		} else {
			this.setState({
				searchState: 0
			})
		}
	}
	getList(fieldsValue) { //获取全部校区信息
		let self = this;
		fpost('/api/educational/class/makeUp/history/list', fieldsValue)
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					if (res.result) {
						res.result.records.forEach((item, i) => {
							item.key = i;
						});
						self.setState({
							pageResult: res.result.records,
							total: parseInt(res.result.total)
						})
					}
				} else {
					message.error(res.message)
				}
			});
	}
	render() {
		const columns = [{
			title: '学员姓名（学号）',
			key: '0',
			fixed: 'left',
			width: 180,
			className: 'f-align-center',
			render: (text, record, index) => {
				return (
					<div>
			   			{record.studentName}({record.studentNo})
			   		</div>
				)
			}
		}, {
			title: '课程名称',
			key: '1',
			width: 180,
			className: 'f-align-center',
			render: (text, record, index) => {
				return (
					<div title={record.courseName}>
			   			<i className='iconfont icon-shu f-mr2'></i>
			   			<i className='courseName f-line1'>{record.courseName}</i>
			   		</div>
				)
			}
		}, {
			title: '当前课次',
			className: 'f-align-center',
			dataIndex: 'age',
			key: '2',
			width: 150,
			render: (text, record) => (
				<span>{record.classTimes}</span>
			)
		}, {
			title: '上课时间',
			className: 'f-align-center',
			dataIndex: 'classDate',
			key: '3',
			width: 150,
		}, {
			title: '班级',
			className: 'f-align-center',
			dataIndex: 'className',
			key: '4',
			width: 150,
		}, {
			title: '校区',
			className: 'f-align-center',
			dataIndex: 'schoolAreaName',
			key: '5',
			width: 150,
		}, {
			title: '教室',
			className: 'f-align-center',
			dataIndex: 'classRoomName',
			key: '6',
			width: 150,
		}, {
			title: '上课老师',
			className: 'f-align-center',
			key: '7',
			width: 150,
			dataIndex: 'teacherName'
		}, {
			title: '助教',
			className: 'f-align-center',
			key: '8',
			width: 150,
			dataIndex: 'assistantName'
		}, {
			title: '备注',
			className: 'f-align-center',
			key: '9',
			width: 150,
			render: (text, record, index) => {
				return (<div title={record.remark} className='f-line1' style={{width:'150px'}}>
		                {record.remark}
		            </div>);
			}
		}, {
			title: '操作',
			className: 'f-align-center',
			key: '10',
			width: 150,
			fixed: 'right',
			render: (text, record, index) => {
				return (<div>
					{record.signUpStatus=='2'?
		                <Button className='f-radius4 f-bg-green f-white f-viewdetail-check' onClick={()=>this.showModal(record)}><i className='iconfont icon-biji f-mr1 f-viewdetail-down f-fz5'></i>记补课</Button>
					:
					''
					}
					{record.signUpStatus=='3'?
		                <Button className='f-radius4 f-bg-green f-white f-viewdetail-check' onClick={()=>this.showModal(record)}><i className='iconfont icon-biji f-mr1 f-viewdetail-down f-fz5'></i>记补课</Button>
					:
					''
					}
					{record.signUpStatus=='1'?
		                 <Button className='f-radius4 f-viewdetail-check' onClick={()=>this.showModal(record)}><i className='iconfont icon-biji f-mr1 f-viewdetail-down f-fz5'></i>修改补课</Button>
					:
		               ''
					}
					{record.signUpStatus=='4'?
		                 <Button className='f-radius4 f-viewdetail-check' onClick={()=>this.showModal(record)}><i className='iconfont icon-biji f-mr1 f-viewdetail-down f-fz5'></i>修改补课</Button>
					:
		               ''
					}

		            </div>);
			}
		}];
		const {
			size
		} = this.state;
		const {
			getFieldDecorator
		} = this.props.form;
		const formItemLayout = {
			labelCol: {
				span: 6
			},
			wrapperCol: {
				span: 18
			}
		};
		const formItemLayout1 = {
			labelCol: {
				span: 4
			},
			wrapperCol: {
				span: 20
			}
		};
		return (
			<Form className='classListPage wid100'>
				<Breadcrumb className="f-mb3">
				    <Breadcrumb.Item>教务教学</Breadcrumb.Item>
				    <Breadcrumb.Item>上课记录</Breadcrumb.Item>
				    <Breadcrumb.Item>
				   		<span className="f-fz5 f-bold">记补课</span>
				    </Breadcrumb.Item>
				</Breadcrumb>
				<div className='Topselect f-bg-white f-radius1 f-box-shadow1' style={{width:'100%'}}>
				<Modal
			          title='记补课'
			          visible={this.state.visible}
			          onOk={this.handleOk}
			          onCancel={this.handleCancel}
			           confirmLoading={this.state.confirmLoading}
			            maskClosable={false}
			          >
						<div className='width100'>
			    			<FormItem {...formItemLayout1} label="补课老师">
		                  {getFieldDecorator('teacherId1', {
		                  	rules: [{required:true,message:'请选择补课老师'}],
		                  	initialValue:this.state.initteacherId
		                  })(
		                   <Select
		                  	  showSearch
		                  	  allowClear
					          placeholder='请选择上课老师'
					          style={{ width: '100%' }}
					          optionFilterProp="children"
            					filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
					        >
								{this.state.masterTeachers?
			                        	this.state.masterTeachers.map((item,i)=> {
			                        		return(<Option key={item.id} value={item.id}>{ item.name }</Option>)
			                        	})
			                        	:null
			                      }
					        </Select>
		                  )}
            				</FormItem>
            				<FormItem {...formItemLayout1} label='补课时间'>
				          {getFieldDecorator('makeUpTime',{
				          	rules: [{required:true,message:'请选择补课时间'}],
		                  	initialValue:this.state.initmakeUpTime?moment(this.state.initmakeUpTime, dateFormat):null
				          })(
				            	<DatePicker
					          showTime
					          format="YYYY-MM-DD HH:mm:ss"
					          placeholder="请选择时间"/>
				          )}
				     </FormItem>
				     <FormItem {...formItemLayout1} label="补课教室">
		                  {getFieldDecorator('roomId', {
		                  	rules: [{required:true,message:'请选择补课教室'}],
		                  	valuePropName: 'value',
		                  	initialValue:this.state.initroomId
		                  })(
		                    <Select showSearch allowClear placeholder="请选择" optionFilterProp="children"
		                        filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}>
			                      {this.state.classroomList?
			                        	this.state.classroomList.map((item,i)=> {
			                        		return(<Option key={item.id} value={item.id}>{ item.name }</Option>)
			                        	})
			                        	:null
			                      }
		                      </Select> 
		                  )}
            				</FormItem>
            				</div>
					</Modal>
					<Tabs defaultActiveKey="1" style={{textAlign:'center'}} tabPosition='top' animated={false} onChange={this.callback.bind(this)}>
						<TabPane tab={<span><i className='iconfont icon-hricon33 f-mr2'></i>精确查找</span>} key="1" style={{textAlign:'left'}}>
				    			<div style={{padding:'0 0 30px 26px'}}>
				    				<Input placeholder="请输入班级名称模糊查询" onChange={this.className.bind(this)} className='inputBG pageInpHeight f-radius1' style={{width:'352px',border:0}}/>
				    				<Button type="primary" icon="search" style={{height:'40px'}} onClick={this.jqSearch.bind(this)}>搜索</Button>
				    			</div>
				    		</TabPane>
					    <TabPane tab={<span><i className='iconfont icon-shaixuan f-mr2' ></i>高级筛选</span>} key="2">
					    		<Row gutter={40}>
					          <Col span={9} >
					          	 <Col span={7} className='f-fz4 f-black f-h4-lh4'>班级名称:&emsp;</Col>
					          	 <Col span={17} >
					          	 	<FormItem>
					                  {getFieldDecorator('className', {
					                  })(
					                  	<Input placeholder='请输入班级名称' className='inputBG pageInpHeight'/>
					                  )}
					                </FormItem>
							     </Col>
					          </Col>
					          <Col span={7} >
					          	 <Col span={8} className='f-fz4 f-black f-h4-lh4'>学员:&emsp;</Col>
					          	  <Col span={16} >
					          	  	<FormItem>
					                  {getFieldDecorator('studentName', {
					                  })(
					                  	<Input placeholder='请输入姓名或学号' className='inputBG pageInpHeight'/>
					                  )}
					                </FormItem>
						           </Col>
					          </Col>
					          <Col span={7} >
					             <Col span={8} className='f-fz4 f-black f-h4-lh4'>课程名称:&emsp;</Col>
					              <Col span={15} >
					              <FormItem {...formItemLayout}>
					                  {getFieldDecorator('courseId', {
					                  })(
							            <Select showSearch allowClear placeholder="全部课程" optionFilterProp="children"
					                        filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}>
						                      {this.state.course?
						                        	this.state.course.map((item,i)=> {
						                        		return(<Option key={item.id} value={item.id}>{ item.name }</Option>)
						                        	})
						                        	:null
						                      }
					                      </Select>
							          )}
					        	     	</FormItem>
						            </Col>
					          </Col>
					        </Row>
					        <Row className='' gutter={40}>
					        		<Col span={9}>
					        		<Col span={7} className='f-fz4 f-black f-h4-lh4'>上课日期:&emsp;</Col>
					        		<Col span={17} >
						    			 <Col span={11}>
						    			 <FormItem>
						                  {getFieldDecorator('startDate', {
						                  })(
							    			 	<DatePicker
									          disabledDate={this.disabledStartDate.bind(this)}
									          format="YYYY-MM-DD"
									          setFieldsValue={this.state.startValue}
									          placeholder="开始时间"
									          onChange={this.onStartChange.bind(this)}
									          onOpenChange={this.handleStartOpenChange.bind(this)}/>
						    			 	)}
				                			</FormItem>
						    			 </Col>
						    			 <Col span={2} className='f-align-center f-h4-lh4'>--</Col>
						    			  <Col span={11}>
						    			  <FormItem>
						                  {getFieldDecorator('endDate', {
						                  })(
								        <DatePicker
								          disabledDate={this.disabledEndDate.bind(this)}
								          format="YYYY-MM-DD"
								          setFieldsValue={this.state.endValue}
								          placeholder="结束时间"
								          onChange={this.onEndChange.bind(this)}
								          open={this.state.endOpen}
								          onOpenChange={this.handleEndOpenChange.bind(this)}/>
								        )}
				                			</FormItem>
							         	</Col>
							        	</Col>
					        		</Col>
					        		<Col span={7} >
					             <Col span={8} className='f-fz4 f-black f-h4-lh4'>教室:&emsp;</Col>
					              <Col span={16} >
					              	<FormItem {...formItemLayout}>
					                  {getFieldDecorator('classRoomId', {
					                  })(
							           <Select showSearch allowClear placeholder="请选择" optionFilterProp="children"
				                        filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}>
					                      {this.state.classroomList?
						                        	this.state.classroomList.map((item,i)=> {
						                        		return(<Option key={item.id} value={item.id}>{ item.name }</Option>)
						                        	})
						                        	:null
					                      }
				                      </Select> 
						            		)}
									</FormItem>
						            </Col>
					          	</Col>
					          	<Col span={7} >
					             <Col span={8} className='f-fz4 f-black f-h4-lh4'>上课老师:&emsp;</Col>
					              <Col span={15} >
					              	<FormItem {...formItemLayout}>
					                  {getFieldDecorator('teacherId', {
					                  })(
							            <Select
					                  	  showSearch
					                  	  allowClear
								          placeholder='请选择上课老师'
								          style={{ width: '100%' }}
								          optionFilterProp="children"
                        					filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
								        >
											{this.state.masterList?
							                        	this.state.masterList.map((item,i)=> {
							                        		return(<Option key={item.id} value={item.id}>{ item.name }</Option>)
							                        	})
							                        	:null
						                      }
								        </Select>
						            )}
									</FormItem>
						            </Col>
					          	</Col>
					        </Row>
					        <Row className='f-pb3'>
					        		<Col span={12} className='f-align-right f-pr4'>
					        			<Button style={{border: '1px solid #999999',borderRadius: '4px',width:'110px',height:'40px'}} onClick={this.reset.bind(this)}>
					        				<i className='iconfont icon-reset f-mr2'></i>重置
					        			</Button>
					        		</Col>
					        		<Col span={12} className='f-align-left'>
					        			<Button type="primary" style={{borderRadius: '4px',width:'110px',height:'40px'}} onClick={this.advancedSearch.bind(this)}>
					        				<i className='iconfont icon-hricon33 f-mr2'></i>搜索
					        			</Button>
					        		</Col>
					        </Row>
					    </TabPane>
					 </Tabs>
				</div>
				<div className='f-bg-white f-pd4 f-box-shadow1 f-mt5 f-radius1'>
					<div className='f-flex' style={{justifyContent: 'space-between'}}>
						<h3 className="f-title-blue f-mb3">记补课列表</h3>
					</div>
					<div>
						{this.state.pageResult?
							<div>
								<Table columns={columns} dataSource={this.state.pageResult} scroll={{ x: 1600}} bordered pagination={false}/>
									<div className='f-flex f-mt3' style={{justifyContent:'flex-end'}}>
										<div>
											<Pagination current={this.state.currentPage} total={this.state.total} onShowSizeChange={this.onShowSizeChange.bind(this)} showSizeChanger showQuickJumper onChange={this.pageChange.bind(this)}/>
										</div>
									</div>
							</div>:<Loading/>
						}
					</div>
				</div>
			</Form>
		)
	}
}

const RemedialTeach = Form.create()(Main);
export {
	RemedialTeach
}