import React, {
	Component
} from 'react';
import store from 'store2';
import {
	Breadcrumb,
	Button,
	Select,
	Table,
	Modal,
	Tabs,
	Input,
	DatePicker,
	Row,
	Col,
	Form,
	Pagination,
	message
} from 'antd';
import '../main.less';
//使用fetch请求接口数据
import $ from 'jquery';
import {
	Loading
} from '../../common/g.js';
import {
	fpost
} from '../../common/io.js';
const TabPane = Tabs.TabPane;
const confirm = Modal.confirm;
const Option = Select.Option;
const u = require('../../common/io.js')
const FormItem = Form.Item;
let classname = {};
//教务教学默认打开的首页
class Main extends Component {
	constructor(props) {
		super(props)
		this.state = {
			size: 'large',
			startValue: null,
			endValue: null,
			endOpen: false,
			className: null, //班级名称
			courseName: null, //课程名称
			headmaster: null, //班主任
			pageResult: null, //分页返回的列表总信息
			currentPage: 1, //当前页码
			pageSize: 10, //每页显示条数
			total: null, //总数
			searchState: 0,
			course: [],
			classroomList: [],
			masterList: [], //班主任列表
		}
		this.showDeleteConfirm = this.showDeleteConfirm.bind(this)
	}
	disabledStartDate = (startValue) => { //只读的开始日期
		const endValue = this.state.endValue;
		if (!startValue || !endValue) {
			return false;
		}
		return startValue.valueOf() > endValue.valueOf();
	}

	disabledEndDate = (endValue) => { //只读的结束日期
		const startValue = this.state.startValue;
		if (!endValue || !startValue) {
			return false;
		}
		return endValue.valueOf() <= startValue.valueOf();
	}

	onChange = (field, value) => {
		this.setState({
			[field]: value,
		});
	}
	//开始日期改变
	onStartChange = (value) => {
		this.onChange('startValue', value);
	}
	//结束日期改变
	onEndChange = (value) => {
		this.onChange('endValue', value);
	}

	handleStartOpenChange = (open) => {
		if (!open) {
			this.setState({
				endOpen: true
			});
		}
	}

	handleEndOpenChange = (open) => {
		this.setState({
			endOpen: open
		});
	}
	className(e) { //班级名称
		let v = e.target.value;
		this.setState({
			className: v
		});
	}

	reset() { //高级筛选里的重置
		this.props.form.resetFields();
		this.setState({
			currentPage: 1
		}, function() {
			this.advancedSearch()
		});
	}
	advancedSearch() { //筛选搜索
		this.setState({
			currentPage: 1
		}, function() {
			this._getForm(); //表单内容
		});
	}
	_getMaster = (v) => { //获取班主任下拉
		const self = this;
		fpost('/api/hr/staff/listStaffs', {
				name: v ? v : ''
			})
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					self.setState({
						masterList: res.result
					})
				} else {
					message.error(res.message)
				}
			});
	}
	_getClassroom() { //获取教室
		const self = this;
		fpost('/api/system/schoolarea/listClassRoom ')
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					self.setState({
						classroomList: res.result
					})
				} else {
					message.error(res.message)
				}
			});
	}
	_getForm() {
		this.props.form.validateFields(
			(err, fieldsValue) => {
				fieldsValue['name'] = fieldsValue['name'] ? fieldsValue['name'] : '';
				fieldsValue['startDate'] = fieldsValue['startDate'] ? fieldsValue['startDate'].format('YYYY-MM-DD HH:mm:ss') : '';
				fieldsValue['endDate'] = fieldsValue['endDate'] ? fieldsValue['endDate'].format('YYYY-MM-DD HH:mm:ss') : "";
				fieldsValue['pageSize'] = this.state.pageSize;
				fieldsValue['currentPage'] = this.state.currentPage;
				fieldsValue['courseId'] = fieldsValue['courseId'] ? fieldsValue['courseId'] : '';
				fieldsValue['headmasterId'] = fieldsValue['headmasterId'] ? fieldsValue['headmasterId'] : '';
				fieldsValue['defaultRoomId'] = fieldsValue['defaultRoomId'] ? fieldsValue['defaultRoomId'] : "";
				fieldsValue['state'] = fieldsValue['state'] ? fieldsValue['state'] : "";

				this.getList(fieldsValue);
			}
		)
	}
	getList(fieldsValue) { //获取列表
		let self = this;
		fpost('/api/educational/class/list', fieldsValue)
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					if (res.result) {
						res.result.records.forEach((item, i) => {
							item.key = i;
						});
						self.setState({
							pageResult: res.result.records,
							total: parseInt(res.result.total)
						}, function() {
							self._event();
						})
					}
				} else {
					message.error(res.message)
				}
			});
	}
	//是否确认删除对应的弹窗事件
	showDeleteConfirm(index) {
		let self = this;
		confirm({
			title: '确定要删除这条记录吗?',
			okText: '确定',
			okType: 'danger',
			cancelText: '取消',
			onOk() {
				self._delete(index);
			},
			onCancel() {},
		});
	}
	_delete(index) { //删除调用接口
		const self = this;
		$.ajax({
			url: u.hostname() + '/api/educational/class/delete',
			type: 'get',
			cache: false,
			headers: {
				'Accept': 'application/json, text/plain, */*',
				'Content-Type': 'application/x-www-form-urlencoded',
				'x-auth-token': store.get('sessionId'), //登录功能放开后需要加上的
			},
			data: {
				classId: index
			},
			success: function(res) {
				if (res.success == true) {
					message.success('删除成功');
					if (self.state.searchState == 0) {
						self.jqSearch();
					} else {
						self.advancedSearch();
					}
				} else {
					message.error(res.message)
				}
			}
		})
	}
	componentDidMount() {
		this._getMaster(); //班主任
		this._getClassroom();
		this.jqSearch();
		this._getCourse();
	}
	_event() {}
	_getCourse() { //获取课程
		const self = this;
		fpost('/api/system/course/listCourse')
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					self.setState({
						course: res.result
					})
				} else {
					message.error(res.message)
				}
			});
	}
	pageChange(pageNumber) { //列表分页获取当前页码
		this.setState({
			currentPage: pageNumber
		}, function() {
			if (!this.state.searchState) {
				classname['name'] = $.trim($('.inputBG').val());
				classname.pageSize = this.state.pageSize;
				classname.currentPage = this.state.currentPage;
				this.getList(classname);
			} else {
				this._getForm(); //表单内容
			}
		});
	}
	onShowSizeChange(current, pageSize) {
		this.setState({
			currentPage: current,
			pageSize: pageSize
		}, function() {
			if (this.state.searchState == 0) {
				this.jqSearch();
			} else {
				this.advancedSearch()
			}
		})
	}
	jqSearch() { //精确搜索
		this.setState({
			currentPage: 1,
		}, function() {
			classname['name'] = $.trim($('.inputBG').val());
			classname.pageSize = this.state.pageSize;
			classname.currentPage = this.state.currentPage;
			this.getList(classname);
		});
	}
	callback(key) {
		if (key == 2) {
			this.setState({
				searchState: 1
			})
		} else {
			this.setState({
				searchState: 0
			})
		}
	}
	render() {
		const columns = [{
			title: '学员名称（学号）',
			key: '0',
			fixed: 'left',
			width: 150,
			className: 'f-align-center select',
			render: (text, record, index) => {
				return (
					<i>小祁(010)</i>
				)
			}
		}, {
			title: '课程名称',
			key: '1',
			width: 180,
			className: 'f-align-center',
			render: (text, record) => {
				this._event();
				return (
					<div title={record.courseName} className='f-line1' style={{width:'180px'}}>
			   			<i className='iconfont icon-shu f-mr2'></i>
			   			<i className='courseName f-line1'>{record.courseName}</i>
			   		</div>
				)
			}
		}, {
			title: '当前课次',
			key: '2',
			width: 120,
			className: 'f-align-center',
			render: (text, record, index) => {
				return (
					<i>1</i>
				)
			}
		}, {
			title: '上课时间',
			dataIndex: 'classStartTime',
			key: '3',
			width: 180,
			className: 'f-align-center',
		}, {
			title: '班级',
			key: '4',
			width: 180,
			className: 'f-align-center',
			dataIndex: 'name'
		}, {
			title: '校区',
			dataIndex: 'schoolAreaName',
			key: '5',
			width: 170,
			className: 'f-align-center',
		}, {
			title: '上课老师',
			key: '6',
			width: 150,
			className: 'f-align-center timeSD',
			dataIndex: 'headmasterName'
		}, {
			title: '助教',
			key: '7',
			className: 'f-align-center',
			width: 150,
			dataIndex: 'headmasterName'
		}, {
			title: '备注',
			key: '8',
			className: 'f-align-center',
			width: 150,
			render: (text, record, index) => {
				return (<div>
						{record.state==1?
							'已排课未开课':null
						}
						{record.state==2?
							'已开课':null
						}
						{record.state==3?
							'已结课':null
						}
		            </div>);
			}
		}, {
			title: '补课时间',
			key: '9',
			className: 'f-align-center',
			width: 150,
			dataIndex: 'classRoomName'
		}, {
			title: '补课老师',
			key: '10',
			width: 150,
			className: 'f-align-center',
			dataIndex: 'headmasterName'
		}, {
			title: '补课教室',
			key: '11',
			width: 150,
			className: 'f-align-center',
			render: (text, record) => (
				<div>
					¥ {record.salePrice}
					{record.chargeMode==1?
						<img className='f-ml2 f-vertical-middle' src='http://img.fancyedu.com/sys/ic/operation/1512640700929_qi.png' style={{width:'30px',height:"30px"}}/>
						:null
					}
					{record.chargeMode==2?
						<img className='f-ml2 f-vertical-middle' src='http://img.fancyedu.com/sys/ic/operation/1510120850178_course.png' style={{width:'30px',height:"30px"}}/>
						:null
					}
					{record.chargeMode==3?
						<img className='f-ml2 f-vertical-middle' src='http://img.fancyedu.com/sys/ic/operation/1512640710303_shi.png' style={{width:'30px',height:"30px"}}/>
						:null
					}
				</div>
			)
		}, {
			title: '操作',
			key: '12',
			fixed: 'right',
			width: 150,
			className: 'f-align-center',
			render: (text, record, index) => {
				return (<div>
		                <i className='iconfont icon-bianji1 f-pointer f-mr5 f-fz5' onClick={()=>{window.location.href='/teach/modifyRecord?id?id='+record.id}}></i>
		                <i className='iconfont icon-chakan1 f-pointer f-fz5' onClick={()=>{window.location.href='/teach/checkRecord?id?id='+record.id}}></i>
		            </div>);
			}
		}];
		const {
			size
		} = this.state;
		const {
			getFieldDecorator
		} = this.props.form;
		const formItemLayout = {
			labelCol: {
				span: 6
			},
			wrapperCol: {
				span: 18
			}
		};
		return (
			<div className='classListPage wid100'>
				<Breadcrumb className="f-mb3">
				    <Breadcrumb.Item>教务教学</Breadcrumb.Item>
				    <Breadcrumb.Item>上课记录</Breadcrumb.Item>
				    <Breadcrumb.Item>
				   		<span className="f-fz5 f-bold">补课记录</span>
				    </Breadcrumb.Item>
				</Breadcrumb>
				<div className='Topselect f-bg-white f-radius1 f-box-shadow1' style={{width:'100%'}}>
					<Tabs defaultActiveKey="1" style={{textAlign:'center'}} tabPosition='top' animated={false} onChange={this.callback.bind(this)}>
						<TabPane tab={<span><i className='iconfont icon-hricon33 f-mr2'></i>精确查找</span>} key="1" style={{textAlign:'left'}}>
				    			<div style={{padding:'0 0 30px 26px'}}>
				    				<Input placeholder="请输入班级名称模糊查询" onChange={this.className.bind(this)} className='inputBG pageInpHeight f-radius1' style={{width:'352px',border:0}}/>
				    				<Button type="primary" icon="search" style={{height:'40px'}} onClick={this.jqSearch.bind(this)}>搜索</Button>
				    			</div>
				    		</TabPane>
					    <TabPane tab={<span><i className='iconfont icon-shaixuan f-mr2' ></i>高级筛选</span>} key="2">
					    		<Form style={{padding:'0 20px'}}>
					    		<Row gutter={40}>
					          <Col span={11} >
					          	 <Col span={6} className='f-fz4 f-black f-h4-lh4'>班级名称:&emsp;</Col>
					          	 <Col span={18} >
					          	 	<FormItem>
					                  {getFieldDecorator('name', {
					                  })(
					                  	<Input placeholder='请输入班级名称' className='inputBG pageInpHeight'/>
					                  )}
					                </FormItem>
							     </Col>
					          </Col>
					          <Col span={7} >
					          	 <Col span={8} className='f-fz4 f-black f-h4-lh4'>课程名称:&emsp;</Col>
					          	  <Col span={16} >
						          	 <FormItem>
						                  {getFieldDecorator('courseId', {
						                  	valuePropName: 'value',
						                  })(
						                  	<Select showSearch allowClear placeholder="请选择" optionFilterProp="children"
						                        filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}>
							                      {this.state.course?
								                        	this.state.course.map((item,i)=> {
								                        		return(<Option key={item.id} value={item.id}>{ item.name }</Option>)
								                        	})
								                        	:null
							                      }
						                      </Select>
						                   )}
				                  	</FormItem>
						           </Col>
					          </Col>
					          <Col span={6} >
					             <Col span={8} className='f-fz4 f-black f-h4-lh4'>班主任:&emsp;</Col>
					              <Col span={15} >
						             <FormItem {...formItemLayout}>
						                  {getFieldDecorator('headmasterId', {
						                  })(
						                  	 <Select
						                  	  showSearch
						                  	  allowClear
									          placeholder='请选择班主任'
									          style={{ width: '100%' }}
									          optionFilterProp="children"
	                        					filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
									        >
												{this.state.masterList?
								                        	this.state.masterList.map((item,i)=> {
								                        		return(<Option key={item.id} value={item.id}>{ item.name }</Option>)
								                        	})
								                        	:null
							                      }
									        </Select>
						                   )}
					                </FormItem>
						           </Col>
					          </Col>
					        </Row>
					        <Row className='' gutter={40}>
					        		<Col span={11}>
					        		<Col span={6} className='f-fz4 f-black f-h4-lh4'>开班时间:&emsp;</Col>
					        		<Col span={18} >
						    			<Col span={11}>
							    			 <FormItem>
						                  {getFieldDecorator('startDate', {
						                  })(
								            	<DatePicker showTime disabledDate={this.disabledStartDate.bind(this)} format="YYYY-MM-DD HH:mm:ss"
									          setFieldsValue={this.state.startValue} placeholder="开始时间"
									          onChange={this.onStartChange.bind(this)} onOpenChange={this.handleStartOpenChange.bind(this)}
									        />
								            	)}
						                </FormItem>
							    			 </Col>
							    			 <Col span={2} className='f-align-center f-h4-lh4'>--</Col>
							    			  <Col span={11}>
							    			  <FormItem>
						                  {getFieldDecorator('endDate', {
						                  })(
								            <DatePicker showTime disabledDate={this.disabledEndDate.bind(this)} format="YYYY-MM-DD HH:mm:ss"
									          setFieldsValue={this.state.endValue} placeholder="结束时间" onChange={this.onEndChange.bind(this)}
									          open={this.state.endOpen} onOpenChange={this.handleEndOpenChange.bind(this)}
									          />
								          )}
						                </FormItem>
							         	</Col>
							        	</Col>
					        		</Col>
					        		<Col span={7} >
					             <Col span={8} className='f-fz4 f-black f-h4-lh4'>教室:&emsp;</Col>
					              <Col span={16} >
					              <FormItem {...formItemLayout}>
					                  {getFieldDecorator('defaultRoomId', {
					                  })(
					                  	<Select showSearch allowClear placeholder="请选择" optionFilterProp="children"
					                        filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}>
						                      {this.state.classroomList?
							                        	this.state.classroomList.map((item,i)=> {
							                        		return(<Option key={item.id} value={item.id}>{ item.name }</Option>)
							                        	})
							                        	:null
						                      }
					                      </Select>
					                  )}
					                </FormItem>
						            </Col>
					          	</Col>
					          	<Col span={6} >
					             <Col span={8} className='f-fz4 f-black f-h4-lh4'>状态:&emsp;</Col>
					              <Col span={15} >
					              <FormItem {...formItemLayout}>
					                  {getFieldDecorator('state', {
					                  })(
					                  	<Select allowClear placeholder="全部状态" optionFilterProp="children"
					                        filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}>
					                        	 <Option value="">全部状态</Option>
								              <Option value="1">已排课未开课</Option>
								              <Option value="2">已开课</Option>
								              <Option value="3">已结课</Option>
					                      </Select>
					                   )}
					                </FormItem>
						            </Col>
					          	</Col>
					        </Row>
					        <Row className='f-pb3'>
					        		<Col span={12} className='f-align-right f-pr4'>
					        			<Button style={{border: '1px solid #999999',borderRadius: '4px',width:'110px',height:'40px'}} onClick={this.reset.bind(this)}>
					        				<i className='iconfont icon-reset f-mr2'></i>重置
					        			</Button>
					        		</Col>
					        		<Col span={12} className='f-align-left'>
					        			<Button type="primary" style={{borderRadius: '4px',width:'110px',height:'40px'}} onClick={this.advancedSearch.bind(this)}>
					        				<i className='iconfont icon-hricon33 f-mr2'></i>搜索
					        			</Button>
					        		</Col>
					        </Row>
					    		</Form>
					    </TabPane>
					 </Tabs>
				</div>
				<div className='f-bg-white f-pd4 f-box-shadow1 f-mt5 f-radius1 f-mb4'>
					<div className='f-flex' style={{justifyContent: 'space-between'}}>
						<h3 className="f-title-blue f-mb3">记补课列表</h3>
					</div>
					{this.state.pageResult?
						<div>
							<Table columns={columns} dataSource={this.state.pageResult} scroll={{ x: 2000}} bordered pagination={false}/>
								<div className='f-flex f-mt3' style={{justifyContent:'flex-end'}}>
									<div>
										<Pagination current={this.state.currentPage} total={this.state.total} onShowSizeChange={this.onShowSizeChange.bind(this)} showSizeChanger showQuickJumper onChange={this.pageChange.bind(this)}/>
									</div>
								</div>
						</div>:<Loading/>
					}
				</div>
				
			</div>
		)
	}
}
const RemedialRecord = Form.create()(Main);
export {
	RemedialRecord
}