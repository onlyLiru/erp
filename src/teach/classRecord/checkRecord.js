import React, {
	Component
} from 'react';
import store from 'store2';
import {
	Breadcrumb,
	Row,
	Col,
	Button,
	message
} from 'antd';
import '../main.less';
import {
	getUrlParam
} from '../../common/g.js';
import $ from 'jquery';
//使用fetch请求接口数据

const u = require('../../common/io.js');

//教务教学默认打开的首页
export class CheckRecord extends Component {
	constructor(props) {
		super(props)
		this.state = {
			studentList: [], //学员上课信息
			classStartDateTime: '', //上课时间
			consumePeriod: '', //消耗课时
			teacherName: '', //老师名称
			assistantName: '', //助教
			classRoomName: '', //教室
			classContent: '', //上课内容
			homework: '', //课后作业
			remark: '', //备注
			className: '', //班级名称
			registrationNum: '', //上课人数
			normalNum: '', //正常上课人数
		}
	}
	componentDidMount() {
		this._getDetail(); //获取详情
	}
	_getDetail() { //获取全部校区信息
		const self = this;
		$.ajax({
			type: "get",
			url: u.hostname() + "/api/educational/class/signup/read",
			cache: false,
			headers: {
				'Accept': 'application/json, text/plain, */*',
				'Content-Type': 'application/x-www-form-urlencoded',
				'x-auth-token': store.get('sessionId'), //登录功能放开后需要加上的
			},
			data: {
				classTimesDetailId: getUrlParam('id'),
			},
			success: function(res) {
				if (res.success == true) {
					if (res.result) {
						res.result.studentList.forEach((item, i) => {
							item.key = i;
						});
						self.setState({
							studentList: res.result.studentList,
							classStartDateTime: res.result.classStartDateTime,
							consumePeriod: res.result.consumePeriod,
							teacherName: res.result.teacherName,
							assistantName: res.result.assistantName,
							classRoomName: res.result.classRoomName,
							classContent: res.result.classContent,
							homework: res.result.homework,
							remark: res.result.remark,
							className: res.result.className,
							registrationNum: res.result.registrationNum,
							normalNum: res.result.normalNum
						})
					}
				} else {
					message.error(res.message)
				}
			}
		});
	}
	render() {
		return (
			<div className='checkRecord'>
				<Breadcrumb className="f-mb3">
				    <Breadcrumb.Item>教务教学</Breadcrumb.Item>
				    <Breadcrumb.Item>上课记录</Breadcrumb.Item>
				    <Breadcrumb.Item>
				   		<span className="f-fz5 f-bold">查看上课记录</span>
				    </Breadcrumb.Item>
				</Breadcrumb>
				<div className='f-box-shadow1 f-radius1 f-bg-white f-mb5 f-pd5 f-pt3' style={{paddingBottom:'35px'}}>
					<div className='f-fz5 f-black f-mb5'>
						{this.state.className}
					</div>
					<Row className='f-mb5'>
						<Col span={24}>
							<span className='f-fz3 f-dark f-mr5'>上课时间</span>
							<span className='f-fz3 f-dark' style={{marginRight:'60px'}}>{this.state.classStartDateTime}</span>
							<span className='f-fz3 f-dark f-mr5'>消耗课时:</span>
							<span className='f-fz3 f-dark f-mr5'>{this.state.consumePeriod}</span>
						</Col>
					</Row>
					{this.state.studentList?this.state.studentList.map((item, i) => {
						return(
							<div key={i} style={{width:'100%'}} className='f-flex f-mb4'>
								<ul className='f-flex f-dark' style={{width:'40%',height:'30px'}}>
									<li className='f-fz5 f-bold' style={{marginRight:'40px'}}>{item.studentName}</li>
									<li className='f-fz3'>{item.signUpStatusName}</li>
								</ul>
								<ul className='f-flex f-dark' style={{width:'60%',height:'30px'}}>
									<li className='f-fz3 f-mr2'>纪律专注</li>
									<li className='f-fz3' style={{marginRight:'40px'}}>{item.disciplineFocus}</li>
									<li className='f-fz3 f-mr2'>活跃参与</li>
									<li className='f-fz3' style={{marginRight:'40px'}}>{item.active}</li>
								</ul>
							</div>
						)
					}):''}
					
					<div style={{width:'100%'}} className='f-flex f-mb4 f-bold'>
						<ul className='f-flex f-dark' style={{width:'40%',height:'30px'}}>
							<li className='f-fz3 f-mr1'>教师：</li>
							<li className='f-fz4' style={{marginRight:"40px"}}>{this.state.teacherName}</li>
							<li className='f-fz3 f-mr1'>助教：</li>
							<li className='f-fz4 f-mr1'>{this.state.assistantName}</li>
						</ul>
						<ul className='f-flex f-dark' style={{width:'60%',height:'30px'}}>
							<li className='f-fz3 f-mr1'>教室：</li>
							<li className='f-fz4' style={{marginRight:"40px"}}>{this.state.classRoomName}</li>
							<li className='f-fz3 f-mr1'>上课人数：</li>
							<li className='f-fz4 f-mr1'>{this.state.normalNum}人（总计{this.state.registrationNum}人）</li>
						</ul>
					</div>
				
				</div>
				<div className='f-box-shadow1 f-radius1 f-bg-white f-mb5 f-pd5 f-pt3'>
					<Row className='f-mb4'>
						<Col span={2} className='f-fz3 f-fz3'>
							上课内容：
						</Col>
						<Col span={22} className='f-fz4 f-fz3'>
							{this.state.classContent}
						</Col>
					</Row>
					<Row className='f-mb4'>
						<Col span={2} className='f-fz3 f-fz3'>
							课后作业：
						</Col>
						<Col span={22} className='f-fz4 f-fz3'>
							{this.state.homework}
						</Col>
					</Row>
					<Row>
						<Col span={2} className='f-fz3 f-fz3'>
							备注：
						</Col>
						<Col span={22} className='f-fz4 f-fz3'>
							{this.state.remark}
						</Col>
					</Row>
				</div>
				<Button style={{height:'40px'}} className='f-right' onClick={()=>{window.history.go(-1)}}>
        				<i className='iconfont icon-fanhui f-mr2'></i>
        				返回
        			</Button>
			</div>
		)
	}
}