import React, {
	Component
} from 'react';
import { Breadcrumb, Button, Table, Modal, DatePicker, Form, Row, Col, message, Input, Pagination } from 'antd';
import '../main.less';
import { Loading } from '../../common/g.js';
import { fpost } from '../../common/io.js';

const FormItem = Form.Item;

class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			visible: false,
			pageResult: null,
			currentPage: 1, //当前页面数
			pageSize: 10, //每页页面显示的列表条数
			total: null, //页面列表总数
		}
	}
	showModal = () => {
		this.setState({
			visible: true, //确定对话框的展示
		});
	}
	handleOk = (e) => { //对话框里的确定按钮对应事件
		this.setState({
			visible: false,
		});
	}
	handleCancel = (e) => { //对话框里的取消按钮对应的事件
		this.props.form.resetFields(); //重置
		this.setState({
			visible: false,
		});
	}
	componentDidMount() {
		this._getData();
	}
	disabledStartDate = (startValue) => { //只读的开始日期
		const endValue = this.state.endValue;
		if(!startValue || !endValue) {
			return false;
		}
		return startValue.valueOf() > endValue.valueOf();
	}

	disabledEndDate = (endValue) => { //只读的结束日期
		const startValue = this.state.startValue;
		if(!endValue || !startValue) {
			return false;
		}
		return endValue.valueOf() <= startValue.valueOf();
	}

	onChange = (field, value) => {
		this.setState({
			[field]: value,
		});
	}
	//开始日期改变
	onStartChange = (value) => {
		this.onChange('startValue', value);
	}
	//结束日期改变
	onEndChange = (value) => {
		this.onChange('endValue', value);
	}

	handleStartOpenChange = (open) => {
		if(!open) {
			this.setState({
				endOpen: true
			});
		}
	}

	handleEndOpenChange = (open) => {
		this.setState({
			endOpen: open
		});
	}
	_search() { //筛选搜索
		this.setState({
			currentPage: 1
		}, function() {
			this.props.form.validateFields(
				(err, fieldsValue) => {
					this._getData(fieldsValue);
				}
			)
		});
	}
	onShowSizeChange (current, pageSize) {
		this.setState({
			currentPage:current,
			pageSize:pageSize
		},function(){
			this._getData();
		})
	}
	_getData(fieldsValue) { //获取列表
		let self = this;
		this.props.form.validateFields(
			(err, fieldsValue) => {
				if(!fieldsValue.name) {
					fieldsValue.name = '';
				}
				fieldsValue.startDate =fieldsValue.startDate? fieldsValue['startDate'].format('YYYY-MM-DD'):'';
				fieldsValue.endDate = fieldsValue.endDate?fieldsValue['endDate'].format('YYYY-MM-DD'):'';
				fieldsValue.pageSize = self.state.pageSize;
				fieldsValue.currentPage = self.state.currentPage;
				self._getListData(fieldsValue);
			})
	}
	_getListData(fieldsValue) {
		let self = this;
		fpost('/api/hr/staff/pageTeacherHourStatistics ', fieldsValue)
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if(res.success == true) {
					if(res.result) {
						res.result.records.forEach((item, i) => {
							item.key = i;
						});
						self.setState({
							pageResult: res.result.records,
							total: parseInt(res.result.total)
						})
					}
				} else {
					message.error(res.message)
				}
			});
	}
	pageChange(pageNumber) { //列表分页获取当前页码
		this.setState({
			currentPage: pageNumber
		}, function() {
			this.props.form.validateFields(
				(err, fieldsValue) => {
					this._getData(fieldsValue);
				}
			)
		});
	}
	render() {
		const {
			getFieldDecorator
		} = this.props.form;
		const columns = [{
			title: '教师姓名（工号）',
			key: '1',
			className: 'f-align-center',
			width: 150,
			dataIndex: 'name'
		}, {
			title: '班级数',
			dataIndex: 'classCount',
			className: 'f-align-center',
			key: '2',
			width: 150,
		}, {
			title: '学生课时总计',
			key: '3',
			className: 'f-align-center',
			width: 150,
			dataIndex: 'studentHourCount'
		}, {
			title: '老师课时总计',
			className: 'f-align-center',
			key: '4',
			width: 150,
			dataIndex: 'teacherHourCount'
		}, {
			title: '操作',
			className: 'f-align-center',
			key: '12',
			width: 150,
			render: (text, record, index) => {
				return(
					<i className='iconfont icon-chakan f-pointer' onClick={()=>{window.location.href='/teach/statisticsDetail?id='+record.id}}></i>
				)
			}
		}];
		
		return(<div className='courseArranging classListPage wid100'>
				<Breadcrumb className="f-mb3">
				    <Breadcrumb.Item>教务教学</Breadcrumb.Item>
				    <Breadcrumb.Item>教务</Breadcrumb.Item>
				    <Breadcrumb.Item>
				   		<span className="f-fz5 f-bold">教师课时统计</span>
				    </Breadcrumb.Item>
				</Breadcrumb>
				<div>
				<div className='f-bg-white Topselect f-radius1 f-box-shadow1' style={{padding:'20px 20px 15px 20px'}}>
					<Form>
						 <Row className='' gutter={40}>
						 		<Col span={8} >
						 			<FormItem>
					        	          {getFieldDecorator('name', {
					        	          })(
						             	<Input placeholder='请输入教师姓名/工号' className='pageInpHeight'/>
					          		)}
					        	     </FormItem>
					          </Col>
					        		<Col span={12}>
					        			<Col span={5} className='f-align-right f-fz4 f-black f-h4-lh4'>时间段:&emsp;</Col>
						        		<Col span={19} >
							    			 <Col span={11}>
							    			 <FormItem>
								          {getFieldDecorator('startDate')(
								            	<DatePicker
									          disabledDate={this.disabledStartDate.bind(this)}
									          format="YYYY-MM-DD"
									          setFieldsValue={this.state.startValue}
									          placeholder="开始时间"
									          onChange={this.onStartChange.bind(this)}
									          onOpenChange={this.handleStartOpenChange.bind(this)}/>
								            	)}
								     	</FormItem>
							    			 </Col>
							    			 <Col span={2} className='f-align-center f-h4-lh4'>--</Col>
							    			  <Col span={11}>
							    			  <FormItem>
								          {getFieldDecorator('endDate')(
								            <DatePicker
									          disabledDate={this.disabledEndDate.bind(this)}
									          format="YYYY-MM-DD"
									          setFieldsValue={this.state.endValue}
									          placeholder="结束时间"
									          onChange={this.onEndChange.bind(this)}
									          open={this.state.endOpen}
									          onOpenChange={this.handleEndOpenChange.bind(this)}/>
								            )}
								     	</FormItem>
								         	</Col>
								        	</Col>
					        		</Col>
					        		
					          	<Col span={4}>
		    							<Button type="primary" style={{borderRadius: '4px',width:'110px',height:'40px'}} onClick={this._search.bind(this)}>
					        				<i className='iconfont icon-hricon33 f-mr2'></i>
					        				搜索
					        			</Button>
					          	</Col>
					        </Row>
					</Form>
				</div>
			
				<div className='f-bg-white f-pd4 f-box-shadow1 f-mt5 f-radius1 '>
					<div className='f-pb3 f-flex' style={{justifyContent: 'space-between'}}>
						<h3 className="f-title-blue">教师课时统计表</h3>
					</div>
					<Modal
			          title='操作'
			          visible={this.state.visible}
			          onOk={this.handleOk}
			          onCancel={this.handleCancel}
			          footer={[
			            <Button key="back" size="large" onClick={this.handleCancel}>取消</Button>,
			            <Button key="submit" type="primary" size="large" onClick={this.handleOk}>
			              确定
			            </Button>,
			          ]}>
						<Row>
							<Col span={6}>
								<a className="uk-form-file f-mr5 f-relative f-btn-blue f-pointer" style={{overflow:'hidden'}}>
									导入员工工资
									<input id="upload-select" type="file" className='f-pointer'/>
								</a>
							</Col>
							<Col span={6}>
								<a className='f-btn' style={{border: "1px solid #D9D9D9",color:'#666666'}}>
									下载工资模版
								</a>
							</Col>
						</Row>
					</Modal>
					{this.state.pageResult?
						<div>
							<Table columns={columns} dataSource={this.state.pageResult} bordered pagination={false}/>
								<div className='f-flex f-mt3' style={{justifyContent:'flex-end'}}>
									<div>
										<Pagination current={this.state.currentPage} total={this.state.total} onShowSizeChange={this.onShowSizeChange.bind(this)} showSizeChanger showQuickJumper onChange={this.pageChange.bind(this)}/>
									</div>
								</div>
						</div>:<Loading/>
					}
				</div>
				</div>
		</div>);
	}
}
const HourStatistics = Form.create()(Main);
export {
	HourStatistics
}