import React, {
	Component
} from 'react';
import { Breadcrumb, Button, Table, Form, Row, Col, Select, message } from 'antd';
import '../main.less';
import { fpost } from '../../common/io.js';
import {getUrlParam } from '../../common/g.js';
let pageId = getUrlParam('id') || '';

const Option = Select.Option;
const FormItem = Form.Item;

class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			teachersData: [],
			className: null,
			pageData: null,
			currentPage: 1, //当前页码
			total: null, //总条数
			classIds: [],
			teacherHourCount: null,
			studentHourCount: null,
			result: 0
		}
	}
	pageChange(pageNumber) { //列表分页获取当前页码
		this.setState({
			currentPage: pageNumber
		}, function() {
			this.props.form.validateFields(
				(err, fieldsValue) => {
					this._getData(fieldsValue);
				}
			)
		});
	}
	componentDidMount() {
		this._getClass(); //获取全部班级信息
	}
	_getClass() {
		const self = this;
		fpost('/api/hr/staff/listTeachserClasses', {
				teacherId: pageId
			})
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if(res.success == true) {
					if(res.result) {
						self.setState({
							classIds: res.result
						});
					}
				} else {
					message.error(res.message)
				}
			});
	}
	disabledStartDate = (startValue) => { //只读的开始日期
		const endValue = this.state.endValue;
		if(!startValue || !endValue) {
			return false;
		}
		return startValue.valueOf() > endValue.valueOf();
	}

	disabledEndDate = (endValue) => { //只读的结束日期
		const startValue = this.state.startValue;
		if(!endValue || !startValue) {
			return false;
		}
		return endValue.valueOf() <= startValue.valueOf();
	}

	onChange = (field, value) => {
		this.setState({
			[field]: value,
		});
	}
	//开始日期改变
	onStartChange = (value) => {
		this.onChange('startValue', value);
	}
	//结束日期改变
	onEndChange = (value) => {
		this.onChange('endValue', value);
	}

	handleStartOpenChange = (open) => {
		if(!open) {
			this.setState({
				endOpen: true
			});
		}
	}

	handleEndOpenChange = (open) => {
		this.setState({
			endOpen: open
		});
	}
	reset() { //高级筛选里的重置
		this.props.form.resetFields();
	}
	changeClass(e) {
		this.setState({
			className: e
		});
	}
	_search() { //搜索
		this.setState({
			currentPage: 1
		}, function() {
			this.props.form.validateFields(
				(err, fieldsValue) => {
					this._getData(fieldsValue);
				}
			)
		});
	}
	_getData(fieldsValue) { //获取列表
		let self = this;
		this.props.form.validateFields(
			(err, fieldsValue) => {
				if(!fieldsValue.classId) {
					fieldsValue.classId = '';
				}
				self._getListData(fieldsValue);
			})
	}
	_getListData(fieldsValue) {
		let self = this;
		fpost('/api/hr/staff/listTeacherHourClassStatistics ', fieldsValue)
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if(res.success == true) {
					if(res.result) {

						self.setState({
							pageData: res.result.teacherHourList,
							total: parseInt(res.result.total),
							studentHourCount: res.result.studentHourCount,
							teacherHourCount: res.result.teacherHourCount,
						})
						if(self.state.pageData.length == 0) {
							self.setState({
								result: 0
							})
						} else {
							self.setState({
								result: 1
							})
						}
					}
				} else {
					message.error(res.message)
				}
			});
	}
	render() {
		const {
			getFieldDecorator
		} = this.props.form;
		const formItemLayout = {
			labelCol: {
				span: 6
			},
			wrapperCol: {
				span: 18
			}
		};
		const columns = [{
			title: '教师姓名（工号）',
			key: '2',
			className: 'f-align-center',
			width: 150,
			render: (text, record, index) => {
				return(
					<span style={{color: '#4A4A4A'}}>{record.name}</span>
				)
			}
		}, {
			title: '时间',
			key: '1',
			className: 'f-align-center',
			width: 150,
			dataIndex: 'classTime'
		}, {
			title: '校区',
			className: 'f-align-center',
			key: '3',
			width: 150,
			dataIndex:'schoolArea',
		}, {
			title: '班级',
			dataIndex: 'className',
			key: '4',
			className: 'f-align-center',
			width: 150,
		}, {
			title: '上课人数',
			className: 'f-align-center',
			dataIndex: 'attendClassCount',
			key: '5',
			width: 150,
		}, {
			title: '学生课时',
			key: '6',
			className: 'f-align-center',
			width: 150,
			dataIndex: 'studentHourCount'
		}, {
			title: '教师课时',
			key: '7',
			className: 'f-align-center',
			width: 150,
			dataIndex: 'teacherHourCount'
		}];

		return(<div className='courseArranging classListPage wid100'>
				<Breadcrumb className="f-mb3">
				    <Breadcrumb.Item>教务教学</Breadcrumb.Item>
				    <Breadcrumb.Item>教务</Breadcrumb.Item>
				    <Breadcrumb.Item>
				   		<span className="f-fz5 f-bold">教师课时统计详情</span>
				    </Breadcrumb.Item>
				</Breadcrumb>
				<div>
				<div className='f-bg-white Topselect f-radius1 f-box-shadow1' style={{padding:'20px 20px 15px 20px'}}>
					<Form>
						 <Row className='' gutter={40}>
						 		<Col span={8} >
					        			<Col span={4} className='f-fz4 f-black'>班级:</Col>
						 			<Col span={20}>
						              <FormItem {...formItemLayout}>
						                  {getFieldDecorator('classId', {
						                  })(
						                  	<Select showSearch
						                        allowClear
						                        placeholder="选择班级"
						                        optionFilterProp="children"
						                        filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
						                  	>
						                        {
							                        	this.state.classIds.map((item,i)=> {
							                        		return(<Option key={i} value={item.id}>{ item.name }</Option>)
							                        	})
							                        }
						                    </Select>  
						                  )}
						                </FormItem>
					                </Col>
					          	</Col>
					          	<Col span={6} className='f-pdl3'>
		    							<Button type="primary" style={{borderRadius: '4px',width:'110px',height:'40px'}} onClick={this._search.bind(this)}>
					        				<i className='iconfont icon-hricon33 f-mr2'></i>
					        				搜索
					        			</Button>
					          	</Col>
					        </Row>
					</Form>
				</div>
			{this.state.result?
				<div className='f-bg-white f-pd4 f-box-shadow1 f-mt5 f-radius1 '>
					<div className='f-pb3 f-flex f-inline-block'>
						<h3 className="f-title-blue">教师课时统计表</h3>
						<i style={{color: ' #2187FF'}} className='f-fz3'>（学生课时总计{this.state.studentHourCount}/教师课时总计{this.state.teacherHourCount}）</i>
					</div>
						<div>
							<Table columns={columns} dataSource={this.state.pageData} bordered pagination={false}/>
						</div>
				</div>:null
			}
				</div>
		</div>);
	}
}
const StatisticsDetail = Form.create()(Main);
export {
	StatisticsDetail
}