import React, {
	Component
} from 'react';
import { Breadcrumb, Table, Form, Row, Col, message } from 'antd';
import '../main.less';
import { fpost } from '../../common/io.js'; //同步、异步请求
import {getUrlParam } from '../../common/g.js';
const u=require('../../common/io.js');

class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			visible: false,
			batch: false, //是否批量
			teachersData: [],
			pageResult:null,
			classTimesDetailVO:{}
		}
	}
	componentDidMount() {
		this._getDetail();  //    获取页面详情
	}
	_getDetail() { //获取全部校区信息
		let self = this;
		fpost('/api/educational/class/times/detail/checkAttendance', {
			classTimesDetailId:getUrlParam('id')
		})
		.then((res) => {
			return res.json();
		})
		.then((res) => {
			if(res.success == true) {
				if(res.result) {
					if(res.result.studentList){
						res.result.studentList.forEach((item, i) => {
							item.key = i;
						});
					}
					self.setState({
						pageResult: res.result.studentList,
						classTimesDetailVO:res.result.classTimesDetailVO
					})
				}
			} else {
				message.error(res.message)
			}
		});
  }
	render() {
		const columns = [{
			title: '姓名（学号）',
			key: '1',
			className: 'f-align-center',
			width: 150,
			render: (text, record, index) => {
				return(
					<i>{record.name}({record.stuNo})</i>
				)
			}
		}, {
			title: '性别',
			className: 'f-align-center',
			key: '2',
			width: 50,
			render: (text, record, index) => {
				return(
					<i>
						{record.gender=='1'?'男':''}
						{record.gender=='2'?'女':''}
					</i>
				)
			}
		}, {
			title: '母亲电话',
			key: '3',
			className: 'f-align-center',
			width: 140,
			dataIndex: 'motherPhone'
		}, {
			title: '父亲电话',
			className: 'f-align-center',
			key: '4',
			width: 140,
			dataIndex: 'fatherPhone'
		}, {
			title: '其他电话',
			className: 'f-align-center',
			dataIndex: 'otherPhone',
			key: '5',
			width: 140,
		}, {
			title: '上课情况',
			className: 'f-align-center',
			dataIndex: 'name',
			key: '6',
			className: 'f-align-center',
			width: 300,
			render: (text, record, index) => {
				return(
					<div>
			  			<input disabled className='f-mr1' style={{border:' 1px solid #D9D9D9',borderRadius: '2px',width:'18px',height:'18px'}}/>
			  			<i className='f-fz4 f-mr4' style={{color:' #555D6A'}}>出勤</i>
			  			<input disabled className='f-mr1' style={{border:' 1px solid #D9D9D9',borderRadius: '2px',width:'18px',height:'18px'}}/>
			  			<i className='f-fz4 f-mr4' style={{color:' #555D6A'}}>请假</i>
			  			<input disabled className='f-mr1' style={{border:' 1px solid #D9D9D9',borderRadius: '2px',width:'18px',height:'18px'}}/>
			  			<i className='f-fz4 f-mr4' style={{color:' #555D6A'}}>旷课</i>
			  		</div>
				)
			}
		}];
		return(<div className='courseArranging classListPage wid100 pointListDetail'>
				<Breadcrumb className="f-mb3">
				    <Breadcrumb.Item>教务教学</Breadcrumb.Item>
				    <Breadcrumb.Item>教务</Breadcrumb.Item>
				    <Breadcrumb.Item>
				   		<span className="f-fz5 f-bold">点名表详情</span>
				    </Breadcrumb.Item>
				</Breadcrumb>
				<div className='section-to-print'>
				<div className='f-bg-white f-radius1' style={{padding:'10px 20px 30px 20px'}}>
					<div className='f-fz5 f-black f-mb5'>{this.state.classTimesDetailVO.schoolAreaName}</div>
					<Row className='f-mb2'>
						<Col span={8}>
							<span className='f-dark f-fz3'>班级：</span>
							<span className='f-15 f-fz3'>{this.state.classTimesDetailVO.className}</span>
						</Col>
						<Col span={8}>
							<span className='f-dark f-fz3'>课程：</span>
							<span className='f-15 f-fz3'>{this.state.classTimesDetailVO.courseName}</span>
						</Col>
						<Col span={8}>
							<span className='f-dark f-fz3'>教师：</span>
							<span className='f-15 f-fz3'>{this.state.classTimesDetailVO.teacherName}</span>
						</Col>
					</Row>
					<Row>
						<Col span={8}>
							<span className='f-dark f-fz3'>教室：</span>
							<span className='f-15 f-fz3'>{this.state.classTimesDetailVO.classRoomName}</span>
						</Col>
						<Col span={8}>
							<span className='f-dark f-fz3'>开班时间：</span>
							<span className='f-15 f-fz3'>{this.state.classTimesDetailVO.classDate}</span>
						</Col>
						<Col span={8}>
							<span className='f-dark f-fz3'>上课时间：</span>
							<span className='f-15 f-fz3'>{this.state.classTimesDetailVO.displayStartDate}</span>
						</Col>
					</Row>
				</div>
			
				<div className='f-bg-white f-pd4 f-mt5 f-radius1 '>
					<div className='f-pb2 f-flex' style={{justifyContent: 'space-between'}}>
						<h3 className="f-title-blue f-mb3">班级学生列表</h3>
						<div className='section-not-to-print'><a className="f-btn-blue"  onClick={()=>{window.print()}}>打印</a></div>
					</div>
					<div className='f-bg-white print '>
					{this.state.pageResult?this.state.pageResult.length>0?
						<Table columns={columns} dataSource={this.state.pageResult} bordered  pagination={false}/>:
						<div className='f-align-center'>暂无数据...</div>
						:<div className='f-align-center'>暂无数据...</div>
						
					}
					</div>
				</div>
				
				</div>
		</div>);
	}
}
const PointListDetail = Form.create()(Main);
export {
	PointListDetail
}