import React, {
	Component
} from 'react';
import {
	Breadcrumb,
	Button,
	Select,
	Table,
	Tabs,
	Input,
	DatePicker,
	Row,
	Col,
	Form,
	message,
	Pagination
} from 'antd';
import '../main.less';
//使用fetch请求接口数据
import $ from 'jquery'
import {
	fpost
} from '../../common/io.js'; //同步、异步请求
import {
	Loading,
	getUnicodeParam
} from '../../common/g.js';
const TabPane = Tabs.TabPane;
const Option = Select.Option;
const FormItem = Form.Item;
let classname = {};

//教务教学默认打开的首页
class Main extends Component {
	constructor(props) {
		super(props)
		this.state = {
			startValue: null,
			endValue: null,
			endOpen: false,
			className: null, //班级名称
			courseName: null, //课程名称
			teacher: null, //上课老师
			startTime: null, //开始时间
			endTime: null, //结束时间
			assistant: null, //助教
			classroom: null, //教室
			state: null, //搜索状态
			currentPage: 1, //当前页码
			pageSize: 10, //每页条数
			total: 40, //总条数
			loading: 1, //确定是否加载中
			pageResult: null,
			searchState: 0,
			course: [], //获取课程
			masterList: [], //班主任列表
			zhujiaoData: [], //助教列表
			classroomList: [], //获取教室
			name: getUnicodeParam('name')
		}
	}
	disabledStartDate = (startValue) => { //只读的开始日期
		const endValue = this.state.endValue;
		if (!startValue || !endValue) {
			return false;
		}
		return startValue.valueOf() > endValue.valueOf();
	}

	disabledEndDate = (endValue) => { //只读的结束日期
		const startValue = this.state.startValue;
		if (!endValue || !startValue) {
			return false;
		}
		return endValue.valueOf() <= startValue.valueOf();
	}

	onChange = (field, value) => {
		this.setState({
			[field]: value,
		});
	}
	//开始日期改变
	onStartChange = (value) => {
		this.onChange('startValue', value);
	}
	//结束日期改变
	onEndChange = (value) => {
		this.onChange('endValue', value);
	}

	handleStartOpenChange = (open) => {
		if (!open) {
			this.setState({
				endOpen: true
			});
		}
	}

	handleEndOpenChange = (open) => {
		this.setState({
			endOpen: open
		});
	}
	reset() { //高级筛选里的重置
		// 	let v=this.props.form.getFieldValue ('courseName');
		this.props.form.resetFields();
		this.setState({
			currentPage: 1
		}, function() {
			this._getForm();
		});
	}
	className(e) { //班级名称
		let v = e.target.value;
		this.setState({
			className: v
		});
	}
	jqSearch() { //精确查找
		this.setState({
			currentPage: 1,
		}, function() {
			classname['className'] = $.trim($('.inputBG').val());
			classname.pageSize = this.state.pageSize;
			classname.currentPage = this.state.currentPage;
			this.getList(classname);
		});
	}
	componentDidMount() {
		this.jqSearch();
		this._getCourse(); //获取全部课程
		this._getMaster();
		this._getZhujiao();
		this._getClassroom();
	}
	_getClassroom() { //获取教室
		const self = this;
		fpost('/api/system/schoolarea/listClassRoom ')
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					self.setState({
						classroomList: res.result
					})
				} else {
					message.error(res.message)
				}
			});
	}
	_getMaster() { //获取班主任下拉
		const self = this;
		fpost('/api/hr/staff/listStaffByCondForDropDown', {
				type: 4
			})
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					self.setState({
						masterList: res.result
					})
				} else {
					message.error(res.message)
				}
			});
	}
	_getZhujiao() { //获取助教下拉
		const self = this;
		fpost('/api/hr/staff/listStaffByCondForDropDown', {
				type: 5
			})
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					self.setState({
						zhujiaoData: res.result
					})
				} else {
					message.error(res.message)
				}
			});
	}
	_getCourse() { //获取课程
		const self = this;
		fpost('/api/system/course/listCourse')
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					self.setState({
						course: res.result
					})
				} else {
					message.error(res.message)
				}
			});
	}
	advancedSearch() { //筛选搜索
		this.setState({
			currentPage: 1
		}, function() {
			this._getForm(); //表单内容
		});
	}
	onShowSizeChange(current, pageSize) {
		this.setState({
			currentPage: current,
			pageSize: pageSize
		}, function() {
			if (this.state.searchState == 0) {
				this.jqSearch();
			} else {
				this.advancedSearch()
			}
		})
	}
	pageChange(pageNumber) { //列表分页获取当前页码
		this.setState({
			currentPage: pageNumber
		}, function() {
			if (!this.state.searchState) {
				classname['className'] = $.trim($('.inputBG').val());
				classname.pageSize = this.state.pageSize;
				classname.currentPage = this.state.currentPage;
				this.getList(classname);
			} else {
				this._getForm(); //表单内容
			}
		});
	}
	_getForm() {
		this.props.form.validateFields(
			(err, fieldsValue) => {
				fieldsValue.isSignUp = fieldsValue.isSignUp ? fieldsValue.isSignUp : '';
				fieldsValue.className = fieldsValue.className ? fieldsValue.className : '';
				fieldsValue.courseId = fieldsValue.courseId ? fieldsValue.courseId : '';
				fieldsValue.teacherId = fieldsValue.teacherId ? fieldsValue.teacherId : '';
				fieldsValue.roomId = fieldsValue.roomId ? fieldsValue.roomId : '';
				fieldsValue.assistantId = fieldsValue.assistantId ? fieldsValue.assistantId : '';
				fieldsValue['startDate'] = fieldsValue['startDate'] ? fieldsValue['startDate'].format('YYYY-MM-DD') : '';
				fieldsValue['endDate'] = fieldsValue['endDate'] ? fieldsValue['endDate'].format('YYYY-MM-DD') : "";
				fieldsValue['pageSize'] = this.state.pageSize;
				fieldsValue['currentPage'] = this.state.currentPage;
				this.getList(fieldsValue);
			}
		)
	}
	callback(key) {
		if (key == 2) {
			this.setState({
				searchState: 1
			})
		} else {
			this.setState({
				searchState: 0
			})
		}
	}
	getList(fieldsValue) { //获取列表
		let self = this;
		fpost('/api/educational/class/times/detail/list', fieldsValue)
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					if (res.result) {
						res.result.records.forEach((item, i) => {
							item.key = i;
						});
						self.setState({
							pageResult: res.result.records,
							total: parseInt(res.result.total)
						})
					}
				} else {
					message.error(res.message)
				}
			});
	}
	render() {
		const columns = [{
			title: '班级名称',
			key: '1',
			fixed: 'left',
			width: 150,
			className: 'f-align-center',
			dataIndex: 'className'
		}, {
			title: '上课老师',
			dataIndex: 'teacherName',
			key: '2',
			width: 150,
			className: 'f-align-center',
		}, {
			title: '助教',
			dataIndex: 'assistantName',
			key: '3',
			width: 150,
			className: 'f-align-center',
		}, {
			title: '课程',
			dataIndex: 'name',
			key: '4',
			width: 150,
			className: 'f-align-center',
			render: (text, record) => (
				<div title={record.courseName}>
			   			<i className='iconfont icon-shu f-mr2'></i>
			   			<i className='courseName f-line1'>{record.courseName}</i>
			   		</div>
			)
		}, {
			title: '上课日期',
			dataIndex: 'displayStartDate',
			key: '5',
			width: 150,
			className: 'f-align-center',
		}, {
			title: '上课时段',
			dataIndex: 'displayStartTime',
			key: '6',
			width: 150,
			className: 'f-align-center',
		}, {
			title: '校区',
			key: '7',
			className: 'f-align-center',
			width: 150,
			dataIndex: 'schoolAreaName'
		}, {
			title: '教室',
			key: '8',
			className: 'f-align-center',
			width: 150,
			dataIndex: 'classRoomName'
		}, {
			title: '状态',
			key: '9',
			className: 'f-align-center',
			width: 150,
			render: (text, record) => (
				<span>{record.isSignUp=='2'?<i style={{color: '#FF0000'}}>{record.displaySignUp}</i>:<i>{record.displaySignUp}</i>}</span>
			)
		}, {
			title: '操作',
			key: '11',
			width: 150,
			fixed: 'right',
			className: 'f-align-center',
			render: (text, record, index) => {
				return (<div>
		                <i className='iconfont icon-chakan f-pointer f-mr5' onClick={()=>{window.location.href='/teach/pointListDetail?id='+record.id}}></i>
		            </div>);
			}
		}];
		const {
			getFieldDecorator
		} = this.props.form;
		const formItemLayout = {
			labelCol: {
				span: 6
			},
			wrapperCol: {
				span: 18
			}
		};
		return (
			<div className='classListPage wid100'>
				<Breadcrumb className="f-mb3">
				    <Breadcrumb.Item>教务教学</Breadcrumb.Item>
				    <Breadcrumb.Item>教务</Breadcrumb.Item>
				    <Breadcrumb.Item>
				   		<span className="f-fz5 f-bold">点名表</span>
				    </Breadcrumb.Item>
				</Breadcrumb>
				<div className='Topselect f-bg-white f-radius1 f-box-shadow1' style={{width:'100%'}}>
					<Tabs defaultActiveKey="1" style={{textAlign:'center'}} tabPosition='top' animated={false} onChange={this.callback.bind(this)}>
						<TabPane tab={<span><i className='iconfont icon-hricon33 f-mr2'></i>精确查找</span>} key="1" style={{textAlign:'left'}}>
				    			<div style={{padding:'0 0 30px 26px'}}>
				    				<Input placeholder="请输入班级名称模糊查询" onChange={this.className.bind(this)} defaultValue={this.state.name?this.state.name:''} className='inputBG pageInpHeight f-radius1' style={{width:'352px',border:0}}/>
				    				<Button type="primary" icon="search" onClick={this.jqSearch.bind(this)} style={{height:'40px'}}>搜索</Button>
				    			</div>
				    		</TabPane>
					    <TabPane tab={<span><i className='iconfont icon-shaixuan f-mr2' ></i>高级筛选</span>} key="2">
					    		<Form style={{padding:'0 20px'}}>
					    		<Row gutter={40}>
					          <Col span={9} >
					          	 <Col span={7} className='f-fz4 f-black f-h4-lh4'>班级名称:&emsp;</Col>
					          	 <Col span={17} >
					          	 	<FormItem {...formItemLayout}>
					        	          {getFieldDecorator('className', {
								         initialValue:this.state.name?this.state.name:''
					        	          })(
							         	<Input placeholder='请输入班级名称' className='inputBG pageInpHeight'/>
							         )}
					        	     </FormItem>
							     </Col>
					          </Col>
					          <Col span={8} >
					          	 <Col span={8} className='f-fz4 f-black f-h4-lh4'>课程名称:&emsp;</Col>
					          	  <Col span={16} >
					          	  	<FormItem {...formItemLayout}>
					                  {getFieldDecorator('courseId', {
					                  })(
							            <Select showSearch allowClear placeholder="全部课程" optionFilterProp="children"
					                        filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}>
						                      {this.state.course?
							                        	this.state.course.map((item,i)=> {
							                        		return(<Option key={item.id} value={item.id}>{ item.name }</Option>)
							                        	})
							                        	:null
						                      }
					                      </Select>
						            	)}
					        	     	</FormItem>
						           </Col>
					          </Col>
					          <Col span={7} >
					             <Col span={8} className='f-fz4 f-black f-h4-lh4'>上课老师:&emsp;</Col>
					              <Col span={15} >
					              	<FormItem {...formItemLayout}>
					                  {getFieldDecorator('teacherId', {
					                  })(
							            <Select
					                  	  showSearch
					                  	  allowClear
								          placeholder='请选择上课老师'
								          style={{ width: '100%' }}
								          optionFilterProp="children"
                        					filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
								        >
											{this.state.masterList?
							                        	this.state.masterList.map((item,i)=> {
							                        		return(<Option key={item.id} value={item.id}>{ item.name }</Option>)
							                        	})
							                        	:null
						                      }
								        </Select>
						            )}
					        	     	</FormItem>
						            </Col>
					          </Col>
					        </Row>
					        <Row className='' gutter={40}>
					        		<Col span={9}>
					        		<Col span={7} className='f-fz4 f-black f-h4-lh4'>上课日期:&emsp;</Col>
					        		<Col span={17} >
						    			 <Col span={11}>
						    			 	<FormItem {...formItemLayout}>
								          {getFieldDecorator('startDate')(
							    			 	<DatePicker
									          disabledDate={this.disabledStartDate.bind(this)}
									          format="YYYY-MM-DD"
									          setFieldsValue={this.state.startValue}
									          placeholder="开始时间"
									          onChange={this.onStartChange.bind(this)}
									          onOpenChange={this.handleStartOpenChange.bind(this)}/>	
								        )}
								     </FormItem>
						    			 </Col>
						    			 <Col span={2} className='f-align-center f-h4-lh4'>--</Col>
						    			  <Col span={11}>
						    			  	<FormItem {...formItemLayout}>
								          {getFieldDecorator('endDate')(
									        <DatePicker
									          disabledDate={this.disabledEndDate.bind(this)}
									          format="YYYY-MM-DD"
									          setFieldsValue={this.state.endValue}
									          placeholder="结束时间"
									          onChange={this.onEndChange.bind(this)}
									          open={this.state.endOpen}
									          onOpenChange={this.handleEndOpenChange.bind(this)}/>
									        )}
								     </FormItem>
							         	</Col>
							        	</Col>
					        		</Col>
					        		<Col span={8} >
					        			<Col span={12}>
							             <Col span={8} className='f-fz4 f-black f-h4-lh4'>教室:&emsp;</Col>
							              <Col span={16} className='f-pr2'>
							              	<FormItem {...formItemLayout}>
							                  {getFieldDecorator('roomId', {
							                  })(
									            <Select showSearch allowClear placeholder="请选择" optionFilterProp="children"
							                        filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}>
								                      {this.state.classroomList?
									                        	this.state.classroomList.map((item,i)=> {
									                        		return(<Option key={item.id} value={item.id}>{ item.name }</Option>)
									                        	})
									                        	:null
								                      }
							                      </Select>
								            		)}
											    </FormItem>
								            </Col>
								        </Col>
								        <Col span={12}>
								        		<Col span={8} className='f-fz4 f-black f-h4-lh4'>助教:&emsp;</Col>
								              <Col span={16} >
								              	<FormItem {...formItemLayout}>
								                  {getFieldDecorator('assistantId', {
								                  })(
										            <Select
								                  	  showSearch
								                  	  allowClear
											          placeholder='请选择助教'
											          style={{ width: '100%' }}
											          optionFilterProp="children"
			                        					filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
											        >
														{this.state.zhujiaoData?
										                        	this.state.zhujiaoData.map((item,i)=> {
										                        		return(<Option key={item.id} value={item.id}>{ item.name }</Option>)
										                        	})
										                        	:null
									                      }
											        </Select>
									            )}
												    </FormItem>
									            </Col>
								        </Col>
					          	</Col>
					          	
					          	<Col span={7} >
					             <Col span={8} className='f-fz4 f-black f-h4-lh4'>状态:&emsp;</Col>
					              <Col span={15} >
					              	<FormItem {...formItemLayout}>
					                  {getFieldDecorator('isSignUp', {
					                  })(
							            <Select placeholder='全部状态'>
							              <Option value="">全部状态</Option>
							              <Option value="0">未上课</Option>
							              <Option value="2">应记上课</Option>
							            </Select>
						            )}
									    </FormItem>
						            </Col>
					          	</Col>
					        </Row>
					        <Row className='f-pb3'>
					        		<Col span={12} className='f-align-right f-pr4'>
					        			<Button style={{border: '1px solid #999999',borderRadius: '4px',width:'110px',height:'40px'}} onClick={this.reset.bind(this)}>
					        				<i className='iconfont icon-reset f-mr2'></i>重置
					        			</Button>
					        		</Col>
					        		<Col span={12} className='f-align-left'>
					        			<Button type="primary" style={{borderRadius: '4px',width:'110px',height:'40px'}} onClick={this.advancedSearch.bind(this)}>
					        				<i className='iconfont icon-hricon33 f-mr2'></i>搜索
					        			</Button>
					        		</Col>
					        </Row>
					    		</Form>
					    </TabPane>
					 </Tabs>
				</div>
				<div className='f-bg-white f-pd4 f-box-shadow1 f-mt5 f-radius1'>
					<div className='f-flex' style={{justifyContent: 'space-between'}}>
						<h3 className="f-title-blue f-mb3">排课列表</h3>
					</div>
					{this.state.pageResult?
						<div>
							<Table columns={columns} dataSource={this.state.pageResult} scroll={{ x: 1500}} bordered pagination={false}/>
								<div className='f-flex f-mt3' style={{justifyContent:'flex-end'}}>
									<div>
										<Pagination current={this.state.currentPage} total={this.state.total} onShowSizeChange={this.onShowSizeChange.bind(this)} showSizeChanger showQuickJumper onChange={this.pageChange.bind(this)}/>
									</div>
								</div>
						</div>:<Loading/>
					}
				</div>
			</div>
		)
	}
}
const PointList = Form.create()(Main);
export {
	PointList
}