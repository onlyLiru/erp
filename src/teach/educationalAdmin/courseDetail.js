import React, {
	Component
} from 'react';
import store from 'store2';
import moment from 'moment';
import {
	Breadcrumb,
	Button,
	Select,
	DatePicker,
	Row,
	Col,
	Form,
	message
} from 'antd';
import '../main.less';
import $ from 'jquery';
import {
	_getDays,
	getUrlParam,
	getUnicodeParam
} from '../../common/g.js';
//使用fetch请求接口数据
import {
	fpost
} from '../../common/io.js'; //同步、异步请求
const Option = Select.Option;
const u = require('../../common/io.js');
const FormItem = Form.Item;
const dateFormat = 'YYYY-MM-DD HH:mm:ss';
const tagDetail = [];

//教务教学默认打开的首页
class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			size: 'large',
			startValue: null,
			endValue: null,
			endOpen: false,
			course: [], //获取课程
			classroomList: [], //获取教室
			allClass: [],
			startTime: null,
			endTime: null,
			classList: [],
			closeClassTimes: '', //完课课次
			noClassTimes: '', //未上课课次
			noSignUpTimes: '', //未记上课课次
			detail: [], //类似日历里的班级名称
			time: [], //类似日历里的时间和星期
			teacherName: ''
		}
	}

	disabledStartDate = (startValue) => { //只读的开始日期
		const endValue = this.state.endValue;
		if (!startValue || !endValue) {
			return false;
		}
		return startValue.valueOf() > endValue.valueOf();
	}

	disabledEndDate = (endValue) => { //只读的结束日期
		const startValue = this.state.startValue;
		if (!endValue || !startValue) {
			return false;
		}
		return endValue.valueOf() <= startValue.valueOf();
	}

	onChange = (field, value) => {
		this.setState({
			[field]: value,
		});
	}
	//开始日期改变
	onStartChange = (value) => {
		this.onChange('startValue', value);
	}
	//结束日期改变
	onEndChange = (value) => {
		this.onChange('endValue', value);
	}

	handleStartOpenChange = (open) => {
		if (!open) {
			this.setState({
				endOpen: true
			});
		}
	}

	handleEndOpenChange = (open) => {
		this.setState({
			endOpen: open
		});
	}
	_getCourse() { //获取课程
		const self = this;
		fpost('/api/system/course/listCourse')
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					self.setState({
						course: res.result
					})
				} else {
					message.error(res.message)
				}
			});
	}
	_getClassroom() { //获取教室
		const self = this;
		fpost('/api/system/schoolarea/listClassRoom ')
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					self.setState({
						classroomList: res.result
					})
				} else {
					message.error(res.message)
				}
			});
	}
	_listDetail(str) {
		let data = {};
		this.props.form.validateFields(
			(err, fieldsValue) => {
				fieldsValue['startDate'] = fieldsValue['startDate'] ? fieldsValue['startDate'].format('YYYY-MM-DD HH:mm:ss') : '';
				fieldsValue['endDate'] = fieldsValue['endDate'] ? fieldsValue['endDate'].format('YYYY-MM-DD HH:mm:ss') : '';
				fieldsValue['roomId'] = fieldsValue['roomId'] ? fieldsValue['roomId'] : '';
				fieldsValue['courseId'] = fieldsValue['courseId'] ? fieldsValue['courseId'] : '';
				fieldsValue['isSignUp'] = fieldsValue['isSignUp'] ? fieldsValue['isSignUp'] : '';
				data.startDate = fieldsValue['startDate'];
				data.endDate = fieldsValue['endDate'];
				data.state = fieldsValue['isSignUp'];
				data.classIds = str;
				fpost('/api/educational/period/table', data)
					.then((res) => {
						return res.json();
					})
					.then((res) => {
						if (res.success == true) {
							if (res.result) {
								let tt = []; //类似日历里的班级名称
								let time = []; //类似日历里的时间
								let dateList = [];
								for (let i in res.result.classMap) {
									tt.push(res.result.classMap[i]);
									let singDetail = [];
									for (let j in res.result.classMap[i].dateList) {
										let ww = {};
										if (!res.result.classMap[i].dateList[j].length) {
											ww.list = '';
										} else {
											ww.list = res.result.classMap[i].dateList[j];
										}
										if (ww.list) {
											let ss = [];
											for (let t in ww.list) {
												for (let jj in ww.list[t].time) {
													ww.list[t].start = jj;
													ww.list[t].end = ww.list[t].time[jj];
													ww.list[t].index = i;
												}
											}
										}
										singDetail.push(ww)
									}
									res.result.classMap[i].listdetail = singDetail;
								}
								for (let i in tt[0].dateList) {
									let singTime = {};
									singTime.time = i.split(':')[0]; //日期
									singTime.week = '周' + i.split(':')[1]; //星期
									time.push(singTime)
								}
								this.setState({
									closeClassTimes: res.result.closeClassTimes,
									noClassTimes: res.result.noClassTimes,
									noSignUpTimes: res.result.noSignUpTimes,
									detail: tt,
									time: time
								}, function() {
									this._event();
									$('.bordertop').css('width', '180px !important')
									for (var i = 0; i < $('.bordertop').length; i++) {
										$('.bordertop').eq(i).css('width', '180px !important')
									}
									for (var i = 0; i < this.state.detail.length; i++) {
										let list = {};
										let ss = ''
										for (let j in this.state.detail[i].dateList) {
											list.name = this.state.detail[i].dateList[j];
											if (!this.state.detail[i].dateList[j].length) {
												ss += '';
											} else {
												ss += '<div class="f-white f-pointer" style="background:red;padding:12px 0">1234</div>';
											}
										}
									}
									for (var i = 0; i < $('.pgListname').length; i++) {
										let html = '';
										let str = $('.pgListname').eq(i).html().substring(0, 8);
										if ($('.pgListname').eq(i).html().length > 8) {
											html = str + '...';
										} else {
											html = str;
										}
										$('.pgListname').eq(i).html(html);
									}
									this._hover();
								});
							} else {
								this.setState({
									closeClassTimes: '',
									noClassTimes: '',
									noSignUpTimes: '',
									detail: [],
									time: []
								})
							}
						} else {
							message.error(res.message)
						}
					});
			}
		)
	}
	_hover() {
		$('.rightDetail').hover(function() {
			let id = $(this).attr('data-id');
			fpost('/api/educational/period/table/info', {
					classId: id
				})
				.then((res) => {
					return res.json();
				})
				.then((res) => {
					if (res.success == true) {
						var title = $('<div/>').html(
							'班级：' + res.result.name + '&#10;课程：' + res.result.courseName + '&#10;教室：' + res.result.classRoomName + '&#10;上课老师：' + res.result.teacherName +
							'&#10;助教：' + res.result.taTeacherName + '&#10;学生：' + res.result.studentNum
						).text();
						$(this).attr('title', title);
					} else {
						message.error(res.message)
					}
				});
		}, function() {

		})
	}
	getData(fieldsValue) {
		fpost('/api/educational/period/class/list', fieldsValue)
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					if (res.result) {
						res.result.forEach((item, i) => {
							item.key = i;
						});
						this.setState({
							classList: res.result,
						}, function() {
							this._event();
							let ids = [];
							for (var i = 0; i < $('.selectClass').length; i++) {
								ids.push($('.selectClass').eq(i).attr('data-id'));
							}
							let str = JSON.stringify(ids).split('[')[1].split(']')[0];
							var reg = new RegExp('"', "g");
							str = str.replace(reg, "");
							this._listDetail(str);
						})
					}
				} else {
					message.error(res.message)
				}
			});
	}
	_getDetail() {
		const self = this;
		let data = {}
		this.props.form.validateFields(
			(err, fieldsValue) => {
				fieldsValue['startDate'] = fieldsValue['startDate'] ? fieldsValue['startDate'].format('YYYY-MM-DD HH:mm:ss') : '';
				fieldsValue['endDate'] = fieldsValue['endDate'] ? fieldsValue['endDate'].format('YYYY-MM-DD HH:mm:ss') : '';
				fieldsValue['classId'] = fieldsValue['classId'] ? fieldsValue['classId'] : '';
				fieldsValue['roomId'] = fieldsValue['roomId'] ? fieldsValue['roomId'] : '';
				fieldsValue['courseId'] = fieldsValue['courseId'] ? fieldsValue['courseId'] : '';
				fieldsValue['state'] = fieldsValue['isSignUp'] ? fieldsValue['isSignUp'] : '';
				data.startDate = fieldsValue['startDate'];
				data.endDate = fieldsValue['endDate'];
				data.classId = fieldsValue['classId'];
				data.roomId = fieldsValue['roomId'];
				data.courseId = fieldsValue['courseId'];
				data.state = fieldsValue['state'];
				data.teacherId = getUrlParam('id');
				this.getData(data);
			}
		)
	}
	_getClass() {
		const self = this;
		$.ajax({
			type: 'get',
			url: u.hostname() + '/api/educational/class/select/list',
			cache: false,
			headers: {
				'Accept': 'application/json, text/plain, */*',
				'Content-Type': 'application/x-www-form-urlencoded',
				'x-auth-token': store.get('sessionId'), //登录功能放开后需要加上的
			},
			data: {
				schoolGroupId: getUrlParam('schoolGroupId') || '',
				teacherId: getUrlParam('id')
			},
			success: function(res) {
				if (res.success == true) {
					if (res.result) {
						self.setState({
							allClass: res.result
						})
					}
				} else {
					message.error(res.message)
				}
			}
		})
	}
	_event() {
		const self = this;
		$('.bordertop').css('width', '180px !important')
		$('.listName').css('height', $('#t_r_content .bordertop').eq(0).css('height'));
		for (var i = 0; i < $('#cl_freeze tr').length; i++) {
			var j = i + 1;
			$('#cl_freeze tr').eq(i).css('height', $('#t_r_content tr').eq(j).css('height'));
		}
		$('.selectClass').eq($('.selectClass').length - 1).css('border', 'none');
		$('.selectClass').off().on('click', function() {
			$(this).toggleClass('bg9B');
			if ($(this).hasClass('bg9B')) { //班级为选中状态
				tagDetail.push($(this).attr('data-id'))
			} else { //班级为未选中状态
				for (var i = 0; i < tagDetail.length; i++) {
					if ($(this).attr('data-id') == tagDetail[i]) {
						tagDetail.splice(i, 1)
					}
				}
			}
			if (tagDetail.length) {
				let str = JSON.stringify(tagDetail).split('[')[1].split(']')[0];
				var reg = new RegExp('"', "g");
				str = str.replace(reg, "");
				self._listDetail(str)
			} else {
				let tt = [];
				for (let j = 0; j < $('.selectClass').length; j++) {
					tt.push($('.selectClass').eq(j).attr('data-id'));
				}
				let str1 = JSON.stringify(tt).split('[')[1].split(']')[0];
				var reg = new RegExp('"', "g");
				str1 = str1.replace(reg, "");
				self._listDetail(str1)
			}
		});
	}
	componentDidMount() {
		let self = this;
		self._getClass();
		self._getCourse(); //获取全部课程
		self._getClassroom();
		self.setState({
			teacherName: getUnicodeParam('name')
		})

		$('.listName').css('height', $('#t_r_content .bordertop').eq(0).css('height'));
		for (var i = 0; i < $('#cl_freeze tr').length; i++) {
			var j = i + 1;
			$('#cl_freeze tr').eq(i).css('height', $('#t_r_content tr').eq(j).css('height'));
		}
		// 	 	班级列表选择班级
		var d = new Date()
		var day = d.getDate()
		var year = d.getFullYear();


		var month1 = d.getMonth();
		if (month1 == 0) {
			month1 = 12;
			year = year - 1;
		}
		if (month1 < 10) {
			month1 = "0" + month1;
		}
		var myDate = new Date(year, month1, 0);
		var lastDay = year + "-" + month1 + "-" + myDate.getDate(); // 上个月的最后一天
		self.setState({
			endValue: year + "-" + month1 + "-" + myDate.getDate() + '00:00:00',
			startValue: year + "-" + month1 + "-" + "01" + ' ' + '00:00:00'
		}, function() {
			self._getDetail();
		});
	}
	_search() { //执行搜索
		let s1 = null;
		let s2 = null;
		let self = this;
		self.props.form.validateFields(
			(err, fieldsValue) => {
				if (fieldsValue['startDate']) {
					s1 = fieldsValue['startDate'].format('YYYY-MM-DD');
				} else {
					message.error('开始时间不能为空');
					return false;
					s1 = '';
				}
				if (fieldsValue['endDate']) {
					s2 = fieldsValue['endDate'].format('YYYY-MM-DD');
				} else {
					message.error('结束时间不能为空');
					return false;
					s2 = '';
				}
			}
		)
		if (s1 && s2) {
			if (_getDays(s1, s2) <= 31) {
				if (s1 > s2) {
					message.error('开始事件不能大于结束时间哦');
					return false;
				} else {
					self._getDetail();
				}
			} else {
				message.error('只能搜索最多31天以内的内容哦，请重新选择时间段');
				return false;
			}
		}
	}

	render() {
		const {
			size
		} = this.state;
		const {
			getFieldDecorator
		} = this.props.form;
		const formItemLayout = {
			labelCol: {
				span: 6
			},
			wrapperCol: {
				span: 18
			}
		};
		return (
			<div className='classListPage wid100'>
				<Breadcrumb className="f-mb3">
				    <Breadcrumb.Item>教务教学</Breadcrumb.Item>
				    <Breadcrumb.Item>教务</Breadcrumb.Item>
				    <Breadcrumb.Item>
				   		<span className="f-fz5 f-bold">教师课程信息详情</span>
				    </Breadcrumb.Item>
				</Breadcrumb>
				<div className='Topselect f-bg-white f-radius1 f-box-shadow1' style={{width:'100%'}}>
					<div className='f-fz5 f-black f-mb5' style={{padding:'10px 0 0 20px'}}>{this.state.teacherName}老师的课程详情</div>
			    		<Form style={{padding:'0 20px'}}>
				    		<Row gutter={40}>
				          <Col span={11}>
				        		<Col span={5} className='f-fz4 f-black'>时间段:&emsp;</Col>
				        		<Col span={19} >
					    			 <Col span={11}>
					    			 	<FormItem>
					         		{getFieldDecorator('startDate',{
					         			rules: [{required:true,message:'请选择开始时间'}],
									   initialValue:this.state.startValue?moment(this.state.startValue, dateFormat):null,
					         		})(
						    			 	<DatePicker
						    			 		showTime
								          disabledDate={this.disabledStartDate.bind(this)}
								          format="YYYY-MM-DD HH:mm:ss"
								          setFieldsValue={this.state.startValue}
								          placeholder="开始时间"
								          onChange={this.onStartChange.bind(this)}
								          onOpenChange={this.handleStartOpenChange.bind(this)}/>
						    			 	)}
									</FormItem>
					    			 </Col>
					    			 <Col span={2} className='f-align-center'>--</Col>
					    			  <Col span={11}>
					    			  <FormItem>
					         		{getFieldDecorator('endDate',{
					         			rules: [{required:true,message:'请选择结束时间'}],
									   initialValue:this.state.endValue?moment(this.state.endValue, dateFormat):null,
					         		})(
								        <DatePicker
								        		showTime
								          disabledDate={this.disabledEndDate.bind(this)}
								          format="YYYY-MM-DD HH:mm:ss"
								          setFieldsValue={this.state.endValue}
								          placeholder="结束时间"
								          onChange={this.onEndChange.bind(this)}
								          open={this.state.endOpen}
								          onOpenChange={this.handleEndOpenChange.bind(this)}/>
								        )}
									</FormItem>
						         	</Col>
							     </Col>
				        		</Col>
				          <Col span={7} >
				          	 <Col span={8} className='f-fz4 f-black'>班级名称:&emsp;</Col>
				              <Col span={15} >
				              	<FormItem {...formItemLayout}>
					                  {getFieldDecorator('classId', {
					                  	valuePropName: 'value',
					                  })(
						            <Select placeholder='请选择班级' allowClear>
				                   		{
				                        	this.state.allClass.map((item,i)=> {
				                        		return(<Option key={i} value={item.id}>{ item.name }</Option>)
				                        	})
				                        }
							        </Select> 
					            		)}
								     </FormItem>
					            </Col>
				          </Col>
				          <Col span={5} >
				             <Col span={5} className='f-fz4 f-black'>教室:&emsp;</Col>
				              <Col span={15} >
				              	<FormItem {...formItemLayout}>
					                  {getFieldDecorator('roomId', {
					                  	valuePropName: 'value',
					                  })(
						            <Select showSearch allowClear placeholder="请选择" optionFilterProp="children"
				                        filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}>
					                      {this.state.classroomList?
						                        	this.state.classroomList.map((item,i)=> {
						                        		return(<Option key={item.id} value={item.id}>{ item.name }</Option>)
						                        	})
						                        	:null
					                      }
				                      </Select>
						            )}
								     </FormItem>
					            </Col>
				          </Col>
				        </Row>
				        <Row className='' gutter={40} style={{paddingBottom:'30px'}}>
				        		<Col span={11}>
					        		<Col span={5} className='f-fz4 f-black'>课程名称:&emsp;</Col>
					        		<Col span={19} >
					        			<FormItem {...formItemLayout}>
					                  {getFieldDecorator('courseId', {
					                  	valuePropName: 'value',
					                  })(
										<Select showSearch allowClear placeholder="全部课程" optionFilterProp="children"
					                        filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}>
						                      {this.state.course?
							                        	this.state.course.map((item,i)=> {
							                        		return(<Option key={item.id} value={item.id}>{ item.name }</Option>)
							                        	})
							                        	:null
						                      }
					                      </Select>
						            )}
								     </FormItem>
								</Col>
				        		</Col>
				        		<Col span={7} >
				             <Col span={8} className='f-fz4 f-black'>上课状态:&emsp;</Col>
				              <Col span={16} >
				              	<FormItem {...formItemLayout}>
					                  {getFieldDecorator('isSignUp', {
					                  	initialValue:""
					                  })(
						            <Select allowClear>
						              <Option value="">全部状态</Option>
						              <Option value="0">未记上课</Option>
						              <Option value="1">记上课</Option>
						              <Option value="2">应记上课</Option>
						            </Select>
					            )}
								     </FormItem>
					            </Col>
				          	</Col>
				          	<Col span={5} >
				             	<Button type="primary" style={{borderRadius: '4px',width:'110px',height:'40px'}} onClick={this._search.bind(this)}>
				        				<i className='iconfont icon-hricon33 f-mr2'></i>搜索
				        			</Button>
				          	</Col>
				        </Row>
			    		</Form>
				</div>
				<div className='f-bg-white f-pd4 f-box-shadow1 f-mt5 f-mb2 f-radius1'>
					<Row>
						<Col span={12}>
							<Col span={8} className='f-fz3 f-dark'>完课课次：&emsp;{this.state.closeClassTimes}</Col>
							<Col span={8} className='f-fz3 f-dark'>未上课课次：<i style={{color:' #2187FF'}}>{this.state.noClassTimes}</i></Col>
							<Col span={8} className='f-fz3 f-dark'>未记上课课次：<i style={{color: 'red'}}>{this.state.noSignUpTimes}</i></Col>
						</Col>
						<Col className='f-align-right'>
							<i className='f-fz3 f-dark'>图例说明&emsp;</i>
							<Button style={{border:'none',background: '#9B9B9B',cursor:'text'}} className='f-white'>未记上课</Button>&emsp;
							<Button style={{border:'none',background: '#FF5F1C',cursor:'text'}} className='f-white'>记上课</Button>&emsp;
							<Button style={{border:'none',background: '#2187FF',cursor:'text'}} className='f-white'>应记上课</Button>
						</Col>
					</Row>
				</div>
				<div className='f-flex'>
					<Col span={5} className='f-mr3 f-bg-white f-pb3'>
						{this.state.classList.map((item, i) => {
							return(
								<ul key={i} className='f-bold f-pointer selectClass' data-id={item.classId} style={{borderBottom: '0.5px solid #BCBCBC',padding:'10px'}}>
									<li className='f-mb2'>
										<i className='f-dark f-fz2'>班级:&emsp;</i>
										<i className='f-fz3' style={{color:'#2187FF'}}>{item.className}</i>
									</li>
									<li className='f-mb2 f-dark'>
										<i className='f-fz2'>课程:&emsp;</i>
										<i className='f-fz3'>{item.courseName}</i>
									</li>
									<li className='f-mb2'>
										<i className='f-dark f-fz2'>学生:&emsp;</i>
										<i className='f-fz3' style={{color:'#2187FF'}}>{item.studentNum}</i>
									</li>
								</ul>
							)
						})}
					</Col>
					<Col span={19} className='f-bg-white f-pb3' style={{overflow:'scroll'}}>
						<div className='f-mr2'> 
					<div className="t_left"> 
						<table className='tablefix'> 
							<tbody>
								<tr> 
									<th style={{width:"40%"}} className='listName' style={{fontSize:'14px'}}>班级名称</th> 
								</tr> 
							</tbody>
						</table>  
						<div className="cl_freeze" id="cl_freeze"> 
							<table className='tablefix'> 
								<tbody>
									{this.state.detail.map((item,i)=> {
				                    		return(
				                    			<tr key={i}> 
											  <td title={item.className} className='pgListname'>{item.className}</td> 
											</tr> 
				                    		)
				                    	})}
								</tbody>
							</table> 
						</div> 
					</div> 
				<div className="t_r"> 
				  <div className="t_r_content" id="t_r_content"> 
					  <table className='tablefix'> 
						<tbody>
							<tr className='f-bg-white'>
							{this.state.time.map((item,i)=> {
			                		return(
			                			<td key={i} style={{width:'10%'}} className="bordertop">
								  	 {item.time}<br/>{item.week}
								  	</td> 
			                		)
			                	})}
							</tr> 
					{
						this.state.detail.map((item,i)=> {
							return(
								<tr key={i}>
								{item.listdetail.map((x,y)=>{
									return (
										<td key={y} style={{width:'10%'}} className="bordertop">
											{x.list?
												<div>
													{x.list.map((info,index)=>{
														return(
															<div key={index}>
																{info.colorStyle==1?
																	<div className='f-white f-pointer rightDetail' style={{padding:'12px 0',background: '#9B9B9B',borderBottom: '0.5px solid #D4D4D4'}} data-id={info.index}>{info.start}-{info.end}
																	</div>:null
																}
																{info.colorStyle==2?
																	<div className='f-white f-pointer rightDetail f-relative' style={{padding:'12px 0',background: '#FF5F1C',borderBottom: '0.5px solid #D4D4D4'}} data-id={info.index}>{info.start}-{info.end}
																	</div>
																	:null
																}
																{info.colorStyle==3?
																	<div className='f-white f-pointer rightDetail' style={{padding:'12px 0',background: '#2187FF',borderBottom: '0.5px solid #D4D4D4'}} data-id={info.index}>{info.start}-{info.end}</div>:null
																}
															</div>
														)
													})}
												</div>
												:<div></div>
											}
									  </td>
									)
								})}
								</tr> 
							)
			            	})
					}
						</tbody>
					  </table> 
				  </div> 
				</div>
			</div> 
					</Col>
				</div>
			</div>
		)
	}
}

const CourseDetail = Form.create()(Main);
export {
	CourseDetail
}