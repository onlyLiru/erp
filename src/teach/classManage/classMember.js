import React, {
	Component
} from 'react';
import {
	Breadcrumb,
	Button,
	Table,
	Row,
	Col,
	Form,
	message,
	Pagination
} from 'antd';
import store from 'store2';
import '../main.less';
import {
	fpost
} from '../../common/io.js';
import {
	getUrlParam,
	Loading,
	numberFormate
} from '../../common/g.js';
import $ from 'jquery';
const u = require('../../common/io.js');
export class Main extends Component {
	constructor(props) {
		super(props)
		this.state = {
			currentPage: 1,
			pageSize: 10,
			pageResult: null, //分页返回的列表总信息
			sumInfo: {
				memberCount: null,
				freshCount: null,
				freshRate: null,
				hasContinuedCount: null,
				hasContinuedRate: null,
				insertCount: null,
				insertRate: null,
				totalMaterialAmount: null,
				totalReceivableAmount: null
			}, //底下的汇总信息
			classInfo: {
				name: null,
				classStartTime: null,
				classEndTime: null,
				teacherName: null,
				salePrice: null,
				classStartTimeList: []
			}, //头部的班级信息展示字段
		}
	}
	componentDidMount() {
		this._getDetail(); //获取页面详情
	}
	_getDetail() {
		const self = this;
		fpost('/api/educational/class/classMember', {
				classId: getUrlParam('id'),
			})
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					res.result.memberList.forEach((item, i) => {
						item.key = i;
					});
					self.setState({
						pageResult: res.result.memberList,
						sumInfo: res.result.sumInfo,
						classInfo: res.result.classInfo,
					}, function() {
						$('.totalReceivableAmount').html(numberFormate($('.totalReceivableAmount').html()));
						$('.totalMaterialAmount').html(numberFormate($('.totalMaterialAmount').html()));
						$('.classStartTime').html($('.classStartTime').html().split(' ')[0]);
						$('.classEndTime').html($('.classEndTime').html().split(' ')[0]);
						let html = '';
						let str = $('.className').html().substring(0, 10);
						if ($('.className').html().length > 10) {
							html = str + '...';
						} else {
							html = str;
						}
						$('.className').html(html);
					});
				} else {
					message.error(res.message)
				}
			});
	}
	render() {
		const columns = [{
			title: '序号',
			key: '0',
			width: 100,
			fixed: 'left',
			className: 'f-align-center',
			render: (text, record, index) => {
				return (
					<div>{index}</div>
				)
			}
		}, {
			title: '学员姓名',
			key: '1',
			width: 150,
			dataIndex: 'studentName',
			className: 'f-align-center',
		}, {
			title: '头像',
			key: '2',
			width: 100,
			className: 'f-align-center',
			render: (text, record, index) => {
				return (
					<div>
					{record.headImgUrl?
						<img src={record.headImgUrl} style={{width:'50px',height:'50px',borderRadius:'100%'}}/>:''
					}
					</div>
				)
			}
		}, {
			title: '年龄',
			key: '3',
			width: 100,
			className: 'f-align-center',
			render: (text, record, index) => {
				return (
					<div>
						{record.age?<i>{record.age}岁</i>:''}
					</div>
				)
			}
		}, {
			title: '手机号码',
			key: '4',
			width: 180,
			className: 'f-align-center',
			dataIndex: "phone"
		}, {
			title: '学费',
			key: '5',
			className: 'f-align-center',
			width: 150,
			render: (text, record) => (
				<div>
				¥ {record.receivableAmount}
				{record.chargeMode==1?
					<img className='f-ml2 f-vertical-middle' src='http://img.fancyedu.com/sys/ic/operation/1512640700929_qi.png' style={{width:'30px',height:"30px"}}/>
					:null
				}
				{record.chargeMode==2?
					<img className='f-ml2 f-vertical-middle' src='http://img.fancyedu.com/sys/ic/operation/1510120850178_course.png' style={{width:'30px',height:"30px"}}/>
					:null
				}
				{record.chargeMode==3?
					<img className='f-ml2 f-vertical-middle' src='http://img.fancyedu.com/sys/ic/operation/1512640710303_shi.png' style={{width:'30px',height:"30px"}}/>
					:null
				}
			</div>
			)
		}, {
			title: '学费状态',
			dataIndex: 'isArrearStr',
			key: '6',
			width: 150,
			className: 'f-align-center',
		}, {
			title: '优惠状态',
			dataIndex: 'isOriginalStr',
			key: '7',
			width: 150,
			className: 'f-align-center',
		}, {
			title: '报班状态',
			dataIndex: 'stateStr',
			key: '8',
			width: 150,
			className: 'f-align-center',
		}, {
			title: '是否续报',
			dataIndex: 'isContinueStr',
			key: '9',
			width: 150,
			className: 'f-align-center',
		}, {
			title: '是否新生',
			dataIndex: 'isFreshManStr',
			key: '10',
			width: 150,
			className: 'f-align-center',
		}, {
			title: '报课来源',
			dataIndex: 'salesFromStr',
			key: '11',
			width: 150,
			className: 'f-align-center',
		}, {
			title: '课程顾问',
			dataIndex: 'salesName',
			key: '12',
			width: 150,
			className: 'f-align-center',
		}, {
			title: '所属校区',
			dataIndex: 'schoolAreaName',
			key: '13',
			width: 150,
			className: 'f-align-center',
		}, {
			title: '操作人',
			key: '14',
			className: 'f-align-center',
			width: 150,
			render: (text, record, index) => {
				return (<div>
		            {record.operatorName}
		            </div>);
			}
		}, {

			title: '操作',
			key: '15',
			fixed: 'right',
			className: 'f-align-center',
			width: 150,
			render: (text, record, index) => {
				return (<div>
		                <Button className='f-radius4 f-bg-green f-white' onClick={()=>{window.location.href='/front/signup/step1?type=1&id=956071731762606081'}}>续报</Button>
		            </div>);
			}
		}];

		const {
			getFieldDecorator
		} = this.props.form;
		return (<div className='courseArranging classListPage'>
				<Breadcrumb className="f-mb3">
				    <Breadcrumb.Item>教务教学</Breadcrumb.Item>
				    <Breadcrumb.Item>班级管理</Breadcrumb.Item>
				    <Breadcrumb.Item><a href='/teach/classList'>班级列表</a></Breadcrumb.Item>
				    <Breadcrumb.Item>
				   		<span className="f-fz5 f-bold">班级成员</span>
				    </Breadcrumb.Item>
				</Breadcrumb>
				<div className='f-bg-white f-pd4 f-box-shadow1 f-mt4 f-radius1 f-mb4'>
					<Row className='f-mb5'>
						<Col span={7} className='f-align-left'>
							<i className='f-dark f-fz3'>班级名称：</i><i className='f-fz4 className' title={this.state.classInfo.name} style={{color:'#151515'}}>{this.state.classInfo.name}</i>
						</Col>
						<Col span={7} className='f-align-left'>
							<i className='f-dark f-fz3'>班级起止日期：</i><i className='f-fz4' style={{color:'#151515'}}>
								<i className='classStartTime'>{this.state.classInfo.classStartTime}</i>到
								<i className='classEndTime'>{this.state.classInfo.classEndTime}</i>
							</i>
						</Col>
						<Col span={5} className='f-align-left'>
							<i className='f-dark f-fz3'>上课老师：</i><i className='f-fz4' style={{color:'#151515'}}>{this.state.classInfo.teacherName}</i>
						</Col>
						<Col span={5} className='f-align-left'>
							<i className='f-dark f-fz3'>学费：</i><i className='f-fz4' style={{color:'#151515'}}>
								{this.state.classInfo.salePrice}元／期
							</i>
						</Col>
					</Row>
					<Row className='f-pb4 f-mb4' style={{borderBottom: '1px solid #DCDCDC'}}>
						<Col span={7} className='f-align-left'>
							<i className='f-dark f-fz3'>上课时段：</i><i className='f-fz4' title={this.state.classInfo.classStartTimeList} style={{color:'#151515'}}>
								{this.state.classInfo.classStartTimeList[0]?this.state.classInfo.classStartTimeList[0]:''}
								{this.state.classInfo.classStartTimeList[1]?'......':''}
							</i>
						</Col>
						<Col span={7} className='f-align-left'>
							<i className='f-dark f-fz3'>教室：</i><i className='f-fz4' style={{color:'#151515'}}>{this.state.classInfo.classRoomName}</i>
						</Col>
						<Col span={5} className='f-align-left'>
							<i className='f-dark f-fz3'>课时数：</i><i className='f-fz4' style={{color:'#151515'}}>{this.state.classInfo.totalCoursePeriod}</i>
						</Col>
						<Col span={5} className='f-align-left'>
						</Col>
					</Row>
					<Row>
						<Col span={24} className='f-align-right f-pr4'>
							<a className="f-btn-blue f-mr5" onClick={()=>{window.location.href='/teach/recordAttend?name='+this.state.classInfo.name}}>
								记上课
							</a>
							<a className="f-btn-blue f-mr5" onClick={()=>{window.location.href='/teach/classRecord?name='+this.state.classInfo.name}}>
								上课记录
							</a>
							<a className="f-btn-blue f-mr5" onClick={()=>{window.location.href='/teach/pointList?name='+this.state.classInfo.name}}>
								点名表
							</a>
							<a className="f-btn-blue" onClick={()=>{window.location.href='/teach/editClass?id='+getUrlParam('id')}}>
								编辑班级
							</a>
						</Col>
					</Row>
				</div>
				<div className='f-bg-white f-pd4 f-box-shadow1 f-radius1 f-mb6'>
					<div className='f-flex' style={{justifyContent: 'space-between'}}>
						<h3 className="f-title-blue f-mb3">学员列表</h3>
						<a className="f-btn-blue f-mb3" href={u.hostname()+'/api/educational/class/exportClassInfoAndMembers?classIds='+getUrlParam('id')+'&x-auth-token='+store.get('sessionId')}>
							<i className='iconfont icon-chuangjian f-mr1'></i>班级学员导出
						</a>
					</div>
					<Table columns={columns} dataSource={this.state.pageResult} scroll={{ x: 2050}} bordered pagination={false}/>
				</div>
				<div style={{height:'60px',lineHeight:'60px',background: '#D0D0D0'}} className='f-box-shadow2 f-radius1 f-align-center f-mb3 f-black'>
					学费实收：<i className='totalReceivableAmount'>{this.state.sumInfo.totalReceivableAmount}</i>元 |
					物品总计：<i className='totalMaterialAmount'>{this.state.sumInfo.totalMaterialAmount}</i>元 
				</div>
				<div style={{height:'60px',lineHeight:'60px',background: '#2187FF'}} className='f-box-shadow2 f-radius1 f-align-center f-mb3 f-white'>
					续班率：{this.state.sumInfo.hasContinuedCount}÷{this.state.sumInfo.memberCount}={this.state.sumInfo.hasContinuedRate} |
					插班率：{this.state.sumInfo.insertCount}÷{this.state.sumInfo.memberCount}={this.state.sumInfo.insertRate}  |
					新生率：{this.state.sumInfo.freshCount}÷{this.state.sumInfo.memberCount}={this.state.sumInfo.freshRate}（当前页结果）
				</div>
				
		</div>);
	}
}
const ClassMember = Form.create()(Main);
export {
	ClassMember
}