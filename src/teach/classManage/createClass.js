import React, {
	Component
} from 'react';
import {
	Breadcrumb,
	Form,
	Row,
	Col,
	Select,
	Input,
	Radio,
	DatePicker,
	Button,
	Table,
	Modal,
	message,
	TimePicker
} from 'antd';
import '../main.less';
import moment from 'moment';
import $ from 'jquery'
import {
	fpost,
	fpostArray
} from '../../common/io.js';
import {
	getUrlParam
} from '../../common/g.js';
const Option = Select.Option;
const confirm = Modal.confirm;
const FormItem = Form.Item;
const dateFormat = 'YYYY-MM-DD HH:mm:ss';
let recordName;
let recordId;
let period; //每次上课数值
let listPeriod;
let zdSchData;
let assiant = ''; //助教,
let deleteID = []; //修改进来删除的列表的话，已有数据的id组合

function zdschoolChange(value) {
	zdSchData = `${value}`;
}

function showDeleteConfirm(deleteList) { //删除表格事件
	confirm({
		title: '确定要删除这条记录吗?页面保存才会生效哦',
		okText: '确定',
		okType: 'danger',
		cancelText: '取消',
		onOk() {
			message.success('删除成功');
			let num = $('.arrangeTotal').html();
			$('.arrangeTotal').html(parseInt(num - deleteList.find('td.period div').html()));
			deleteList.remove(); //删除成功
			if ($('.pageTable tbody tr').length == 0) {
				$('.pageTable .ant-table-placeholder').show();
			}
		},
		onCancel() {},
	});
}
export class Main extends Component {
	constructor(props) {
		super(props)
		this.state = {
			holidayState: 0,
			visible: false,
			visibleKS: false,
			visible1: false,
			startValue: null,
			startValueM: null,
			endValue: null,
			endValueM: null,
			endOpen: false,
			startValue1: null,
			endValue1: null,
			endOpen1: false,
			allSchool: [], //所在校区
			courseId: null, //课程选择
			name: null, //班级名称
			startTime: null, //开班开始时间
			endTime: null, //开班结束时间
			salePrice: '', //学费标准
			totalCoursePeriod: '', //课时总计
			startDate: null,
			endDate: null,
			modalHour: null, //弹框消耗课时
			isFree: 1,
			consumeCoursePeriod: null, //每次上课
			headmasterId: null, //班主任
			teacherId: null, //上课老师
			defaultRoomId: null, //教室名称
			rated: null, //额定人数
			taTeacherId: null, //助教
			course: null,
			startDateM: null, //第二个弹框的开始日期
			endDateM: null, //第二个弹框的开始日期
			modalTitle: '教室空闲时间查询',
			modalList: '教室名称',
			modalResult: [],
			_getAssiant: [], //助教
			teacherUsableTimeList: '',
			classRoomUsableTimeList: '',
			masterList: [], //班主任列表
			classroomList: [],
			initResult: {}, //页面初始化
			classTimesRuleVOList: [],
			iconLoading: false,
			startTimeLeft: '',
			tableStart: null,
			tableEnd: null,
			index: '',
			indexS: '2', //值为2时表示点击页面上的继续添加，为1时表示点击表格上的修改按钮对应的弹框
			editPage: 0,
			listIndex: '9999999999',
			editState: 0, //修改班级进来的继续添加
			visiblePeriod: false,
			ypks: '', //已排课时
			editAdd: '', //修改进来的继续添加
			listId: '', //table表格的每条数据的id
			masterInfo: [], //班主任、助教等下拉信息
			ksend: '', //排课结束时间已更新为
			modalJhkc: '', //计算出来的计划课次，课时总计/弹框的消耗课时
			jhkc: '', //弹框默认的计划课次
		}
		this._radio = this._radio.bind(this);
		this.showDeleteConfirm = this.showDeleteConfirm.bind(this);
		this.editModal = this.editModal.bind(this);
		this.ksTotal1 = this.ksTotal1.bind(this); //课时总计
		this.ksTotalM = this.ksTotalM.bind(this); //学费标准
	}
	disabledStartDateM = (startValueM) => { //只读的开始日期
		const endValueM = this.state.endValueM;
		if (!startValueM || !endValueM) {
			return false;
		}
		return startValueM.valueOf() > endValueM.valueOf();
	}
	disabledEndDateM = (endValueM) => { //只读的结束日期
		const startValueM = this.state.startValueM;
		if (!endValueM || !startValueM) {
			return false;
		}
		return endValueM.valueOf() <= startValueM.valueOf();
	}
	onChangeM = (field, value) => {
		this.setState({
			[field]: value,
		});
	}
	onStartChangeM = (value) => {
		this.onChange('startValueM', value);
	} //开始日期改变
	onEndChangeM = (value) => {
		this.onChange('endValueM', value);
	} //结束日期改变
	handleStartOpenChangeM = (openM) => {
		if (!openM) {
			this.setState({
				endOpenM: true
			});
		}
	}
	handleEndOpenChangeM = (openM) => {
		this.setState({
			endOpenM: openM
		});
	}

	disabledStartDate = (startValue) => { //只读的开始日期
		const endValue = this.state.endValue;
		if (!startValue || !endValue) {
			return false;
		}
		return startValue.valueOf() > endValue.valueOf();
	}
	disabledEndDate = (endValue) => { //只读的结束日期
		const startValue = this.state.startValue;
		if (!endValue || !startValue) {
			return false;
		}
		return endValue.valueOf() <= startValue.valueOf();
	}
	onChange = (field, value) => {
		this.setState({
			[field]: value,
		});
	}
	onStartChange = (value) => {
		this.onChange('startValue', value);
	} //开始日期改变
	onEndChange = (value) => {
		this.onChange('endValue', value);
	} //结束日期改变
	handleStartOpenChange = (open) => {
		if (!open) {
			this.setState({
				endOpen: true
			});
		}
	}
	handleEndOpenChange = (open) => {
		this.setState({
			endOpen: open
		});
	}

	disabledStartDate1 = (startValue1) => { //只读的开始日期
		const endValue1 = this.state.endValue1;
		if (!startValue1 || !endValue1) {
			return false;
		}
		return startValue1.valueOf() > endValue1.valueOf();
	}
	disabledEndDate1 = (endValue1) => { //只读的结束日期
		const startValue1 = this.state.startValue1;
		if (!endValue1 || !startValue1) {
			return false;
		}
		return endValue1.valueOf() <= startValue1.valueOf();
	}
	onChange1 = (field, value) => {
		this.setState({
			[field]: value,
		});

	}
	onStartChange1 = (value) => {
		this.onChange1('startValue1', value);
	} //开始日期改变
	onEndChange1 = (value) => {
		this.onChange1('endValue1', value);
	} //结束日期改变
	handleStartOpenChange1 = (open1) => {
		if (!open1) {
			this.setState({
				endOpen1: true
			});
		}
	}
	handleEndOpenChange1 = (open1) => {
		this.setState({
			endOpen1: open1
		});
	}
	editModal(data) {
		let ss;
		let jhkc;
		//		this._getAssiant();  //获取助教列表
		for (let i = 0; i < $('.pageTable table tbody .isSkipHoliday').length; i++) {
			let num = $('.arrangeTotal').html();
			if ($('.pageTable table tbody .isSkipHoliday div').eq(i).attr('data-deleteid') == data.id) {
				jhkc = $('.pageTable table tbody .period div').eq(i).html() / $('.pageTable table tbody .period div').eq(i).attr('data-id');
				$('.pageTable table tbody .isSkipHoliday div').eq(i).parents('tr').attr('data-no', 1);
				$('.attendClassroom').attr('data-id', $('.pageTable table tbody .isSkipHoliday div').eq(i).parents('tr').find('td.roomId div').attr('data-id'));
				$('.attendClassroom').val($('.pageTable table tbody .isSkipHoliday div').eq(i).parents('tr').find('td.roomId div').html());
				$('.attendTeacher').val($('.pageTable table tbody .isSkipHoliday div').eq(i).parents('tr').find('td.teacherId div').html());
				$('.attendTeacher').attr('data-id', $('.pageTable table tbody .isSkipHoliday div').eq(i).parents('tr').find('td.teacherId div').attr('data-id'));
				//				$('.xhPeriod').val($('.pageTable table tbody .isSkipHoliday div').eq(i).parents('tr').find('td.period div').attr('data-id'));
				if ($('.pageTable table tbody .isSkipHoliday div').eq(i).parents('tr').find('td.isSkipHoliday div').attr('data-day') != 0) {
					this.setState({
						holidayState: 1,
						modalHour: $('.pageTable table tbody .isSkipHoliday div').eq(i).parents('tr').find('td.period div').attr('data-id')
					})
				} else {
					this.setState({
						holidayState: 0,
					})
				}
				$('.xhPeriod').eq($('.pageTable table tbody .isSkipHoliday div').eq(i).parents('tr').find('td.period div').attr('data-id')).attr('selected', true)
				for (let j = 0; j < $('.assiantVal option').length; j++) {
					if ($('.pageTable table tbody .isSkipHoliday div').eq(i).parents('tr').find('td.assistantId div').attr('data-id') == $('.assiantVal option').eq(j).val()) {
						$('.assiantVal option').eq(j).attr('selected', 'selected');
					}
				}
			}
		}
		let obj = [];
		obj = data.weekday.toString().split(',');
		for (let i = 0; i < $('.work').length; i++) { //编辑时查看星期
			for (let j = 0; j < obj.length; j++) {
				if (obj[j] == $('.work').eq(i).attr('data-id')) {
					$('.work').eq(i).prop("checked", true)
				}
			}
		}
		//		this.showModal();
		this.setState({
			modalHour: data.period,
			tableStart: data.startDate,
			tableEnd: data.endDate,
			startTimeLeft: data.displayStartDate.split(' ')[1].split(':')[0],
			startTimeRight: data.displayStartDate.split(' ')[1].split(':')[1],
			endTimeLeft: data.displayEndDate.split(' ')[1].split(':')[0],
			endTimeRight: data.displayEndDate.split(' ')[1].split(':')[1],
			index: data.key,
			indexS: 1,
			startValue1: this.props.form.getFieldValue('startDate') ? this.props.form.getFieldValue('startDate') : data.startDate,
			listId: data.id,
		});
		let start = data.startDate;
	}
	_delete(data) { //删除
		fpost('/api/educational/class/times/rule/delete ', {
				classTimesRuleId: data,
				classId: getUrlParam('id')
			})
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					message.success('删除成功');
					$('input.work').prop("checked", false);
					this._init1();
					this.setState({
						classRoomUsableTimeList: '',
						teacherUsableTimeList: ''
					})
				} else {
					message.error(res.message)
				}
			});
	}
	showDeleteConfirm(record) { //删除表格事件
		let self = this;
		if (record.isAttendClass == true) {
			message.error('已上课，不可以删除哦')
			return false;
		} else {
			confirm({
				title: '确定要删除这条记录吗?页面保存才会生效哦',
				okText: '确定',
				okType: 'danger',
				cancelText: '取消',
				onOk() {
					let id = record.id;
					deleteID.push(id);
					self._delete(id);
					if ($('.pageTable tbody tr').length == 0) {
						$('.pageTable .ant-table-placeholder').show();
					}
				},
				onCancel() {},
			});
		}
	}
	showModalJX = () => {
		this.props.form.setFieldsValue({
			startDate: this.props.form.getFieldValue('classStartTime'),
			endDate: this.props.form.getFieldValue('classEndTime'),
			jhkc: ''
		})
		this.setState({
			modalResult: [],
			startTimeLeft: '',
			startTimeRight: '',
			endTimeLeft: '',
			endTimeRight: '',
			tableStart: null
		})
		if (parseInt($('.arrangeTotal').html()) >= parseInt(this.props.form.getFieldValue('totalCoursePeriod'))) {
			message.info('已排课时总数不能大于课时总计哦');
			return false;
		} else {
			$('.attendClassroom').attr('data-id', '');
			$('.attendClassroom').val('');
			$('.attendTeacher').val('');
			$('.attendTeacher').attr('data-id', '');
			$('.assiantVal').val('请选择');
			this.showModal();
		}
	}
	showModal = () => {
		if (this.props.form.getFieldValue('consumeCoursePeriod').length) {
			period = this.props.form.getFieldValue('consumeCoursePeriod');
		} else {
			period = '';
		}
		if (!this.props.form.getFieldValue('schoolAreaId').toString()) {
			message.error('请先选择校区');
			return false;
		} else {
			this.setState({
				schoolAreaIdM: this.props.form.getFieldValue('schoolAreaIdM'),
			})
			//			if(!this.state.tableStart){
			//				this.setState({
			//					tableStart: this.props.form.getFieldValue('classStartTime'),
			//				})
			//			}
			//			if(!this.state.tableEnd){
			//				this.setState({
			//					tableEnd: this.props.form.getFieldValue('classEndTime'),
			//				})
			//			}
		}
		this.setState({
			visible: true, //确定对话框的展示
			modalResult: []
		}, function() {
			$('.modalSS input').val(this.state.tableStart);
		});
	}
	_getAssiant() { //获取第一个弹框的助教列表
		const self = this;
		let schoolAreaId = this.state.selectedSchoolAreaId || this.state.initResult.schoolAreaId || '';
		console.log(schoolAreaId);
		console.log('schoolAreaId');
		fpost('/api/hr/staff/listStaffByCondForDropDown', {
				type: 6,
				schoolAreaId: schoolAreaId
			})
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					if (res.result) {
						res.result.forEach((item, i) => {
							item.key = i;
						});
						self.setState({
							_getAssiant: res.result
						});
					}
				} else {
					message.error(res.message)
				}
			});
	}
	handleOk = (e) => { //对话框里的确定按钮对应事件
		this.setState({
			visible: false,
		});
	}
	handleOkPeriod = (e) => { //对话框里的获取已排课时确定
		if (this.state.listId) {
			this.editEdit(); //修改进来的修改
		} else {
			this.editadd(); //修改进来的继续添加
		}

	}
	handleCancelKs = (e) => {
		this.setState({
			visibleKS: false,
		});
	}
	handleOkKs(modal) {
		//		this._getModalSave(modal); //按顺序执行弹框的保存
		//		this.setState({
		//			visibleKS: true,
		//		});
		let xhPeriod = $('.xhPeriod').val();
		let totalCoursePeriod = this.props.form.getFieldValue('totalCoursePeriod');
		let result = totalCoursePeriod / xhPeriod;
		let y = String(result).indexOf(".") + 1; //获取小数点的位置
		let count = String(result).length - y; //获取小数点后的个数
		if (!this.props.form.getFieldValue('jhkc')) {
			message.error('计划所排课次必填哦', 3);
			return false;
		}
		if (y > 0) {
			let error1 = '计算出来的课次约为:' + result.toFixed(2) + '为小数，请重新设置课时总计或消耗课时保证计算出来的课次不为小数才可以哦';
			message.error(error1, 3);
			return false;
		} else {
			this.setState({
				modalJhkc: result
			})
		}
		let error2 = '计算出来的课次为：' + result + ',所填计划所排课次不能大于计算出来的课次哦';
		if (result && this.props.form.getFieldValue('jhkc') > result) {
			message.error(error2, 3);
			return false;
		}
		let param = {
			startDate: modal.start,
			weekday: modal.weekday,
			classTimes: this.props.form.getFieldValue('jhkc'),
			isSkipHoliday: modal.isSkipHoliday,
			period: $('.xhPeriod').val(),
			//			totalPeriod: this.props.form.getFieldValue('totalCoursePeriod') ? this.props.form.getFieldValue('totalCoursePeriod') : ''
		};
		fpostArray('/api/educational/class/times/rule/calculateEndTime', param)

			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					this.setState({
						visibleKS: true,
					});
					let data = res.result.split(' ')[0]
					this.props.form.setFieldsValue({
						endDate: moment(data, 'YYYY-MM-DD'),
					})
					this.setState({
						tableEnd: moment(data, 'YYYY-MM-DD'),
						ksend: data
					})
					modal.end = data;
					modal.endTime = modal.end + ' ' + modal.endPeriod;
					this.setState({
						modal: modal
					})
				} else {
					message.error(res.message)
				}
			});

	}
	jhkc(v) { //计划排课的onChange
		if (isNaN(v.target.value) || v.target.value <= 0) {
			v.target.value = v.target.value.substring(0, v.target.value.length - 1)
		}
	}
	handleOkKs1() {
		let modal = this.state.modal;
		this.setState({
			visibleKS: false,
		}, function() {
			this._getModalSave(modal); //按顺序执行弹框的保存
		});

	}
	_init1() { //获取页面初始化
		const self = this;
		fpost('/api/educational/class/find', {
				classId: getUrlParam('id')
			})
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					res.result.classTimesRuleVOList.forEach((item, i) => {
						item.key = i;
					});
					self.setState({
						classTimesRuleVOList: res.result.classTimesRuleVOList,
						listId: '',
						totalCoursePeriod: res.result.totalCoursePeriod,
					});
					self.props.form.setFieldsValue({
						classEndTime: moment(res.result.classEndTime)
					})
					let arrangeTotal = 0;
					for (var i = 0; i < $('.pageTable tbody tr').length; i++) {
						arrangeTotal += parseFloat($('tbody tr').eq(i).find('td.period div').html());
					}
					$('.arrangeTotal').html(arrangeTotal);
				} else {
					message.error(res.message)
				}
			});
	}
	editEdit() {
		fpostArray('/api/educational/class/times/rule/update', this.state.editAdd)
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					if (res.result) {
						this.setState({
							visiblePeriod: false,
							visible: false,
							classRoomUsableTimeList: '',
							teacherUsableTimeList: '',
							tableStart: ''
						}, function() {
							$('input.work').prop("checked", false);
							this._init1();
							this.props.form.setFieldsValue({
								startDate: this.props.form.getFieldValue('classStartTime'),
								endDate: this.props.form.getFieldValue('classEndTime'),
							})
						})
					}
				} else {
					message.error(res.message)
				}
			});
	}
	editadd() {
		fpostArray('/api/educational/class/times/rule/add', this.state.editAdd)
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					if (res.result) {
						this.setState({
							visiblePeriod: false,
							visible: false,
							classRoomUsableTimeList: '',
							teacherUsableTimeList: ''
						}, function() {
							$('input.work').prop("checked", false);
							this._init1();
						})
					}
				} else {
					message.error(res.message)
				}
			});
	}
	handleCancelPeriod = (e) => { //对话框里的确定已排课时取消
		this.setState({
			visiblePeriod: false
		})
	}
	handleOk1 = (e) => { //第二个对话框里的确定按钮对应事件
		const self = this;
		if (self.state.modalResult.length == 0) {
			message.error('还没有搜索出来数据可供选择哦');
			return false;
		}
		if (recordName) {
			$('.modalButton2').attr('disabled', true);
			self.setState({
				visible1: false,
			}, function() {
				$('.modalButton2').attr('disabled', false);
				self.props.form.setFieldsValue({
					modalSecRoomId: '',
					modalSecTeacherId: ''
				});
			});

			if (self.state.modalTitle == '教室空闲时间查询') {
				$('.attendClassroom').val(recordName);
				$('.attendClassroom').attr('data-id', recordId);
			}
			if (self.state.modalTitle == '老师空闲时间查询') {
				$('.attendTeacher').val(recordName);
				$('.attendTeacher').attr('data-id', recordId);
			}
			if (self.state.modalTitle == '助教空闲时间查询') {
				$('.attendAssiant').val(recordName);
				$('.attendAssiant').attr('data-id', recordId);
			}
			this.handleCancel1();
			self.setState({
				modalResult: [],
				startValueM: null,
				endValueM: null
			});
		} else {
			if (self.state.modalTitle == '教室空闲时间查询') {
				message.error('请先选择教室');
				return false;
			}
			if (self.state.modalTitle == '老师空闲时间查询') {
				message.error('请先选择老师');
				return false;
			}
			if (self.state.modalTitle == '助教空闲时间查询') {
				message.error('请先选择助教');
				return false;
			}
		}
	}
	handleCancel1 = (e) => { //第二个对话框里的取消按钮对应事件
		$('.ant-calendar-picker-container').hide();
		this.setState({
			startDateM: this.props.form.getFieldValue('startDate'),
			endDateM: this.props.form.getFieldValue('endDate'),
			visible1: false,
			modalResult: []
		}, function() {
			this.modalReset();
		});
		this.props.form.setFieldsValue({
			modalSecRoomId: '',
			modalSecTeacherId: ''
		});
		$('.modalButton2').attr('disabled', false);
	}
	showModal1 = () => {
		let start;
		if (this.props.form.getFieldValue('startDate')) {
			start = this.props.form.getFieldValue('startDate')
		} else if (this.state.startValue1) {
			start = this.state.startValue1;
		} else {
			start = null;
		}
		let end;
		if (this.props.form.getFieldValue('endDate')) {
			end = this.props.form.getFieldValue('endDate')
		} else if (this.state.endValue1) {
			end = this.state.endValue1;
		} else {
			end = null;
		}
		this.setState({
			schoolAreaIdM: this.props.form.getFieldValue('schoolAreaId'),
			startValue1: start,
			endValue1: end,
			modalResult: []
		}, function() {
			this.setState({
				visible1: true, //确定对话框的展示
			}, function() {})

		})
	}
	_modalRequest() { //按第二个弹框的搜索请求
		const self = this;
		const param = {};
		self.setState({
			modalResult: []
		});
		let http;
		if (self.state.modalTitle == '教室空闲时间查询') {
			http = '/api/educational/class/classRoom/occupancy'
		}
		if (self.state.modalTitle == '老师空闲时间查询' || self.state.modalTitle == '助教空闲时间查询') {
			http = '/api/educational/class/teacher/occupancy'
		}
		if (self.props.form.getFieldValue('startDate')) {
			param.start = self.props.form.getFieldValue('startDate').format('YYYY-MM-DD')
		} else {
			self.setState({
				modalResult: []
			});
			message.error('开始时间不能为空哦');
			return false;
		}
		if (self.props.form.getFieldValue('endDate')) {
			param.end = self.props.form.getFieldValue('endDate').format('YYYY-MM-DD')
		} else {
			self.setState({
				modalResult: []
			});
			message.error('结束时间不能为空哦');
			return false;
		}
		if (self.props.form.getFieldValue('startDateQ')) {
			param.time1 = self.props.form.getFieldValue('startDateQ').format('HH:mm:ss')
		} else {
			message.error('开始时段不能为空哦');
			return false;
		}
		if (self.props.form.getFieldValue('endDateQ')) {
			param.time2 = self.props.form.getFieldValue('endDateQ').format('HH:mm:ss')
		} else {
			message.error('结束时段不能为空哦');
			return false;
		}
		if (param.time1) {
			param.start = param.start + ' ' + param.time1
		}
		if (param.time2) {
			param.end = param.end + ' ' + param.time2
		}
		if (param.time1 && param.time2) {
			if (param.time1 > param.time2) {
				message.error('开始时段不能大于结束时段哦，请重新选择');
				return false;
			}
		}
		param.schoolAreaId = self.props.form.getFieldValue('schoolAreaIdM');
		let m1 = parseInt(param.start.split("-")[1].replace(/^0+/, "")) + parseInt(param.start.split("-")[0]) * 12;
		let m2 = parseInt(param.end.split("-")[1].replace(/^0+/, "")) + parseInt(param.end.split("-")[0]) * 12;
		if (m2 - m1 > 12) {
			message.error('日期段最多不能相差12个月哦，请重新选择');
			return false;
		}
		let paramData = {};
		//		if (self.state.modalTitle == '教室空闲时间查询') {
		//		} else {
		//			paramData = {
		//				startDate: param.start,
		//				endDate: param.end,
		//				schoolAreaId: param.schoolAreaId,
		//				teacherId: self.props.form.getFieldValue('modalSecTeacherId')
		//			}
		//		}
		paramData = {
			startDate: param.start,
			endDate: param.end,
			schoolAreaId: param.schoolAreaId,
			weekdays: self.props.form.getFieldValue('modalWeek') ? self.props.form.getFieldValue('modalWeek').toString() : ''
		}
		fpost(http, paramData)
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					if (res.result.length > 0) {
						res.result.forEach((item, i) => {
							item.key = i;
						});
						//						for (let i = 0; i < res.result.length; i++) {
						//							let pp = [];
						//							for (let j in res.result[i].usableTimeList[0]) {
						//								pp.push(j + '--' + res.result[i].usableTimeList[0][j]);
						//							}
						//							res.result[i].list = pp;
						//						}
						self.setState({
							modalResult: res.result
						});
						//						let ss = [];
						//						for (let i in res.result[0].usableTimeList[0]) {
						//							ss.push(i + '--' + res.result[0].usableTimeList[0][i]);
						//						}
						//						if (self.state.modalTitle == '老师空闲时间查询') {
						//							self.setState({
						//								teacherUsableTimeList: res.result[0].usableTimeList[0],
						//								teacherUsableTimeList1: JSON.stringify(res.result[0].usableTimeList[0]).split('{')[1].split('}')[0]
						//							})
						//						}
						//						if (self.state.modalTitle == '教室空闲时间查询') {
						//							self.setState({
						//								classRoomUsableTimeList: res.result[0].usableTimeList[0],
						//								//									classRoomUsableTimeList1: JSON.stringify(res.result[0].usableTimeList[0]).split('{')[1].split('}')[0],
						//							})
						//						}
					} else {
						self.setState({
							modalResult: []
						});
						message.info('暂无数据...')
					}
				} else {
					message.error(res.message)
				}
			});
	}
	handleCancel = (e) => { //对话框里的取消按钮对应的事件
		$('.pageTable table tbody tr').attr('data-no', '');
		$('.ant-calendar-picker-container').hide();
		this.setState({
			visible: false,
			startValue1: null,
			endValue1: null,
			startDate: null,
			editModal: '',
			startTimeLeft: '',
			startTimeRight: '',
			endTimeRight: '',
			endTimeLeft: '',
			tableStart: null,
			tableEnd: null,
			indexS: 2,
			listIndex: '99999999',
			listId: ''
		});
		$('.startTimeLeft').val('');
		$('.startTimeRight').val('');
		$('.endTimeLeft').val('');
		$('.endTimeRight').val('');
		$('.consume').find("option").eq(0).prop("selected", true); //消耗课时
		$('input').prop("checked", false);
		$('.modalButton').attr('disabled', false);
	}
	disabledStartDate = (startValue) => { //只读的开始日期
		const endValue = this.state.endValue;
		if (!startValue || !endValue) {
			return false;
		}
		return startValue.valueOf() > endValue.valueOf();
	}

	disabledEndDate = (endValue) => { //只读的结束日期
		const startValue = this.state.startValue;
		if (!endValue || !startValue) {
			return false;
		}
		return endValue.valueOf() <= startValue.valueOf();
	}

	onChange = (field, value) => {
		this.setState({
			[field]: value,
		});
	}

	onStartChange = (value) => { //开始日期改变
		this.onChange('startValue', value);
	}

	onEndChange = (value) => { //结束日期改变
		this.onChange('endValue', value);
	}

	handleStartOpenChange = (open) => {
		if (!open) {
			this.setState({
				endOpen: true
			});
		}
	}

	handleEndOpenChange = (open) => {
		this.setState({
			endOpen: open
		});
	}
	everyHours = (rule, value, callback) => { //额定人数为大于1的整数
		const form = this.props.form;
		let reg = /^[1-9]\d*$/;
		if (value && !reg.test(value)) {
			callback('请输入大于1的整数');
		} else {
			callback();
		}
	}
	everyHours1 = (rule, value, callback) => { //学费标准／课时总计／每次上课为大于0的保留小数点后一位
		const form = this.props.form;
		if (value) {
			if (isNaN(value) || value < 0 || (value.indexOf(".") != -1 && String(value).length - (String(value).indexOf(".") + 1) > 1)) {
				callback('请输入大于0并且最多含有一位小数的数字');
			} else {
				callback();
			}
		} else {
			callback();
		}
	}
	changeModal(v) {
		this.setState({
			modalHour: v.target.value,
		})
	}
	_changeTime(v) {
		let hour = v.target.value.split(':')[0]; //小时
		let min = v.target.value.split(':')[1]; //分钟
		$(v.target).siblings('.startTimeLeft').val(hour); //开始小时
		$(v.target).siblings('.startTimeRight').val(min); //开始分钟
		$(v.target).siblings('.endTimeLeft').val(hour); //结束小时
		$(v.target).siblings('.endTimeRight').val(min); //结束分钟
		v.target.value = '快速选择';
	}
	//常用时间段
	commonInterval() {
		if ($('.commonInterval').css('display') == 'none') {
			$('.commonInterval').css('display', 'block');
			//			$('.commonInterval li').eq(0).attr('disabled',true)
			$('.commonInterval li').off().on('click', function() {
				$('.commonInterval').css('display', 'none')
			})
			$('.commonInterval').mouseover(function() {
				$('.commonInterval').css('display', 'block')
			})
			$('.commonInterval').mouseout(function() {
				$('.commonInterval').css('display', 'none')
			});
			$('.commonInterval li').off().on('click', function() { //常用时间段
				let startTime = $(this).html().split('-')[0]; //开始时间
				let endTime = $(this).html().split('-')[1]; //结束时间
				$('.startTimeLeft').val(startTime.split(':')[0]); //开始小时
				$('.startTimeRight').val(startTime.split(':')[1]); //开始分钟
				$('.endTimeLeft').val(endTime.split(':')[0]); //结束小时
				$('.endTimeRight').val(endTime.split(':')[1]); //结束分钟
				$('.commonInterval').css('display', 'none');
			});
		} else {
			$('.commonInterval').css('display', 'none')
		}
	}
	_modalConfirm() { //第一个弹框的确定按钮
		let weekday = [];
		for (var i = 0; i < $('.weekday').length; i++) {
			if ($('.weekday').eq(i).prop("checked")) {
				weekday.push($('.weekday').eq(i).attr('data-id'))
			}
		}
		weekday = weekday.toString(); //弹框里的星期几
		if (weekday.length == 0) {
			message.error('请选择设置的排课为星期几');
			return false;
		}
		if (!this.props.form.getFieldValue('startDate') || !this.props.form.getFieldValue('endDate')) {
			message.error('请先把表单信息填写完整');
			return false;
		}
		if (this.props.form.getFieldValue('startDate') > this.props.form.getFieldValue('endDate')) {
			message.error('开始时间不能大于结束时间哦');
			return false;
		}
		let start = null;
		let end = null;
		let startTime = null; //弹框保存的开始时间
		let endTime = null; //弹框保存的结束时间
		let holiday = null; //是否跳过法定节假日
		let consumeHour = null; //弹框消耗课时
		let startPeriod = null;
		let endPeriod = null;

		if (this.props.form.getFieldValue('startDate')) {
			start = this.props.form.getFieldValue('startDate').format('YYYY-MM-DD');
		}
		if (this.props.form.getFieldValue('endDate')) {
			end = this.props.form.getFieldValue('endDate').format('YYYY-MM-DD');
		}
		if (!$('.startTimeLeft').val() || !$('.startTimeRight').val() || !$('.endTimeLeft').val() || !$('.endTimeRight').val()) {
			message.error('请选择时间');
			return false;
		}
		if (!this.state.modalHour && !$('.xhPeriod').val()) {
			message.error('请选择消耗课时');
			return false;
		}
		if (!$('.attendClassroom').attr('data-id')) {
			message.error('请先选择教室');
			return false;
		}
		if (!$('.attendTeacher').attr('data-id')) {
			message.error('请先选择老师');
			return false;
		}
		if ($('.startTimeLeft').val() + ':' + $('.startTimeRight').val() && $('.endTimeLeft').val() + ':' + $('.endTimeRight').val()) {
			if ($('.startTimeLeft').val() + ':' + $('.startTimeRight').val() < $('.endTimeLeft').val() + ':' + $('.endTimeRight').val()) {

			} else {
				message.error('结束时间必须大于开始时间时间哦');
				return false;
			}
		}
		startTime = start + ' ' + $('.startTimeLeft').val() + ':' + $('.startTimeRight').val(); //开始时间
		endTime = end + ' ' + $('.endTimeLeft').val() + ':' + $('.endTimeRight').val(); //结束时间
		if (startTime && endTime) {
			if (startTime < endTime) {} else {
				message.error('结束时间必须大于开始时间哦');
				return false;
			}
		}
		startPeriod = $('.startTimeLeft').val() + ':' + $('.startTimeRight').val(); //开始时间
		endPeriod = $('.endTimeLeft').val() + ':' + $('.endTimeRight').val(); //结束时间

		holiday = this.state.holidayState ? 1 : 0; //是否跳过节假日
		consumeHour = this.state.modalHour; //弹框的消耗课时
		let modal = {};

		modal.startTime = startTime;
		modal.endTime = endTime;
		modal.weekday = weekday;
		modal.startPeriod = startPeriod;
		modal.endPeriod = endPeriod;
		if (this.state.modalHour) {}
		modal.modalHour = this.state.modalHour || period;
		modal.start = start;
		modal.end = end;
		modal.holiday = holiday;
		modal.classroom = $('.attendClassroom').val();
		modal.teacher = $('.attendTeacher').val();
		modal.teacherId = $('.attendTeacher').attr('data-id');
		modal.roomId = $('.attendClassroom').attr('data-id');
		modal.assiant = $('.assiantVal').val();
		if ($('.assiantVal').val() == '请选择') {
			modal.assistantId = '';
		} else {
			modal.assistantId = $('.assiantVal').val();
		}
		modal.isSkipHoliday = holiday;
		this.setState({
			modal: modal,
		});
		this.handleOkKs(modal);
		//		this._getModalSave(modal); //弹框的保存
	}
	_modalSave(modal) { //弹框的保存
		let data = '';
		this.props.form.setFieldsValue({
			modalSecRoomId: '',
			modalSecTeacherId: '',
		});
		modal.startTime = modal.startDate + ' ' + modal.startTime;
		modal.endTime = modal.endDate + ' ' + modal.endTime;
		$('.pageTable tbody tr').attr('data-no', '');
		this.setState({
			visible: false,
			modalResult: [],
			classRoomUsableTimeList: '',
			teacherUsableTimeList: '',
			tableStart: '',
			tableEnd: ''
		});
		if (this.state.indexS == 1) {
			if ($('.pageTable table tbody tr').length) {
				data = $('.pageTable table tbody tr').eq(this.state.listIndex).find('.roomId div').attr('data-idd');
				if ($('.pageTable table tbody tr').eq(this.state.listIndex).find('td.isSkipHoliday div').attr('data-origin')) {
					modal.stateState = $('.pageTable table tbody tr').eq(this.state.listIndex).find('td.isSkipHoliday div').attr('data-origin');
					if ($('.pageTable table tbody tr').eq(this.state.listIndex).find('td.isSkipHoliday div').attr('data-editSstate')) {
						modal.stateState = $('.pageTable table tbody tr').eq(this.state.listIndex).find('td.isSkipHoliday div').attr('data-editSstate');
					}
				} else {
					modal.editConAdd = $('.pageTable table tbody tr').eq(this.state.listIndex).find('td.isSkipHoliday div').attr('data-day');
				}
				modal.listid = $('.pageTable table tbody tr').eq(this.state.listIndex).find('td.isSkipHoliday div').attr('data-listid');
				$('.pageTable table tbody tr').eq(this.state.listIndex).remove(); //删除成功
			}
			this.setState({
				editState: 1, //修改进来的 修改原有计划
			})
		} else {
			modal.listid = null;
			modal.stateState = null;
			modal.editConAdd = null;
		}
		$('.modalButton').attr('disabled', false);
		$('.pageTable tbody').append('<tr class="ant-table-row  ant-table-row-level-0" data-edit="1">\
			<td class="f-align-center weekNum weekday isSkipHoliday"><div data-id=' + modal.weekday + ' data-day=' + modal.isSkipHoliday + ' data-deleteid=' + this.state.listIndex + ' data-editSstate=' + modal.stateState + ' data-editConAdd=' + modal.editConAdd + ' data-listid=' + modal.listid + ' data-now="1">' + modal.weekday + '</div></td>\
			<td class="f-align-center start tableStart" data-id=' + modal.start + '>' + modal.startTime + '</td>\
			<td class="f-align-center tableEnd">' + modal.endTime + '</td>\
			<td class="f-align-center roomId"><div data-id=' + modal.roomId + ' data-idd=' + data + '>' + modal.classroom + '</div></td>\
			<td class="f-align-center teacherId"><div data-id=' + modal.teacherId + '>' + modal.teacher + '</div></td>\
			<td class="f-align-center assistantId"><div data-id=' + modal.assistantId + '>' + assiant + '</div></td>\
			<td class="f-align-center modalHour period" ><div data-id=' + modal.period + '>' + listPeriod + '</div></td>\
			<td class="f-align-center">\
				<div><i class="f-pointer f-mr5 iconfont icon-lajixiang f-fz5 showDeleteConfirm"></i><i class="iconfont icon-bianji1 f-pointer f-mr5 f-fz5 editModal"></i></div>\
			</td>\
		</tr>');
		$('input.work').prop("checked", false);
		this.setState({
			modalHour: '',
			indexS: 2,
			listIndex: '9999999',
			tableStart: '',
			tableEnd: ''
		})
		this._event();
		this.handleCancel();
		$('.pageTable .ant-table-placeholder').hide();
		let arrangeTotal1 = 0;
		for (var i = 0; i < $('.pageTable tbody tr').length; i++) {
			arrangeTotal1 += parseFloat($('tbody tr').eq(i).find('td.period div').html());
		}
		$('.arrangeTotal').html(arrangeTotal1);
		let max = '0000-00-00 00:00';
		for (var i = 0; i < $('tbody tr td.tableEnd').length; i++) {
			if (max < $('tbody tr td.tableEnd').eq(i).html()) {
				max = $('tbody tr td.tableEnd').eq(i).html();
			}
		}
		this.props.form.setFieldsValue({
			classEndTime: moment(max),
			totalCoursePeriod: arrangeTotal1
		})
	}
	_getModalSave(modal) {
		const self = this;
		const clazz = {
			//			classTimesRuleVO: {}
		};
		//		clazz.clazz.consumeCoursePeriod = this.props.form.getFieldValue('consumeCoursePeriod') ? this.props.form.getFieldValue('consumeCoursePeriod') : '';
		clazz.startDate = modal.start;
		clazz.endDate = modal.end;
		clazz.startTime = modal.startPeriod;
		clazz.endTime = modal.endPeriod;
		clazz.weekday = modal.weekday;
		clazz.isSkipHoliday = modal.isSkipHoliday;
		clazz.period = $('.xhPeriod').val();
		clazz.roomId = $('.attendClassroom').attr('data-id');
		clazz.teacherId = $('.attendTeacher').attr('data-id');
		clazz.classroom = $('.attendClassroom').val();
		clazz.teacher = $('.attendTeacher').val();
		clazz.assiant = $('.assiantVal').val();
		if ($('.assiantVal').val() != '请选择') {
			clazz.assistantId = $('.assiantVal').val();
		} else {
			clazz.assistantId = '';
		}
		clazz.classId = getUrlParam('id');
		const jxAdd = {
			schoolAreaId: this.props.form.getFieldValue('schoolAreaId'),
			classTimesRuleVOList: []
		}
		const listData = {};
		if (!getUrlParam('id')) { //新增班级
			this._modalSaveTime(clazz);
		} else { //修改班级
			if (this.state.indexS == 2) { //继续添加
				this._modalSaveTime(clazz);
			} else {
				this._modalSaveTime(clazz);
			}
		}
	}
	_modalSaveTime(clazz) {
		let data = {
			startDate: clazz.startDate + ' ' + clazz.startTime + ':00',
			endDate: clazz.endDate + ' ' + clazz.endTime + ':00',
			isHoliday: clazz.isSkipHoliday,
			weekday: clazz.weekday,
		};
		fpost('/api/educational/class/times/rule/checkInvalidClassTimesRule', data)
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					//				message.success('添加成功', 2);
					if (getUrlParam('id')) {
						this._mdoalSaveNext(clazz)
					} else { //新增班级
						this._mdoalSaveNextAdd(clazz)
					}
				} else {
					message.error(res.message);
				}
			});
	}
	_mdoalSaveNextAdd(clazz) { //新增班级
		const self = this;
		const jxAdd = {
			schoolAreaId: this.props.form.getFieldValue('schoolAreaId'),
			classTimesRuleVOList: [{
				startDate: clazz.startDate,
				endDate: clazz.endDate,
				startTime: clazz.startTime,
				endTime: clazz.endTime,
				teacherId: clazz.teacherId,
				roomId: clazz.roomId,
				id: self.state.listId ? self.state.listId : '',
				weekday: clazz.weekday, // 选择的星期
				isSkipHoliday: clazz.isSkipHoliday,
			}]
		}
		for (let i = 0; i < $('.pageTable tbody tr').length; i++) {
			if (i != self.state.listIndex) {
				let list = {};
				list.startDate = $('.pageTable tbody tr').eq(i).find('td.tableStart').html().split(' ')[0];
				list.endDate = $('.pageTable tbody tr').eq(i).find('td.tableEnd').html().split(' ')[0];
				list.startTime = $('.pageTable tbody tr').eq(i).find('td.tableStart').html().split(' ')[1];
				list.endTime = $('.pageTable tbody tr').eq(i).find('td.tableEnd').html().split(' ')[1];
				list.teacherId = $('.pageTable tbody tr').eq(i).find('td.teacherId div').attr('data-id');
				list.roomId = $('.pageTable tbody tr').eq(i).find('td.roomId div').attr('data-id');
				list.id = self.state.listId ? self.state.listId : '';
				list.weekday = $('.pageTable tbody tr').eq(i).find('td.isSkipHoliday div').html(); // 选择的星期
				list.isSkipHoliday = $('.pageTable tbody tr').eq(i).find('td.isSkipHoliday div').attr('data-day');
				jxAdd.classTimesRuleVOList.push(list);
			}
		}
		fpostArray('/api/educational/class/times/rule/checkTimeValid', jxAdd)
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					if (res.result == 'ok') {
						self._getCourseTotal(clazz);
					} else {
						message.error(res.result)
					}
				} else {
					message.error(res.message)
				}
			});
	}
	_getCourseTotal(clazz) {
		let data = {
			startDate: clazz.startDate,
			endDate: clazz.endDate,
			startTime: clazz.startTime,
			endTime: clazz.endTime,
			weekday: clazz.weekday,
			period: clazz.period,
			isSkipHoliday: clazz.isSkipHoliday,
			beforeTimeRangeList: []
		};
		for (let i = 0; i < $('.pageTable tbody tr').length; i++) {
			if (i != this.state.listIndex) {
				let list = {};
				list.startDate = $('.pageTable tbody tr').eq(i).find('td.tableStart').html().split(' ')[0];
				list.endDate = $('.pageTable tbody tr').eq(i).find('td.tableEnd').html().split(' ')[0];
				list.startTime = $('.pageTable tbody tr').eq(i).find('td.tableStart').html().split(' ')[1];
				list.endTime = $('.pageTable tbody tr').eq(i).find('td.tableEnd').html().split(' ')[1];
				list.weekday = $('.pageTable tbody tr').eq(i).find('td.isSkipHoliday div').html();
				list.period = $('.pageTable tbody tr').eq(i).find('td.period div').attr('data-id');
				list.isSkipHoliday = $('.pageTable tbody tr').eq(i).find('td.isSkipHoliday div').attr('data-day');
				data.beforeTimeRangeList.push(list);
			}

		}
		fpostArray('/api/educational/class/times/rule/countCoursePeriod', data)
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					this.setState({
						visible: false,
					}, function() {
						listPeriod = res.result;
						this._modalSave(clazz);
					})
				} else {
					message.error(res.message)
				}
			});
	}
	_mdoalSaveNext(clazz) { //获取排课课时
		const self = this;
		const data = {
			id: self.state.listId ? self.state.listId : '',
			startDate: clazz.startDate,
			endDate: clazz.endDate,
			startTime: clazz.startTime,
			endTime: clazz.endTime,
			weekday: clazz.weekday, // 选择的星期
			isSkipHoliday: clazz.isSkipHoliday,
			teacherId: clazz.teacherId,
			roomId: clazz.roomId,
			classId: getUrlParam('id'),
			assistantId: clazz.assistantId,
			period: clazz.period
		};
		fpostArray('/api/educational/class/times/rule/prompt', data)
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					if (res.result) {
						//						listPeriod = res.result;
						self.setState({
							ypks: res.result,
							visiblePeriod: true,
							editAdd: data
						})
					}
				} else {
					message.error(res.message)
				}
			});
	}
	_event() {
		const self = this;
		$('.showDeleteConfirm').off().on('click', function() { //表格列表的删除按钮
			let deleteList = $(this).parents('tr');
			showDeleteConfirm(deleteList)
		});

		$('.editModal').off().on('click', function() {
			self.setState({
				jhkc: ''
			})
			let index = $(this).parents('tr').index();
			let modalhour = $(this).parents('tr').find('td.modalHour div').attr('data-id');
			let jhkc = $(this).parents('tr').find('.period div').html() / $(this).parents('tr').find('.period div').attr('data-id');
			self.setState({
				listIndex: index,
				indexS: 1,
				jhkc: $(this).parents('tr').find('.period div').html() / $(this).parents('tr').find('.period div').attr('data-id'),
				modalHour: modalhour,
				startDate: $(this).parents('tr').find('.start').attr('data-id'),
				startTimeRight: $(this).parents('tr').find('td.tableStart').html().split(' ')[1].split(':')[1],
				tableStart: $(this).parents('tbody').find('tr').eq(index).find('td.tableStart').html().split(' ')[0],
				tableEnd: $(this).parents('tr').find('td.tableEnd').html().split(' ')[0] + '',
				startTimeLeft: $(this).parents('tr').find('td.tableStart').html().split(' ')[1].split(':')[0],
				endTimeLeft: $(this).parents('tr').find('td.tableEnd').html().split(' ')[1].split(':')[0],
				endTimeRight: $(this).parents('tr').find('td.tableEnd').html().split(' ')[1].split(':')[1]
			}, function() {
				self.props.form.setFieldsValue({
					startDate: moment(self.state.tableStart),
					endDate: moment(self.state.tableEnd),
					jhkc: jhkc
				})
				//				setTimeout(() => {
				//					self.showModal()
				//				}, 300);
			});
			if (!$(this).parents('tr').find('td.isSkipHoliday div').attr('data-listid')) {
				self.setState({
					indexS: 2
				})
			} else {
				self.setState({
					indexS: 1
				})
			}
			$(this).parents('tr').attr('data-no', 1);

			$('.startTimeLeft').val($(this).parents('tr').find('td.tableStart').html().split(' ')[1].split(':')[0]);
			$('.startTimeRight').val($(this).parents('tr').find('td.tableStart').html().split(' ')[1].split(':')[1]);
			$('.endTimeLeft').val($(this).parents('tr').find('td.tableEnd').html().split(' ')[1].split(':')[0]);
			$('.endTimeRight').val($(this).parents('tr').find('td.tableEnd').html().split(' ')[1].split(':')[1]);

			if ($(this).parents('tr').attr('data-edit')) {
				for (let i = 0; i < $('.pageTable table tbody .isSkipHoliday').length; i++) {
					$(this).parents('tr').attr('data-no', 1);
					$('.attendClassroom').attr('data-id', $('.pageTable table tbody tr').eq(index).find('td.roomId div').attr('data-id'));
					$('.attendClassroom').val($('.pageTable table tbody tr').eq(index).find('td.roomId div').html());
					$('.attendTeacher').val($('.pageTable table tbody tr').eq(index).find('td.teacherId div').html());
					$('.attendTeacher').attr('data-id', $('.pageTable table tbody tr').eq(index).find('td.teacherId div').attr('data-id'));
					$('.xhPeriod').val($('.pageTable table tbody tr').eq(index).find('td.period div').attr('data-id'));
					period = $('.pageTable table tbody tr').eq(index).find('td.period div').attr('data-id')
					$('.xhPeriod option').eq(index).attr('selected', 'selected')
					if ($('.pageTable table tbody tr').eq(index).find('td.isSkipHoliday div').attr('data-day') != 0) {
						self.setState({
							holidayState: 1,
							//								modalHour:$('.pageTable table tbody .isSkipHoliday div').eq(index).parents('tr').find('td.period div').attr('data-id')
						})
					} else {
						self.setState({
							holidayState: 0,
						})
					}
					for (let j = 0; j < $('.assiantVal option').length; j++) {
						if ($('.pageTable table tbody tr').eq(index).find('td.assistantId div').attr('data-id') == $('.assiantVal option').eq(j).val()) {
							$('.assiantVal option').eq(j).attr('selected', 'selected');
						}
					}
				}
				let obj1 = [];
				obj1 = $('.pageTable table tbody tr').eq(index).find('td.isSkipHoliday div').attr('data-id').toString().split(',');
				for (let i = 0; i < $('.work').length; i++) { //编辑时查看星期
					for (let j = 0; j < obj1.length; j++) {
						if (obj1[j] == $('.work').eq(i).attr('data-id')) {
							$('.work').eq(i).prop("checked", true)
						}
					}
				}
			}
			self.showModal();
		});
		let arrangeTotal = 0;
		for (var i = 0; i < $('.pageTable tbody tr').length; i++) {
			arrangeTotal += parseFloat($('tbody tr').eq(i).find('td.period div').html());
		}
		$('.arrangeTotal').html(arrangeTotal);
		$('.pageLeft').css('height', $('.pageRight').css('height'))
	}
	holidayState(v) { //是否跳过假节日
		if (v.target.checked) {
			this.setState({
				holidayState: 1
			})
		} else {
			this.setState({
				holidayState: 0
			})
		}
	}
	workingDay(v) { //弹框的工作日选择
		if (v.target.checked) {
			$('.workingDay').prop("checked", true);
		} else {
			$('.workingDay').prop("checked", false);
		}
	}
	weekend(v) { //弹框的周末选择
		if (v.target.checked) {
			$('.weekend').prop("checked", true)
		} else {
			$('.weekend').prop("checked", false)
		}
	}
	startTimeLeft(v) { //小时数onchange
		let reg = /^(0?[0-9]|1[0-9]|2[0-3])$/;
		if (!reg.test(v.target.value)) {
			if (isNaN(v.target.value)) {
				v.target.value = v.target.value.substring(0, v.target.value.length - 1)
			}
			message.error("小时数大于0但是不能超过23");
			if (v.target.value > 23) {
				v.target.value = v.target.value.substring(0, v.target.value.length - 1)
			}
		}
	}
	startTimeRight(v) { //分钟数onchange
		let reg = /^(0?[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9])$/;
		if (!reg.test(v.target.value)) {
			if (isNaN(v.target.value)) {
				v.target.value = v.target.value.substring(0, v.target.value.length - 1)
			}
			message.error("分钟数大于0但是不能超过59");
			if (v.target.value > 59) {
				v.target.value = v.target.value.substring(0, v.target.value.length - 1)
			}
		}
	}
	timeBlur(v) {
		if (v.target.value.length == 1 && v.target.value < 9) {
			v.target.value = '0' + v.target.value;
		}
	}
	pageSave() { //整个页面的保存
		let self = this;
		self.props.form.validateFields(
			(err, fieldsValue) => {
				if (!err) {
					if (fieldsValue['classStartTime'] && fieldsValue['classEndTime']) {
						if (fieldsValue['classStartTime'] > fieldsValue['classEndTime']) {
							message.error('开始时间不能大于结束时间哦');
							return false;
						}
					}
					const clazz = {
						clazz: {},
						classTimesRuleVOList: [],
					};
					const clazz1 = {
						name: fieldsValue.name,
						schoolAreaId: fieldsValue.schoolAreaId,
						courseId: fieldsValue.courseId,
						chargeMode: fieldsValue.chargeMode,
						salePrice: fieldsValue.salePrice,
						totalCoursePeriod: fieldsValue.totalCoursePeriod,
						consumeCoursePeriod: fieldsValue.consumeCoursePeriod.length ? fieldsValue.consumeCoursePeriod : "",
						classStartTime: fieldsValue['classStartTime'] ? fieldsValue['classStartTime'].format('YYYY-MM-DD HH:mm:ss') : '',
						classEndTime: fieldsValue['classEndTime'] ? fieldsValue['classEndTime'].format('YYYY-MM-DD HH:mm:ss') : '',
						headmasterId: fieldsValue.headmasterId ? fieldsValue.headmasterId : '',
						defaultRoomId: fieldsValue.defaultRoomId ? fieldsValue.defaultRoomId : '',
						teacherId: fieldsValue.teacherId ? fieldsValue.teacherId : '',
						taTeacherId: fieldsValue.taTeacherId ? fieldsValue.taTeacherId : '',
						state: '1',
						rated: fieldsValue.rated,
						desc: fieldsValue.desc,
						id: getUrlParam('id')
					}
					clazz.clazz.name = fieldsValue.name;
					clazz.clazz.schoolAreaId = fieldsValue.schoolAreaId;
					clazz.clazz.courseId = fieldsValue.courseId;
					clazz.clazz.chargeMode = fieldsValue.chargeMode;
					clazz.clazz.salePrice = fieldsValue.salePrice;
					clazz.clazz.totalCoursePeriod = fieldsValue.totalCoursePeriod;
					clazz.clazz.consumeCoursePeriod = fieldsValue.consumeCoursePeriod.length ? fieldsValue.consumeCoursePeriod : "";
					clazz.clazz.classStartTime = fieldsValue['classStartTime'] ? fieldsValue['classStartTime'].format('YYYY-MM-DD HH:mm:ss') : '';
					clazz.clazz.classEndTime = fieldsValue['classEndTime'] ? fieldsValue['classEndTime'].format('YYYY-MM-DD HH:mm:ss') : '';
					clazz.clazz.headmasterId = fieldsValue.headmasterId ? fieldsValue.headmasterId : '';
					clazz.clazz.defaultRoomId = fieldsValue.defaultRoomId ? fieldsValue.defaultRoomId : '';
					clazz.clazz.teacherId = fieldsValue.teacherId ? fieldsValue.teacherId : '';
					clazz.clazz.taTeacherId = fieldsValue.taTeacherId ? fieldsValue.taTeacherId : '';
					clazz.clazz.state = '1';
					clazz.clazz.rated = fieldsValue.rated;
					clazz.clazz.desc = fieldsValue.desc;
					for (var i = 0; i < $('.pageTable table tbody tr').length; i++) {
						let list = {};
						if ($('.pageTable table tbody tr').eq(i).find('td.roomId div').attr('data-idd')) {
							list.id = $('.pageTable table tbody tr').eq(i).find('td.roomId div').attr('data-idd');
						}
						list.startDate = $('.pageTable table tbody tr').eq(i).find('td.tableStart').html().split(' ')[0];
						list.startTime = $('.pageTable table tbody tr').eq(i).find('td.tableStart').html().split(' ')[1];
						list.teacherId = $('.pageTable table tbody tr').eq(i).find('td.teacherId div').attr('data-id');
						list.roomId = $('.pageTable table tbody tr').eq(i).find('td.roomId div').attr('data-id');
						list.isSkipHoliday = $('.pageTable table tbody tr').eq(i).find('td.isSkipHoliday div').attr('data-day');
						list.weekday = $('.pageTable table tbody tr').eq(i).find('td.weekday div').attr('data-id');
						list.assistantId = $('.pageTable table tbody tr').eq(i).find('td.assistantId div').attr('data-id');
						list.period = $('.pageTable table tbody tr').eq(i).find('td.period div').attr('data-id');
						list.endDate = $('.pageTable table tbody tr').eq(i).find('td.tableEnd').html().split(' ')[0];
						list.endTime = $('.pageTable table tbody tr').eq(i).find('td.tableEnd').html().split(' ')[1];
						clazz.classTimesRuleVOList.push(list);
					}
					if (parseInt($('.arrangeTotal').html()) > parseInt(fieldsValue.totalCoursePeriod)) {
						message.info('已排课时总数不能大于课时总计哦');
						return false;
					}
					if (clazz.classTimesRuleVOList.length == 0) {
						message.error('排课计划不能为空');
						return false;
					}
					//					clazz.deletedClassTimesRuleIdList=deleteID.length?deleteID:'';
					//					clazz.deletedClassTimesRuleIdList = deleteID;
					if (getUrlParam('id')) { //从修改班级入口进来的
						self._editAll(clazz1); //  修改班级
					} else {
						self._saveAll(clazz); //  新增班级
					}
				} else {
					message.error('信息还未完善哦，请先完善信息');
				}
			}
		)
	}
	_editAll(clazz) { //  修改班级
		fpostArray('/api/educational/class/update', clazz)
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					if (res.result) {
						this.setState({
							iconLoading: true
						});
						message.success('修改成功');
						setTimeout(() => {
							window.location.href = '/teach/classList';
						}, 500);
					}
				} else {
					message.error(res.message);
				}
			});
	}
	_saveAll(clazz) { //  新增班级
		fpostArray('/api/educational/class/add', clazz)
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					if (res.result) {
						this.setState({
							iconLoading: true
						});
						message.success('新增成功');
						setTimeout(() => {
							window.location.href = '/teach/classList';
						}, 500);
					}
				} else {
					message.error(res.message);
				}
			});
	}
	ksTotal(v) { //课时总计
		this.setState({
			totalCoursePeriod: v.target.value
		})
	}

	_getSchool() { //获取全部校区信息
		const self = this;
		let school = [];
		fpost('/api/system/schoolarea/findUserSchoolAreaInfo')
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					var userSchoolArea = res.result.userSchoolArea;
					self.setState({
						allSchool: userSchoolArea,
						createS: userSchoolArea.length == 1 ? userSchoolArea[0]['id'] : [],
						selectedSchoolAreaId: userSchoolArea.length == 1 ? userSchoolArea[0]['id'] : ''
					}, () => {
						if (userSchoolArea.length == 1) {
							console.log(this.state.selectedSchoolAreaId);
							this._getClassroom(this.state.selectedSchoolAreaId);
							this._getMaster('', this.state.selectedSchoolAreaId);
						}
					})
				} else {
					message.error(res.message)
				}
			});
	}
	_getCourse() {
		const self = this;
		let school = [];
		fpost('/api/system/course/listCourse')
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					self.setState({
						course: res.result
					})
				} else {
					message.error(res.message)
				}
			});
	}
	_radio(record) { //第二个弹框的单选按钮
		recordName = record.name;
		recordId = record.pkId;
		//		this.setState({
		//			classRoomUsableTimeList1: JSON.stringify(record.usableTimeList[0]).split('{')[1].split('}')[0],
		//		})
	}
	modalReset() { //第二个弹框的重置
		this.props.form.setFieldsValue({
			startDateM: this.state.startValue1,
			endDateM: this.state.endValue1,
			startDateQ: null,
			endDateQ: null,
			modalWeek: [],
			schoolAreaIdM: this.props.form.getFieldValue('schoolAreaId'),
			modalSecRoomId: '',
			modalSecTeacherId: ''
		});
		fpost('/api/system/schoolarea/listClassRoom ', {
				schoolAreaId: this.props.form.getFieldValue('schoolAreaId')
			})
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					this.setState({
						classroomList: res.result
					})
				} else {
					this.setState({
						classroomList: []
					})
					message.error(res.message)
				}
			});
	}
	_teacher() { //点击第一个弹框的查看老师上课情况
		if (this.props.form.getFieldValue('startDate') > this.props.form.getFieldValue('endDate')) {
			message.error('开始时间不能大于结束时间哦');
			return false;
		}
		this.setState({
			modalTitle: '老师空闲时间查询',
			modalList: '老师名称',
			modalResult: []
		}, function() {
			this.showModal1();
			this.props.form.setFieldsValue({
				modalSecRoomId: '',
				modalSecTeacherId: ''
			});

			/*设置默认值*/
			var startTimeLeft = $('.startTimeLeft').val();
			var startTimeRight = $('.startTimeRight').val();
			var startDateQ = startTimeLeft + ':' + startTimeRight;
			startDateQ = startTimeLeft ? moment(startDateQ, 'HH:mm:ss') : null;

			var endTimeLeft = $('.endTimeLeft').val();
			var endTimeRight = $('.endTimeRight').val();
			var endDateQ = endTimeLeft + ':' + endTimeRight;
			endDateQ = endTimeLeft ? moment(endDateQ, 'HH:mm:ss') : null;

			let weekday = [];
			for (var i = 0; i < $('.weekday').length; i++) {
				if ($('.weekday').eq(i).prop("checked")) {
					weekday.push($('.weekday').eq(i).attr('data-id'))
				}
			}
			// console.log(weekday);

			this.props.form.setFieldsValue({
				startDateM: this.props.form.getFieldValue('startDate'),
				endDateM: this.props.form.getFieldValue('endDate'),
				startDateQ: startDateQ,
				endDateQ: endDateQ,
				modalWeek: weekday
			})
		})
	}
	_classroom() { //点击第一个弹框的查看教室占用情况
		if (this.props.form.getFieldValue('startDate') > this.props.form.getFieldValue('endDate')) {
			message.error('开始时间不能大于结束时间哦');
			return false;
		}
		this.setState({
			modalTitle: '教室空闲时间查询',
			modalList: '教室名称',
			modalResult: []
		}, function() {
			this.showModal1();
			this.props.form.setFieldsValue({
				modalSecRoomId: '',
				modalSecTeacherId: ''
			});

			/*设置默认值*/
			var startTimeLeft = $('.startTimeLeft').val();
			var startTimeRight = $('.startTimeRight').val();
			var startDateQ = startTimeLeft + ':' + startTimeRight;
			startDateQ = startTimeLeft ? moment(startDateQ, 'HH:mm:ss') : null;

			var endTimeLeft = $('.endTimeLeft').val();
			var endTimeRight = $('.endTimeRight').val();
			var endDateQ = endTimeLeft + ':' + endTimeRight;
			endDateQ = endTimeLeft ? moment(endDateQ, 'HH:mm:ss') : null;

			let weekday = [];
			for (var i = 0; i < $('.weekday').length; i++) {
				if ($('.weekday').eq(i).prop("checked")) {
					weekday.push($('.weekday').eq(i).attr('data-id'))
				}
			}
			// console.log(weekday);

			this.props.form.setFieldsValue({
				startDateM: this.props.form.getFieldValue('startDate'),
				endDateM: this.props.form.getFieldValue('endDate'),
				startDateQ: startDateQ,
				endDateQ: endDateQ,
				modalWeek: weekday
			})
		})
	}
	modalSearch() {
		this._modalRequest();
	}
	_getMaster = (v, schoolAreaId) => { //获取班主任下拉
		const self = this;
		schoolAreaId = schoolAreaId || this.state.selectedSchoolAreaId || this.state.initResult.schoolAreaId || '';
		fpost('/api/hr/staff/listStaffByCondForDropDown', {
				name: v ? v : '',
				type: 6,
				schoolAreaId: schoolAreaId || ''
			})
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					self.setState({
						masterList: res.result
					})
					let masterInfo = []; //助教、老师等下拉搜索信息
					for (let i = 0; i < res.result.length; i++) {
						let list = {};
						list.id = res.result[i].id;
						list.name = res.result[i].name + '-' + res.result[i].department + '-' + res.result[i].mobile;
						masterInfo.push(list)
					}
					self.setState({
						masterInfo: masterInfo
					})
				} else {
					message.error(res.message)
				}
			});
	}
	_getClassroom1() {
		const self = this;
		fpost('/api/system/schoolarea/listClassRoom ', {
				schoolAreaId: this.state.initResult.schoolAreaId
			})
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					self.setState({
						classroomList: res.result
					})
				} else {
					self.setState({
						classroomList: []
					})
					message.error(res.message)
				}
			});
	}
	_getClassroom(e) { //获取教室
		const self = this;
		self.state.initResult.defaultRoomId = []
		self.setState({
			classroomList: [],
		});
		this.props.form.getFieldValue['defaultRoomId'] = ''
		if (!e) {
			$('.classroomName .ant-select-selection-selected-value').hide();
			return false
		}
		fpost('/api/system/schoolarea/listClassRoom ', {
				schoolAreaId: e
			})
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					if (res.result.length) {
						self.setState({
							classroomList: res.result
						})
					} else {
						self.setState({
							classroomList: []
						})
					}
					$('.classroomName .ant-select-selection-selected-value').hide();
				} else {
					message.error(res.message)
				}
			});
	}
	_init() { //获取页面初始化
		const self = this;
		fpost('/api/educational/class/find', {
				classId: getUrlParam('id')
			})
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					res.result.classTimesRuleVOList.forEach((item, i) => {
						item.key = i;
					});
					self.setState({
						initResult: res.result,
						salePrice: res.result.salePrice, //学费标准
						totalCoursePeriod: res.result.totalCoursePeriod, //课时总计
						classTimesRuleVOList: res.result.classTimesRuleVOList,
						desc: res.result.desc
					}, () => {
						this._getMaster();
						this._getAssiant();
					});
					let arrangeTotal = 0;
					for (var i = 0; i < $('.pageTable tbody tr').length; i++) {
						arrangeTotal += parseFloat($('tbody tr').eq(i).find('td.period div').html());
					}
					$('.arrangeTotal').html(arrangeTotal);
					this._getClassroom1();
					this._event();
				} else {
					message.error(res.message)
				}
			});
	}
	componentDidMount() {
		console.log(this.state.initResult);
		console.log(this.state.createS);
		this._getSchool(); //获取全部校区
		this._getCourse(); //获取课程
		// this._getMaster(); //获取班主任
		// this._getAssiant();
		if (getUrlParam('id')) {
			this._init(); //获取页面初始化
			$('.new').html('修改班级');
			this.setState({
				editPage: 1
			})
		}
	}
	changeCourse(v) { //改变课程this.state.initResult.salePrice 学费标准
		const self = this;
		fpost('/api/system/course/findCourse', {
				id: v
			})
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					self.setState({
						salePrice: res.result.regularPrice,
						totalCoursePeriod: res.result.regularPeriods
					})

				} else {
					message.error(res.message)
				}
			});
	}
	_changeSchool(v) { //第二个弹框的选择校区change
		this.setState({
			_changeSchool: v
		});
		if (this.state.modalTitle == '教室空闲时间查询') {
			$('.modalClassC .ant-select-selection-selected-value').hide();
			fpost('/api/system/schoolarea/listClassRoom ', {
					schoolAreaId: v
				})
				.then((res) => {
					return res.json();
				})
				.then((res) => {
					if (res.success == true) {
						if (res.result.length > 0) {
							$('.ant-select-selection-selected-value').show();
							this.setState({
								classroomList: res.result
							})
						} else {
							this.setState({
								classroomList: []
							})
						}
					} else {
						this.setState({
							classroomList: []
						})
						message.error(res.message)
					}
				});
		}
	}
	_changeAssiant(v) {
		for (let i = 0; i < $(v.target).find('option').length; i++) {
			if ($(v.target).find('option').eq(i).val() == v.target.value) {
				if ($(v.target).find('option').eq(i).html() != '请选择') {
					assiant = $(v.target).find('option').eq(i).html()
				}
			}
		}
	}
	ksTotalM(e) { //学费标准
		if (isNaN(e.target.value) || e.target.value < 0) {
			e.target.value = e.target.value.substring(0, e.target.value.length - 1)
			return false;
		}
		if (e.target.value) {
			if (e.target.value > 999999999.99) {
				e.target.value = e.target.value.substring(0, e.target.value.length - 1)
			}
			if (e.target.value < 0 || (e.target.value.indexOf(".") != -1 && String(e.target.value).length - (String(e.target.value).indexOf(".") + 1) > 2)) {
				e.target.value = e.target.value.substring(0, e.target.value.length - 1);
			}
		} else {}
	}
	ksTotal1(e) { //课时总计、额定人数
		if (isNaN(e.target.value) || e.target.value < 0) {
			e.target.value = e.target.value.substring(0, e.target.value.length - 1)
			return false;
		}
		if (e.target.value) {
			if (e.target.value < 0 || (e.target.value.indexOf(".") != -1)) {
				e.target.value = e.target.value.substring(0, e.target.value.length - 1)
			}
			if (e.target.value > 9999) {
				e.target.value = e.target.value.substring(0, e.target.value.length - 1)
			}
		}
	}
	_getClssRoom() {
		if (!this.props.form.getFieldValue('schoolAreaId')) {
			message.error('请先选择校区，才可以选择班级哦');
			return false;
		}
	}
	_getClssRoom1() {
		$('.classroomName .ant-select-selection-selected-value').show();
	}
	_validaName = (rule, value, callback) => { //班级名称正则
		const form = this.props.form;
		if (value && value.length > 50) {
			callback('班级名称长度最多为50哦');
		} else {
			callback();
		}
	}
	_jqStr() { //第二个弹框限制字符长度
		for (var i = 0; i < $('.strTru').length; i++) {
			let html = '';
			let str = $('.strTru').eq(i).html().substring(0, 8);
			if ($('.strTru').eq(i).html().length > 8) {
				html = str + '...';
			} else {
				html = str;
			}
			$('.strTru').eq(i).html(html);
		}
	}
	render() {
		const self = this;
		const {
			getFieldDecorator
		} = this.props.form;
		const formItemLayout = {
			labelCol: {
				span: 6
			},
			wrapperCol: {
				span: 18
			}
		};
		const formItemLayout1 = {
			labelCol: {
				span: 7
			},
			wrapperCol: {
				span: 16
			}
		};
		const columns = [{
			title: '星期',
			key: '0',
			width: 100,
			className: 'f-align-center weekday isSkipHoliday',
			render: (text, record, index) => {
				return (
					<div data-id={record.weekday} data-day={record.isSkipHoliday==true?'1':'0'} data-origin={record.id} data-deleteid={record.id} data-listid={record.id}>{record.weekday}</div>
				)
			}
		}, {
			title: '开始时间',
			key: '1',
			width: 150,
			className: 'f-align-center tableStart',
			dataIndex: 'displayStartDate'
		}, {
			title: '结束时间',
			key: '2',
			width: 150,
			className: 'f-align-center tableEnd',
			dataIndex: 'displayEndDate'
		}, {
			title: '教室',
			key: '3',
			width: 150,
			className: 'f-align-center roomId',
			render: (text, record, index) => {
				return (
					<div data-id={record.roomId} data-idd={record.id}>{record.classRoomName}</div>
				)
			}
		}, {
			title: '上课老师',
			key: '4',
			width: 150,
			className: 'f-align-center teacherId',
			render: (text, record, index) => {
				return (
					<div data-id={record.teacherId} className='f-table-teacher' style={{width:'150px',overflow: 'hidden',textOverflow: 'ellipsis',whiteSpace: 'nowrap'}} title={record.teacherName}>{record.teacherName}</div>
				)
			}
		}, {
			title: '助教',
			key: '5',
			width: 150,
			className: 'f-align-center assistantId',
			render: (text, record, index) => {
				return (
					<div data-id={record.assistantId}>{record.assistantName}</div>
				)
			}
		}, {
			title: '已排课时',
			key: '6',
			width: 150,
			className: 'f-align-center period',
			render: (text, record, index) => {
				return (<div data-id={record.period}>
					{record.allocationPeriod}
		            </div>);
			}
		}, {
			title: '操作',
			key: '7',
			width: 100,
			className: 'f-align-center',
			render: (text, record, index) => {
				this._event();
				return (<div>
		                <i className='f-pointer f-mr3 iconfont icon-lajixiang f-fz5' onClick={()=>{this.showDeleteConfirm(record)}}></i>
		                <i className='iconfont icon-bianji1 f-pointer f-fz5 editModal' onClick={()=>{this.editModal(record)}}></i>
		            </div>);
			}
		}];
		const columns1 = [{
			title: this.state.modalList,
			key: '0',
			width: 200,
			className: 'f-align-center',
			render: (text, record, index) => {
				this._jqStr();
				return (
					<div title={record.name} className='f-line1'>
			   			<input type='radio' name='radio' className='radio f-line1' onClick={()=>this._radio(record)}/><i className='courseName strTru f-line1 f-ml2'>{record.name}</i>
			   		</div>
				)
			}
		}];
		return (<div className='createClass classListPage Topselect'>
				<Breadcrumb className="f-mb3">
				    <Breadcrumb.Item>教务教学</Breadcrumb.Item>
				    <Breadcrumb.Item><a href="">班级管理</a></Breadcrumb.Item>
				    <Breadcrumb.Item>
				   		<span className="f-fz5 f-bold new">新建班级</span>
				    </Breadcrumb.Item>
				</Breadcrumb>
				
				<Form className='f-box-shadow1 f-radius1 f-bg-white f-flex' style={{minHeight:'750px'}}>
					<Modal
			          visible={this.state.visiblePeriod}
			          title="提示"
			          onOk={this.handleOkPeriod}
			          onCancel={this.handleCancelPeriod}>
			          <p>已排课时:{this.state.ypks}课时</p>
			        </Modal>
			        <Modal
			          visible={this.state.visibleKS}
			          title="提示"
			          onOk={this.handleOkKs1.bind(this)}
			          onCancel={this.handleCancelKs}>
			          <p>排课的结束时间已更新为:{this.state.ksend}</p>
			        </Modal>
					<div className='pageLeft' style={{width:'350px',
						minWidth: '350px',
						background: 'url("http://img.fancyedu.com/sys/ic/operation/1509585093744_newClassBg.png") 0% 0% / 100% no-repeat',
						paddingTop:'46px',
						backgroundSize:' 100% 100%',
					}}>
					
						<img src='http://img.fancyedu.com/sys/ic/operation/1509585104658_newClassIcon.png' className='f-block' style={{margin:'0 auto',height:'134px'}}/>
						<div className='f-align-center f-white f-fz7 f-mt3' style={{marginBottom:'50%'}}>班级详情</div>
						<Row className='f-pr4 f-pl4 f-mb5' style={{height:'35px'}}>
								<FormItem 
									{...formItemLayout1} 
									label="所在校区" 
								>
				                  {
				                  	getFieldDecorator('schoolAreaId', {
				                  	rules: [{
				                  		required:true,
				                  		message:'请选择校区'
				                  	}],
									initialValue:this.state.initResult.schoolAreaId ? this.state.initResult.schoolAreaId.toString() : this.state.createS
				                  })(
				                  	<Select
				                        showSearch
				                        placeholder="选择校区"
				                        optionFilterProp="children"
				                        onChange={
				                        	(v)=> {
				                        		this.setState({
				                        			selectedSchoolAreaId:v
				                        		},()=> {
				                        			this._getClassroom(v);
				                        			this._getMaster('',v);
				                        		});
				                        	}
				                        }
				                        disabled={this.state.editPage?true:false}
				                        filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
				                      >
				                      {this.state.allSchool?
					                        	this.state.allSchool.map((item,i)=> {
					                        		return(<Option key={item.id} value={item.id}>{ item.name }</Option>)
					                        	})
				                        :null
				                     }
				                      </Select> 
				                  )}
			                  </FormItem>
						</Row>
						<Row className='f-pr4 f-pl4 f-mb5' style={{height:'35px'}}>
							<FormItem {...formItemLayout1} label="课程选择">
				                  {getFieldDecorator('courseId', {
				                  	rules: [{required:true,message:'请选择课程'}],
				                  	initialValue:this.state.initResult.courseId ? (this.state.initResult.courseId).toString() : []
				                  })(
				                  	<Select showSearch placeholder="请选择" optionFilterProp="children"
				                  		disabled={this.state.editPage?true:false}
				                  		onChange={this.changeCourse.bind(this)}
				                        filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}>
				                      {this.state.course?
					                        	this.state.course.map((item,i)=> {
					                        		return(<Option key={item.id} value={item.id}>{ item.name }</Option>)
					                        	})
					                        	:null
				                      }
				                      </Select>
				                   )}
		                  	</FormItem>
						</Row>
						<Row className='f-pr4 f-pl4 f-mb5' style={{height:'35px'}}>
							<FormItem {...formItemLayout1} label="班级名称">
				                  {getFieldDecorator('name', {
				                  	rules: [{required:true,message:'请输入班级名称'},
				                  		{validator:this._validaName}
				                  	],
				                  	initialValue:this.state.initResult.name ? (this.state.initResult.name).toString() : ''
				                  })(
						              <Input placeholder='请输入班级名称' className='pageInpHeight'
						              />
				                 )}
			                  </FormItem>
						</Row>
					</div>
					<div className='pageRight f-pl5 f-pt5' style={{width:'70%'}}>
						<Row className='f-mb6'>
							<Col span={6}>
								<div className='f-fz4 f-dark f-mb2'>收费模式</div>
								<FormItem label="" {...formItemLayout}>
						                {getFieldDecorator('chargeMode', {
								        	initialValue:this.state.isFree
								    	})(
								    		<Radio checked><i className='f-gray f-fz3'>按期</i></Radio>
								    	)}
					        		</FormItem>
							</Col>
							<Col span={6}  style={{paddingRight:"40px"}}>
								<div className='f-fz4 f-dark f-mb2 label'>学费标准</div>
									<Row>
										<Col span={16}>
										<FormItem {...formItemLayout} label="">
						                  {getFieldDecorator('salePrice', {
						                  	rules: [{
						                  		required: true,
								              	message: '请输入学费标准',
						                  	},{
						                  	}],
						                  	initialValue:this.state.salePrice ? (this.state.salePrice).toString() : ''
						                  })(
						                  	<Input placeholder='请输入'
						                  	onChange= {
													(e)=> {
														this.ksTotalM(e);
													}
												}
						                  	className='pageInpHeight'/>
						                  )}
					                  </FormItem>
					                  </Col>
					                  <Col span={8} className='f-pale f-fz3 f-pl3'>
					                  	元／期
					                  </Col>
				                  </Row>
							</Col>
							
							<Col span={6}  style={{paddingRight:"40px"}}>
								<div className='f-fz4 f-dark f-mb2 label'>课时总计</div>
									<Row>
										<Col span={16}>
										<FormItem {...formItemLayout} label="" >
						                  {getFieldDecorator('totalCoursePeriod', {
						                  	rules: [
						                  	{
						                  		required: true,
								              	message: '请输入课时总计',
						                  	},
						                  	
						                  	],
						                  	initialValue:this.state.totalCoursePeriod ? (this.state.totalCoursePeriod).toString() : null
						                  })(
						                  	<Input placeholder='请输入' className='pageInpHeight' 
						                  		onChange= {
													(e)=> {
														this.ksTotal1(e);
													}
												}
						                  		onBlur={this.ksTotal.bind(this)} />
						                  )}
					                  </FormItem>
					                  </Col>
					                  <Col span={8} className='f-pale f-fz3 f-pl3'>
					                  	课时
					                  </Col>
				                  </Row>
				                  <Row>
				                  	<Col span={24}>
				                  		<div className='f-pale f-fz2 f-mb1'>如果每节课包含1课时，则课时总计等于上课次数</div>
				                  	</Col>
				                  </Row>
							</Col>
							
							<Col span={6}  style={{paddingRight:"40px"}}>
								<div className='f-fz4 f-dark f-mb2'>每次上课</div>
									<Row>
										<Col span={16}>
										<FormItem {...formItemLayout}>
						                  {getFieldDecorator('consumeCoursePeriod', {
						                  	initialValue:this.state.initResult.consumeCoursePeriod?this.state.initResult.consumeCoursePeriod:[]
						                  })(
						                  	<Select placeholder='请选择'
						                  		style={{width:'100%',background: '#F3F3F8',borderRadius: '4px',border:'none'}} >
		                   						<Option value='1'>1</Option>
		                   						<Option value='2'>2</Option>
		                   						<Option value='3'>3</Option>
		                   						<Option value='4'>4</Option>
		                   						<Option value='5'>5</Option>
		                   						<Option value='6'>6</Option>
		                   						<Option value='7'>7</Option>
		                   						<Option value='8'>8</Option>
		                   						<Option value='9'>9</Option>
		                   						<Option value='10'>10</Option>
		                   					</Select>
						                  )}
					                  </FormItem>
					                  </Col>
					                  <Col span={8} className='f-pale f-fz3 f-pl3'>
					                  	课时
					                  </Col>
				                  </Row>
				                  <Row>
				                  	<Col span={24}>
				                  		<div className='f-pale f-fz2 f-mb1'>创建排课计划默认消耗的课时</div>
				                  	</Col>
				                  </Row>
							</Col>
						</Row>
						
						<Row className='f-mb6'>
							<Col span={12} style={{paddingRight:"40px"}}>
								<div className='f-fz4 f-dark f-mb2 label'>开班时间</div>
								<Row>
									<Col span={11}>
										<FormItem {...formItemLayout}>
								         {getFieldDecorator('classStartTime',{
								         	rules:[{
								         		required: true,
								            		message: '请选择开始时间!',
								         	}],
						                  	initialValue:this.state.initResult.classStartTime? moment(this.state.initResult.classStartTime, dateFormat):null,
								         })(
							    			 	<DatePicker disabledDate={this.disabledStartDate.bind(this)} format="YYYY-MM-DD HH:mm:ss"
							    			 		disabled={this.state.editPage?true:false}
									          setFieldsValue={this.state.startValue} placeholder="开始时间" showTime onChange={this.onStartChange.bind(this)}
									          onOpenChange={this.handleStartOpenChange.bind(this)}
								    			 />
						    			 	)}
									     </FormItem>
							    			 </Col>
							    			 <Col span={2} className='f-align-center f-h4-lh4'>--</Col>
							    			  <Col span={11}>
							    			  	<FormItem {...formItemLayout}>
									         {getFieldDecorator('classEndTime',{
									         	rules:[{
									         		required: true,
									            		message: '请选择结束时间!',
									         	}],
						                  		initialValue:this.state.initResult.classEndTime? moment(this.state.initResult.classEndTime, dateFormat):null,
									         })(
										        <DatePicker disabledDate={this.disabledEndDate.bind(this)} showTime format="YYYY-MM-DD HH:mm:ss"
										        disabled={this.state.editPage?true:false}
										          setFieldsValue={this.state.endValue} placeholder="结束时间"  onChange={this.onEndChange.bind(this)}
										          open={this.state.endOpen} onOpenChange={this.handleEndOpenChange.bind(this)}
										          />
										        )}
									     </FormItem>
							        </Col>
			                  	</Row>
							</Col>
							<Col span={6} style={{paddingRight:"40px"}}>
								<div className='f-fz4 f-dark f-mb2'>班主任</div>
									<FormItem {...formItemLayout}>
						                  {getFieldDecorator('headmasterId', {
						                  	initialValue:this.state.initResult.headmasterId?(this.state.initResult.headmasterId).toString():'',
						                  })(
						                  	 <Select
						                  	  showSearch
						                  	  allowClear
									          placeholder='请选择班主任'
									          style={{ width: '100%' }}
									          onFocus={
									          	()=> {
									          		var schoolAreaId = schoolAreaId || this.state.selectedSchoolAreaId || this.state.initResult.schoolAreaId || '';
									          		if(!schoolAreaId) {
									          			message.error('请先选择校区');
									          		};
									          	}
									          }
									          optionFilterProp="children"
	                        					filterOption={
	                        						(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
	                        					}
									        >
												{this.state.masterInfo?
								                        	this.state.masterInfo.map((item,i)=> {
								                        		return(<Option key={item.id} value={item.id}>{ item.name}</Option>)
								                        	})
								                        	:null
							                      }
									        </Select>
						                   )}
					                </FormItem>
							</Col>
							<Col span={6} style={{paddingRight:"40px"}}>
								<div className='f-fz4 f-dark f-mb2 label'>上课老师</div>
									<FormItem {...formItemLayout} label="">
					                  {getFieldDecorator('teacherId', {
					                  	rules: [
						                  	{
						                  		required: true,
									            message: '请先选择上课老师',
						                  	}
						                 ],
					                  	initialValue:this.state.initResult.teacherId ? (this.state.initResult.teacherId).toString() : []
					                  })(
					                  	<Select
						                  	  showSearch
						                  	  allowClear
									          placeholder='请选择上课老师'
									          style={{ width: '100%' }}
									          onFocus={
									          	()=> {
									          		var schoolAreaId = schoolAreaId || this.state.selectedSchoolAreaId || this.state.initResult.schoolAreaId || '';
									          		if(!schoolAreaId) {
									          			message.error('请先选择校区');
									          		};
									          	}
									          }
									          optionFilterProp="children"
	                        					filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
									        >
												{this.state.masterInfo?
								                        	this.state.masterInfo.map((item,i)=> {
								                        		return(<Option key={item.id} value={item.id}>{ item.name}</Option>)
								                        	})
								                        	:null
							                      }
									        </Select>
					                  )}
				                  </FormItem>
							</Col>
						</Row>
						<Row className='f-mb7'>
							<Col span={6} style={{paddingRight:"40px"}}>
								<div className='f-fz4 f-dark f-mb2'>教室名称</div>
								<Row className='classroomName'>
									<FormItem {...formItemLayout}>
					                  {getFieldDecorator('defaultRoomId', {
					                  	initialValue:this.state.initResult.defaultRoomId ? (this.state.initResult.defaultRoomId).toString() : ''
					                  })(
					                  	<Select showSearch allowClear placeholder="请选择" optionFilterProp="children"
					                  		onFocus={this._getClssRoom.bind(this)}
					                  		onChange={this._getClssRoom1.bind(this)}
					                        filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}>
						                      {this.state.classroomList?
							                        	this.state.classroomList.map((item,i)=> {
							                        		return(<Option key={item.id} value={item.id}>{ item.name }</Option>)
							                        	})
							                        	:null
						                      }
					                      </Select>
					                  )}
					                </FormItem>
			                  	</Row>
							</Col>
							<Col span={6} style={{paddingRight:"40px"}}>
								<div className='f-fz4 f-dark f-mb2 label'>额定人数</div>
									<FormItem {...formItemLayout} label="">
					                  {getFieldDecorator('rated', {
					                  	rules: [
					                  	{
					                  		required: true,
								            message: '请输入额定人数',
					                  	},
					                  	],
					                  	initialValue:this.state.initResult.rated ? (this.state.initResult.rated).toString() : []
					                  })(
					                  	<Input placeholder='请输入'
					                  		onChange= {
													(e)=> {
														this.ksTotal1(e);
													}
												}
					                  	className='pageInpHeight'/>
					                  )}
				                  </FormItem>
							</Col>
							<Col span={6} style={{paddingRight:"40px"}}>
								<div className='f-fz4 f-dark f-mb2'>助教</div>
									<FormItem {...formItemLayout} label="">
					                  {getFieldDecorator('taTeacherId', {
					                  	initialValue:this.state.initResult.taTeacherId ? (this.state.initResult.taTeacherId).toString() : ''
					                  })(
					                  	<Select
						                  	  showSearch
						                  	  allowClear
									          placeholder='请选择助教'
									          style={{ width: '100%' }}
									          optionFilterProp="children"
	                        					filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
									        >
												{this.state.masterInfo?
								                        	this.state.masterInfo.map((item,i)=> {
								                        		return(<Option key={item.id} value={item.id}>{ item.name}</Option>)
								                        	})
								                        	:null
							                      }
									        </Select>
					                  )}
				                  </FormItem>
							</Col>
						</Row>
						<Row className='f-mb4'>
							<Col span={6}>
								<i className='f-fz4 f-dark f-mb3' style={{marginRight:'30px'}}>上课时间</i>
		                   		<Button type="primary jxadd" onClick={this.showModalJX}>继续添加</Button>
		                   		<Modal
						          title='设置排课'
						          visible={this.state.visible}
						          onOk={this._modalConfirm.bind(this)}
						          onCancel={this.handleCancel}
						          maskClosable={false}
						          width='750px'
						          >
		                   			<div className='f-pdl5 f-pdr5 f-bg-white classListPage'>
		                   				<Row className='pkstart'>
		                   					<Col span={5} className='f-fz4' style={{color:'#4A4A4A'}}><i className='modalLabelTip'>*</i>排课时段：</Col>
		                   					<Col span={19}>
		                   						<Col span={11} className='width100 modalSS'>
													<FormItem {...formItemLayout}>
												         {getFieldDecorator('startDate',{
												         	initialValue:this.state.tableStart?moment(this.state.tableStart, dateFormat):null
												         })(
											    			 	<DatePicker disabledDate={this.disabledStartDate1.bind(this)}
													          format="YYYY-MM-DD" setFieldsValue={this.state.startValue1}
													          placeholder="开始时间" onChange={this.onStartChange1.bind(this)}
													          onOpenChange={this.handleStartOpenChange1.bind(this)}
													          />
										    			 	)}
												     </FormItem>
										    			 </Col>
										    			 <Col span={2} className='f-align-center'>--</Col>
										    			  <Col span={11} className='width100'>
										    			  	<FormItem {...formItemLayout}>
												         {getFieldDecorator('endDate',{
												         	initialValue:this.state.tableEnd?moment(this.state.tableEnd, dateFormat):null
												         })(
													        <DatePicker disabledDate={this.disabledEndDate1.bind(this)}
													          format="YYYY-MM-DD" setFieldsValue={this.state.endValue1}
													          placeholder="结束时间" onChange={this.onEndChange1.bind(this)}
													          open={this.state.endOpen1} onOpenChange={this.handleEndOpenChange1.bind(this)}
													          />
													        )}
												     </FormItem>
										        </Col>
		                   					</Col>
		                   				</Row>
		                   				<Row style={{borderBottom: '1px solid #C7C7C7'}} className='f-pb4 pkstart'>
		                   					<Col span={5} className='f-fz4' style={{color:'#4A4A4A'}}><i className='modalLabelTip'>*</i>计划排：</Col>
		                   					<Col span={8}>
		                   						<FormItem>
											         {getFieldDecorator('jhkc',{
											         	initialValue:this.state.jhkc?this.state.jhkc:''
											         })(
												        <Input placeholder='请输入所排课次' onChange={this.jhkc.bind(this)}/>
												        )}
											     </FormItem>
		                   					</Col>
		                   					<Col span={4} className='f-align-left f-ml1'>
											     课次
		                   					</Col>
		                   				</Row>
		                   				<Row className='f-mt5'>
		                   					<Col span={4}>
		                   						<input type='checkbox' style={{
		                   							border: '1px solid #D9D9D9',
													borderRadius: '2px',
													width:'18px',
													height:'18px'
		                   						}} onClick={this.workingDay.bind(this)}/>
		                   						<i className='f-fz3 f-mr3' style={{color: '#4A4A4A'}}>工作日</i>
		                   					</Col>
		                   					<Col span={4}>
		                   						<input type='checkbox' style={{
		                   							border: '1px solid #D9D9D9',
													borderRadius: '2px',
													width:'18px',
													height:'18px'
		                   						}} onClick={this.weekend.bind(this)}/>
		                   						<i className='f-fz3' style={{color: '#4A4A4A'}}>周末</i>
		                   					</Col>
		                   				</Row>
		                   				
		                   				<Row className='f-mt5'>
		                   					<Col span={4}>
		                   						<input type='checkbox' className='f-mr2' style={{
		                   							border: '1px solid #D9D9D9',
													borderRadius: '2px',
													width:'18px',
													height:'18px'
		                   						}} className='workingDay weekday work' data-id='1'/>
		                   						<i className='f-fz3 f-mr3' style={{color: '#4A4A4A'}}>星期一</i>
		                   					</Col>
		                   					<Col span={4}>
		                   						<input type='checkbox' className='f-mr2' style={{
		                   							border: '1px solid #D9D9D9',
													borderRadius: '2px',
													width:'18px',
													height:'18px'
		                   						}} className='workingDay weekday work' data-id='2'/>
		                   						<i className='f-fz3 f-mr3' style={{color: '#4A4A4A'}}>星期二</i>
		                   					</Col>
		                   					<Col span={4}>
		                   						<input type='checkbox' className='f-mr2' style={{
		                   							border: '1px solid #D9D9D9',
													borderRadius: '2px',
													width:'18px',
													height:'18px'
		                   						}} className='workingDay weekday work' data-id='3'/>
		                   						<i className='f-fz3 f-mr3' style={{color: '#4A4A4A'}}>星期三</i>
		                   					</Col>
		                   					<Col span={4}>
		                   						<input type='checkbox' className='f-mr2' style={{
		                   							border: '1px solid #D9D9D9',
													borderRadius: '2px',
													width:'18px',
													height:'18px'
		                   						}} className='workingDay weekday work' data-id='4'/>
		                   						<i className='f-fz3 f-mr3' style={{color: '#4A4A4A'}}>星期四</i>
		                   					</Col>
		                   					<Col span={4}>
		                   						<input type='checkbox' className='f-mr2' style={{
		                   							border: '1px solid #D9D9D9',
													borderRadius: '2px',
													width:'18px',
													height:'18px'
		                   						}} className='workingDay weekday work' data-id='5'/>
		                   						<i className='f-fz3' style={{color: '#4A4A4A'}}>星期五</i>
		                   					</Col>
		                   				</Row>
		                   				
		                   				<Row className='f-mt4 f-pb4' style={{borderBottom: '1px solid #C7C7C7'}}>
		                   					<Col span={4}>
		                   						<input type='checkbox' className='f-mr2' style={{
		                   							border: '1px solid #D9D9D9',
													borderRadius: '2px',
													width:'18px',
													height:'18px'
		                   						}} className='weekend weekday work' data-id='6'/>
		                   						<i className='f-fz3 f-mr3' style={{color: '#4A4A4A'}}>星期六</i>
		                   					</Col>
		                   					<Col span={4}>
		                   						<input type='checkbox' className='f-mr2' style={{
		                   							border: '1px solid #D9D9D9',
													borderRadius: '2px',
													width:'18px',
													height:'18px'
		                   						}} className='weekend weekday work' data-id='7'/>
		                   						<i className='f-fz3' style={{color: '#4A4A4A'}}>星期日</i>
		                   					</Col>
		                   				</Row>
		                   				<div className='f-mt5 f-relative f-pb4' style={{borderBottom: '1px solid #C7C7C7'}}>
										    	<Button type="primary" style={{height:'40px',left:0,top:0}} className='absolute commonButton' onClick={this.commonInterval}>常用时间段<i className='iconfont icon-fa-sort' style={{marginLeft:'40px'}}></i></Button>
											<ul className='f-absolute f-align-left f-pl4 commonInterval f-radius1 f-box-shadow1' style={{top:'40px',left:0,width:'200px',display:'none'}}>
												<li className='f-fz3'>08:00-10:00</li>
												<li>08:00-11:00</li>
												<li>08:30-10:00</li>
												<li>08:30-11:30</li>
												<li>10:00-12:00</li>
												<li>12:00-15:00</li>
												<li>13:00-16:00</li>
												<li>14:00-16:00</li>
												<li>15:00-18:00</li>
												<li>15:20-16:50</li>
												<li>16:00-18:00</li>
												<li>16:30-18:30</li>
												<li>18:00-20:00</li>
												<li>18:30-20:00</li>
											</ul>
											{/*<div className='f-mt1'>
												{self.state.classRoomUsableTimeList?<div>教室可用时间:{self.state.classRoomUsableTimeList1}</div>:""}
												{self.state.teacherUsableTimeList?<div>上课老师可用时间:{self.state.teacherUsableTimeList1}</div>:''}
											</div>*/}
										</div>
		                   			</div>
		                   			<Row className='f-mt4'>
		                   				<Col span={12} className='f-fz3 f-black'>
		                   					<i className='f-mr1'><i className='modalLabelTip'>*</i>开始时间：</i>
							        	         <select style={{width:'96px',height:'30px',background: '#F3F3F8',borderRadius: '4px',border:'none'}} className='f-mr1 modalStartTime timeInter f-create-select' 
			                   						 onChange={this._changeTime.bind(this)}>
			                   						<option value='快速选择'>快速选择</option>
			                   						<option value='08:00'>08:00</option>
			                   						<option value='08:30'>08:30</option>
			                   						<option value='10:00'>10:00</option>
			                   						<option value='12:00'>12:00</option>
			                   						<option value='13:00'>13:00</option>
			                   						<option value='14:00'>14:00</option>
			                   						<option value='15:00'>15:00</option>
			                   						<option value='15:20'>15:20</option>
			                   						<option value='15:30'>15:30</option>
			                   						<option value='16:00'>16:00</option>
			                   						<option value='16:30'>16:30</option>
			                   						<option value='18:00'>18:00</option>
			                   						<option value='18:30'>18:30</option>
			                   					</select>
		                   				<input defaultValue={this.state.startTimeLeft} style={{width:'46px',border:'none',borderBottom: '1px solid #979797'}} className='f-mr1 startTimeLeft' onChange={this.startTimeLeft.bind(this)} onBlur={this.timeBlur.bind(this)}/>:
		                   				<input defaultValue={this.state.startTimeRight} style={{width:'46px',border:'none',borderBottom: '1px solid #979797'}} className='f-mr1 startTimeRight' onChange={this.startTimeRight.bind(this)} onBlur={this.timeBlur.bind(this)}/>
		                   				</Col>
		                   				<Col span={12} className='f-fz3 f-black'>
		                   					<i className='f-mr1'><i className='modalLabelTip'>*</i>结束时间：</i>
		                   					<select style={{width:'96px',height:'30px',background: '#F3F3F8',borderRadius: '4px',border:'none'}} className='f-mr1 modalEndTime timeInter f-create-select' 
		                   						onChange={this._changeTime.bind(this)}>
		                   						<option value='快速选择'>快速选择</option>
		                   						<option value='10:00'>10:00</option>
		                   						<option value='11:00'>11:00</option>
		                   						<option value='11:30'>11:30</option>
		                   						<option value='12:00'>12:00</option>
		                   						<option value='14:30'>14:30</option>
		                   						<option value='15:00'>15:00</option>
		                   						<option value='16:00'>16:00</option>
		                   						<option value='16:50'>16:50</option>
		                   						<option value='17:00'>17:00</option>
		                   						<option value='18:00'>18:00</option>
		                   						<option value='18:30'>18:30</option>
		                   						<option value='20:00'>20:00</option>
		                   					</select>
		                   					<input  defaultValue={this.state.endTimeLeft} style={{width:'46px',border:'none',borderBottom: '1px solid #979797'}} className='f-mr1 endTimeLeft' onChange={this.startTimeLeft.bind(this)} onBlur={this.timeBlur.bind(this)}/>:
		                   					<input defaultValue={this.state.endTimeRight} style={{width:'46px',border:'none',borderBottom: '1px solid #979797'}} className='f-mr1 endTimeRight' onChange={this.startTimeRight.bind(this)} onBlur={this.timeBlur.bind(this)}/>
		                   				</Col>
		                   			</Row>
		                   			<Row className='f-mt4'>
		                   				<Col span={12} className='f-fz3 f-black'>
		                   					<i className='f-mr1'><i className='modalLabelTip'>*</i>消耗课时：</i>
		                   					<select style={{width:'96px',height:'30px',background: '#F3F3F8',borderRadius: '4px',border:'none'}} className='f-mr1 timeInter consume xhPeriod f-create-select' onChange={this.changeModal.bind(this)} value={this.state.modalHour?this.state.modalHour:period}>
		                   						<option value='1'>1</option>
		                   						<option value='2'>2</option>
		                   						<option value='3'>3</option>
		                   						<option value='4'>4</option>
		                   						<option value='5'>5</option>
		                   						<option value='6'>6</option>
		                   						<option value='7'>7</option>
		                   						<option value='8'>8</option>
		                   						<option value='9'>9</option>
		                   						<option value='10'>10</option>
		                   					</select>
		                   					课时
		                   				</Col>
		                   				<Col span={12} className='f-fz3 f-black'>
		                   					<i className='f-mr1'><i className='modalLabelTip'>*</i>上课教室：</i>
		                   					<input style={{width:'96px',height:'30px',background: '#F3F3F8',borderRadius: '4px',border:'none'}} disabled
		                   						 readOnly className='f-mr1 consume attendClassroom timeInter startTime f-align-center' data-id=''/>
		                   					<Button type="primary" style={{borderRadius:"100px"}} onClick={this._classroom.bind(this)}>查看教室占用情况</Button>
		                   				</Col>
		                   			</Row>
		                   			
		                   			<Row className='f-mt4 f-pb4' style={{borderBottom: '1px solid #C7C7C7'}}>
		                   				<Col span={12} className='f-fz3 f-black'>
		                   					<i className='f-mr1'><i className='modalLabelTip'>*</i>上课老师：</i>
		                   					<input style={{width:'96px',height:'30px',background: '#F3F3F8',borderRadius: '4px',border:'none'}} readOnly disabled
		                   						className='timeInter consume attendTeacher f-mr1 startTime f-align-center'/>
		                   					<Button type="primary" style={{borderRadius:"100px"}} onClick={this._teacher.bind(this)}>查看老师上课情况</Button>
		                   				</Col>
		                   				<Col span={12} className='f-fz3 f-black'>
		                   					<i style={{marginRight:'32px'}}>&emsp;助教：</i>
		                   					<select style={{width:'96px',height:'30px',background: '#F3F3F8',borderRadius: '4px',border:'none'}} 
		                   						className='f-mr1 timeInter assiantVal f-create-select'
		                   						onChange={this._changeAssiant.bind(this)}
		                   						>
		                   						<option key='请选择' value='请选择'>
		                   							请选择
							                  	</option>
		                   					{ this.state.masterInfo.map((elem, index) => {
							                  return(
							                  	<option key={elem.id} value={elem.id}>
							                  	{elem.name}
							                  	</option>
							                  )
							                }) 
										  }
		                   					</select>
		                   				</Col>
		                   			</Row>
		                   			
		                   			<Row className='f-mt4'>
		                   				<Col span={20}>
		                   					<input type='checkbox' className='f-mr1 checkboxInp' checked={this.state.holidayState?1:0}  onClick={this.holidayState.bind(this)}/><i className='f-fz3 f-mr1 isDay' style={{color:'#4A4A4A'}}>跳过节假日</i>
		                   				</Col>
		                   			</Row>
		                   			
		                   		</Modal>
		                   		 <Modal
						          title={this.state.modalTitle}
						          visible={this.state.visible1}
						          onOk={this.handleOk1}
						          onCancel={this.handleCancel1}
						          width='900px'
						          maskClosable={false}
						          footer={[
						            <Button key="back" size="large" onClick={this.handleCancel1}>取消</Button>,
						            <Button key="submit" type="primary" className='modalButton2' size="large" onClick={this.handleOk1.bind(this)}>
						             确定
						            </Button>,
						          ]}
						        >
						          <Row className='width100'>
									<Col span={14}>
									<Col span={5} className='f-align-right f-fz4 f-black f-h4-lh4 label f-label'>选择日期:&emsp;</Col>
						        		<Col span={19} >
							    			 <Col span={11}>
							    			 <FormItem>
								          {getFieldDecorator('startDateM',{
									          initialValue:this.state.startValue1?moment(this.state.startValue1, dateFormat):null
								          })(
								            	<DatePicker
									          disabledDate={this.disabledStartDateM.bind(this)}
									          format="YYYY-MM-DD"
									          setFieldsValue={this.state.startValueM}
									          placeholder="开始日期"
									          onChange={this.onStartChangeM.bind(this)}
									          onOpenChange={this.handleStartOpenChangeM.bind(this)}/>
								            	)}
								     	</FormItem>
							    			 </Col>
							    			 <Col span={2} className='f-align-center f-h4-lh4'>--</Col>
							    			  <Col span={11}>
							    			  <FormItem>
								          {getFieldDecorator('endDateM',{
									          initialValue:this.state.endValue1?moment(this.state.endValue1, dateFormat):null
								          })(
								            <DatePicker
									          disabledDate={this.disabledEndDateM.bind(this)}
									          format="YYYY-MM-DD"
									          setFieldsValue={this.state.endValueM}
									          placeholder="结束日期"
									          onChange={this.onEndChangeM.bind(this)}
									          open={this.state.endOpenM}
									          onOpenChange={this.handleEndOpenChangeM.bind(this)}/>
								            )}
								     	</FormItem>
								         	</Col>
								        	</Col>
									</Col>
									<Col span={10}>
										<Col span={8} className='f-align-right f-fz4 f-black f-h4-lh4 f-label'>请选择校区:&emsp;</Col>
						        			<Col span={16} >
											<FormItem {...formItemLayout}>
									          {getFieldDecorator('schoolAreaIdM', {
									          	initialValue:this.state.schoolAreaIdM
									          })(
									            <Select
							                        showSearch
							                        disabled
							                        placeholder="选择校区"
							                        optionFilterProp="children"
							                        filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
							                      	onChange={this._changeSchool.bind(this)}
							                      >
							                      {this.state.allSchool?
								                        	this.state.allSchool.map((item,i)=> {
								                        		return(<Option key={item.id} value={item.id}>{ item.name }</Option>)
								                        	})
							                        :null
							                     }
				                      </Select> 
									          )}
									        </FormItem>
									     </Col>
									</Col>
								  </Row>
								  <Row className='width100'>
									<Col span={14}>
									<Col span={5} className='f-align-right f-fz4 f-black f-h4-lh4 f-label'>选择时段:&emsp;</Col>
						        		<Col span={19} >
							    			 <Col span={11}>
							    			 <FormItem>
								          {getFieldDecorator('startDateQ',{
								          })(
								            	 <TimePicker defaultOpenValue={moment('00:00:00', 'HH:mm:ss')} />,
								            	)}
								     	</FormItem>
							    			 </Col>
							    			 <Col span={2} className='f-align-center f-h4-lh4'>--</Col>
							    			  <Col span={11}>
							    			  <FormItem>
								          {getFieldDecorator('endDateQ',{
								          })(
								            	 <TimePicker defaultOpenValue={moment('00:00:00', 'HH:mm:ss')}/>,
								            )}
								     	</FormItem>
								         	</Col>
								        	</Col>
									</Col>
									{/*{this.state.modalTitle=='教室空闲时间查询'?
									<Col span={10}>
										<Col span={8} className='f-align-right f-fz4 f-black f-h4-lh4 f-label'>教室:&emsp;</Col>
							        			<Col span={16} className='modalClassC'>
												<FormItem {...formItemLayout}>
								                  {getFieldDecorator('modalSecRoomId', {
								                  })(
								                  	<Select showSearch allowClear placeholder="请选择" optionFilterProp="children"
								                        filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}>
									                      {this.state.classroomList?
										                        	this.state.classroomList.map((item,i)=> {
										                        		return(<Option key={item.id} value={item.id}>{ item.name }</Option>)
										                        	})
										                        	:null
									                      }
								                      </Select>
								                  )}
								                </FormItem>
									     </Col>
									   </Col>:
									   <Col span={10}>
									     <Col span={8} className='f-align-right f-fz4 f-black f-h4-lh4 f-label'>老师:&emsp;</Col>
							        			<Col span={16} >
												<FormItem {...formItemLayout} label="">
									                  {getFieldDecorator('modalSecTeacherId', {
									                  	initialValue:this.state.initResult.teacherId ? (this.state.initResult.teacherId).toString() : []
									                  })(
									                  	<Select
									                  	  showSearch
									                  	  allowClear
												          placeholder='请选择老师'
												          style={{ width: '100%' }}
												          optionFilterProp="children"
				                        					filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
												        >
															{this.state.masterInfo?
											                        	this.state.masterInfo.map((item,i)=> {
											                        		return(<Option key={item.id} value={item.id}>{ item.name}</Option>)
											                        	})
											                        	:null
										                      }
												        </Select>
									                  )}
				                  			</FormItem>
									     </Col>
									   </Col>
									}*/}
									<Col span={10} className='zdschool'>
										<Col span={8} className='f-align-right f-fz4 f-black f-h4-lh4'>星期:&emsp;</Col>
							        			<Col span={16} className='modalClassC'>
												<FormItem {...formItemLayout}>
								                  {getFieldDecorator('modalWeek', {
								                  })(
								                  	<Select
												    mode="multiple"
												    style={{ width: '100%' }}
												    placeholder="请选择"
												    onChange={zdschoolChange}
												  >
												    <Option key={1} value='1'>星期一</Option>
												    <Option key={2} value='2'>星期二</Option>
												    <Option key={3} value='3'>星期三</Option>
												    <Option key={4} value='4'>星期四</Option>
												    <Option key={5} value='5'>星期五</Option>
												    <Option key={6} value='6'>星期六</Option>
												    <Option key={7} value='7'>星期日</Option>
												  </Select>
								                  )}
								                </FormItem>
									     </Col>
									   </Col>
								  </Row>
								  <Row style={{borderBottom: '1px solid #C7C7C7'}} className='f-pb3'>
								  	<Col span={10} className='f-align-right f-pr4'>
							        			<Button style={{border: '1px solid #999999',borderRadius: '4px',width:'110px',height:'40px'}}  onClick={this.modalReset.bind(this)}>
							        				<i className='iconfont icon-reset f-mr2'></i>重置
							        			</Button>
							        		</Col>
							        		<Col span={12} className='f-align-left'>
							        			<Button type="primary" style={{borderRadius: '4px',width:'110px',height:'40px'}} onClick={this.modalSearch.bind(this)}>
							        				<i className='iconfont icon-hricon33 f-mr2'></i>搜索
							        			</Button>
							        		</Col>
								  </Row>
								  {this.state.modalResult?
									  <Row className='f-mb1 f-mt1'>
									  	<i className='f-fz4 f-black'>请选择{this.state.modalTitle == '教室空闲时间查询'?'教室':'老师'}</i>
									  </Row>
									  :null
								  }
								  {this.state.modalResult?
								  	<Table columns={columns1} bordered dataSource={this.state.modalResult} pagination={false} scroll={{ y: 280 }}/>
								  	:null
								  }
						        </Modal>
							</Col>
							<Col span={6}>
								<i className='f-fz4 f-dark f-mb3'>已排课时总数：<span className='arrangeTotal'>0</span></i>
							</Col>
						</Row>
						<Row className='f-mb2 pageTable f-create-pk'>
							<Col span={23}>
								<Table columns={columns} bordered dataSource={this.state.classTimesRuleVOList} pagination={false}/>
							</Col>
						</Row>
						<Row className='f-mb4'>
							<Col span={23}>
								<div className='f-fz4 f-dark f-mb3'>备注</div>
								<FormItem
						          label=""
						          {...formItemLayout}>
						          {getFieldDecorator('desc', {
						            initialValue:this.state.desc,
						            rules:[{max:100,message: '最多输入100个字符'}]
						          })(
						          	<Input type="textarea" rows={2} placeholder='请输入备注'/>
						          )}
						        </FormItem>
							</Col>
						</Row>
						<Row>
							<Col className='f-align-right f-mb4' span={23}>
								<Button style={{height:'40px'}} className='f-mr5 f-viewdetail-check' onClick={()=>{window.location.href='/teach/classList'}}>
				        				<i className='iconfont icon-fanhui f-viewdetail-down'></i>
				        				返回
				        			</Button>
							    	<Button type="primary" style={{height:'40px'}} loading={this.state.iconLoading} onClick={this.pageSave.bind(this)} className='f-viewdetail-check'><i className='iconfont icon-queding f-viewdetail-down'></i>保存</Button>
			        			</Col>
						</Row>
					</div>
				</Form>
		</div>);
	}
}
const CreateClass = Form.create()(Main);
export {
	CreateClass
}