import React, {
	Component
} from 'react';
import store from 'store2';
import {
	Breadcrumb,
	Button,
	Select,
	Table,
	Icon,
	Modal,
	Tabs,
	Input,
	DatePicker,
	Row,
	Col,
	Form,
	Dropdown,
	Menu,
	Pagination,
	message
} from 'antd';
import '../main.less';
//使用fetch请求接口数据
import $ from 'jquery';
import {
	Loading
} from '../../common/g.js';
import {
	fpost
} from '../../common/io.js';
const TabPane = Tabs.TabPane;
const confirm = Modal.confirm;
const Option = Select.Option;
const u = require('../../common/io.js')
const FormItem = Form.Item;
let classname = {};
let isb = [];
//教务教学默认打开的首页
class Main extends Component {
	constructor(props) {
		super(props)
		this.state = {
			size: 'large',
			startValue: null,
			endValue: null,
			endOpen: false,
			className: null, //班级名称
			courseName: null, //课程名称
			headmaster: null, //班主任
			pageResult: null, //分页返回的列表总信息
			currentPage: 1, //当前页码
			pageSize: 10, //每页显示条数
			total: null, //总数
			searchState: 0,
			course: [],
			classroomList: [],
			masterList: [], //班主任列表
		}
		this.showDeleteConfirm = this.showDeleteConfirm.bind(this)
	}
	disabledStartDate = (startValue) => { //只读的开始日期
		const endValue = this.state.endValue;
		if (!startValue || !endValue) {
			return false;
		}
		return startValue.valueOf() > endValue.valueOf();
	}

	disabledEndDate = (endValue) => { //只读的结束日期
		const startValue = this.state.startValue;
		if (!endValue || !startValue) {
			return false;
		}
		return endValue.valueOf() <= startValue.valueOf();
	}

	onChange = (field, value) => {
		this.setState({
			[field]: value,
		});
	}
	//开始日期改变
	onStartChange = (value) => {
		this.onChange('startValue', value);
	}
	//结束日期改变
	onEndChange = (value) => {
		this.onChange('endValue', value);
	}

	handleStartOpenChange = (open) => {
		if (!open) {
			this.setState({
				endOpen: true
			});
		}
	}

	handleEndOpenChange = (open) => {
		this.setState({
			endOpen: open
		});
	}
	className(e) { //班级名称
		let v = e.target.value;
		this.setState({
			className: v
		});
	}

	reset() { //高级筛选里的重置
		this.props.form.resetFields();
		this.setState({
			currentPage: 1
		}, function() {
			this.advancedSearch()
		});
	}
	advancedSearch() { //筛选搜索
		this.setState({
			currentPage: 1
		}, function() {
			this._getForm(); //表单内容
		});
	}
	_getMaster = (v) => { //获取班主任下拉
		const self = this;
		fpost('/api/hr/staff/listStaffByCondForDropDown', {
				name: v ? v : '',
				type: 4
			})
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					self.setState({
						masterList: res.result
					})
				} else {
					message.error(res.message)
				}
			});
	}
	_getClassroom() { //获取教室
		const self = this;
		fpost('/api/system/schoolarea/listClassRoom ')
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					self.setState({
						classroomList: res.result
					})
				} else {
					message.error(res.message)
				}
			});
	}
	_getForm() {
		this.props.form.validateFields(
			(err, fieldsValue) => {
				fieldsValue['name'] = fieldsValue['name'] ? fieldsValue['name'] : '';
				fieldsValue['startDate'] = fieldsValue['startDate'] ? fieldsValue['startDate'].format('YYYY-MM-DD HH:mm:ss') : '';
				fieldsValue['endDate'] = fieldsValue['endDate'] ? fieldsValue['endDate'].format('YYYY-MM-DD HH:mm:ss') : "";
				fieldsValue['pageSize'] = this.state.pageSize;
				fieldsValue['currentPage'] = this.state.currentPage;
				fieldsValue['courseId'] = fieldsValue['courseId'] ? fieldsValue['courseId'] : '';
				fieldsValue['headmasterId'] = fieldsValue['headmasterId'] ? fieldsValue['headmasterId'] : '';
				fieldsValue['defaultRoomId'] = fieldsValue['defaultRoomId'] ? fieldsValue['defaultRoomId'] : "";
				fieldsValue['state'] = fieldsValue['state'] ? fieldsValue['state'] : "";

				this.getList(fieldsValue);
			}
		)
	}
	getList(fieldsValue) { //获取列表
		let self = this;
		fpost('/api/educational/class/list', fieldsValue)
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					if (res.result) {
						res.result.records.forEach((item, i) => {
							item.key = i;
						});
						self.setState({
							pageResult: res.result.records,
							total: parseInt(res.result.total)
						}, function() {
							isb = [];
							$('tbody input.checkbox').prop('checked', false);
							self._event();
						})
					}
				} else {
					message.error(res.message)
				}
			});
	}
	//是否确认删除对应的弹窗事件
	showDeleteConfirm(index) {
		let self = this;
		confirm({
			title: '确定要删除这条记录吗?',
			okText: '确定',
			okType: 'danger',
			cancelText: '取消',
			onOk() {
				self._delete(index);
			},
			onCancel() {},
		});
	}
	_delete(index) { //删除调用接口
		const self = this;
		$.ajax({
			url: u.hostname() + '/api/educational/class/delete',
			type: 'get',
			cache: false,
			headers: {
				'Accept': 'application/json, text/plain, */*',
				'Content-Type': 'application/x-www-form-urlencoded',
				'x-auth-token': store.get('sessionId'), //登录功能放开后需要加上的
			},
			data: {
				classId: index
			},
			success: function(res) {
				if (res.success == true) {
					message.success('删除成功');
					if (self.state.searchState == 0) {
						self.jqSearch();
					} else {
						self.advancedSearch();
					}
				} else {
					message.error(res.message)
				}
			}
		})
	}
	componentDidMount() {
		this._getMaster(); //班主任
		this._getClassroom();
		this.jqSearch();
		this._getCourse();
	}
	_event() {
		$('table tbody tr .checkbox').off().on('click', function() {
			$("tbody tr").each(function(i, e) {
				if ($(e).find('input').is(":checked")) {
					if ($(e).find('input').attr('data-isbn') != isb[i]) {
						//	            					使用indexOf查找某元素，确认数组中不存在这个元素时再把该元素添加至数组末尾
						if (isb.indexOf($(e).find('input').attr('data-isbn')) == -1) {
							isb.push($(e).find('input').attr('data-isbn'));
						}
					}
					$(e).find('input').attr('data-select', '')
				} else {
					for (var i = 0; i < isb.length; i++) {
						if (isb[i] == $(e).find('input').attr('data-isbn')) {
							//					        	数组从i位置开始,删除一个元素
							isb.splice(i, 1);
							break;
						}
					}
				}
			});
		});
		for (var i = 0; i < $('.nameTr').length; i++) {
			let html = '';
			let str = $('.nameTr').eq(i).html().substring(0, 8);
			if ($('.nameTr').eq(i).html().length > 8) {
				html = str + '...';
			} else {
				html = str;
			}
			$('.nameTr').eq(i).html(html);
		}
		for (var i = 0; i < $('.showshow').length; i++) {
			let ying = $('.showshow').eq(i).find('.ying').html();
			let shi = $('.showshow').eq(i).find('.shi').html();
			let wid = Math.round(shi / ying * 10000) / 100.00 + "%" + ' ' + '100%';
			if (!shi) {
				$('.showshow').eq(i).css('background-size', 0);
			} else {
				$('.showshow').eq(i).css('background-size', wid);
			}
		}
		$('.listName').css('height', $('#t_r_content .bordertop').eq(0).css('height'));
		for (var i = 0; i < $('#cl_freeze tr').length; i++) {
			var j = i + 1;
			$('#cl_freeze tr').eq(i).css('height', $('#t_r_content tr').eq(j).css('height'));
		}
	}
	_getCourse() { //获取课程
		const self = this;
		fpost('/api/system/course/listCourse')
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if (res.success == true) {
					self.setState({
						course: res.result
					})
				} else {
					message.error(res.message)
				}
			});
	}
	pageChange(pageNumber) { //列表分页获取当前页码
		this.setState({
			currentPage: pageNumber
		}, function() {
			if (!this.state.searchState) {
				classname['name'] = $.trim($('.inputBG').val());
				classname.pageSize = this.state.pageSize;
				classname.currentPage = this.state.currentPage;
				this.getList(classname);
			} else {
				this._getForm(); //表单内容
			}
		});
	}
	allSelect() { //全选
		isb = [];
		for (let i = 0; i < $('.ant-table-fixed-left table tbody tr td.select').length; i++) {
			$('.ant-table-fixed-left table tbody tr').eq(i).find('input.checkbox').prop('checked', true);
			isb.push($('.ant-table-fixed-left table tbody tr td.select').eq(i).find('input.checkbox').attr('data-isbn'));
		}
	}
	cancelAllselect() { //取消全选
		isb = [];
		for (let i = 0; i < $('.ant-table-fixed-left table tbody tr td.select').length; i++) {
			$('.ant-table-fixed-left table tbody tr td.select').eq(i).find('input.checkbox').prop('checked', false);
		}
	}
	onShowSizeChange(current, pageSize) {
		this.setState({
			currentPage: current,
			pageSize: pageSize
		}, function() {
			if (this.state.searchState == 0) {
				this.jqSearch();
			} else {
				this.advancedSearch()
			}
		})
	}
	jqSearch() { //精确搜索
		this.setState({
			currentPage: 1,
		}, function() {
			classname['name'] = $.trim($('.inputBG').val());
			classname.pageSize = this.state.pageSize;
			classname.currentPage = this.state.currentPage;
			this.getList(classname);
		});
	}
	_export() { //批量导出
		if (isb.length == 0) {
			message.warning('请至少选择一个班级');
			return false;
		} else {
			$('._export').attr('href', u.hostname() + '/api/educational/class/exportClassInfoAndMembers?classIds=' + isb.toString() + '&x-auth-token=' + store.get('sessionId'))
		}
	}
	callback(key) {
		if (key == 2) {
			this.setState({
				searchState: 1
			})
		} else {
			this.setState({
				searchState: 0
			})
		}
	}
	render() {
		const columns = [{
			title: '选择',
			key: '0',
			fixed: 'left',
			width: 80,
			className: 'f-align-center select',
			render: (text, record, index) => {
				return (
					<span className='f-relative' style={{width:'14px',height:'14px',display:'block',left:'45%',backgroundColor: '#fff'}}>
						<input name={record.id} type="checkbox" style={{left:0,top:0,backgroundColor: '#fff'}} className="check checkbox f-absolute hightC_white" data-rel='1' data-id={record.id} data-isbn={record.id} data-select='1'/>
					</span>
				)
			}
		}, {
			title: '班级名称',
			key: '1',
			width: 200,
			className: 'f-align-center',
			render: (text, record) => {
				return (
					<div className='courseName f-line1 f-blue f-pointer nameTr' title={record.name} onClick={()=>{window.location.href='/teach/classMember?id='+record.id}}>{record.name}</div>
				)
			}
		}, {
			title: '上课人数',
			key: '2',
			width: 120,
			className: 'f-align-center',
			render: (text, record, index) => {
				return (
					<div className='f-relative' style={{width:'100px',height:'14px',lineHeight:'14px',margin:'0px auto 0px 8px !important',background: '#DDDDDD',borderRadius: '8px'}}>
			  			<div className='f-absolute showshow f-white' style={{
				  			width:'100px',
				  			height:'14px',
				  			borderRadius: '8px',
				  			background: 'url("http://img.fancyedu.com/sys/ic/operation/1509585093744_newClassBg.png") 0% 0% / 100% no-repeat',
							backgroundSize:'0',
				  			top:0}}><i className='shi'>{record.registrationNum}</i>/<i className='ying'>{record.rated}</i>
				  		</div>
			  		</div>
				)
			}
		}, {
			title: '班主任',
			dataIndex: 'headmasterName',
			key: '3',
			width: 150,
			className: 'f-align-center',
		}, {
			title: '课程',
			key: '4',
			width: 180,
			className: 'f-align-center',
			render: (text, record) => {
				this._event();
				return (
					<div title={record.courseName} className='f-line1' style={{width:'180px'}}>
			   			<i className='iconfont icon-shu f-mr2'></i>
			   			<i className='courseName f-line1'>{record.courseName}</i>
			   		</div>
				)
			}
		}, {
			title: '开班日期',
			dataIndex: 'classStartTime',
			key: '5',
			width: 170,
			className: 'f-align-center',
		}, {
			title: '上课时段',
			key: '6',
			width: 150,
			className: 'f-align-center timeSD',
			render: (text, record, index) => {
				this._event();
				return (<div title={record.classStartTimeList?record.classStartTimeList:''}>
					{record.classStartTimeList?record.classStartTimeList[0]?record.classStartTimeList[0]:'':''}<br/>
					{record.classStartTimeList?record.classStartTimeList[1]?record.classStartTimeList[1]:'':''}<br/>
					{record.classStartTimeList?record.classStartTimeList[2]?record.classStartTimeList[2]:'':''}<br/>
					{record.classStartTimeList?record.classStartTimeList[3]?'......':'':''}
		            </div>);
			}
		}, {
			title: '校区',
			key: '7',
			className: 'f-align-center',
			width: 150,
			dataIndex: 'schoolAreaName'
		}, {
			title: '班级状态',
			key: '8',
			className: 'f-align-center',
			width: 150,
			render: (text, record, index) => {
				return (<div>
						{record.state==1?
							'已排课未开课':null
						}
						{record.state==2?
							'已开课':null
						}
						{record.state==3?
							'已结课':null
						}
		            </div>);
			}
		}, {
			title: '教室',
			key: '9',
			className: 'f-align-center',
			width: 150,
			dataIndex: 'classRoomName'
		}, {
			title: '学费',
			key: '10',
			width: 150,
			className: 'f-align-center',
			render: (text, record) => (
				<div>
					¥ {record.salePrice}
					{record.chargeMode==1?
						<img className='f-ml2 f-vertical-middle' src='http://img.fancyedu.com/sys/ic/operation/1512640700929_qi.png' style={{width:'30px',height:"30px"}}/>
						:null
					}
					{record.chargeMode==2?
						<img className='f-ml2 f-vertical-middle' src='http://img.fancyedu.com/sys/ic/operation/1510120850178_course.png' style={{width:'30px',height:"30px"}}/>
						:null
					}
					{record.chargeMode==3?
						<img className='f-ml2 f-vertical-middle' src='http://img.fancyedu.com/sys/ic/operation/1512640710303_shi.png' style={{width:'30px',height:"30px"}}/>
						:null
					}
				</div>
			)
		}, {
			title: '操作',
			key: '11',
			fixed: 'right',
			width: 150,
			className: 'f-align-center',
			render: (text, record, index) => {
				return (<div>
		                <i onClick={()=>{this.showDeleteConfirm(record.id)}} className='f-pointer f-mr3 iconfont icon-lajixiang f-fz5'></i>
		                <i className='iconfont icon-bianji1 f-pointer f-mr3 f-fz5' onClick={()=>{window.location.href='/teach/editClass?id='+record.id}}></i>
		            		<Dropdown overlay={
		            			 <Menu>
							    <Menu.Item>
							      <span onClick={()=>{window.location.href='/teach/viewDetails?id='+record.id}}>排课明细</span>
							    </Menu.Item>
							  </Menu>
		            			} placement="bottomRight">
		            			 <Button type="primary f-classlist-check" className="f-radius4 f-mr1">
					          查看<Icon type="down" className='f-classlist-down'/>
					        </Button>
						 </Dropdown>
		            </div>);
			}
		}];
		const {
			size
		} = this.state;
		const {
			getFieldDecorator
		} = this.props.form;
		const formItemLayout = {
			labelCol: {
				span: 6
			},
			wrapperCol: {
				span: 18
			}
		};
		return (
			<div className='classListPage wid100'>
				<Breadcrumb className="f-mb3">
				    <Breadcrumb.Item>教务教学</Breadcrumb.Item>
				    <Breadcrumb.Item>班级管理</Breadcrumb.Item>
				    <Breadcrumb.Item>
				   		<span className="f-fz5 f-bold">班级列表</span>
				    </Breadcrumb.Item>
				</Breadcrumb>
				<div className='Topselect f-bg-white f-radius1 f-box-shadow1' style={{width:'100%'}}>
					<Tabs defaultActiveKey="1" style={{textAlign:'center'}} tabPosition='top' animated={false} onChange={this.callback.bind(this)}>
						<TabPane tab={<span><i className='iconfont icon-hricon33 f-mr2'></i>精确查找</span>} key="1" style={{textAlign:'left'}}>
				    			<div style={{padding:'0 0 30px 26px'}}>
				    				<Input placeholder="请输入班级名称模糊查询" onChange={this.className.bind(this)} className='inputBG pageInpHeight f-radius1' style={{width:'352px',border:0}}/>
				    				<Button type="primary" icon="search" style={{height:'40px'}} onClick={this.jqSearch.bind(this)}>搜索</Button>
				    			</div>
				    		</TabPane>
					    <TabPane tab={<span><i className='iconfont icon-shaixuan f-mr2' ></i>高级筛选</span>} key="2">
					    		<Form style={{padding:'0 20px'}}>
					    		<Row gutter={40}>
					          <Col span={11} >
					          	 <Col span={6} className='f-fz4 f-black f-h4-lh4'>班级名称:&emsp;</Col>
					          	 <Col span={18} >
					          	 	<FormItem>
					                  {getFieldDecorator('name', {
					                  })(
					                  	<Input placeholder='请输入班级名称' className='inputBG pageInpHeight'/>
					                  )}
					                </FormItem>
							     </Col>
					          </Col>
					          <Col span={7} >
					          	 <Col span={8} className='f-fz4 f-black f-h4-lh4'>课程名称:&emsp;</Col>
					          	  <Col span={16} >
						          	 <FormItem>
						                  {getFieldDecorator('courseId', {
						                  	valuePropName: 'value',
						                  })(
						                  	<Select showSearch allowClear placeholder="请选择" optionFilterProp="children"
						                        filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}>
							                      {this.state.course?
								                        	this.state.course.map((item,i)=> {
								                        		return(<Option key={item.id} value={item.id}>{ item.name }</Option>)
								                        	})
								                        	:null
							                      }
						                      </Select>
						                   )}
				                  	</FormItem>
						           </Col>
					          </Col>
					          <Col span={6} >
					             <Col span={8} className='f-fz4 f-black f-h4-lh4'>班主任:&emsp;</Col>
					              <Col span={15} >
						             <FormItem {...formItemLayout}>
						                  {getFieldDecorator('headmasterId', {
						                  })(
						                  	 <Select
						                  	  showSearch
						                  	  allowClear
									          placeholder='请选择班主任'
									          style={{ width: '100%' }}
									          optionFilterProp="children"
	                        					filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
									        >
												{this.state.masterList?
								                        	this.state.masterList.map((item,i)=> {
								                        		return(<Option key={item.id} value={item.id}>{ item.name }</Option>)
								                        	})
								                        	:null
							                      }
									        </Select>
						                   )}
					                </FormItem>
						           </Col>
					          </Col>
					        </Row>
					        <Row className='' gutter={40}>
					        		<Col span={11}>
					        		<Col span={6} className='f-fz4 f-black f-h4-lh4'>开班时间:&emsp;</Col>
					        		<Col span={18} >
						    			<Col span={11}>
							    			 <FormItem>
						                  {getFieldDecorator('startDate', {
						                  })(
								            	<DatePicker showTime disabledDate={this.disabledStartDate.bind(this)} format="YYYY-MM-DD HH:mm:ss"
									          setFieldsValue={this.state.startValue} placeholder="开始时间"
									          onChange={this.onStartChange.bind(this)} onOpenChange={this.handleStartOpenChange.bind(this)}
									        />
								            	)}
						                </FormItem>
							    			 </Col>
							    			 <Col span={2} className='f-align-center f-h4-lh4'>--</Col>
							    			  <Col span={11}>
							    			  <FormItem>
						                  {getFieldDecorator('endDate', {
						                  })(
								            <DatePicker showTime disabledDate={this.disabledEndDate.bind(this)} format="YYYY-MM-DD HH:mm:ss"
									          setFieldsValue={this.state.endValue} placeholder="结束时间" onChange={this.onEndChange.bind(this)}
									          open={this.state.endOpen} onOpenChange={this.handleEndOpenChange.bind(this)}
									          />
								          )}
						                </FormItem>
							         	</Col>
							        	</Col>
					        		</Col>
					        		<Col span={7} >
					             <Col span={8} className='f-fz4 f-black f-h4-lh4'>教室:&emsp;</Col>
					              <Col span={16} >
					              <FormItem {...formItemLayout}>
					                  {getFieldDecorator('defaultRoomId', {
					                  })(
					                  	<Select showSearch allowClear placeholder="请选择" optionFilterProp="children"
					                        filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}>
						                      {this.state.classroomList?
							                        	this.state.classroomList.map((item,i)=> {
							                        		return(<Option key={item.id} value={item.id}>{ item.name }</Option>)
							                        	})
							                        	:null
						                      }
					                      </Select>
					                  )}
					                </FormItem>
						            </Col>
					          	</Col>
					          	<Col span={6} >
					             <Col span={8} className='f-fz4 f-black f-h4-lh4'>状态:&emsp;</Col>
					              <Col span={15} >
					              <FormItem {...formItemLayout}>
					                  {getFieldDecorator('state', {
					                  })(
					                  	<Select allowClear placeholder="全部状态" optionFilterProp="children"
					                        filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}>
					                        	 <Option value="">全部状态</Option>
								              <Option value="1">已排课未开课</Option>
								              <Option value="2">已开课</Option>
								              <Option value="3">已结课</Option>
					                      </Select>
					                   )}
					                </FormItem>
						            </Col>
					          	</Col>
					        </Row>
					        <Row className='f-pb3'>
					        		<Col span={12} className='f-align-right f-pr4'>
					        			<Button style={{border: '1px solid #999999',borderRadius: '4px',width:'110px',height:'40px'}} onClick={this.reset.bind(this)}>
					        				<i className='iconfont icon-reset f-mr2'></i>重置
					        			</Button>
					        		</Col>
					        		<Col span={12} className='f-align-left'>
					        			<Button type="primary" style={{borderRadius: '4px',width:'110px',height:'40px'}} onClick={this.advancedSearch.bind(this)}>
					        				<i className='iconfont icon-hricon33 f-mr2'></i>搜索
					        			</Button>
					        		</Col>
					        </Row>
					    		</Form>
					    </TabPane>
					 </Tabs>
				</div>
				<div className='f-bg-white f-pd4 f-box-shadow1 f-mt5 f-radius1 f-mb4'>
					<div className='f-flex' style={{justifyContent: 'space-between'}}>
						<h3 className="f-title-blue f-mb3">账号列表</h3>
						<a className="f-btn-blue _export" onClick={this._export.bind(this)}>
						<i className='iconfont icon-chuangjian f-mr1'></i>
						班级学员批量导出</a>
					</div>
					{this.state.pageResult?
						<div>
							<div className='f-mb2'>
								<Button className='f-mr2' onClick={this.allSelect.bind(this)}>全选</Button><Button onClick={this.cancelAllselect.bind(this)}>取消全部</Button>
							</div>
							<Table columns={columns} dataSource={this.state.pageResult} scroll={{ x: 1700}} bordered pagination={false}/>
								<div className='f-flex f-mt3' style={{justifyContent:'flex-end'}}>
									<div>
										<Pagination current={this.state.currentPage} total={this.state.total} onShowSizeChange={this.onShowSizeChange.bind(this)} showSizeChanger showQuickJumper onChange={this.pageChange.bind(this)}/>
									</div>
								</div>
						</div>:<Loading/>
					}
				</div>
				
			</div>
		)
	}
}
const ClassList = Form.create()(Main);
export {
	ClassList
}