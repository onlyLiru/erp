import React, { Component } from 'react';
import { Breadcrumb, Row, Col, Modal, Button, Input, Select, Form, Table, Radio, Checkbox, DatePicker, message } from 'antd';
import { getUnicodeParam, Loading, Pagination, undefinedToEmpty } from '../../common/g.js';
import { fpost, fpostArray } from '../../common/io.js';
import moment from 'moment';

const dateFormat = 'YYYY-MM-DD HH:mm:ss';
const FormItem = Form.Item;
const Option = Select.Option;
const RadioGroup = Radio.Group;
const { TextArea } = Input;

export default class extends Component {
	constructor(props) {
		super(props);
		this.name = getUnicodeParam('potentialStudentName');
		this.phone = getUnicodeParam('motherPhone');
		this.potentialStudentId = getUnicodeParam('potentialStudentId');

		this.state = {
			visibleType: false,
			linkUpData: null,
			courseList: [],
			courseArr: [],
			curKey: 0,
			editId: '',
			editByIdData: {},
			schoolList: [],
			status: true,
			isShowSelect: false,
			total: 1,
			isEdit: false,
			isInvite: '1',
			isPromise: '1',
			isRequest: false,
			purposeList: [],
			searchPara: { 
				currentPage: 1, 
				pageSize: 10, 
				isInvite: '', 
				name: this.name || '',
				phone: this.phone || '',
				potentialStudentId: this.potentialStudentId 
			}
		}
	
		this.editConnectId.bind(this);
		this.initCount = 0;
	}
	setModal() {
		this.initCount++;
		this.setState({
			visibleType: true,
			isShowSelect: true,
			status: false,
			editId: '',
			isEdit: false,
			isInvite: '1',
			isPromise: '1'
		})
		if(this.initCount > 1) {
			let domForm = this.addForm.props.form;
				domForm.setFieldsValue({
					isInvite: '1',
					confirmState: '1',
					visitState: '1'
				})
		}
	}
	updateInvite(v) {
		if(v=='1') {
			this.setState({
				isInvite: '1',
				isPromise: '1'
			})
		}else if(v=='0') {
			this.setState({
				isInvite: '0',
				isPromise: '0'
			})
		}
	}
	updatePromise(v) {
		if(v=='1') {
			this.setState({
				isPromise: '1'
			})
		}else if(v=='0') {
			this.setState({
				isPromise: '0'
			})
		}
	}
	getDataList(para) {
		fpost('/api/marketing/potentialStudentConnect/pagePotentialStudentConnect', para).then(res=>res.json()).then(res=>{
			if(res.success == true) {
				let arr = [];
				res.result.records.map((e, i)=>{
					e.key = i;
					arr.push(e)
				})
				this.setState({
					linkUpData: arr,
					total: res.result.total
				})
			}else {
				message.error(res.message);
			}
		});
	}
	cancelSetStock() {
		this.setState({
			visibleType: false,
			editByIdData: {},
			status: false,
			purposeList: [],
			courseArr:[]
		})
	}
	setCourseList(v, key, purposeList) {
		let courseArr = this.state.courseArr, curKey = this.state.curKey, isHas = false;
		courseArr[key] = courseArr[key]? courseArr[key] : {};

		courseArr.forEach((e, i)=>{
			if(e.courseId == v) {
				isHas = true;
				return;
			}
		})

		if(isHas && (curKey != key)) {
			message.error('请不要选择重复的课程!');
			return false;
		}else {
			purposeList[key].props.potentialConnectIntentions.courseId = v;
			this.setState({
				curKey: key,
				status: true,
				purposeList: purposeList
			});
		}
		courseArr[key].courseId = v;

	}
	updateCourse() {
		let dataList = this.state.courseArr;
			dataList.push({courseId:undefined, intentionLevel:undefined});
			this.setState({
				courseArr: dataList
			});
	}
	selectIntention(v, key, purposeList) {
		const courseArr = this.state.courseArr;
		courseArr[key] = courseArr[key]? courseArr[key] : {};

		purposeList[key].props.potentialConnectIntentions.intentionLevel = v;
		courseArr[key].intentionLevel = v;
		this.setState({
			purposeList,
			status: true
		});
	}
	saveData() {
		let self = this, courseGroup = [], isError = false, domForm = this.addForm.props.form;
		let obj = domForm.getFieldsValue();	
			this.state.courseArr.map((e, i)=>{
				courseGroup.push({courseId: e.courseId, intentionLevel: e.intentionLevel? e.intentionLevel : '1', id: e.id})
			});
			obj.potentialConnectIntentions = courseGroup;
			obj.connectTime = obj.connectTime? obj.connectTime.format('YYYY-MM-DD HH:mm:ss') : '';
			obj.planedVisitTime = obj.planedVisitTime? obj.planedVisitTime.format(dateFormat) : '';
			obj.potentialStudentId = this.potentialStudentId;
			obj.connectWay = obj.connectWay? obj.connectWay : '';
			if(this.state.isEdit)  obj.visitSchoolAreaId = this.state.editByIdData.visitSchoolAreaId; 

			if(this.state.editId) obj.id = this.state.editId; 	

			if(obj.potentialConnectIntentions.length==0) {
				message.error('请填写完整!');
				return;
			}

			obj.potentialConnectIntentions.forEach((e, i)=>{
				if(e == undefined || e.courseId == undefined || e.intentionLevel == undefined) {
					message.error('请填写完整!');
					isError = true;
					return;
				}
			})

			if(isError) return;

		//是否邀约为否的时候, 清空诺访, 到访时间, 到访.
		if(obj.isInvite==0){
			obj.confirmState = '';
			obj.visitState = '';
			obj.planedVisitTime = '';
		}else{
			if(obj.confirmState==0) {
				obj.visitState = '';
				obj.planedVisitTime = '';
			}
		}


		//用这个字段 potentialConnectIntentions
		domForm.validateFields(	
				(err, fieldsValue) => {
		        	if(err){
						message.error('请填写完整!');
						isError = true;
		           }
			}
		)

		if(isError) return;

		this.setState({
			isRequest: true
		})

		obj = undefinedToEmpty(obj);

		fpostArray('/api/marketing/potentialStudentConnect/savePotentialStudentConnect', obj).then(res=>res.json()).then(res=>{
			if(res.success == true) {
				let searchPara = this.state.searchPara;
					searchPara.currentPage = 1;
				self.setState({
					visibleType: false,
					isShowSelect: false,
					editByIdData: {},
					status: false,
					searchPara: searchPara,
					purposeList: [],
					courseArr: []
				}, function(){
					self.addForm.props.form.resetFields();
				});
				self.getDataList(searchPara);
				//window.location.reload();
			}else {
				message.error(res.message);
			}
			self.setState({
				isRequest: false
			})
		})
	}
	editConnectId(id) {
		let self = this, editId = this.state.editId;
			this.initCount++;
		fpost('/api/marketing/potentialStudentConnect/findPotentialStudentConnect', { id: id }).then(res=>res.json()).then(res=>{
			if( res.success == true ) {
				self.setState({
					editByIdData: res.result,
					visibleType: true,
					editId: id,
					isEdit: true,
					isShowSelect: true,
					status: editId != id || editId == '',
					isInvite: res.result.isInvite != null? res.result.isInvite : '1',
					isPromise: res.result.confirmState != null? res.result.confirmState : res.result.isInvite == '0'? '0' : '1',
					courseArr: res.result.potentialConnectIntentions
				});
				if(this.initCount > 1) {
					let domForm = this.addForm.props.form;
					domForm.setFieldsValue({
						isInvite: res.result.isInvite != null? res.result.isInvite : '1',
						confirmState: res.result.confirmState != null? res.result.confirmState : '1',
						visitState: res.result.visitState != null? res.result.visitState : '1'
					})
				}
			}else {
				message.error(res.message);
			}
		});
	}
	updateStatus() {
		this.setState({
			status: true
		});
	}
	upDatePurposeList(obj) {
		this.setState({
			purposeList: obj,
			status: true
		});
	}
	onChangeBack(page, pageSize) {
		let obj = this.state.searchPara;
			obj.currentPage = page;
		this.getDataList(obj);
	}
	onShowSizeChangeBack(current, size) {
		let obj = this.state.searchPara;
			obj.pageSize = size;
			obj.currentPage = 1;
		this.getDataList(obj);
	}
	removeCourse(v) {
		let dataList = this.state.courseArr;
		this.setState({
			courseArr: dataList.filter((e, index)=>{
				return index != v;
			})
		});
	}
	componentDidMount() {
		//获取沟通列表
		this.getDataList(this.state.searchPara);

		//获取课程列表
		fpost('/api/system/course/listCourse').then(res=>res.json()).then(res=>{
			if(res.success == true) {
				this.setState({
					courseList: res.result
				})
			}else {
				message.error(res.message);
			}
		})

		//获取校区列表
		fpost('/api/system/schoolarea/findUserSchoolAreaInfo').then(res=>res.json()).then(res=>{
			if(res.success == true) {
				this.setState({
					schoolList: res.result.userSchoolArea
				})
			}else {
				message.error(res.message)
			}
		})
	}

	render() {
		const columns = [
			{
			  title: '沟通时间',
			  dataIndex: 'connectTime',
			  key: 'connectTime',
			  className:'f-align-center',
			}, {
			  title: '意向课程(意向度)',
			  dataIndex: 'potentialConnectIntentionStr',
			  key: 'potentialConnectIntentionStr',
			  className:'f-align-center',
			  render: text => (
			  	<span dangerouslySetInnerHTML={{__html: text}}></span>
			  )
			}, {
			  title: '沟通方式',
			  dataIndex: 'connectWayStr',
			  key: 'connectWayStr',
			  className:'f-align-center',
			}, {
			  title: '承诺到访',
			  key: 'confirmStateStr',
			  dataIndex: 'confirmStateStr',
			  className:'f-align-center',
			}, {
			  title: '预约到访时间',
			  dataIndex: 'planedVisitTime',
			  key: 'planedVisitTime',
			  className:'f-align-center',
			}, {
			  title: '预约到访校区',
			  dataIndex: 'visitSchoolAreaName',
			  key: 'visitSchoolAreaName',
			  className:'f-align-center',
			}, {
			  title: '沟通内容',
			  dataIndex: 'connectMemo',
			  key: 'connectMemo',
			  className:'f-align-center',
			}, {
			  title: '操作',
			  key: 'action',
			  className:'f-align-center',
			  render: (text, record) => (
				<div>
					<Button className='f-radius4' onClick={ ()=>{ this.editConnectId(record.id) } } style={{ color:'#666', padding: '0 15px 0 9px'}}><i className='iconfont icon-bianji1 f-pointer f-mr1' style={{fontSize:'14px'}}></i>编辑</Button>
				</div>
			  )
			}];

		return (<div className='classListPage wid100'>
					<Breadcrumb className="f-mb3">
					    <Breadcrumb.Item>市场运营</Breadcrumb.Item>
					    <Breadcrumb.Item>客户管理</Breadcrumb.Item>
					    <Breadcrumb.Item>客户列表</Breadcrumb.Item>
					    <Breadcrumb.Item className='f-fz5 f-bold'>查看沟通</Breadcrumb.Item>
					</Breadcrumb>
					<div className='Topselect f-bg-white f-radius1 f-box-shadow1 f-pd4'>
						<SearchFormCreate name={this.name} phone={this.phone} getDataList={ this.getDataList.bind(this) } searchPara={ this.state.searchPara }/>
					</div>
					<div className='f-bg-white f-pd4 f-box-shadow1 f-mt1 f-radius1 f-mt5'>
						<div className='f-bg-white f-radius1'>
							<div className='f-pb3 f-flex' style={{justifyContent: 'space-between'}}>
								<h3 className="f-title-blue">沟通日志</h3>
								<div>
									<a className="f-btn-green f-ml3" onClick={ this.setModal.bind(this) }>
										<i className='iconfont icon-tianjiajiahaowubiankuang f-mr2'></i>新建沟通
									</a>
									<Modal title={this.state.isEdit? '编辑沟通' : '新建沟通'}
										wrapClassName="vertical-center-modal"
										visible={ this.state.visibleType }
										onOk={ this.saveData.bind(this) }
										confirmLoading={ this.state.isRequest }
										width={ 700 }
										onCancel={ this.cancelSetStock.bind(this) }
									>
										<AddFormCreate isInvite={this.state.isInvite} isEdit={this.state.isEdit} isPromise={this.state.isPromise} updateInvite={this.updateInvite.bind(this)} updatePromise={this.updatePromise.bind(this)} updateCourse={this.updateCourse.bind(this)} upDatePurposeList={this.upDatePurposeList.bind(this)} purposeList={this.state.purposeList} removeCourse={ this.removeCourse.bind(this) } updateStatus={ this.updateStatus.bind(this) } status={ this.state.status } schoolList={ this.state.schoolList } isShowSelect={ this.state.isShowSelect } editByIdData={ this.state.editByIdData } setCourseList={ this.setCourseList.bind(this) } selectIntention={ this.selectIntention.bind(this) } courseList={ this.state.courseList } wrappedComponentRef={ (inst) => this.addForm = inst }/>
									</Modal>
								</div>
							</div>
							{
								this.state.linkUpData != null?
								<Table columns={columns} pagination={ Pagination(this.state.total, this.state.searchPara, this.getDataList.bind(this)) } dataSource={this.state.linkUpData} bordered className='f-align-center' scroll={{ x: 1300 }}/>
								: <Loading />
							}
						</div>
					</div>
				</div>)
	}
}

class SearchForm extends Component {
	searchCondition() {
		let obj = this.props.form.getFieldValue("isInvite"), searchPara = this.props.searchPara;
		if(obj) {
			searchPara.isInvite = '1';
		} else {
			searchPara.isInvite = '';
		}
		this.props.getDataList(searchPara);
	}
	render() {
		const { getFieldDecorator } = this.props.form;
		return (<div>
					<dl className='f-inline-block' style={{ margin: '0 60px 0 20px' }}>
						<dt className='f-fz2 f-mb1'>客户姓名</dt>
						<dd style={{ color:'#243650' }} className='f-fz4'>{this.props.name}</dd>
					</dl>
					<dl className='f-inline-block'>
						<dt className='f-fz2 f-mb1'>手机号码</dt>
						<dd style={{ color:'#243650' }} className='f-fz4'>{this.props.phone}</dd>
					</dl>
					<Form className='f-right'>
						<FormItem className='f-inline-block' style={{ marginLeft:'30px' }} wrapperCol={{ style: { display:'inline-block', verticalAlign:'middle' } }}>
							{getFieldDecorator('isInvite', {
					            rules: [{
					              required: false,
					            }]
					          })(
					          	<Checkbox>只看邀约</Checkbox>
					          )}
						</FormItem>
						<FormItem className='f-inline-block' style={{ marginLeft:'30px' }}>
							<Button className='f-mr5' type="primary" style={{borderRadius: '4px',width:'110px',height:'40px'}} onClick={ this.searchCondition.bind(this) }>
		        				<i className='iconfont icon-hricon33 f-mr2'></i>
		        				搜索
		        			</Button>
						</FormItem>
					</Form>
				</div>)
	}
}
const SearchFormCreate = Form.create()(SearchForm);

class AddForm extends Component {
	constructor(props){
		super(props);
		
		this.state = {
			courseList: this.props.courseList
		}

		this.arrData = [];
	}
	removeRecord(v) {
		let dataList = this.props.purposeList.length && this.props.status? this.props.purposeList : this.arrData;
		let arrs = [];
		if(dataList.length == 1) {
			message.error('不能再删了!')
			return;
		}

		arrs = dataList.filter((e, index)=>{
				return index != v
			})

		this.props.upDatePurposeList(arrs.map((e, i)=>{
			return (<SelectForm dataLength={arrs.length} potentialConnectIntentions={ e.props.potentialConnectIntentions } removeRecord={this.removeRecord.bind(this)} key={i} countIndex={i} courseList={ this.props.courseList } setCourseList={this.setCourseList.bind(this)} selectIntention={this.selectIntention.bind(this)}/>)
		}));

		this.props.removeCourse(v);
	}
	handleChange(v){
		this.props.form.setFieldsValue({
			courseId: v
		})
	}
	isShowPromise(e) {
		this.props.updateInvite(e.target.value);
	}
	setPromise(e) {
		this.props.updatePromise(e.target.value);
	}
	setCourseList(v, key) {
		let dataList = this.props.purposeList.length && this.props.status? this.props.purposeList : this.arrData;
		this.props.setCourseList(v, key, dataList);
	}
	selectIntention(v, key) {
		let dataList = this.props.purposeList.length && this.props.status? this.props.purposeList : this.arrData;
		this.props.selectIntention(v, key, dataList);
	}
	addCourse() {
		let self = this;
		let dataList = this.props.purposeList.length && this.props.status? this.props.purposeList : this.arrData;
			self.keyIndex = self.countIndex = dataList.length - 1;
		if(dataList.length) {
			dataList = dataList.map((e, i)=>{
				return (<SelectForm dataLength={dataList.length+1} potentialConnectIntentions={ e.props.potentialConnectIntentions } removeRecord={this.removeRecord.bind(this)} key={i} countIndex={i} courseList={ this.props.courseList } setCourseList={this.setCourseList.bind(this)} selectIntention={this.selectIntention.bind(this)}/>)
			})
		}

		dataList.push(<SelectForm dataLength={dataList.length+1} wrappedComponentRef={ (inst) => self.selectForm = inst } potentialConnectIntentions={{courseId:undefined, intentionLevel:undefined}} removeRecord={self.removeRecord.bind(self)} key={++self.keyIndex} countIndex={++self.countIndex} courseList={ self.props.courseList } setCourseList={self.setCourseList.bind(self)} selectIntention={self.selectIntention.bind(self)}/>);
		self.props.updateStatus();
		self.props.upDatePurposeList(dataList);
		self.props.updateCourse();
	}
	_selectSchool(v) {
		this.props.form.setFieldsValue({
			visitSchoolAreaId: v
		});
	}
	setConnectWay(v) {
		console.log(v);
	}
	render() {
		const { getFieldDecorator } = this.props.form;
		const { isInvite } = this.props;
		const { connectWay, connectTime, visitSchoolAreaId, visitSchoolAreaName, confirmState = '1', visitState = '1', planedVisitTime, connectMemo } = this.props.editByIdData;

		this.arrData = [];

		const potentialConnectIntentions = this.props.editByIdData.potentialConnectIntentions;
		this.countIndex = this.props.purposeList.length? this.props.purposeList.length : potentialConnectIntentions?potentialConnectIntentions.length : 0;
		this.keyIndex = this.props.purposeList.length? this.props.purposeList.length : potentialConnectIntentions? potentialConnectIntentions.length : 0;

		if(potentialConnectIntentions && potentialConnectIntentions.length) {
			potentialConnectIntentions.map((e, i)=>{
				this.arrData.push(<SelectForm dataLength={potentialConnectIntentions.length} potentialConnectIntentions={ e } removeRecord={this.removeRecord.bind(this)} key={i} countIndex={i} courseList={ this.props.courseList } setCourseList={this.setCourseList.bind(this)} selectIntention={this.selectIntention.bind(this)}/>)
			})
		}else {
			this.arrData.push(<SelectForm dataLength={1} potentialConnectIntentions={{courseId:undefined, intentionLevel:undefined}} removeRecord={this.removeRecord.bind(this)} key={this.keyIndex} countIndex={this.countIndex} courseList={ this.props.courseList } setCourseList={this.setCourseList.bind(this)} selectIntention={this.selectIntention.bind(this)}/>);
		}

		const purposeList = this.props.purposeList.length && this.props.status? this.props.purposeList : this.arrData;

		return (<Form>
					<Row>
						<Col span={ 12 }>
							<FormItem label='沟通类型' labelCol={{ span:8 }} wrapperCol={{ style: { width: '60%', display:'inline-block', verticalAlign:'middle' } }}>
								{getFieldDecorator('connectType', {
						            rules: [{
						              required: false,
						            }],
						            initialValue: ''
						          })(
						            <span>售前</span>
						          )}
							</FormItem>
						</Col>
						<Col span={ 12 }>
							<FormItem label='沟通方式' labelCol={{ span:8 }} wrapperCol={{ style: { width: '60%', display:'inline-block', verticalAlign:'middle' } }}>
								{getFieldDecorator('connectWay', {
						            rules: [{
						              required: true,
						              message: '请选择沟通方式',
						            }],
						            initialValue: connectWay
						          })(
                                	<Select
                        			    className='f-vertical-middle' 
                        			    style={{ width: 200 }} 
                        			    placeholder="请选择" 
                        			    onChange={ this.setConnectWay.bind(this) }
                        			  >
                        			    <Option value='1'>聊天工具</Option>
                        			    <Option value='2'>电话</Option>
                        			    <Option value='3'>面对面沟通</Option>
                        		  </Select>
						          )}
							</FormItem>
						</Col>
					</Row>
					<Row>
						<Col span={ 12 }>
							<FormItem style={{ marginBottom:'20px' }}
								label="沟通时间"
								labelCol={{ span:8 }}
								wrapperCol={{style:{width:'60%', display:'inline-block', verticalAlign:'middle'}}}
							>
								{
									getFieldDecorator('connectTime', { 
										rules: [{ 
											required: true, 
											message: '请输入时间' 
										}],
										initialValue: moment(connectTime || new Date(), dateFormat)
									})(<DatePicker showTime={true}/>)
								}
							</FormItem>
						</Col>
						<Col span={ 12 }>
							{
								this.props.isShowSelect? <FormItem style={{ marginBottom:'20px' }}
									label="校区"
									labelCol={{ span:8 }}
									wrapperCol={{style:{width:'60%', display:'inline-block', verticalAlign:'middle'}}}
								>
									{
										getFieldDecorator('visitSchoolAreaId', { 
											rules: [{ 
												required: true, 
												message: '请选择校区' 
											}],
											initialValue: visitSchoolAreaId? visitSchoolAreaId : this.props.schoolList.length==1? this.props.schoolList[0].id : undefined
										})(
											this.props.isEdit? <span>{visitSchoolAreaName}</span> : <Select
								                size="large" 
								                placeholder="请选择校区" 
								                style={{ width:'200px' }} 
								                onSelect={ this._selectSchool.bind(this) } 
								            >
								                {
								                    this.props.schoolList.length>1? this.props.schoolList.map((e, i)=>{
								                    	return <Option value={e.id} key={i}>{e.name}</Option>
								                    }) : this.props.schoolList.length==1? <Option value={this.props.schoolList[0].id}>{this.props.schoolList[0].name}</Option> : null 
								                }
								            </Select>
										)
									}
								</FormItem> : ''
							}
						</Col>
					</Row>
					<Row className='f-flex' style={{ alignItems:'center' }}>
						<Col span={ 12 } className='f-vertical-middle f-inline-block'>
							<FormItem style={{ marginBottom:0 }}
								label="是否邀约"
								labelCol={{ span:8 }}
								wrapperCol={{style:{width:'60%', display:'inline-block', verticalAlign:'middle'}}}
							>
								{
									getFieldDecorator('isInvite', { 
										rules: [{ 
											required: true, 
											message: '请选择是否邀约'
										}],
										initialValue: isInvite
									})(<RadioGroup size="large" onChange={ this.isShowPromise.bind(this) }>
								        <Radio value={'1'}>是</Radio>
								        <Radio value={'0'}>否</Radio>
								      </RadioGroup>)
								}
							</FormItem>
							{
								isInvite=='1'? (<FormItem style={{ marginBottom:0 }}
								label="承诺到访"
								labelCol={{ span:8 }}
								wrapperCol={{style:{width:'60%', display:'inline-block', verticalAlign:'middle'}}}
							>
								{
									getFieldDecorator('confirmState', { 
										rules: [{ 
											required: true, 
											message: '请选择承诺到访'
										}],
										initialValue: confirmState==null || this.props.isPromise=='1'? '1' : confirmState
									})(<RadioGroup size="large" onChange={this.setPromise.bind(this)}>
								        <Radio value={'1'}>是</Radio>
								        <Radio value={'0'}>否</Radio>
								      </RadioGroup>)
								}
							</FormItem>) : ''
							}
							{
								this.props.isPromise=='1' && this.props.isInvite=='1'? (<FormItem style={{ marginBottom:0 }}
								label="到访"
								labelCol={{ span:8 }}
								wrapperCol={{style:{width:'60%', display:'inline-block', verticalAlign:'middle'}}}
							>
								{
									getFieldDecorator('visitState', { 
										rules: [{ 
											required: false, 
										}],
										initialValue: visitState==null || this.props.isPromise=='1'? '1' : visitState
									})(<RadioGroup size="large">
								        <Radio value={'1'}>是</Radio>
								        <Radio value={'0'}>否</Radio>
								      </RadioGroup>)
								}
							</FormItem>) : ''
							}
						</Col>
						<Col span={ 12 }>
							{
								this.props.isPromise=='1' && this.props.isInvite=='1'? (<FormItem 
								style={{ marginBottom:0 }}
								label="预约到访时间"
								labelCol={{ span:8 }}
								wrapperCol={{style:{width:'60%', display:'inline-block', verticalAlign:'middle'}}}
							>
								{
									getFieldDecorator('planedVisitTime', { 
										rules: [{ 
											required: true, 
											message: '请输入时间' 
										}],
										initialValue: moment(planedVisitTime || new Date(), dateFormat)
									})(<DatePicker />)
								}
							</FormItem>) : '' 
							}
						</Col>
					</Row>
					<Row className='f-mt3'>
						<Col span={ 4 } className='f-align-right'><span className='f-inline-block f-pt1' style={{ paddingRight:'3px', color:'rgba(0, 0, 0, 0.75)' }}><i className='f-mr1' style={{ color:'#f04134', fontSize:'14px', fontFamily: 'SimSun' }}>*</i>意向课程：</span>
						</Col>
						<Col span={ 20 }>
							{
								purposeList.map((e, i)=>{
									return (<div key={i} className='f-over-hide'>{e}</div>)
								})
							}
							<div>
								<Button style={{ color:'#196fe1', borderColor:'#196fe1' }} onClick={ this.addCourse.bind(this) }>继续添加课程</Button>
							</div>
						</Col>
					</Row>
					<Row className='f-mt5'>
						<Col span={ 24 }>
							<FormItem 
								style={{ marginBottom:0 }}
								label="沟通内容"
								labelCol={{ span:4 }}
								wrapperCol={{ span: 20, style:{ display:'inline-block', verticalAlign:'middle'}}}
							>
								{
									getFieldDecorator('connectMemo', { 
										rules: [{ 
											required: true, 
											message: '请输入沟通内容' 
										},
							            {
							            	max: 500,
							            	min: 1,
							            	transform: (val) =>{
							            		return val!=undefined? val.replace(/\s+/g,'').replace(/[^\x00-\xff]/g,'01') : '';
							            	},
							            	message: '长度不能超过500（250个汉字）'
							            }],
										initialValue: connectMemo || ''
									})(<TextArea placeholder='请输入沟通内容' />)
								}
							</FormItem>
						</Col>
					</Row>
				</Form>)
	}
}

class SelectForm extends Component {
	constructor(props) {
		super(props);
		this.setCourseList.bind(this);
		this.selectIntention.bind(this)
	}
	setCourseList(v, key) {
		this.props.setCourseList(v, key);
	}
	removeRecord(v) {
		this.props.removeRecord(v);
	}
	selectIntention(v, key) {
		this.props.selectIntention(v.target.value, key);
	}
	render() {
					const potentialConnectIntentions = this.props.potentialConnectIntentions;
					const courseId = potentialConnectIntentions? potentialConnectIntentions.courseId : undefined;
					const intentionLevel = potentialConnectIntentions? potentialConnectIntentions.intentionLevel : undefined;
					const dataLength = this.props.dataLength;

					return (<div className='purposeList'>
								<FormItem className='f-inline-block f-mb3' wrapperCol={{ style: { width:'30%', display:'inline-block', verticalAlign:'middle' } }}>
									<Select
			            			    className='f-vertical-middle' 
			            			    style={{ width: 200 }} 
			            			    placeholder="请选择" 
			            			    value={courseId}
			            			    onChange={ (v)=>{ this.setCourseList(v, this.props.countIndex) } }
			            			  >
			            			    {
			            			    	this.props.courseList.map((e, i)=>{
				            			    	return (<Option key={i} value={e.id}>{e.name}</Option>)
				            			    })
			            			    }
			            		  </Select>
								</FormItem>
								<FormItem className='f-inline-block f-ml5 f-mb3' label='意向度' wrapperCol={{ style: { display:'inline-block', verticalAlign:'middle' } }}>
									<RadioGroup value={intentionLevel? intentionLevel : '1'} onChange={ (v)=>{ this.selectIntention(v, this.props.countIndex) } }>
			                	        <Radio value={'1'}>高</Radio>
			                	        <Radio value={'2'}>中</Radio>
			                	        <Radio value={'3'}>低</Radio>
			                	    </RadioGroup>
								</FormItem>
								{
									dataLength>1? (<FormItem className='f-inline-block f-right' style={{marginRight:'30px'}}>
									<i className='iconfont icon-lajixiang' onClick={ ()=>{ this.removeRecord(this.props.countIndex) } }></i>
								</FormItem>) : ''
								}
						</div>)
	}
}
const AddFormCreate = Form.create()(AddForm);