import React, {
	Component
} from 'react';
import {
	Breadcrumb,
	Row,
	Col,
	Form,
	Input,
	Radio,
	Select,
	Button,
	DatePicker,
	Upload,
	Icon,
	message,
	Modal
} from 'antd';
import moment from 'moment';
import SelectCity from '../common/selectCity.js';
import {
	fpost,
	hostname
} from '../common/io.js';
import {
	getUrlParam,
	undefinedToEmpty
} from '../common/g.js';

const dateFormat = 'YYYY-MM-DD HH:mm:ss';
const FormItem = Form.Item;
const RadioGroup = Radio.Group;
const Option = Select.Option;

export default class extends Component {
	constructor(props) {
		super(props);
		this.state = {
			resourceId: '',
			isRequest: false
		}
	}
	updateResourceId(v) {
		this.setState({
			resourceId: v
		});
	}
	componentWillReceiveProps(nextProps) {
		if (this.props.location.search != nextProps.location.search) {
			window.location.reload();
		}
	}
	saveCustomerData() {
		let obj = this.addForm.props.form.getFieldsValue(),
			isError = false;
		obj.birthday = obj.birthday ? obj.birthday.format(dateFormat) : '';
		obj.resourceId = this.state.resourceId;
		obj.invalidReason = obj.isValid == '1' ? '' : obj.invalidReason;

		if (getUrlParam('id')) obj.id = getUrlParam('id');

		this.addForm.props.form.validateFields({
				first: true,
				force: true
			},
			(err, fieldsValue) => {
				if (err) {
					message.error('请填写完整!');
					isError = true;
				}
			}
		)

		if (isError) return;

		if (obj.fatherPhone == '' && obj.motherPhone == '' && obj.otherPhone == '') {
			message.warning('请至少填写一个手机号!');
			return;
		}

		this.setState({
			isRequest: true
		})

		obj = undefinedToEmpty(obj);

		fpost('/api/marketing/savePotentialStudent', obj).then(res => res.json()).then(res => {
			if (res.success == true) {
				message.success(res.message, 2, function() {
					window.location.href = '/market/client';
				});
			} else {
				message.error(res.message);
			}

			this.setState({
				isRequest: false
			})
		})
	}
	componentDidMount() {
		//解决右边菜单看不到的问题
		document.getElementsByClassName('f-main-content')[0].style.minWidth = 'auto';
	}
	render() {
		return (<div className='classListPage'>
					<Breadcrumb className="f-mb3">
					    <Breadcrumb.Item>市场运营</Breadcrumb.Item>
					    <Breadcrumb.Item>客户管理</Breadcrumb.Item>
					    <Breadcrumb.Item><span className='f-fz5 f-bold'>{ getUrlParam('id')? '编辑客户' : '创建客户' }</span></Breadcrumb.Item>
					</Breadcrumb>
					<div className='f-pb3 f-flex' style={{justifyContent: 'space-between'}}>
						<h3 className="f-title-blue">基本信息</h3>
					</div>
					<div className='f-bg-white f-radius1 f-box-shadow1'>
						<AddFormCreate wrappedComponentRef={ (inst) => this.addForm = inst } updateResourceId={this.updateResourceId.bind(this)}/>
					</div>
					<div className='f-pt4'>
						<Row>
							<Col span={6} className='f-right'>
								<Button className='f-mr3' onClick={ ()=>{ window.history.back() } }>取消</Button>
								<Button className='f-bg-blue f-white' style={{ borderColor:'#2187FF' }} onClick={ this.saveCustomerData.bind(this) }>保存</Button>
							</Col>
						</Row>
					</div>
				</div>)
	}
}

class AddForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			audioCoverFileList: [],
			imageUrl: null,
			isdisabled: false,
			isValidState: '1',
			provinceId: '',
			cityId: '',
			districtId: '',
			salesId: '',
			formData: {},
			personList: [],
			previewVisible: false,
			previewImage: ''
		}
	}
	_audioCoverChange({
		file,
		fileList
	}) {

		if (file.status == 'uploading') {
			this.setState({
				isdisabled: true
			});
		} else {
			this.setState({
				isdisabled: false
			});
		}

		if (fileList[0] && fileList[0].response && fileList[0].response.result) {
			if (fileList[0].response.result.id) this.props.updateResourceId(fileList[0].response.result.id);
		} else {
			this.setState({
				audioCoverFileList: fileList,
				imageUrl: null
			})
		};
	}
	_getSelectPerson(v) { //获取选择的课程顾问
		this.setState({
			salesId: v
		});
	}
	_getSelectCity(v) { //获取省市区
		/*this.setState({
			provinceId: v[0],
			cityId: v[1],
			districtId: v[2]
		})*/
		this.props.form.setFieldsValue({
			provinceId: v[0],
			cityId: v[1],
			districtId: v[2]
		});
	}
	handlePreview = (file) => {
		this.setState({
			previewImage: file.url || file.thumbUrl,
			previewVisible: true,
		});
	}
	handleCancel = () => this.setState({
		previewVisible: false
	})
	setInvalid = (e) => {
		if (e.target.value == '1') {
			this.setState({
				isValidState: '1'
			});
		} else if (e.target.value == '0') {
			this.setState({
				isValidState: '0'
			})
		}
	}
	disabledEndDate = (current) => {
		return current && current.valueOf() > Date.now();
	}
	componentDidMount() {
		//获取课程顾问人员列表
		fpost('/api/hr/staff/listStaffByCondForDropDown', {
			type: 1
		}).then(res => res.json()).then(res => {
			if (res.success == true) {
				this.setState({
					personList: res.result
				})
			} else {
				message.error(res.message);
			}
		})

		if (!getUrlParam('id')) return;

		fpost('/api/marketing/findPotentialStudent', {
			id: getUrlParam('id')
		}).then(res => res.json()).then(res => {
			if (res.success == true) {
				if (res.result.resourceId) {
					this.setState({
						formData: res.result,
						isValidState: res.result.isValid,
						audioCoverFileList: [{
							uid: -1,
							name: 'xxx.png',
							status: 'done',
							url: res.result.headImgUrl,
						}]
					});
				} else {
					this.setState({
						formData: res.result,
						isValidState: res.result.isValid
					});
				}
			} else {
				message.error(res.message);
			}
		});

	}
	render() {
		const {
			getFieldDecorator
		} = this.props.form;
		const {
			name,
			birthday,
			fatherPhone = '',
			invalidReason,
			salesId,
			gender,
			motherPhone = '',
			otherPhone = '',
			recruitSource,
			isPublicSchool,
			grade,
			currentSchoolName,
			isValid,
			provinceId,
			cityId,
			districtId,
			address,
			resourceId
		} = this.state.formData;
		const isValidState = this.state.isValidState;

		const uploadImgButton = (
			<div>
		        <Icon type="plus" />
	        	<div className="ant-upload-text">选择图片</div>
		      </div>
		);
		const {
			previewVisible,
			previewImage,
			audioCoverFileList
		} = this.state;

		return (<Form>
					<Row>
						<Col span={ 23 } className='f-mt5'>
							<Row>
								<Col span={ 9 }>
									<FormItem label='姓名' labelCol={{ span: 6 }} wrapperCol={{ style: {  display:'inline-block', width:'40%', verticalAlign:'middle' } }}>
										{getFieldDecorator('name', {
								            rules: [{
								              required: true,
								              message: '请输入姓名',
								              whiteSpace: true
								            },
								            {
								            	max: 50,
								            	min: 1,
								            	transform: (val) =>{
								            		return val!=undefined? val.replace(/\s+/g,'').replace(/[^\x00-\xff]/g,'01') : '';
								            	},
								            	message: '长度不能超过50（25个汉字）'
								            }],
								            initialValue: name || ''
								          })(
								            <Input placeholder="请输入姓名"/>
								          )}
									</FormItem>
									<FormItem label='学员生日' labelCol={{ span: 6 }} wrapperCol={{ style: {  display:'inline-block', width:'40%', verticalAlign:'middle' } }}>
										{getFieldDecorator('birthday', {
								            rules: [{
								              required: false,
								              message: '',
								            }],
								            initialValue: birthday? moment(birthday, 'YYYY-MM-DD HH:mm:ss') : undefined
								          })(
								            <DatePicker disabledDate={this.disabledEndDate}/>
								          )}
									</FormItem>
									
									<FormItem label='父亲电话' labelCol={{ span: 6 }} wrapperCol={{ style: {  display:'inline-block', width:'40%', verticalAlign:'middle' } }}>
										{getFieldDecorator('fatherPhone', {
								            rules: [{
								              pattern: /(^1[3|4|5|8][0-9]\d{4,8}$)|(^(\d{2,4}-?)?\d{7,8}$)/,
				              				  message: '请输入正确的号码'
								            }],
								            initialValue: fatherPhone
								          })(
								            <Input placeholder=""/>
								          )}
									</FormItem>
									<FormItem label='有效客户' labelCol={{ span: 6 }} wrapperCol={{ style: {  display:'inline-block', width:'60%', verticalAlign:'middle' } }}>
										{getFieldDecorator('invalidCustomer', {
								            rules: [{
								              required: false,
								            }],
								            initialValue: '1'
								          })(
		                    		            <div>
            		            		            <FormItem className='f-inline-block' style={{marginBottom:0}}>
            		            		            			{getFieldDecorator('isValid', {
            		            		            	            rules: [{
            		            		            	              required: false,
            		            		            	            }],
            		            		            	            initialValue: isValid || '1'
            		            		            	          })(
														            <RadioGroup onChange={ this.setInvalid.bind(this) }>
														            	<Radio value={'1'}>是</Radio>
													                    <Radio value={'0'}>否</Radio>
													                </RadioGroup>
            		            		            	          )}
            		            		            </FormItem>
        		            	                	<FormItem style={{ display: isValidState==0? 'inline-block' : 'none', marginBottom:0 }}>
        		            	                		{getFieldDecorator('invalidReason', {
        		            	                            rules: [{
        		            	                              required: isValidState=='1'? false : true,
        		            	                              message: '请输入无效原因',
        		            	                            }],
        		            	                            initialValue: invalidReason
        		            	                          })(
        		            	                            <Input placeholder="请输入无效原因"/>
        		            	                          )}
        		            	                	</FormItem>
		                    		            </div>
								          )}
									</FormItem>
									<FormItem label='市场顾问' labelCol={{ span: 6 }} wrapperCol={{ style: {  display:'inline-block', width:'40%', verticalAlign:'middle' } }}>
										{getFieldDecorator('salesId', {
								            rules: [{
								              required: false,
								            }],
								            initialValue: salesId
								          })(
						                    	<Select size="large" placeholder='请选择市场顾问'
                									allowClear>
                									{
                									    this.state.personList.length? this.state.personList.map(d => <Option value={ d.id } key={d.id}>
                									        { `${d.name}-${d.department}-${d.mobile}` }
                									    </Option>) : ''
                									}
                								</Select>
								          )}
									</FormItem>
								</Col>
								<Col span={ 9 }>
									<FormItem label='性别' labelCol={{ span: 6 }} wrapperCol={{ style: {  display:'inline-block', width:'40%', verticalAlign:'middle' } }}>
										{getFieldDecorator('gender', {
								            rules: [{
								              required: false,
								              message: '',
								            }],
								           	initialValue: gender || '1'
								          })(
								            <RadioGroup>
							                    <Radio value={'1'}>男</Radio>
							                    <Radio value={'2'}>女</Radio>
							                </RadioGroup>
								          )}
									</FormItem>
									<FormItem label='母亲电话' labelCol={{ span: 6 }} wrapperCol={{ style: {  display:'inline-block', width:'40%', verticalAlign:'middle' } }}>
										{getFieldDecorator('motherPhone', {
								            rules: [{
								              pattern: /(^1[3|4|5|8][0-9]\d{4,8}$)|(^(\d{2,4}-?)?\d{7,8}$)/,
				              				  message: '请输入正确的号码'
								            }],
								            initialValue: motherPhone
								          })(
								            <Input placeholder=""/>
								          )}
									</FormItem>
									<FormItem label='其他电话' labelCol={{ span: 6 }} wrapperCol={{ style: {  display:'inline-block', width:'40%', verticalAlign:'middle' } }}>
										{getFieldDecorator('otherPhone', {
								            rules: [{
								              pattern: /(^1[3|4|5|8][0-9]\d{4,8}$)|(^(\d{2,4}-?)?\d{7,8}$)/,
				              				  message: '请输入正确的号码'
								            }],
								            initialValue: otherPhone
								          })(
								            <Input placeholder=""/>
								          )}
									</FormItem>
									<FormItem label='市场来源' labelCol={{ span: 6 }} wrapperCol={{ style: {  display:'inline-block', width:'40%', verticalAlign:'middle' } }}>
										{getFieldDecorator('recruitSource', {
								            rules: [{
								              required: true,
								              message: '请选择市场来源'
								            }],
								            initialValue: recruitSource
								          })(
						                    	<Select
						            			    className='f-vertical-middle'
						            			    optionFilterProp="children"
						            			    placeholder="请选择市场来源"
						            			    filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
						            			  >
						            			  	<Option value=''>请选择</Option>
						            			    <Option value="1">电话邀约</Option>
						            			    <Option value="2">地推活动</Option>
						            			    <Option value="3">网络数据</Option>
						            			    <Option value="4">渠道推广</Option>
						            			    <Option value="5">网络推广</Option>
						            			    <Option value="6">主动上门</Option>
						            			    <Option value="7">陌生来电</Option>
						            			    <Option value="8">老带新</Option>
						            			    <Option value="9">内部转化</Option>
						            		  </Select>
								          )}
									</FormItem>
								</Col>
								<Col span={6}>
									<FormItem label='上传照片' labelCol={{style: { verticalAlign: 'top', marginRight:'20px' }}} wrapperCol={{ style: { display:'inline-block', width:'150px' } }}>
											{getFieldDecorator('resourceId', {
									            rules: [{
									              required: false,
									              message: '',
									            }],
									            initialValue: resourceId
									          })(
									            <Upload
        					    		          action={hostname()+'/api/common/resource/uploadImg'}
        					    		          listType="picture-card"
        					    		          accept="image/*"
        					    		          fileList={ audioCoverFileList }
        					    		          onChange={this._audioCoverChange.bind(this)}
        					    		          onPreview={this.handlePreview}
        					    		          className="previewCover"
        					    		        >
        					    		          {audioCoverFileList.length >= 1 ? null : uploadImgButton}
        					    		        </Upload>
									          )}
									</FormItem>
								</Col>
							</Row>
						</Col>
						<Col span={1} className='f-bg-blue f-absolute f-white f-fz5 f-flex' style={{top:0,right:0,bottom:0,alignItems:'center',width:'40px'}}>
							<div style={{width:'18px',margin:'0 auto'}}>客户信息</div>
						</Col>
					</Row>
					<Row style={{ borderTop:'1px solid #ECECEC' }} className='f-pt4'>
						<Col span={ 23 }>
							<Row>
								<Col span={ 9 }>
									<FormItem label='学校类型' labelCol={{ span: 6 }} wrapperCol={{ style: {  display:'inline-block', width:'40%', verticalAlign:'middle' } }}>
										{getFieldDecorator('isPublicSchool', {
								            rules: [{
								              required: false,
								              message: '',
								            }],
								            initialValue: isPublicSchool || '1'
								          })(
								            <RadioGroup>
							                    <Radio value={'1'}>公立</Radio>
							                    <Radio value={'0'}>私立</Radio>
							                </RadioGroup>
								          )}
									</FormItem>
									<FormItem label='年级' labelCol={{ span: 6 }} wrapperCol={{ style: {  display:'inline-block', width:'40%', verticalAlign:'middle' } }}>
										{getFieldDecorator('grade', {
								            rules: [{
								              required: false,
								              message: '',
								            }],
								            initialValue: grade
								          })(
						                    	<Select
						            			    className='f-vertical-middle'
						            			    placeholder="请选择年级"
						            			    optionFilterProp="children"
						            			    filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
						            			  >
						            			    <Option value="1">托班</Option>
						            			    <Option value="2">幼儿园小班</Option>
						            			    <Option value="3">幼儿园中班</Option>
						            			    <Option value="4">幼儿园大班</Option>
						            			    <Option value="5">一年级</Option>
						            			    <Option value="6">二年级</Option>
						            			    <Option value="7">三年级</Option>
						            			    <Option value="8">四年级</Option>
						            			    <Option value="9">五年级</Option>
						            			    <Option value="10">六年级</Option>
						            			    <Option value="11">初一</Option>
						            			    <Option value="12">初二</Option>
						            			    <Option value="13">初三</Option>
						            			    <Option value="14">高一</Option>
						            			    <Option value="15">高二</Option>
						            			    <Option value="16">高三</Option>
						            			    <Option value="17">大学</Option>
						            			    <Option value="18">成人</Option>
						            			    <Option value="19">其他</Option>
						            		  </Select>
								          )}
									</FormItem>
								</Col>
								<Col span={ 9 }>
									<FormItem label='学校名称' labelCol={{ span: 6 }} wrapperCol={{ style: {  display:'inline-block', width:'40%', verticalAlign:'middle' } }}>
										{getFieldDecorator('currentSchoolName', {
								            rules: [{
								              required: false,
								              message: '',
								            },
								            {
								            	max: 200,
								            	min: 1,
								            	transform: (val) =>{
								            		return val!=undefined? val.replace(/\s+/g,'').replace(/[^\x00-\xff]/g,'01') : '';
								            	},
								            	message: '长度不能超过200（100个汉字）'
								            }],
								            initialValue: currentSchoolName || ''
								          })(
								            <Input placeholder="请输入学校名称"/>
								          )}
									</FormItem>
									<FormItem className='f-hide'>
										{getFieldDecorator('provinceId', {
								            rules: [{
								              required: false,
								            }],
								            initialValue: this.state.provinceId || provinceId
								          })(<span></span>)}
									</FormItem>
									<FormItem className='f-hide'>
										{getFieldDecorator('cityId', {
								            rules: [{
								              required: false,
								            }],
								            initialValue: this.state.cityId || cityId
								          })(<span></span>)}
									</FormItem>
									<FormItem className='f-hide'>
										{getFieldDecorator('districtId', {
								            rules: [{
								              required: false,
								            }],
								            initialValue: this.state.districtId || districtId
								          })(<span></span>)}
									</FormItem>
									<FormItem label='居住区域' style={{marginBottom: 0}} labelCol={{ span: 6 }} wrapperCol={{ style: {  display:'inline-block', width: '40%', verticalAlign:'middle' } }}>
										{getFieldDecorator('area', {
								            rules: [{
								              required: false,
								              message: ''
								            }],
								            initialValue: ''
								          })(
						          	        	<div style={{ whiteSpace: 'nowrap' }}>
	          	        			            	<FormItem className='f-inline-block f-vertical-middle' style={{width: '100%', marginBottom:0}}>
	          	        	            				<SelectCity initialValueData={provinceId && cityId && districtId ? [ provinceId, cityId, districtId ] : null} onSelect={ this._getSelectCity.bind(this) }/>
	          	        			            	</FormItem>
						          	        	</div>
								          )}
									</FormItem>
								</Col>
							</Row>
							<Row>
				            	<FormItem className='f-inline-block f-vertical-middle' style={{width:'100%'}} label='详细地址' labelCol={{style:{display: 'inline-block', width:'9%', verticalAlign: 'middle'}}} wrapperCol={{style:{display: 'inline-block', width: '53%', verticalAlign: 'middle'}}}>
		            				{getFieldDecorator('address', {
		            		            rules: [{
		            		              required: false,
		            		              message: ''
		            		            },
							            {
							            	max: 200,
							            	min: 1,
							            	transform: (val) =>{
							            		return val!=undefined? val.replace(/\s+/g,'').replace(/[^\x00-\xff]/g,'01') : '';
							            	},
							            	message: '长度不能超过200（100个汉字）'
							            }],
		            		            initialValue: address || ''
		            		        	})(
		            		          		<Input placeholder="请填写详细地址" style={{marginBottom: 0}}/>	
		            		        )}
				            	</FormItem>
							</Row>
						</Col>
						<Col span={1} className='f-bg-green f-absolute f-white f-fz5 f-flex' style={{top:0,right:0,bottom:0,alignItems:'center',width:'40px'}}>
							<div style={{width:'18px',margin:'0 auto'}}>学校信息</div>
						</Col>
					</Row>
					<Row>
						<Modal visible={previewVisible} footer={null} onCancel={this.handleCancel}>
				          <img alt="example" style={{ width: '100%' }} src={previewImage} />
				        </Modal>
					</Row>
				</Form>)
	}
}

const AddFormCreate = Form.create()(AddForm)