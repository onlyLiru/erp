import React, {
	Component
} from 'react';
import {
	Button,
	Input,
	Form,
} from 'antd';

import SelectRoleGroups from '../../common/selectRoleGroups.js';

const FormItem = Form.Item;

class MainForm extends Component {
	render() {
		const {
			getFieldDecorator
		} = this.props.form;
		let {
			onSearch
		} = this.props;

		return (<header className="f-box-shadow2 f-radius1 f-bg-white f-pd4">
			<Form layout="inline">
				<FormItem
					label="账号"
				>
					{
						getFieldDecorator('searchCond', { 
							rules: [{
								required: false, 
								message: '请输入姓名／账户名称／手机号' 
							}],
							initialValue:''
						})
						(
							<Input style={{width:'320px'}} placeholder="请输入姓名／账户名称／手机号" />
						)
					}
				</FormItem>

				<FormItem
					label="权限组"
				>
					{
						getFieldDecorator('roleId', {
							rules: [{ 
								required: false, 
								message: '请选择权限组' 
							}],
							initialValue:''
						})(
							<SelectRoleGroups 
								onSelect={
									(v) => {
										this.props.form.setFieldsValue({
											roleId:v
										});
									}
								}
								width="220px"
							/>
						)
					}
				</FormItem>
	        
		        <FormItem>
					<Button 
						type="primary" 
						size="large" 
						icon="search"
						onClick={
							onSearch
						}
					>搜索</Button>
		        </FormItem>

	      	</Form>
		</header>);
	}
}

const MyForm = Form.create()(MainForm);

export default MyForm;