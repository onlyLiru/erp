import React, {
	Component
} from 'react';
import {
	Icon,
	Breadcrumb,
	Modal,
	message
} from 'antd';
import {
	default as SearchForm
} from './searchform.js';
import {
	default as MyList
} from './list.js';
import {
	default as AddForm
} from './add.js';
import {
	fpost
} from '../../common/io.js';

import './main.less';

export default class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			visible: false,
			loading: false,
			result: null,
			defaultData: null,
			pagination: {
				showSizeChanger: true,
				showQuickJumper: true,
				total: 1,
			},
			pageSize: 10,
			currentPage: 1,
			total: 1,
			searchParma: {}
		}
	}
	render() {
		let {
			visible,
			result,
			loading,
			pagination,
			defaultData
		} = this.state;

		return (<section>
			<Breadcrumb className="f-mb3">
			    <Breadcrumb.Item>系统设置</Breadcrumb.Item>
			    <Breadcrumb.Item>
			   		<span className="f-fz5 f-bold">账号管理</span>
			    </Breadcrumb.Item>
			</Breadcrumb>

			<SearchForm 
				ref={ form => this.searchForm = form }
				onSearch= { this._search.bind(this) }
			/>

			<div className="f-box-shadow2 f-radius1 f-bg-white f-pd4 f-mt5">
				<h3 className="f-mb5">
					<span className="f-title-blue f-mt3">账号列表</span>
					<a 
						onClick={ ()=> { this.setState({visible:!this.state.visible}); } }
						className="f-btn-green f-right">
						<Icon className="f-fz7 f-vertical-middle f-bold" type="plus" />新增账号
					</a>
				</h3>
				<MyList 
					onEdit={this._edit.bind(this)}
					onDelete={this._delete.bind(this)}
					data={result}
			        loading={loading}
			        onChange={this._getList.bind(this)}
			        onResetPassWord= { this._resetPassWord.bind(this) }
			        pagination={pagination}
				/>
			</div>

			{
				visible ? 
					<AddForm
						ref={ form => this.addForm = form }
						visible={ visible }
						onSaveOk={ this._saveOk.bind(this)}
						onCancel={ this._cancel.bind(this)}
						defaultData={ defaultData }
					/>
				: null
			}

		</section>);
	}
	componentDidMount() {
		this._getList();
	}
	_search(e) { //搜索
		e.preventDefault();
		this.searchForm.validateFields((err, values) => {
			if (!err) {
				this.setState({
					searchParma: values,
					currentPage: 1,
					pagination: {
						...this.state.pagination,
						current: 1,
					}
				}, () => this._getList());
			}
		});
	}
	_saveOk() {
		this._cancel();
		this._getList();
	}
	_cancel() {
		this.setState({
			visible: false,
			defaultData: null
		});
	}
	_visibleAddModal() {
		this.setState({
			visible: true
		});
	}
	_delete(param) {
		fpost('/api/system/user/deleteUser', param)
			.then(res => res.json())
			.then((res) => {
				if (!res.success) {
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res);
			})
			.then((res) => {
				this._getList();
			})
			.catch((err) => {
				console.log(err);
			});
	}
	_resetPassWord(param) {
		let userId = param.id;

		fpost('/api/system/user/systemResetPasswordByCond', {
				userId
			})
			.then(res => res.json())
			.then((res) => {
				if (!res.success) {
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res);
			})
			.then((res) => {
				Modal.success({
					title: '温馨提醒',
					content: '重置密码成功!',
				});
			})
			.catch((err) => {
				console.log(err);
			});
	}
	_edit(param) {
		let {
			id
		} = param;

		this.setState({
			loading: true
		});

		fpost('/api/system/user/findUser', {
				id: id
			})
			.then(res => res.json())
			.then((res) => {
				if (!res.success) {
					this.setState({
						loading: false
					});
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res);
			})
			.then((res) => {
				this.setState({
					visible: true,
					loading: false,
					defaultData: res.result
				});
			})
			.catch((err) => {
				this.setState({
					loading: false
				});
				console.log(err);
			});
	}
	_getList(pager = {}) {
		this.setState({
			loading: true
		});
		let {
			pageSize,
			currentPage,
			pagination,
			searchParma
		} = this.state;
		pageSize = pager.pageSize || pageSize;
		currentPage = pager.current || currentPage;

		searchParma.pageSize = pageSize;
		searchParma.currentPage = currentPage;

		fpost('/api/system/user/pageUser', searchParma)
			.then(res => res.json())
			.then((res) => {
				if (!res.success || !res.result || !res.result.records) {
					this.setState({
						loading: false
					});
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res.result);
			})
			.then((result) => {
				let data = result.records;
				data.forEach((d) => {
					d.key = d.id
				});

				let total = Number(result.total);

				this.setState({
					result: data,
					pagination: {
						...pagination,
						total: total,
						current: currentPage,
						pageSize: pageSize
					},
					currentPage,
					pageSize,
					total,
					loading: false
				});
			})
			.catch((err) => {
				console.log(err);
			});
	}
}