import React, {
  Component
} from 'react';
import {
  Table,
  Icon,
  Popconfirm,
  Button
} from 'antd';

export default class Main extends Component {
  render() {
    let {
      onEdit,
      onDelete,
      onResetPassWord,
      data,
      loading,
      pagination,
      onChange
    } = this.props;

    const columns = [{
      title: '姓名',
      width: '200px',
      dataIndex: 'name',
      fixed: 'left',
    }, {
      title: '账号名称',
      width: '200px',
      dataIndex: 'loginName'
    }, {
      title: '适用校区',
      width: '300px',
      dataIndex: 'applySchoolArea'
    }, {
      title: '是否员工',
      width: '100px',
      dataIndex: 'isExternal',
      className: "f-align-center",
      render: (value, row, index) => {
        let {
          isExternal
        } = row;
        let txt = isExternal == '0' ? '是' : '否';
        return (<div>
            { txt }
          </div>);
      }
    }, {
      title: '权限组',
      width: '250px',
      dataIndex: 'roleName'
    }, {
      title: '状态',
      width: '50px',
      dataIndex: 'des1',
      render: (value, row, index) => {
        let {
          isDisabled
        } = row;
        let txt = isDisabled == '0' ? '启用' : '禁用';
        return (<div>
            { txt }
          </div>);
      }
    }, {
      title: '手机号',
      width: '150px',
      dataIndex: 'phone'
    }, {
      title: '有效日期',
      width: '200px',
      dataIndex: 'des4',
      render: (value, row, index) => {
        let {
          loginPeriodStart,
          loginPeriodEnd
        } = row;
        return (<div>
            { loginPeriodStart }
            <p>-</p> 
            { loginPeriodEnd }
          </div>);
      }
    }, {
      title: '登录时限',
      width: '100px',
      dataIndex: 'des6',
      render: (value, row, index) => {
        let {
          loginTimeStart,
          loginTimeEnd
        } = row;
        return (<div>
            { loginTimeStart } 
            <span className="f-ml2 f-mr2">-</span> 
            { loginTimeEnd }
          </div>);
      }
    }, {
      title: '操作',
      dataIndex: 'des7',
      width: '200px',
      fixed: 'right',
      render: (value, row, index) => {
        return (<div>
              <Popconfirm 
                title="确定要删除吗?" 
                onConfirm={
                  ()=> onDelete(row)
                }
                okText="确定" 
                cancelText="取消"
              >
                <Icon 
                  className="f-mr3 f-pointer" 
                  type="delete" 
                />
              </Popconfirm>
            <Icon 
              className="f-pointer" 
              type="edit" 
              onClick={ ()=> onEdit(row) }
            />
            <Popconfirm 
                title="确定要重置密码吗?" 
                onConfirm={
                  ()=> onResetPassWord(row)
                }
                okText="确定" 
                cancelText="取消"
              >
                <Button
                  className="f-radius1 f-ml4"
                  type="primary"
                >
                  重置密码
                </Button>
              </Popconfirm>
          </div>);
      }
    }];

    return (
      <Table
          columns={ columns }
          dataSource={ data }
          pagination={ pagination }
          loading={loading}
          onChange={onChange}
          bordered
          scroll={{ x: 1750 }}
      />
    );
  }
}