import React, {
    Component
} from 'react';
import {
    Table,
    Icon,
    Popconfirm
} from 'antd';

export default class Main extends Component {
    render() {
        let {
            onEdit,
            onDelete,
            loading,
            data,
            pagination,
            onChange
        } = this.props;

        const columns = [{
            title: '权限组名称',
            width: '200px',
            dataIndex: 'name'
        }, {
            title: '权限',
            dataIndex: 'menuNames'
        }, {
            title: '状态',
            width: '50px',
            dataIndex: 'deas',
            className: "f-align-center",
            render: (value, row, index) => {
                let isDisable = row.isDisable == '0' ? "启用" : "禁用";
                return (<div>
                    {isDisable}
                </div>);
            }
        }, {
            title: '操作',
            width: '100px',
            dataIndex: 'dess',
            className: "f-align-center",
            render: (value, row, index) => {
                return (<div>
                    <Popconfirm 
                        title="确定要删除吗?" 
                        onConfirm={
                            ()=> onDelete(row)
                        }
                        okText="确定" 
                        cancelText="取消"
                    >
                        <Icon 
                            className="f-mr3 f-pointer" 
                            type="delete" 
                        />
                    </Popconfirm>
                    <Icon 
                        className="f-pointer" 
                        type="edit" 
                        onClick={ ()=> onEdit(row) }
                    />
                </div>);
            }
        }];

        return (
            <Table
                columns={ columns }
                dataSource={ data }
                pagination={ pagination }
                loading={loading}
                onChange={onChange}
                bordered
            />
        );
    }
}