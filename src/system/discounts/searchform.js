import React, {
	Component
} from 'react';
import {
	Button,
	Input,
	Form,
	Select
} from 'antd';

import RangeDate from '../../common/rangeDate.js';
import {
	discountType
} from '../../common/staticData.js';

const FormItem = Form.Item;
const Option = Select.Option;

class MainForm extends Component {
	render() {
		const {
			getFieldDecorator
		} = this.props.form;
		let {
			onSearch
		} = this.props;

		return (<header className="f-box-shadow2 f-radius1 f-bg-white f-pd4">
			<Form layout="inline">
				<FormItem
					label="时间段"
				>
					{
						getFieldDecorator('rangeTime',{
							initialValue:''
						})(
							<RangeDate 
								onSelect={
									(d)=> {
										this.props.form.setFieldsValue({
											rangeTime:d,
											startTime:d.startString,
											endTime:d.endString,
										});
									}
								}
								showTime={true}
							/>
						)
					}
				</FormItem>

				<span className="f-hide">
			        {
						getFieldDecorator('startTime',{
							initialValue:''
						})(
							<Input />
						)
					}
					{
						getFieldDecorator('endTime',{
							initialValue:''
						})(
							<Input />
						)
					}
				</span>

				<FormItem
					label="优惠类型"
				>
					{
						getFieldDecorator('type', { 
							rules: [{  
								message: '请选择优惠类型',
							}],
						})
						(
							<Select
								size="large"
								allowClear
            					style={{minWidth:'160px'}}
            					placeholder="请选择优惠类型"
							>
								{
									discountType.map((d,i)=> {
										return(<Option 
												key={i} 
												value={d.value}
											>{d.label}</Option>);
									})
								}
							</Select>
						)
					}
				</FormItem>
	        
		        <FormItem>
					<Button 
						type="primary" 
						size="large" 
						icon="search"
						onClick={
							onSearch
						}
					>搜索</Button>
		        </FormItem>

	      	</Form>
		</header>);
	}
}

const MyForm = Form.create()(MainForm);

export default MyForm;