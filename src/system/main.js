import React, {
	Component
} from 'react';
import {
	Route,
} from 'react-router-dom';
import {
	default as Layout
} from '../common/layout.js';

import {
	default as Courses
} from './courses/main'; //课程设置
import {
	default as School
} from './school/main'; //校区设置
import {
	default as ClassRoom
} from './classroom/main'; //教室设置
import {
	default as SystemLog
} from './log/main'; //系统日志
import {
	default as SystemParameter
} from './parameter/main'; //系统参数设置
import {
	default as SystemAccount
} from './account/main'; //账号设置
import {
	default as SystemGroup
} from './group/main'; //权限组设置
import {
	default as SystemHoliday
} from './holiday/main'; //节假日设置
import {
	default as SystemDiscounts
} from './discounts/main'; //优惠设置

import './main.less';

let PARENTPATH;

export default ({
	match
}) => {
	window.store.session('tMenu', {
		selectedKeys: 'xtsz'
	})

	PARENTPATH = match.path;

	return (<div>
		<Layout main={ Main } />
	</div>);
}

class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {

		}
	}
	render() {
		return (<div>
			<Route path={ PARENTPATH } exact component={ School } />
			<Route path={ `${ PARENTPATH }/course` } component={ Courses } />
			<Route path={ `${ PARENTPATH }/school` } component={ School } />
			<Route path={ `${ PARENTPATH }/classroom` } component={ ClassRoom } />
			<Route path={ `${ PARENTPATH }/preferential` } component={ SystemDiscounts } />
			<Route path={ `${ PARENTPATH }/log` } component={ SystemLog } />
			<Route path={ `${ PARENTPATH }/parameter` } component={ SystemParameter } />
			<Route path={ `${ PARENTPATH }/user` } component={ SystemAccount } />
			<Route path={ `${ PARENTPATH }/group` } component={ SystemGroup } />
			<Route path={ `${ PARENTPATH }/holiday` } component={ SystemHoliday } />
		</div>);
	}
}