import React, {
  Component
} from 'react';
import {
  Table,
  Icon,
  Popconfirm
} from 'antd';

export default class Main extends Component {
  render() {
    let {
      onEdit,
      onDelete,
      data,
      loading,
    } = this.props;

    const columns = [{
      title: '节假日名称',
      dataIndex: 'index',
      className: "f-align-center",
      width: '100px',
      render: (value, row, index) => {
        return (<span>{index+1}</span>);
      }
    }, {
      title: '节假日名称',
      dataIndex: 'name'
    }, {
      title: '时间段',
      dataIndex: 'modifyTime',
      render: (value, row, index) => {
        return (<div>
          { row.startTime }
          <span className="f-ml2 f-mr2">至</span>
          { row.endTime }
        </div>);
      }
    }, {
      title: '操作',
      dataIndex: 'des',
      className: "f-align-center",
      render: (value, row, index) => {
        return (<div>
          <Popconfirm 
            title="确定要删除吗?" 
            onConfirm={
              ()=> onDelete(row)
            }
            okText="确定" 
            cancelText="取消"
          >
            <Icon 
              className="f-mr3 f-pointer" 
              type="delete" 
            />
          </Popconfirm>
          <Icon 
            className="f-pointer" 
            type="edit" 
            onClick={ ()=> onEdit(row) }
          />
        </div>);
      }
    }];

    return (
      <Table
          columns={ columns }
          dataSource={ data }
          loading={loading}
          bordered
          pagination={false}
      />
    );
  }
}