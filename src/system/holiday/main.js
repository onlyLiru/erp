import React, {
	Component
} from 'react';
import {
	Breadcrumb,
	message,
	Icon
} from 'antd';
import {
	default as MyList
} from './list.js';
import {
	default as AddForm
} from './add.js';
import {
	fpost,
	fpostArray
} from '../../common/io.js';
import './main.less'

export default class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			visible: false,
			loading: false,
			result: null,
			defaultData: null
		}
	}
	render() {
		let {
			visible,
			result,
			loading,
			defaultData
		} = this.state;

		return (<section>
			<Breadcrumb className="f-mb3">
			    <Breadcrumb.Item>系统设置</Breadcrumb.Item>
			    <Breadcrumb.Item>
			   		<span className="f-fz5 f-bold">节假日设置</span>
			    </Breadcrumb.Item>
			</Breadcrumb>

			<div className="f-box-shadow2 f-radius1 f-bg-white f-pd4 f-mt5">
				<h3 className="f-mt5 f-mb5">
					<span className="f-title-blue f-mt3">节假日列表</span>
					<a
						onClick={ 
							this._visibleAddModal.bind(this)
						}
						className="f-btn-green f-right"
					>
						<Icon 
							className="f-fz7 f-vertical-middle f-bold" 
							type="plus" 
						/>
						新增节假日
					</a>
				</h3>

				<MyList 
					onEdit={this._edit.bind(this)}
					onDelete={this._delete.bind(this)}
					data={result}
			        loading={loading}
				/>

			</div>

			{
				visible ? 
					<AddForm
						ref={ form => this.addForm = form }
						visible={ visible }
						onSaveOk={ this._saveOk.bind(this)}
						onCancel={ this._cancel.bind(this)}
						result={ result }
						defaultData={ defaultData }
					/>
				: null
			}

		</section>);
	}
	componentDidMount() {
		this._getList();
	}
	_saveOk() {
		this._cancel();
		this._getList();
	}
	_cancel() {
		this.setState({
			visible: false,
			defaultData: null
		});
	}
	_visibleAddModal() {
		this.setState({
			visible: true
		});
	}
	_delete(param) {
		let {
			id
		} = param;
		let {
			result
		} = this.state;
		result = result.filter((d) => d.id !== id);

		fpostArray('/api/system/holiday/saveHolidays', result)
			.then(res => res.json())
			.then((res) => {
				if (!res.success) {
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res);
			})
			.then((res) => {
				this._getList();
			})
			.catch((err) => {
				console.log(err);
			});
	}
	_edit(d) {
		this.setState({
			visible: true,
			defaultData: d
		});
	}
	_getList() {
		fpost('/api/system/holiday/listHolidays ')
			.then(res => res.json())
			.then((res) => {
				if (!res.success || !res.result) {
					this.setState({
						loading: false
					});
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res.result);
			})
			.then((result) => {
				result.forEach((d) => {
					d.key = d.id
				});

				this.setState({
					result
				});
			})
			.catch((err) => {
				console.log(err);
			});
	}
}