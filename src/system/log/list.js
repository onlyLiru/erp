import React, {
  Component
} from 'react';
import {
  Table,
  Icon
} from 'antd';

export default class Main extends Component {
  render() {
    let {
      data,
      loading,
      pagination,
      onChange
    } = this.props;

    const columns = [{
      title: '登录时间',
      width: '200px',
      dataIndex: 'logTime',
      render: (value, row, index) => {
        return <div>
          <Icon type="calendar" /> { value }
        </div>
      }
    }, {
      title: '用户名',
      width: '200px',
      dataIndex: 'operator',
      render: (value, row, index) => {
        return (<div>
          <Icon type="user" /> { value }
        </div>);
      }
    }, {
      title: '操作类型',
      width: '200px',
      dataIndex: 'bizType',
      render: (value, row, index) => {
        return (<div>
          <Icon type="tags" /> { value }
        </div>);
      }
    }, {
      title: '操作描述',
      dataIndex: 'name',
      render: (value, row, index) => {
        return (<div>
          <Icon type="file-text" /> { value }
        </div>);
      }
    }];

    return (
      <div className="f-box-shadow2 f-radius1 f-bg-white f-pd4 f-mt5">
				<h3 className="f-mb5">
					<span className="f-title-blue">系统日志列表</span>
				</h3>
				<Table
            columns={ columns }
            dataSource={ data }
            pagination={ pagination }
            loading={loading}
            onChange={onChange}
				    bordered
				  />
			</div>
    );
  }
}