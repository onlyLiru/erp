import React, {
	Component
} from 'react';
import {
	Button,
	Input,
	Form,
	DatePicker,
	Select
} from 'antd';

import RangeDate from '../../common/rangeDate.js';
import {
	fpost
} from '../../common/io.js';

const FormItem = Form.Item;
const Option = Select.Option;

class MainForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			operateType: []
		}
	}
	render() {
		const {
			getFieldDecorator
		} = this.props.form;
		let {
			onSearch
		} = this.props;
		let {
			operateType
		} = this.state;

		return (<header className="f-box-shadow2 f-radius1 f-bg-white f-pd4">
			<Form layout="inline">
				<FormItem
					label="时间段"
				>
					{
						getFieldDecorator('rangeTime',{
							initialValue:''
						})(
							<RangeDate 
								onSelect={
									(d)=> {
										this.props.form.setFieldsValue({
											rangeTime:d,
											startTime:d.startString,
											endTime:d.endString,
										});
									}
								}
							/>
						)
					}
				</FormItem>

				<span className="f-hide">
			        {
						getFieldDecorator('startTime',{
							initialValue:''
						})(
							<Input />
						)
					}
					{
						getFieldDecorator('endTime',{
							initialValue:''
						})(
							<Input />
						)
					}
				</span>

				<FormItem
					label="用户名"
				>
					{
						getFieldDecorator('operator', { 
							rules: [{
								required: false, 
								message: '请输入用户名' 
							}],
							initialValue:''
						})
						(
							<Input placeholder="请输入用户名" />
						)
					}
				</FormItem>
				<FormItem
					label="操作类型"
				>
					{
						getFieldDecorator('bizCode', { 
							rules: [{  
								required: false, 
								message: '请输入操作类型',
							}],
						})
						(
							<Select 
								placeholder="请输入操作类型"
								style={{ width: 220 }}
							>
								{
				                    operateType.map((d,i)=> {
				                        return (<Option key={i} value={d.name}>{d.name}</Option>);
				                    })
				                }
							</Select>
						)
					}
				</FormItem>
	        
		        <FormItem>
					<Button 
						type="primary" 
						size="large" 
						icon="search"
						onClick={
							onSearch
						}
					>搜索</Button>
		        </FormItem>

	      	</Form>
		</header>);
	}
	componentDidMount() {
		this._getOperateTypes();
	}
	_getOperateTypes() {
		fpost('/api/common/log/findBusinessTypeDict')
			.then((res) => res.json())
			.then((res) => {
				if (!res.success || !res.result || !res.result.length) {
					throw new Error(res.message || '系统错误');
				};
				return (res.result);
			})
			.then((result) => {
				// console.log(result);
				if (result) {
					this.setState({
						operateType: result
					});
				}
			})
			.catch((err) => {
				console.log(err);
			});
	}
}

const MyForm = Form.create()(MainForm);

export default MyForm;