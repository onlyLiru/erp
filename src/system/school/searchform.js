import React, {
	Component
} from 'react';
import {
	Input,
	Button,
	Col,
	Row,
	Form,
	Select
} from 'antd';
import {
	schoolType
} from '../../common/staticData.js';
import SelectCity from '../../common/selectCity.js';
const FormItem = Form.Item;
const Option = Select.Option;

class MainForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isShowSearch: true
		}
	}
	render() {
		const {
			getFieldDecorator
		} = this.props.form;

		let {
			onSearch
		} = this.props;

		const {
			isShowSearch
		} = this.state;

		const formItemLayout = {
			labelCol: {
				span: 6
			},
			wrapperCol: {
				span: 18
			}
		};

		return (<header className="f-box-shadow2 f-radius1 f-bg-white f-pd4">
			<Form>
				{
					isShowSearch ?
					    <section>
			    			<Row gutter={ 40 }>
			    				<Col span={12}>
			    					<FormItem
			    						label="校区名称"
										{...formItemLayout}
									>
										{
											getFieldDecorator('name')
											(
												<Input 
													size="large"
													placeholder="请输入校区名称"
												/>
											)
										}
									</FormItem>
			    				</Col>
			    				<Col span={ 12 }>
									<FormItem
										label="选择类型"
										{...formItemLayout}
									>
										{
											getFieldDecorator('type', { 
												rules: [{
													required: false, 
													message: '请选择类型',
												}]
											})
											(
												<Select
													size="large"
				                					placeholder="请选择类型"
				                					allowClear
												>
													{
														schoolType.map((d,i)=> {
															return(<Option 
																	key={i} 
																	value={d.value}
																>{d.label}</Option>);
														})
													}
												</Select>
											)
										}
									</FormItem>
								</Col>
			    	        </Row>
	            			<Row gutter={ 40 }>
	            				<Col span={12}>
	            					<FormItem
										label="地区"
										{...formItemLayout}
									>
										{
											getFieldDecorator('areaCode', {
												rules: [{ 
													required: false, 
													message: '请输入地区' 
												}],
											})(
												<SelectCity 
													onSelect={
														(v) => {
															this.props.form.setFieldsValue({
																areaCode: v
															});
														}
													}
													allowClear
												/>
											)
										}
									</FormItem>
	            				</Col>
	            				<Col span={ 12 }>
	        						<FormItem
										label="详细地址"
										{...formItemLayout}
									>
										{
											getFieldDecorator('address', {
												rules: [{ 
													required: false, 
													message: '请输入详细地址' 
												}],
											})(
												<Input placeholder="请输入详细地址" />
											)
										}
									</FormItem>
	        					</Col>
	            	        </Row>
			    	        <div className="f-align-center">
			    	        	<Button 
			    	        		size="large" 
			    	        		className="f-mr4" 
			    	        		onClick={ this._reset.bind(this) }
			    	        	>重置</Button>
			    	        	<Button 
			    	        		type="primary"
			    	        		size="large" 
			    	        		icon="search" 
			    	        		onClick={ onSearch }
			    	        	>查询</Button>
			    	        </div>
		    	        </section>
	    	        : null
				}
	      	</Form>
		</header>);
	}
	_reset() {
		this.props.form.resetFields();
		this.setState({
			isShowSearch: false
		}, () => {
			this.setState({
				isShowSearch: true
			}, () => {
				this.props.onSearch();
			})
		});
	}
}

const MyForm = Form.create()(MainForm);

export default MyForm;