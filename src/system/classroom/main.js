import React, {
	Component
} from 'react';
import {
	Icon,
	Breadcrumb,
	message,
	Pagination
} from 'antd';
import {
	default as SearchForm
} from './searchform.js';
import {
	default as MyList
} from './list.js';
import {
	default as AddForm
} from './add.js';
import {
	fpost
} from '../../common/io.js';
import {
	Loading,
	NoData,
	undefinedToEmpty
} from '../../common/g.js';
import './main.less'

export default class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			visible: false,
			loading: false,
			result: null,
			defaultData: null,

			total: 0,
			currentPage: 1,
			pageSize: 12
		}
	}
	render() {
		let {
			visible,
			result,
			loading,
			defaultData,
			currentPage,
			pageSize,
			total
		} = this.state;

		return (<section>
			<Breadcrumb className="f-mb3">
			    <Breadcrumb.Item>系统设置</Breadcrumb.Item>
			    <Breadcrumb.Item>基础设置</Breadcrumb.Item>
			    <Breadcrumb.Item>
			   		<span className="f-fz5 f-bold">教室设置</span>
			    </Breadcrumb.Item>
			</Breadcrumb>

			<SearchForm 
				ref={ form => this.searchForm = form }
				onSearch= { this._search.bind(this) }
			/>

			<h3 className="f-mt5 f-mb5">
				<span className="f-title-blue f-mt3">教室列表 ({ (total) }）</span>
				<a 
				onClick={
					()=> {
						this.setState({
							visible:!this.state.visible
						});
					}
				}
				className="f-btn-green f-right">
					<Icon className="f-fz7 f-vertical-middle f-bold" type="plus" />新增教室
				</a>
			</h3>
			{
				loading 
				? 
				<Loading /> 
				:
				<div>
					{
						(!result || result.length<=0) 
						? 
						<NoData /> 
						:
						<div>
							<MyList 
								onEdit={this._edit.bind(this)}
								onDelete={this._delete.bind(this)}
								data={result}
							/>
							{
								total ?
									<div className="f-clear">
										<Pagination 
											onChange={
												(page,pageSize)=>{
													console.log(pageSize);
													this.setState({
														currentPage: page
													}, () => {
														this._getList();
													});
												}
											} 
											onShowSizeChange={
												(current,size)=> {
													this.setState({
														currentPage:1,
														pageSize:size
													},this._getList);
												}
											}
											className="f-right" 
											total={total}
											pageSize={pageSize}
											current={currentPage}
											showSizeChanger
											showQuickJumper
											pageSizeOptions={ ['12','24','36','48'] }
										/>
									</div>
								: null
							}
						</div>
					}
				</div>
			}
			{
				visible ? 
					<AddForm
						ref={ form => this.addForm = form }
						visible={ visible }
						onSaveOk={ this._saveOk.bind(this)}
						onCancel={ this._cancel.bind(this)}
						defaultData={ defaultData }
					/>
				: null
			}

		</section>);
	}
	componentDidMount() {
		this._getList();
	}
	_search(e) { //搜索
		e.preventDefault();
		this.searchForm.validateFields((err, values) => {
			if (!err) {
				/*如果值为undefined则替换为空*/
				values = undefinedToEmpty(values);
				this._getList(values);
			}
		});
	}
	_saveOk() {
		this._cancel();
		this._getList();
	}
	_cancel() {
		this.setState({
			visible: false,
			defaultData: null
		});
	}
	_visibleAddModal() {
		this.setState({
			visible: true
		});
	}
	_delete(param) {
		fpost('/api/system/schoolarea/deleteClassRoom', param)
			.then(res => res.json())
			.then((res) => {
				if (!res.success) {
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res);
			})
			.then((res) => {
				this._getList();
			})
			.catch((err) => {
				console.log(err);
			});
	}
	_edit(d) {
		this.setState({
			visible: true,
			defaultData: d
		});
	}
	_getList(param = {}) {
		let {
			currentPage,
			pageSize
		} = this.state;

		param = {
			...param,
			currentPage: currentPage,
			pageSize: pageSize
		}

		fpost('/api/system/schoolarea/pageClassRoom', param)
			.then(res => res.json())
			.then((res) => {
				if (!res.success || !res.result) {
					this.setState({
						loading: false
					});
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res.result);
			})
			.then((result) => {
				this.setState({
					result: result.records,
					total: parseInt(result.total)
				});
			})
			.catch((err) => {
				console.log(err);
			});
	}
}