import React, {
	Component
} from 'react';
import {
	Table,
	Icon,
	Button,
	Popconfirm
} from 'antd';

export default class Main extends Component {
	constructor(props) {
		super(props)
		this.state = {

		}
	}
	render() {
		let {
			data,
			removeType,
			removeCourse,
			editCourse,
			editType
		} = this.props;

		const columns = [{
			key: 1,
			title: "序号",
			width: "60px",
			className: 'f-align-center',
			render: (value, row, index) => {
				return {
					children: <div>{ index+1 }</div>
				}
			}
		}, {
			key: 2,
			title: "课程大类",
			colSpan: 2,
			width: "300px",
			render: (value, row, index) => {
				return {
					children: <div>{ value.name }</div>,
					props: {
						colSpan: 1
					}
				}
			}
		}, {
			key: 3,
			colSpan: 0,
			width: "100px",
			className: 'f-align-center',
			render: (value, row, index) => {
				return {
					children: <div>
						<Popconfirm 
							title="确定要删除吗?" 
							onConfirm={ ()=> {
								removeType(value);
							} }
							okText="确定" 
							cancelText="取消"
						>
							<Button icon="delete">删除</Button>
						</Popconfirm>
						<p className="f-mb3" />
						<Button 
							type="primary" icon="edit"
							onClick={()=> {
								editType(value);
							}}
						>
							编辑
						</Button>
					</div>
				}
			}
		}, {
			key: 4,
			title: "课程",
			className: 'f-align-center',
			render: (value, row, index) => {
				return {
					children: <div className="courses-table-outer">
					<table className="courses-table"> 
						<tbody>
							{ 
								value.courseList.map((d,i)=> {
									return <tr key={i}>
										<td className="f-pl4">{ d.name }</td>
										<td width="100" className="f-align-center f-fz4">
											<Popconfirm 
												title="确定要删除吗?" 
												onConfirm={ ()=> {
													removeCourse(d);
												} }
												okText="确定" 
												cancelText="取消"
											>
												<Icon 
													className="f-mr3 f-pointer" 
													type="delete"
												/>
											</Popconfirm>
											<Icon 
												className="f-pointer" 
												type="edit"
												onClick= {
													()=> {
														editCourse(d);
													}
												} 
											/>
										</td></tr>
									;
								})
							}
						</tbody>
					</table></div>
				}
			}
		}];

		return (
			<Table
    		columns={ columns } 
    		dataSource={ data } 
    		pagination={ false }
    		bordered />
		)
	}
}