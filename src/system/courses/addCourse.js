import React, {
	Component
} from 'react';
import {
	Input,
	Modal,
	Form,
	message,
	InputNumber
} from 'antd';

import SelectCourseType from '../../common/selectCourseType.js';
import {
	fpost
} from '../../common/io.js';

const FormItem = Form.Item;

export default class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			saveing: false, //是否正在保存
		}
	}
	render() {
		let {
			visibleType,
			onCancel,
			defaultData
		} = this.props;
		let {
			saveing
		} = this.state;
		let modalTitle = defaultData ? '编辑' : '新增';
		return (<Modal title={`${modalTitle}课程`}
				visible={ visibleType }
				onOk={ this._validator.bind(this) }
				confirmLoading={ saveing }
				onCancel={ onCancel }
				width="400px"
			>
				<MyForm
					ref={ form => this.form=form }
					defaultData={ defaultData }
				/>
			</Modal>);
	}
	_validator(e) {
		e.preventDefault();
		this.form.validateFields((err, values) => {
			if (!err) {
				this._save(values);
			}
		});
	}
	_save(values) {
		let {
			onOk
		} = this.props;

		this.setState({
			saveing: true
		});

		fpost('/api/system/course/saveCourse', values)
			.then(res => res.json())
			.then((res) => {
				if (!res.success) {
					this.setState({
						saveing: false
					});
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res);
			})
			.then((res) => {
				onOk();
			})
			.catch((err) => {
				this.setState({
					saveing: false
				});
				console.log(err);
			});
	}
}

class MainForm extends React.Component {
	render() {
		const {
			getFieldDecorator
		} = this.props.form;

		let {
			defaultData
		} = this.props;

		const formItemLayout = {
			labelCol: {
				span: 6
			},
			wrapperCol: {
				span: 17
			}
		};

		return (
			<Form>
				<FormItem
					label="选择大类"
					{...formItemLayout}
				>
					{
						getFieldDecorator('subjectId', { 
							rules: [{
								required: true, 
								message: '请选择选择大类' 
							}] 
						})
						(
							<SelectCourseType 
								onSelect= {
									(v)=> {
										this.props.form.setFieldsValue({
											subjectId:v
										});
									}
								}
								initialValueData={ 
									defaultData && defaultData['subjectId']  ?
									defaultData['subjectId'] : null
								}
							/>
						)
					}
				</FormItem>
				<FormItem
					label="课程名称"
					{...formItemLayout}
				>
					{
						getFieldDecorator('name', { 
							rules: [{
								required: true, 
								message: '请输入课程名称' 
							}] 
						})
						(
							<Input placeholder="请输入课程名称" />
						)
					}
				</FormItem>
				<FormItem
					label="费用/期"
					{...formItemLayout}
				>
					{
						getFieldDecorator('regularPrice', { 
							rules: [{
								required: true, 
								message: '请输入费用/期' 
							}] 
						})
						(
							<InputNumber min={0} />
						)
					}
				</FormItem>
				<FormItem
					label="总课时/期"
					{...formItemLayout}
				>
					{
						getFieldDecorator('regularPeriods', { 
							rules: [{
								required: true, 
								message: '请输入总课时/期' 
							}] 
						})
						(
							<InputNumber min={0} />
						)
					}
				</FormItem>
				<FormItem
					label="总课次/期"
					{...formItemLayout}
				>
					{
						getFieldDecorator('regularTimes', { 
							rules: [{
								required: true, 
								message: '请输入总课次/期' 
							}] 
						})
						(
							<InputNumber min={0} />
						)
					}
				</FormItem>

				{
					defaultData ?
						<span className="f-hide">
							{
								getFieldDecorator('id')
								(
									<Input />
								)
							}
						</span>
					: null
				}

	      	</Form>
		);
	}
	componentDidMount() {

		if (this.props.defaultData) { //如果是编辑，设置默认值
			let {
				defaultData
			} = this.props;

			this.props.form.setFieldsValue(defaultData);
		}

	}
}

const MyForm = Form.create()(MainForm);