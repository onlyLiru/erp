import React, {
	Component
} from 'react';
import store from 'store2';
import querystring from 'querystring';
import {
	Breadcrumb,
	message,
	Row,
	Col
} from 'antd';
import {
	default as TeachersList
} from './teachersList.js';
import {
	fpost,
	host
} from '../../common/io.js';
import {
	getUrlParam,
	Loading
} from '../../common/g.js';

let sessionId = store.get('sessionId');

export default class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loading: false,
			visibleAddMember: false,
			result: null,
			pagination: {
				showSizeChanger: true,
				showQuickJumper: true,
				total: 1,
			},
			pageSize: 10,
			currentPage: 1,
			total: 1,
			searchParma: {}
		}
	}
	render() {
		let {
			result,
			loading,
			pagination
		} = this.state;

		if (loading || !result) {
			return <Loading />;
		}

		return (<section>
			<Breadcrumb className="f-mb3">
			    <Breadcrumb.Item>教辅平台</Breadcrumb.Item>
			    <Breadcrumb.Item>
			   		<span className="f-fz5 f-bold">教师列表</span>
			    </Breadcrumb.Item>
			</Breadcrumb>

			<TeachersList 
				data={result}
		        loading={loading}
		        deleteMember={this._deleteMember.bind(this)}
		        addMember={this._addMember.bind(this)}
		        visibleAddMember={this.state.visibleAddMember}
			/>

		</section>);
	}
	componentDidMount() {
		this._getList();
	}
	_getList(pager = {}) {
		this.setState({
			loading: true
		});

		fpost('/api/auxiliary/teacher/listTeacher', {
				curPage: 1,
				pageSize: 10000
			})
			.then((res) => {
				console.log(res);
				return res.json();
			})
			.then((res) => {
				if (!res.success || !res.result) {
					this.setState({
						loading: false
					});
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res.result.records);
			})
			.then((result) => {

				this.setState({
					result: result,
					loading: false
				}, () => {
					console.log(this.state)
				});
			})
			.catch((err) => {
				console.log(err);

				this.setState({
					loading: false
				});
			});
	}
	_deleteMember(data) {
		fpost('/api/auxiliary/teacher/delTeacher', data)
			.then((res) => {
				console.log(res);
				return res.json();
			})
			.then((res) => {
				if (!res.success) {
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res.result);
			})
			.then((result) => {

				this._getList();
			})
			.catch((err) => {

			});
	}
	_addMember(values) {

		fpost('/api/auxiliary/teacher/addTeacher', values)
			.then((res) => {
				console.log(res);
				return res.json();
			})
			.then((res) => {
				if (!res.success) {
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res.result);
			})
			.then((result) => {
				this.setState({
					visibleAddMember: false
				});
				this._getList();
			})
			.catch((err) => {

			});
	}
}