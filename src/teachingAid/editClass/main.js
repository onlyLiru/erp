import React, {
	Component
} from 'react';
import {
	Breadcrumb,
	message,
	Row,
	Col,
	Form,
	Input,
	Button
} from 'antd';
import {
	fpost,
	host
} from '../../common/io.js';
import RangeDate from '../../common/rangeDate.js';
import {
	default as SelectPerson
} from '../../common/selectPerson.js';
import SelectSchool from '../../common/selectSchool.js';
import SelectCourses from '../../common/selectCourses.js';

import './main.less';

const FormItem = Form.Item;

class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {

		}
	}
	render() {
		const {
			getFieldDecorator,
			getFieldsError,
			getFieldError,
			isFieldTouched
		} = this.props.form;

		let {
			defaultData
		} = this.state;

		const formItemLayout = {
			labelCol: {
				span: 8
			},
			wrapperCol: {
				span: 16
			}
		};

		return (<section>
			<Breadcrumb className="f-mb3">
			    <Breadcrumb.Item>教辅平台</Breadcrumb.Item>
			    <Breadcrumb.Item>班级列表</Breadcrumb.Item>
			    <Breadcrumb.Item>
			   		<span className="f-fz5 f-bold">编辑班级信息</span>
			    </Breadcrumb.Item>
			</Breadcrumb>

			<div className="f-box-shadow2 f-radius1 f-bg-white f-over-hide">
				<Form>
					<Row type="flex">
						<Col span={6}>
							<div className="f-bg-blue f-relative lftBar">
								<div className="classIconBox">
									<img src="http://img.fancyedu.com/sys/ic/operation/1509585104658_newClassIcon.png" />
									<h3 className="f-white f-pd2">班级图标</h3>
								</div>
								<div className="formBox">
									<FormItem
										label="所在校区"
										{...formItemLayout}
									>
										{
											getFieldDecorator('userName', {
												rules: [{ 
													required: true, 
													message: '请选择校区' 
												}],
											})(
												<SelectSchool 
													onSelect={
														(v) => {
															this.props.form.setFieldsValue({
																schoolAreaId:v
															});
														}
													}
													placeholder="请选择校区"
													width='100%' 
													url="/api/system/schoolarea/listSchoolArea"
													initialValueData={ 
														defaultData && defaultData['schoolAreaId']  ?
														defaultData['schoolAreaId'] : null
													}
												/>
											)
										}
							        </FormItem>
							        <FormItem
										label="课程名称"
										{...formItemLayout}
									>
										{
											getFieldDecorator('userName', {
												rules: [{ 
													required: true, 
													message: '请输入课程名称' 
												}],
											})(
												<SelectCourses
													courseTypeId={1}
													onSelect= {
														(id,name)=> {
															this.props.form.setFieldsValue({
																courseData:{id,name}
															});
														}
													}
												/>
											)
										}
							        </FormItem>
							        <FormItem
										label="班级名称"
										{...formItemLayout}
									>
										{
											getFieldDecorator('userName', {
												rules: [{ 
													required: true, 
													message: 'Please input your username!' 
												}],
											})(
												<Input />
											)
										}
							        </FormItem>
								</div>
								<img style={{width:'100%',position:'absolute',bottom:'0'}} src="http://img.fancyedu.com/sys/ic/operation/1515246332465_b.png" />
							</div>
						</Col>
						<Col span={18}>
							<div className="f-pd4">
								<Row gutter={24}>
									<Col span={10}>
		            					<FormItem
		        							label="开班时间"
		        						>
		        							{
		        								getFieldDecorator('rangeTime',{
		        									rules: [{ 
														required: true, 
														message: '请选择开班时间' 
													}],
		        									initialValue:''
		        								})(
		        									<RangeDate 
		        										onSelect={
		        											(d)=> {
		        												this.props.form.setFieldsValue({
		        													rangeTime:d,
		        													beginTime:d.startString,
		        													endTime:d.endString,
		        												});
		        											}
		        										}
		        										showTime={true}
		        									/>
		        								)
		        							}
		        						</FormItem>
	        							<span className="f-hide">
	        						        {
	        									getFieldDecorator('beginTime',{
	        										initialValue:''
	        									})(
	        										<Input />
	        									)
	        								}
	        								{
	        									getFieldDecorator('endTime',{
	        										initialValue:''
	        									})(
	        										<Input />
	        									)
	        								}
	        							</span>
									</Col>
									<Col span={10}>
								        <FormItem
								        	label="班主任"
								        >
								        	{
								        		getFieldDecorator('salesId', {
								        			rules: [{  
								        				required:true,
								        				message:'请选择班主任'
								        			}],
								        			initialValue:''
								        		})(
								        			<SelectPerson 
								        				onSelect={
								        					(v)=> {
								        						this.props.form.setFieldsValue({
								        							salesId: v
								        						});
								        					}
								        				}
								        				initialValueData=''
								        			/>
								        		)
								        	}
								        </FormItem>
									</Col>
									<Col span={4}>
										<FormItem
											label="额定人数"
										>
											{
												getFieldDecorator('count', {
													rules: [{ 
														required: true, 
														message: '请输入额定人数' 
													}],
												})(
													<Input addonAfter="人" />
												)
											}
								        </FormItem>
									</Col>
								</Row>
								<FormItem
									label="班级描述"
								>
									{
										getFieldDecorator('userName', {
											rules: [{ 
												required: true, 
												message: '请输入班级描述' 
											}],
										})(
											<Input.TextArea placeholder="请输入" />
										)
									}
						        </FormItem>
						        <div className="f-align-right">
						        	<Button 
						        		type="primary"
						        		size="large"
						        		onClick={
						        			()=> {
						        				this.props.form.validateFields((err, values) => {
						        					if (!err) {
						        						
						        					}
						        					console.log(values);
						        				});
						        			}
						        		}
						        	>保存</Button>
						        </div>
							</div>
						</Col>
					</Row>
				</Form>
			</div>

		</section>);
	}
	componentDidMount() {

	}

}

export default Form.create()(Main);