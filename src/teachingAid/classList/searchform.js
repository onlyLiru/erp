import React, {
	Component
} from 'react';
import $ from 'jquery';
import {
	Input,
	Button,
	Icon,
	Col,
	Row,
	Form,
	DatePicker,
	Select
} from 'antd';
import RangeDate from '../../common/rangeDate.js';
import {
	default as SelectPerson
} from '../../common/selectPerson.js';
import SelectCourses from '../../common/selectCourses.js';
import {
	gradeList,
	fromData
} from '../../common/staticData.js';
import {
	undefinedToEmpty
} from '../../common/g.js';
const FormItem = Form.Item;
const Option = Select.Option;

class MainForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isAdvancedSearch: false
		}
	}
	render() {
		const {
			getFieldDecorator
		} = this.props.form;

		const {
			isAdvancedSearch
		} = this.state;

		const formItemLayout = {
			labelCol: {
				span: 6
			},
			wrapperCol: {
				span: 18
			}
		};

		return (<header className="f-box-shadow2 f-radius1 f-bg-white f-pd4">
			<ul id="J-searchTab" className="f-pale f-pointer searchTab">
				<li data-type={0} className="f-blue cur"><Icon type="search" className="f-mr1 f-fz4" />精确查找</li>
				<li className="f-ml5 f-mr5">|</li>
				<li  data-type={1}><Icon type="filter" className="f-mr1 f-fz4" />高级筛选</li>
			</ul>
			<Form>
				{
					!isAdvancedSearch ? //精确搜索
						<section>
							<Row gutter={ 40 }>
	            				<Col span={8}>
	            					<FormItem
										label="班级名称"
										{...formItemLayout}
									>
										{
											getFieldDecorator('className', { 
												rules: [{
													required: false, 
													message: '请输入班级名称' 
												}] 
											})
											(
												<Input placeholder="请输入班级名称" />
											)
										}
									</FormItem>
	            				</Col>
	            				<Col span={ 8 }>
	        						<FormItem>
										<Button 
											type="primary"
											htmlType="submit"
											size="large" 
											icon="search" 
											onClick={ this._search.bind(this) }
										>搜索</Button>
							        </FormItem>
	        					</Col>
	            	        </Row>

				        </section>
				    ://高级搜索
				    <section>
		    			<Row gutter={ 40 }>
		    				<Col span={10}>
            					<FormItem
									label="班级名称"
									{...formItemLayout}
								>
									{
										getFieldDecorator('name', { 
											rules: [{
												required: false, 
												message: '请输入学员姓名' 
											}] 
										})
										(
											<Input placeholder="请输入学员姓名" />
										)
									}
								</FormItem>
            				</Col>
		    				<Col span={ 7 }>
								<FormItem
									label="课程名称"
									{...formItemLayout}
								>
									{
										getFieldDecorator('grade', {
											rules: [{  
												message: '请选择' 
											}]
										})(
											<Select
												size="large"
												allowClear
			                					placeholder="请选择"
											>
												{
													gradeList.map((d,i)=> {
														return(<Option 
																key={i} 
																value={d.value}
															>{d.label}</Option>);
													})
												}
											</Select>
										)
									}
								</FormItem>
							</Col>
							<Col span={7}>
		    					<FormItem
									label="班主任"
									{...formItemLayout}
								>
									{
										getFieldDecorator('source', {
											rules: [{  
												message: '请选择' 
											}]
										})(
											<SelectPerson
												onSelect={ 
													(v)=> {
														v = v ? v : null;
														this.props.form.setFieldsValue({
															salesId: v
														});
													} 
												}
											/>
										)
									}
								</FormItem>
		    				</Col>
		    	        </Row>
		    	        <Row gutter={ 40 }>
		    				<Col span={10}>
		    					<FormItem
									label="开班时间"
									{...formItemLayout}
								>
									{
										getFieldDecorator('rangeTime',{
											initialValue:''
										})(
											<RangeDate 
												onSelect={
													(d)=> {
														this.props.form.setFieldsValue({
															rangeTime:d,
															beginTime:d.startString,
															endTime:d.endString,
														});
													}
												}
												showTime={true}
											/>
										)
									}
								</FormItem>
								<span className="f-hide">
							        {
										getFieldDecorator('beginTime',{
											initialValue:''
										})(
											<Input />
										)
									}
									{
										getFieldDecorator('endTime',{
											initialValue:''
										})(
											<Input />
										)
									}
								</span>
		    				</Col>
		    				<Col span={ 7 }>
								<FormItem
									label="教室"
									{...formItemLayout}
								>
									{
										getFieldDecorator('salesId', {
											rules: [{  
												message: '请选择' 
											}]
										})(
											<Select
												size="large"
												allowClear
			                					placeholder="请选择"
											>
												{
													fromData.map((d,i)=> {
														return(<Option 
																key={i} 
																value={d.value}
															>{d.label}</Option>);
													})
												}
											</Select>
										)
									}
								</FormItem>
							</Col>
		    	        </Row>
		    	        <div className="f-align-center">
		    	        	<Button 
		    	        		size="large" 
		    	        		className="f-mr4" 
		    	        		onClick={ this._reset.bind(this) }
		    	        	>重置</Button>
		    	        	<Button 
		    	        		type="primary"
		    	        		size="large" 
		    	        		icon="search" 
		    	        		onClick={ this._search.bind(this) }
		    	        	>查询</Button>
		    	        </div>
	    	        </section>
				}
	      	</Form>
		</header>);
	}
	componentDidMount() {
		this._changeSearchType();
	}
	_changeSearchType() {
		const self = this;
		$('#J-searchTab li').on('click', function(e) {
			let type = $(this).attr('data-type');
			let isAdvancedSearch = type == 1 ? true : false;

			$(this).addClass('cur f-blue').siblings().removeClass('cur f-blue');

			self.setState({
				isAdvancedSearch
			});
		});
	}
	_search(e) { //搜索
		let {
			onSearch
		} = this.props;

		this.props.form.validateFields((err, values) => {
			if (!err) {
				/*如果值为undefined则替换为空*/
				values = undefinedToEmpty(values);

				onSearch(values);
			}
		});
	}
	_reset() {
		this.props.form.resetFields();
		this.setState({
			isAdvancedSearch: false
		}, () => {
			this.setState({
				isAdvancedSearch: true
			}, () => {
				this._search();
			})
		});
	}
}

const MyForm = Form.create()(MainForm);

export default MyForm;