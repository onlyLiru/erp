import React, {
  Component
} from 'react';
import {
  Table,
  Icon,
  Dropdown,
  Menu,
  Button,
  message,
  Popconfirm,
  Modal,
  Input,
  Form
} from 'antd';
import {
  Link
} from 'react-router-dom';
import {
  fpost,
  host
} from '../../common/io.js';
import {
  getUrlParam
} from '../../common/g.js';

const FormItem = Form.Item;
const {
  TextArea
} = Input;

export default class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: this.props.visibleAddMember,
      saveing: false,
    }
  }
  render() {

    let {
      data,
      loading,
      deleteMember,
      addMember
    } = this.props;

    if (!data || !data.students) {
      return null
    }

    const columns = [{
      title: '姓名',
      dataIndex: 'name',
      render: (value, row, index) => {
        let {
          name,
          stuNo
        } = row;
        return <div 
          title={`${name}`}
        >
          { name }
        </div>
      }
    }, {
      title: '头像',
      dataIndex: 'imageUrl',
      className: 'f-align-center',
      width: '76px',
      render: (value, row, index) => {
        let {
          headImgUrl
        } = row;
        return (<div className="f-list-avatar">
            <img src= {value}/> 
          </div>);
      }
    }, {
      title: '手机号',
      dataIndex: 'mobile',
      className: 'f-align-center',
    }, {
      title: '操作',
      dataIndex: 'des',
      width: '200px',
      className: "f-align-center",
      render: (value, row, index) => {
        let {
          relationId
        } = row;
        return (<div>
          <Popconfirm 
              title="确定要移出班级吗?" 
              onConfirm={
                  ()=> {
                    deleteMember({
                      relationId,
                      type:0
                    })
                  }
              }
              okText="确定" 
              cancelText="取消"
          >
            <Button>移出班级</Button>
          </Popconfirm>
        </div>);
      }
    }];

    return (
      <div className="f-box-shadow2 f-radius1 f-bg-white f-pd4 f-mt5">

          <h3 className="f-mt5 f-mb5">
            <span className="f-title-blue f-mt3">成员列表</span>
            <a 
            onClick={
              ()=> {
                this.setState({
                  visible:!this.state.visible
                });
              }
            }
            className="f-btn-green f-right">
              <Icon className="f-fz7 f-vertical-middle f-bold" type="plus" />添加成员
            </a>
          </h3>

          <Modal title="添加成员"
            visible={ this.state.visible }
            onOk={ ()=> {
              this.form.validateFields((err, values) => {
                if (!err) {
                  addMember(values);
                }
              });
              
            } }
            confirmLoading={ this.state.saveing }
            onCancel={ ()=> this.setState({visible:false}) }
          >
            <MyForm 
               ref={ form=> this.form=form }
            />
         </Modal>


         <Table
            columns={ columns }
            dataSource={ data.students }
            pagination={ false }
            loading={loading}
            bordered
         />

      </div>
    );
  }

}


class MainForm extends React.Component {
  render() {
    const {
      getFieldDecorator
    } = this.props.form;
    let {
      defaultData
    } = this.props;

    const formItemLayout = {
      labelCol: {
        span: 5
      },
      wrapperCol: {
        span: 18
      }
    };

    return (
      <Form>
            <FormItem
               label="输入手机号"
               {...formItemLayout}
            >
               {
                  getFieldDecorator('mobile', { 
                     rules: [{ 
                        message: '电话号码输入有误',
                        pattern: /^1[0-9]\d{4,9}$/ 
                      },{
                        required: true, 
                        message: '请输入手机号' 
                      }] 
                  })
                  (
                     <div>
                        <Input size="large" placeholder="请输入" /> 
                        <p className="f-pale">TIPS:将发送短信通知受邀人员</p>
                     </div>
                  )
               }
            </FormItem>
         </Form>
    );
  }
  componentDidMount() {
    let {
      defaultData
    } = this.props;

    if (defaultData) { //如果是编辑，设置默认值
      let {
        name,
        id
      } = this.props.defaultData;

      this.props.form.setFieldsValue({
        name,
        id
      });
    }

  }
}

const MyForm = Form.create()(MainForm);