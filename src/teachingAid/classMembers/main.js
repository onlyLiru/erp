import React, {
	Component
} from 'react';
import store from 'store2';
import querystring from 'querystring';
import {
	Breadcrumb,
	message,
	Row,
	Col
} from 'antd';
import {
	default as TeachersList
} from './teachersList.js';
import {
	default as StudentsList
} from './studentsList.js';
import {
	fpost,
	host
} from '../../common/io.js';
import {
	getUrlParam,
	Loading
} from '../../common/g.js';

let sessionId = store.get('sessionId');

export default class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loading: false,
			visibleAddMember: false,
			result: null,
			pagination: {
				showSizeChanger: true,
				showQuickJumper: true,
				total: 1,
			},
			pageSize: 10,
			currentPage: 1,
			total: 1,
			searchParma: {}
		}
	}
	render() {
		let {
			result,
			loading,
			pagination
		} = this.state;

		if (loading || !result) {
			return <Loading />;
		}

		return (<section>
			<Breadcrumb className="f-mb3">
			    <Breadcrumb.Item>教辅平台</Breadcrumb.Item>
			    <Breadcrumb.Item>班级列表</Breadcrumb.Item>
			    <Breadcrumb.Item>
			   		<span className="f-fz5 f-bold">查看班级成员</span>
			    </Breadcrumb.Item>
			</Breadcrumb>

			<div className="f-box-shadow2 f-radius1 f-bg-white f-pd4 f-mt5 f-over-hide">
				<Row gutter={24} type="flex">
					<Col>
						<p className="f-mb2">班级名称</p>
						<h3>{result.className}</h3>
					</Col>
					<Col>
						<p className="f-mb2">教材名称</p>
						<h3>{result.lessonName}</h3>
					</Col>
					<Col>
						<p className="f-mb2">班级成员数</p>
						<h3>{result.totalCount}</h3>
					</Col>
				</Row>
			</div>

			<TeachersList 
				data={result}
		        loading={loading}
		        deleteMember={this._deleteMember.bind(this)}
			/>
			<StudentsList 
				data={result}
		        loading={loading}
		        deleteMember={this._deleteMember.bind(this)}
		        addMember={this._addMember.bind(this)}
		        visibleAddMember={this.state.visibleAddMember}
			/>

		</section>);
	}
	componentDidMount() {
		this._getList();
	}
	_getList(pager = {}) {
		this.setState({
			loading: true
		});

		fpost('/api/auxiliary/class/listClassMemberByClassId', {
				classId: getUrlParam('classId')
			})
			.then((res) => {
				console.log(res);
				return res.json();
			})
			.then((res) => {
				if (!res.success || !res.result) {
					this.setState({
						loading: false
					});
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res.result);
			})
			.then((result) => {

				this.setState({
					result: result,
					loading: false
				}, () => {
					console.log(this.state)
				});
			})
			.catch((err) => {
				console.log(err);

				this.setState({
					loading: false
				});
			});
	}
	_deleteMember(data) {
		fpost('/api/auxiliary/class/removeMemberById', data)
			.then((res) => {
				console.log(res);
				return res.json();
			})
			.then((res) => {
				if (!res.success) {
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res.result);
			})
			.then((result) => {

				this._getList();
			})
			.catch((err) => {

			});
	}
	_addMember(values) {
		values = { ...values,
			classId: getUrlParam('classId')
		}
		fpost('/api/auxiliary/class/addClassMemberByMobile', values)
			.then((res) => {
				console.log(res);
				return res.json();
			})
			.then((res) => {
				if (!res.success) {
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res.result);
			})
			.then((result) => {
				this.setState({
					visibleAddMember: false
				});
				// this._getList();
			})
			.catch((err) => {

			});
	}
}