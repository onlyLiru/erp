import React, {
	Component
} from 'react';
import {
	Form,
	Input,
	Button
} from 'antd';

const FormItem = Form.Item;

class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {

		}
	}
	render() {
		const {
			getFieldDecorator,
			getFieldsError,
			getFieldError,
			isFieldTouched
		} = this.props.form;
		let {
			onSearch
		} = this.props;

		return (<section>
			<Form
				layout="inline"
			>
				<FormItem>
					{
						getFieldDecorator('userName', {
							rules: [{ 
								required: true, 
								message: '请选择校区' 
							}],
						})(
							<Input placeholder="搜索系列名称" style={{width:'400px'}} />
						)
					}
		        </FormItem>
				<FormItem>
					<Button 
						type="primary"
						htmlType="submit"
						size="large" 
						icon="search" 
						onClick={ onSearch }
					>搜索</Button>
		        </FormItem>
			</Form>
		</section>);
	}
}

export default Form.create()(Main);