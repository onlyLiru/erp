import React, {
	Component
} from 'react';
import {
	Breadcrumb,
	Form,
	Input,
	Button,
	Icon,
	Row,
	Col,
	Card,
	Popconfirm,
	message,
	Upload,
	Select,
	Radio
} from 'antd';
import {
	fpost,
	host
} from '../../../common/io.js';
import {
	undefinedToEmpty
} from '../../../common/g.js';
import '../main.less';

const FormItem = Form.Item;
const Option = Select.Option;
const {
	TextArea
} = Input;

class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			visible: true
		}
		this._submmit = this._submmit.bind(this);
	}
	render() {
		let {
			visible
		} = this.state;

		const {
			getFieldDecorator,
			getFieldsError,
			getFieldError,
			isFieldTouched
		} = this.props.form;

		const imageUrl = this.state.imageUrl;

		return (<Form
				layout="inline"
			>
			<Breadcrumb className="f-mb3">
			    <Breadcrumb.Item>教辅平台</Breadcrumb.Item>
			    <Breadcrumb.Item>题库</Breadcrumb.Item>
			    <Breadcrumb.Item>
			   		<span className="f-fz5 f-bold">新增加题目</span>
			    </Breadcrumb.Item>
			</Breadcrumb>

			
			<div className="f-box-shadow2 f-radius1 f-bg-white f-pd4 f-mb5">
				<div>
					<FormItem
						label="选择教材"
					>
						{
							getFieldDecorator('userName', {
								rules: [{ 
									required: true, 
									message: '请选择系列' 
								}],
							})(
								<Input placeholder="选择系列" style={{width:'200px'}} />
							)
						}
			        </FormItem>
			        <FormItem
						label="选择单元"
					>
						{
							getFieldDecorator('userName', {
								rules: [{ 
									required: true, 
									message: '请选择系列' 
								}],
							})(
								<Input placeholder="选择系列" style={{width:'200px'}} />
							)
						}
			        </FormItem>
			        <FormItem
						label="选择题型"
					>
						{
							getFieldDecorator('userName', {
								rules: [{ 
									required: true, 
									message: '请选择系列' 
								}],
							})(
								<Input placeholder="选择系列" style={{width:'200px'}} />
							)
						}
			        </FormItem>
			        <div className="f-pd2 f-mb2" style={{borderBottom:'solid 1px #ccc'}} />
		        </div>
				<h3 className="f-mb5">
					<span className="f-title-blue f-mt3">题目内容</span>
				</h3>
				<Row gutter={24}>
					<Col span="3">
						题干内容(音频) :
					</Col>
					<Col span="21">
						<Upload
							className="img-uploader"
							name="avatar"
							showUploadList={false}
							action="//jsonplaceholder.typicode.com/posts/"
							beforeUpload={
								()=> {
									console.log('beforeUpload');
								}
							}
							onChange={
								()=> {
									console.log('change');
								}
							}
						>
							{
							imageUrl ?
								<img src={imageUrl} alt="" /> :
								<Button>
						          <Icon type="upload" /> 上传音频
						        </Button>
							}
						</Upload>
						<span className="f-pale">【说明:仅支持mp3,mp4,voa格式】</span>
					</Col>
				</Row>
				<div className="f-pd2 f-mb5" style={{borderBottom:'solid 1px #efefef'}} />
				<Row gutter={24}>
					<Col span="3">
						选项 :
					</Col>
					<Col span="21">
						<div>
					        <FormItem
								label="选项数"
							>
								{
									getFieldDecorator('userName', {
										rules: [{ 
											required: true, 
											message: '请输入' 
										}],
										initialValue:"2"
									})(
										<Select style={{width:'80px'}}>
										    <Option value="2">2</Option>
										    <Option value="3">3</Option>
										    <Option value="4">4</Option>
										    <Option value="5">5</Option>
										    <Option value="6">6</Option>
										</Select>
									)
								}
					        </FormItem>
				        </div>
				        <div className="f-pd5">
				        	<ul className="up-list f-clear">
				        		<li className="f-clear">
						        	<span className="serialNumber f-mr5">A</span>
									<Upload
								        className="avatar-uploader f-left"
								        name="avatar"
								        showUploadList={false}
								        action="//jsonplaceholder.typicode.com/posts/"
								        beforeUpload={
								        	()=> {
								        		console.log('before');
								        	}
								        }
								        onChange={
								        	()=> {
								        		console.log('change');
								        	}
								        }
								    >
								        {
								          	imageUrl ?
								            <img src={imageUrl} alt="" className="avatar" /> :
								            <Icon type="plus" className="avatar-uploader-trigger" />
								        }
								    </Upload>
				        		</li>
				        		<li className="f-clear">
						        	<span className="serialNumber f-mr5">B</span>
									<Upload
								        className="avatar-uploader f-left"
								        name="avatar"
								        showUploadList={false}
								        action="//jsonplaceholder.typicode.com/posts/"
								        beforeUpload={
								        	()=> {
								        		console.log('before');
								        	}
								        }
								        onChange={
								        	()=> {
								        		console.log('change');
								        	}
								        }
								    >
								        {
								          	imageUrl ?
								            <img src={imageUrl} alt="" className="avatar" /> :
								            <Icon type="plus" className="avatar-uploader-trigger" />
								        }
								    </Upload>
				        		</li>
				        		<li className="f-clear">
						        	<span className="serialNumber f-mr5">C</span>
									<Upload
								        className="avatar-uploader f-left"
								        name="avatar"
								        showUploadList={false}
								        action="//jsonplaceholder.typicode.com/posts/"
								        beforeUpload={
								        	()=> {
								        		console.log('before');
								        	}
								        }
								        onChange={
								        	()=> {
								        		console.log('change');
								        	}
								        }
								    >
								        {
								          	imageUrl ?
								            <img src={imageUrl} alt="" className="avatar" /> :
								            <Icon type="plus" className="avatar-uploader-trigger" />
								        }
								    </Upload>
				        		</li>
				        		<li className="f-clear">
						        	<span className="serialNumber f-mr5">D</span>
									<Upload
								        className="avatar-uploader f-left"
								        name="avatar"
								        showUploadList={false}
								        action="//jsonplaceholder.typicode.com/posts/"
								        beforeUpload={
								        	()=> {
								        		console.log('before');
								        	}
								        }
								        onChange={
								        	()=> {
								        		console.log('change');
								        	}
								        }
								    >
								        {
								          	imageUrl ?
								            <img src={imageUrl} alt="" className="avatar" /> :
								            <Icon type="plus" className="avatar-uploader-trigger" />
								        }
								    </Upload>
				        		</li>
				        	</ul>
					    </div>
					</Col>
				</Row>
				<div className="f-pd2 f-mb5" style={{borderBottom:'solid 1px #efefef'}} />
				<Row gutter={24}>
					<Col span="3">
						正确答案 :
					</Col>
					<Col span="21">
						<FormItem>
							{
								getFieldDecorator('righ', {
									rules: [{ 
										required: true, 
										message: '请选择' 
									}],
									initialValue:"A"
								})(
									<Select style={{width:'80px'}}>
									    <Option value="A">A</Option>
									    <Option value="B">B</Option>
									    <Option value="C">C</Option>
									    <Option value="D">D</Option>
									    <Option value="E">E</Option>
									</Select>
								)
							}
				        </FormItem>
					</Col>
				</Row>
				<div className="f-pd2 f-mb5" style={{borderBottom:'solid 1px #efefef'}} />
				<Row gutter={24}>
					<Col span="3">
						解析文本 :
					</Col>
					<Col span="21">
						<FormItem>
							{
								getFieldDecorator('mes', {
									rules: [{ 
										required: true, 
										message: '请选择' 
									}],
									initialValue:"A"
								})(
									<TextArea style={{width:'600px'}} rows={4} />
								)
							}
				        </FormItem>
					</Col>
				</Row>
				<div className="f-align-right f-mt5">
					<Button
						size="large"
						className="f-mr2"
					>取消</Button>
					<Button
						onClick={ this._submmit }
						type="primary"
						size="large"
					>保存</Button>
				</div>
			</div>
		</Form>);
	}
	componentDidMount() {

	}
	_submmit(e) {
		e.preventDefault();
		this.props.form.validateFields((err, values) => {
			if (!err) {
				console.log(values);
			}
		});
	}

}
export default Form.create()(Main);