import React, {
	Component
} from 'react';
import {
	Breadcrumb,
	Form,
	Input,
	Button,
	Icon,
	Row,
	Col,
	Card,
	Popconfirm,
	message
} from 'antd';
import MyList from './list.js';
import SearchForm from './searchForm.js';
import AddForm from './addForm.js';
import {
	fpost,
	host
} from '../../../common/io.js';
import {
	undefinedToEmpty
} from '../../../common/g.js';

export default class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			visible: true
		}
	}
	render() {
		let {
			visible
		} = this.state;

		return (<div>

			<Breadcrumb className="f-mb3">
			    <Breadcrumb.Item>教辅平台</Breadcrumb.Item>
			    <Breadcrumb.Item>题库</Breadcrumb.Item>
			    <Breadcrumb.Item>
			   		<span className="f-fz5 f-bold">单元设置</span>
			    </Breadcrumb.Item>
			</Breadcrumb>

			<div className="f-box-shadow2 f-radius1 f-bg-white f-pd4 f-mb5">
				<SearchForm 
					ref={ form => this.searchForm = form }
					onSearch= { this._search.bind(this) }
				/>
			</div>

			<h3 className="f-mb5">
				<span className="f-title-blue f-mt3">单元列表</span>
				<a 
					onClick={ ()=> { this.setState({visible:!this.state.visible}); } }
					className="f-btn-green f-right">
					<Icon className="f-fz7 f-vertical-middle f-bold" type="plus" />添加单元
				</a>
			</h3>

			{
				visible ? 
					<AddForm
						ref={ form => this.addForm = form }
						visible={ visible }
						onSaveOk={ this._saveOk.bind(this)}
						onCancel={ this._cancel.bind(this)}
					/>
				: null
			}

			<MyList 
				onEdit={this._edit.bind(this)}
				onDelete={this._delete.bind(this)}
			/>

		</div>);
	}
	componentDidMount() {
		this._getList();
	}
	_getList(pager = {}) {
		this.setState({
			loading: true
		});

	}
	_search() {
		let {
			onSearch
		} = this.props;

		this.searchForm.validateFields((err, values) => {
			if (!err) {
				/*如果值为undefined则替换为空*/
				values = undefinedToEmpty(values);

				console.log(values);
			}
		});
	}
	_saveOk() {
		this._cancel();
	}
	_cancel() {
		this.setState({
			visible: false,
			defaultData: null
		});
	}
	_visibleAddModal() {
		this.setState({
			visible: true,
			isEdit: false
		});
	}
	_delete(param) {
		alert('delet');
		// fpost('/api/system/schoolarea/deleteSchoolArea', param)
		// 	.then(res => res.json())
		// 	.then((res) => {
		// 		if (!res.success) {
		// 			message.error(res.message || '系统错误');
		// 			throw new Error(res.message || '系统错误');
		// 		};
		// 		return (res);
		// 	})
		// 	.then((res) => {
		// 		this._getList();
		// 	})
		// 	.catch((err) => {
		// 		console.log(err);
		// 	});
	}
	_edit(d) {
		this.setState({
			visible: true,
			// defaultData: d,
			isEdit: true
		});
	}
}