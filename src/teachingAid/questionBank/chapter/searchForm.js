import React, {
	Component
} from 'react';
import {
	Form,
	Input,
	Button
} from 'antd';

const FormItem = Form.Item;

class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {

		}
	}
	render() {
		const {
			getFieldDecorator,
			getFieldsError,
			getFieldError,
			isFieldTouched
		} = this.props.form;
		let {
			onSearch
		} = this.props;

		return (<section>
			<Form
				layout="inline"
			>
				<FormItem
					label="选择系列"
				>
					{
						getFieldDecorator('userName', {
							rules: [{ 
								required: true, 
								message: '请选择系列' 
							}],
						})(
							<Input placeholder="选择系列" style={{width:'200px'}} />
						)
					}
		        </FormItem>
				<FormItem
					label="选择教材"
				>
					{
						getFieldDecorator('userName', {
							rules: [{ 
								required: true, 
								message: '请输入教材名称' 
							}],
						})(
							<Input placeholder="搜索教材名称" style={{width:'200px'}} />
						)
					}
		        </FormItem>
				<FormItem
					label="单元名称"
				>
					{
						getFieldDecorator('userName', {
							rules: [{ 
								required: true, 
								message: '请输入教材名称' 
							}],
						})(
							<Input placeholder="搜索教材名称" style={{width:'200px'}} />
						)
					}
		        </FormItem>
				<FormItem>
					<Button 
						type="primary"
						htmlType="submit"
						size="large" 
						icon="search" 
						onClick={ onSearch }
					>搜索</Button>
		        </FormItem>
			</Form>
		</section>);
	}
}

export default Form.create()(Main);