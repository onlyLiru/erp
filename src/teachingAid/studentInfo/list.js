import React, {
  Component
} from 'react';
import {
  Table,
  Icon,
  Dropdown,
  Menu,
  Button,
} from 'antd';
import {
  Link
} from 'react-router-dom';
import {
  getUnicodeParam
} from '../../common/g.js';

export default class Main extends Component {
  render() {

    let {
      data,
      loading,
      pagination,
      onChange
    } = this.props;

    const columns = [{
      title: '课程名称',
      width: '150px',
      className: 'f-align-center',
      dataIndex: 'lessonName'
    }, {
      title: '班级名称',
      width: '150px',
      className: 'f-align-center',
      dataIndex: 'className'
    }, {
      title: '班主任',
      width: '150px',
      className: 'f-align-center',
      dataIndex: 'teacherName'
    }, {
      title: '校区',
      width: '150px',
      className: 'f-align-center',
      dataIndex: 'schoolInfoName'
    }, {
      title: '开始时间',
      width: '150px',
      className: 'f-align-center',
      dataIndex: 'classStartTime'
    }, {
      title: '结束时间',
      width: '150px',
      className: 'f-align-center',
      dataIndex: 'classEndTime'
    }];

    return (
      <div className="f-box-shadow2 f-radius1 f-bg-white f-pd4 f-mt5">
        <h3 className="f-mb5">
          <span className="f-title-blue f-mt3">课程列表</span>
        </h3>
        <Table
            columns={ columns }
            dataSource={ data }
            bordered
            loading={loading}
          />
      </div>
    );
  }

}