import React, {
	Component
} from 'react';
import {
	Button,
	message
} from 'antd';

import {
	default as RechargeForm
} from './rechargeForm.js';
import {
	default as DrawMoney
} from './drawMoneyForm.js';
import {
	numberFormate
} from '../../common/g.js';

export default class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			visibleRecharge: false,
			visibleDrawMoney: false,
		}
	}
	render() {
		let {
			data
		} = this.props;

		if (!data) {
			return null;
		}

		let {
			visibleRecharge,
			visibleDrawMoney
		} = this.state;

		let {
			id,
			name,
			headImgUrl,
			gender,
			stuNo,
			birthday,
			motherPhone,
			fatherPhone,
			otherPhone,
			registrationTime,
			sourceStr,
			salesName,
			currentSchoolName,
			balanceAmount = 0,

			isPublicSchoolStr,
			gradeStr,
			provinceName,
			cityName,
			districtName,
			address

		} = this.props.data;

		return (<div className="f-box-shadow2 f-radius1 f-over-hide f-bg-white">
			<div className="f-pd5 f-right-title-box">
				<div className="f-bg-blue f-right-title">
					<h3>学生详情信息</h3>
				</div>
	        	<div className="f-clear f-flex">
			        <ul className="f-clear info-list info-bd-top">
			        	<li><span className="f-pale f-mr2">学员姓名:</span>{name}</li>
						<li><span className="f-pale f-mr2">性别:</span>{gender}</li>
						<li><span className="f-pale f-mr2">学号:</span>{stuNo}</li>
						<li><span className="f-pale f-mr2">学员生日:</span>{birthday}</li>
						<li><span className="f-pale f-mr2">母亲电话:</span>{motherPhone}</li>
						<li><span className="f-pale f-mr2">父亲电话:</span>{fatherPhone}</li>
						<li><span className="f-pale f-mr2">绑定手机号:</span>{otherPhone}</li>
						<li><span className="f-pale f-mr2">注册时间:</span>
							{registrationTime}
						</li>
			        </ul>
			        <div className="f-right">
			        	<span className="f-pale f-mr2 f-left">头像:</span>
			        	<span className="avatar f-over-hide f-radius2" >
			        		<img style={{width:'100%'}} src={headImgUrl} />
			        	</span>
			        </div>
		        </div>
        	</div>

	        {
	        	visibleRecharge ? 
	        		<RechargeForm
	        			ref={ form => this.rechargeForm = form }
	        			visible={ visibleRecharge }
	        			onSaveOk={this._rechargeOk.bind(this)}
	        			onCancel={this._rechargeCancel.bind(this)}
	        			studentId={id}
	        		/>
	        	: null
	        }

	        {
	        	visibleDrawMoney ? 
	        		<DrawMoney
	        			ref={ form => this.drawMoneyForm = form }
	        			visible={ visibleDrawMoney }
	        			onSaveOk={this._drawMoneyOk.bind(this)}
	        			onCancel={this._drawMoneyCancel.bind(this)}
	        			studentId={id}
	        			balanceAmount={balanceAmount}
	        		/>
	        	: null
	        }

		</div>);
	}
	_visibleRecharge() { //显示充值
		this.setState({
			visibleRecharge: true
		});
	}
	_rechargeCancel() { //取消充值
		this.setState({
			visibleRecharge: false
		});
	}
	_rechargeOk(id) { //充值成功,返回充值流水id
		let {
			onRechargeOk
		} = this.props;
		this._rechargeCancel();
		onRechargeOk(id);
		message.success('恭喜您,充值成功!');
	}
	_visibleDrawMoney() { //显示提现
		this.setState({
			visibleDrawMoney: true
		});
	}
	_drawMoneyCancel() { //取消提现
		this.setState({
			visibleDrawMoney: false
		});
	}
	_drawMoneyOk() { //提现成功
		let {
			onDrawMoneyOk
		} = this.props;
		this._drawMoneyCancel();
		onDrawMoneyOk();
		message.success('恭喜您,提现成功!');
	}
}