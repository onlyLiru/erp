import React, {
	Component
} from 'react';
import {
	Link
} from 'react-router-dom';

export default class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {

		}
	}
	render() {
		let {
			data
		} = this.props;

		if (!data || !data.courseCounts) {
			return null;
		}

		let {
			courseCounts,
			id,
			potentStudentId,
			name,
			stuNo,
			matherPhone,
			fatherPhone,
			otherPhone
		} = data;
		let phone = matherPhone || fatherPhone || otherPhone;
		let {
			UN_START,
			END_COURSE,
			CHANGE_OF_CLASS_OUT,
			SIGN_UP,
			DROP_COURSE,
			STOP_COURSE,
			ARREAR
		} = courseCounts;

		return (<div>
			<div className="f-pd5 f-box-shadow2 f-radius1 f-over-hide f-bg-white f-mt5 f-right-title-box">
		        <div className="f-bg-yellow f-right-title">
	    			<h3>报课信息</h3>
	    		</div>
		        <table className="info-classes-table">
			        <tbody>
			        	<tr>
			        		<th>未开始</th>
			        		<th>结课</th>
			        		<th>转出</th>
			        		<th>正常报名</th>
			        		<th>退课</th>
			        		<th>停课</th>
			        		<th>欠费</th>
			        	</tr>
			        	<tr>
			        		<td>{UN_START}</td>
			        		<td>{END_COURSE}</td>
			        		<td>{CHANGE_OF_CLASS_OUT}</td>
			        		<td>{SIGN_UP}</td>
			        		<td>{DROP_COURSE}</td>
			        		<td>{STOP_COURSE}</td>
			        		<td>{ARREAR}</td>
			        	</tr>
		        	</tbody>
		        </table>
		        <div className="f-pt2 f-pb2 f-align-right">
		        	<Link 
		        		className="f-yellow" 
		        		to={`/front/students/courses?studentId=${id}&name=${name}&stuNo=${stuNo}`}
	        		>查看全部课程>></Link>
		        </div>
	        </div>
	        {
	        	potentStudentId ? 
			        <div className="f-pt4 f-pb4 f-align-right">
			        	<Link
			        		className="f-green" 
			        		to={`/market/client/viewCommunication?potentialStudentName=${name}&motherPhone=${phone}&potentialStudentId=${potentStudentId}`}
		        		>查看售前沟通日志>></Link>
			        </div>
			    : null
	        }
        </div>);
	}
}