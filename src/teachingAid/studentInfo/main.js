import React, {
	Component
} from 'react';
import {
	Button,
	Breadcrumb,
	message,
	Modal
} from 'antd';
import printJS from 'print-js';
import moment from 'moment';
import {
	Link
} from 'react-router-dom';
import {
	default as EditForm
} from './edit.js';
import {
	default as ViewInfo
} from './view.js';
import {
	default as CourseInfo
} from './courseInfo.js';
import {
	default as AtherInfo
} from './otherInfo.js';
import {
	default as MyList
} from './list.js';
import {
	fpost
} from '../../common/io.js';
import {
	Loading,
	NoData,
	getUrlParam,
	undefinedToEmpty
} from '../../common/g.js';

import './main.less';

export default class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			editng: false,
			saveing: false,
			loading: false,
			result: null,

			loading: false,
			courstData: null
		}
	}
	render() {
		let {
			editng,
			saveing,
			loading,
			result,
			courstData,
		} = this.state;

		if (loading) {
			return <Loading />
		}

		return (<section>
			<Breadcrumb className="f-mb3">
			    <Breadcrumb.Item>前台业务</Breadcrumb.Item>
			    <Breadcrumb.Item>学员列表</Breadcrumb.Item>
			    <Breadcrumb.Item>
			   		<span className="f-fz5 f-bold">学员详情</span>
			    </Breadcrumb.Item>
			</Breadcrumb>

			<div className="f-clear f-mb3">
				<span className="f-title-blue f-mt2">基本信息</span>
				
			</div>

			<ViewInfo 
				data={result}
			/>

			<MyList 
				data={courstData}
		        loading={loading}
		        onChange={this._getList.bind(this)}
			/>

		</section>);
	}
	componentDidMount() {
		this._getData();

		this._getList();
	}
	_getData() {
		let studentId = getUrlParam('studentId');
		this.setState({
			loading: true
		});
		fpost('/api/auxiliary/student/findStudentById.json', {
				studentId
			})
			.then(res => res.json())
			.then((res) => {
				if (!res.success) {
					this.setState({
						loading: false
					});
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res);
			})
			.then((res) => {
				this.setState({
					result: res.result,
					loading: false
				});
			})
			.catch((err) => {
				this.setState({
					loading: false
				});
				console.log(err);
			});
	}
	_getList(pager = {}) {
		this.setState({
			loading: true
		});
		fpost('/api/auxiliary/student/listRegistrationCourseById.json', {
				studentId: getUrlParam('studentId')
			})
			.then(res => res.json())
			.then((res) => {
				if (!res.success || !res.result) {
					this.setState({
						loading: false
					});
					message.error(res.message || '系统错误');
					throw new Error(res.message || '系统错误');
				};
				return (res.result);
			})
			.then((result) => {
				this.setState({
					courstData: result,
					loading: false
				}, () => {
					console.log(this.state)
				});
			})
			.catch((err) => {
				console.log(err);

				this.setState({
					loading: false
				});
			});
	}
}