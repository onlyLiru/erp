import React, {
	Component
} from 'react';
import {
	Route,
	Redirect
} from 'react-router-dom';
import {
	default as Layout
} from '../common/layout.js';

import {
	default as TeachingAid
} from './studentList/main.js'; /* 学员列表 */

import {
	default as StudentDetail
} from './studentInfo/main.js'; /* 学员详情 */

import {
	default as ClassList
} from './classList/main.js'; /* 班级列表 */

import {
	default as ViewMembers
} from './classMembers/main.js'; /* 查看班级成员 */

import {
	default as ManageTeacherAccount
} from './manageTeacherAccount/main.js'; /* 编辑老师账号 */

import {
	default as EditClass
} from './editClass/main.js'; /* 编辑班级信息 */

import {
	default as SeriesSet
} from './questionBank/series/main.js'; /*系列设置*/
import {
	default as TeachingMaterial
} from './questionBank/teachingMaterial/main.js'; /*教材设置*/
import {
	default as ChapterSet
} from './questionBank/chapter/main.js'; /*单元设置*/
import {
	default as TopicSet
} from './questionBank/topic/main.js'; /*题目设置*/
import {
	default as TopicTest
} from './questionBank/test/main.js'; /*连续新增的demo*/


export default ({
	match
}) => {
	window.store.session('tMenu', {
		selectedKeys: 'jfpt'
	})
	return (<div>
		<Layout main={ Main } />
	</div>);
}

class Main extends Component {
	render() {
		return (
			<div>
				<Route exact path={`/teachingAid`} component={TeachingAid} />{/*学员列表*/}
				<Route exact path={`/teachingAid/studentList`} component={TeachingAid} />{/*学员列表*/}
				<Route path={`/teachingAid/studentList/detail`} component={StudentDetail} />{/*学员详情*/}
				<Route exact path={`/teachingAid/classList`} component={ClassList} />{/*班级列表*/}
				<Route path={`/teachingAid/classList/members`} component={ViewMembers} />{/*班级成员*/}
				<Route path={`/teachingAid/teacher/act`} component={ManageTeacherAccount} />{/*教师列表*/}
				<Route path={`/teachingAid/editClass`} component={EditClass} />
				<Route path={`/teachingAid/series/set`} component={SeriesSet} />
				<Route path={`/teachingAid/teaching/material`} component={TeachingMaterial} />
				<Route path={`/teachingAid/chapter/set`} component={ChapterSet} />
				<Route path={`/teachingAid/topic/set`} component={TopicSet} />
				<Route path={`/teachingAid/topic/test`} component={TopicTest} />
			</div>
		);
	}
}