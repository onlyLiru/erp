import React, {
	Component
} from 'react';
import { Row, Col, Steps, Button, message, Input, Form } from 'antd';
import { CodeLayout } from '../common/codeLayout.js';
import { fpostLogin } from '../common/io.js';
import './main.less';

const Step = Steps.Step;
const FormItem = Form.Item;

export class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			current: 0,   //当前选中步骤条第一步
		};
	}
	_next() { //下一步
		this.props.form.validateFields(
			(err, fieldsValue) => {
				if(!err) {
					let v = this.props.form.getFieldValue('loginName');
					this._getUser(fieldsValue);
				}else{
					message.error('请先输入用户名才可以进入下一步操作哦')
				}
			}
		)

	}
	_getUser(v) { //判断用户是否存在
		fpostLogin('/api/pub/findUserByLoginname ',v)
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if(res.success == true) {
					window.location.href = '/IdentityVerify?id='+res.result.id+'&phone='+res.result.phone;
				} else {
					message.error(res.message)
				}
			});
	}
	render() {
		const steps = [{
			title: '填写用户名',
			content: '填写用户名',
		}, {
			title: '身份验证',
			content: '身份验证',
		}, {
			title: '完成',
			content: '完成',
		}];
		const {
			current
		} = this.state;
		const {
			getFieldDecorator
		} = this.props.form;
		return(
			<Form style={{minHeight:'100vh'}} className='Topselect'>
				<CodeLayout/>
	            <div style={{margin:'0 auto',marginTop:'10%',width:'70%'}}>
	            		<div style={{marginBottom:'70px'}}>
	            			<Steps current={current}>
				          {steps.map(item => <Step key={item.title} title={item.title} />)}
				        </Steps>
	            		</div>
	            		<Row>
	            			<Col span={8} className='f-align-right f-fz4 pageInpHeight' style={{color: '#243650',lineHeight:'40px'}}>
	            			用户名&emsp;
	            			</Col>
	            			<Col span={8}>
	            				<FormItem>
			                  {getFieldDecorator('loginName', {
			                  	rules: [{
					              required: true, message: '请输入用户名',
					            }],
			                  })(
			                  	<Input placeholder='请输入用户名' className='pageInpHeight' onPressEnter={this._next.bind(this)}/>
			                  )}
			                </FormItem>
	            			</Col>
	            		</Row>
	            		<Row className='f-align-center'>
	            		 <Button type="primary" onClick={this._next.bind(this)}>下一步</Button>
	            		</Row>
	            </div>
			</Form>
		);
	}
}
const ForgotCode = Form.create()(Main);
export {
	ForgotCode
}