import React, {
	Component
} from 'react';
import { Row, Col, Button, message, Input, Form } from 'antd';
import { CodeLayout } from '../common/codeLayout.js';
import './main.less';
import {fpost } from '../common/io.js';
import { getUrlParam } from '../common/g.js';

const FormItem = Form.Item;

export class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			confirmDirty: false,
		};
	}
	_next() { //下一步
		const data={};
		this.props.form.validateFields(
			(err, fieldsValue) => {
				if(!err) {
					data.password=fieldsValue.password;
					this.nextPage(data);
				}else{
					message.error('请先把信息填写完整才可以进行密码重置哦')
				}
			}
		)
	}
	nextPage(data){
		const url=getUrlParam('target')||'/index';
		fpost('/api/system/user/resetSelfPassword',data)
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if(res.success == true) {
					window.location.href = url;
				} else {
					message.error(res.message)
				}
			});
	}
	handleConfirmBlur = (e) => {
		const value = e.target.value;
		this.setState({
			confirmDirty: this.state.confirmDirty || !!value
		});
	}
	checkConfirm = (rule, value, callback) => {
		const form = this.props.form;
		let reg = /^\d{9,16}$|^(?!\d+$)[0-9A-z_]{6,16}$/;
		if(value && !reg.test(value)) {
			callback('请输入正确密码格式');
		} else {
			callback();
		}
		if(value && this.state.confirmDirty) {
			form.validateFields(['confirm'], {
				force: true
			});
		}
		callback();
	}
	checkPassword = (rule, value, callback) => {
		const form = this.props.form;
		if(value && value !== form.getFieldValue('password')) {
			callback('两次输入的密码不一致!');
		} else {
			callback();
		}
	}
	render() {
		const {
			getFieldDecorator
		} = this.props.form;
		return(
			<Form style={{minHeight:'100vh'}} className='Topselect'>
				<CodeLayout/>
	            <div style={{margin:'0 auto',marginTop:'10%',width:'70%'}}>
	            		<Row>
	            			<Col span={8} className='f-align-right f-fz4 pageInpHeight' style={{color: '#243650',lineHeight:'40px'}}>
	            			新密码&emsp;
	            			</Col>
	            			<Col span={8}>
	            				<FormItem>
			                  {getFieldDecorator('password', {
			                  	rules: [{
					              required: true, message: '请输入新密码',
					            }, {
					              validator: this.checkConfirm,
					            }],
			                  })(
			                  	<Input type='password' placeholder='请输入新密码' className='pageInpHeight'/>
			                  )}
			                </FormItem>
	            			</Col>
		            		<Col span={8} className='f-align-left'>
		            			<div className='f-fz2 f-ml2' style={{color: '#979797'}}>密码由6-16个字符组成，区分大小写（不<br/>能是9位以下的纯数字，不能包含空格）</div>
		            		</Col>
	            		</Row>
	            		<Row className='f-mb5 f-align-center'>
	            			<Col span={8} className='f-align-right f-fz4 pageInpHeight' style={{color: '#243650',lineHeight:'40px'}}>
	            			确认新密码&emsp;
	            			</Col>
	            			<Col span={8}>
	            				<FormItem>
			                  {getFieldDecorator('confirm', {
			                  	rules: [{
						              required: true, message: '请确认两次输入的密码是否一致',
						            }, {
						              validator: this.checkPassword,
						            }],
			                  })(
			                  	<Input type='password' placeholder='请确认新密码' className='pageInpHeight' onBlur={this.handleConfirmBlur} />
			                  )}
			                </FormItem>
	            			</Col>
	            		</Row>
	            		<Row className='f-align-center'>
	            		 <Button type="primary" onClick={this._next.bind(this)}>确定</Button>
	            		</Row>
	            </div>
			</Form>
		);
	}
}
const SetPassword = Form.create()(Main);
export {
	SetPassword
}