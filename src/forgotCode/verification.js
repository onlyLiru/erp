import React, {
	Component
} from 'react';
import {Row, Col, Steps, Button, message, Input, Form } from 'antd';
import { CodeLayout } from '../common/codeLayout.js';
import { fpostLogin } from '../common/io.js';
import { getUrlParam } from '../common/g.js';
import './main.less';
import $ from 'jquery';
const u=require('../common/io.js');
const Step = Steps.Step;
const FormItem = Form.Item;
const phone=getUrlParam('phone');

export class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			current: 1,  //当前选中步骤条第二步
			phone:''
		};
	}
	_next() { //下一步
		this.props.form.validateFields(
			(err, fieldsValue) => {
				if(!err) {
					this.nextPage(fieldsValue);
				}else{
					message.error('请先把表单信息填写完整才可以进入下一步哦')
				}
			}
		)
	}
	nextPage(fieldsValue){
		fpostLogin('/api/pub/modifyUserPassword',{
			userId:getUrlParam('id'),
			password:fieldsValue.password,
			checkCode:fieldsValue.code,
		})
			.then((res) => {
				return res.json();
			})
			.then((res) => {
				if(res.success == true) {
					window.location.href = '/resetPassComplete';
				} else {
					message.error(res.message)
				}
			});
	}
	handleConfirmBlur = (e) => {
		const value = e.target.value;
		this.setState({
			confirmDirty: this.state.confirmDirty || !!value
		});
	}
	checkConfirm = (rule, value, callback) => {
		const form = this.props.form;
		let reg = /^\d{9,16}$|^(?!\d+$)[0-9A-z_]{6,16}$/;
		if(value && !reg.test(value)) {
			callback('请输入正确密码格式');
		} else {
			callback();
		}
		if(value && this.state.confirmDirty) {
			form.validateFields(['confirm'], {
				force: true
			});
		}
		callback();
	}
	checkPassword = (rule, value, callback) => {
		const form = this.props.form;
		if(value && value !== form.getFieldValue('password')) {
			callback('两次输入的密码不一致!');
		} else {
			callback();
		}
	}
	componentDidMount() {
		const self = this;
		self.setState({
			phone:phone
		})
		$('.getVerification').off().on('click', function() {
			self.setAuthTime($(this), getUrlParam('id'), u.hostname()+'/api/message/sms/sendVerificationCodeByUserId', 60);
		})
	}
	setAuthTime(_this, val, url, time, data) {
		var btn = _this,
			obj = this,
			postVal = '';
		if(_this.hasClass("disabled")) {
			return false;
		}
		if(!btn.hasClass('disabled')) {
			postVal = data ? data : {
				"userId": $.trim(val)
			};
			$.post(url, postVal,
				function(res) {
					if(res.success == true) {
						message.info("验证码发送成功！");
						var sendCodeTime = time;
						btn.addClass('disabled');
						btn.val('重新发送(' + sendCodeTime + 'S)');
						if(window.sendCodeInterval) {
							clearInterval(window.sendCodeInterval);
						}
						window.sendCodeInterval = setInterval(function() {
							if(sendCodeTime > 1 && sendCodeTime <= time) {
								sendCodeTime--;
								btn.html('重新发送(' + sendCodeTime + 'S)');
							} else {
								btn.html('重新获取验证码');
								clearInterval(window.sendCodeInterval);
								btn.removeClass('disabled');
							}
						}, 1000);
					} else {
						message.error(res.message);
					}
				}, "json");
		}
	}
	render() {
		const steps = [{
			title: '填写用户名',
			content: '填写用户名',
		}, {
			title: '身份验证',
			content: '身份验证',
		}, {
			title: '完成',
			content: '完成',
		}];
		const {
			current
		} = this.state;
		const {
			getFieldDecorator
		} = this.props.form;
		return(
			<Form style={{minHeight:'100vh'}} className='Topselect IdentityVerifyPage'>
				<CodeLayout/>
	            <div style={{margin:'0 auto',marginTop:'10%',width:'70%'}}>
	            		<div style={{marginBottom:'70px'}}>
	            			<Steps current={current}>
				          {steps.map(item => <Step key={item.title} title={item.title} />)}
				        </Steps>
	            		</div>
	            		<Row>
	            			<Col span={8} className='f-align-right f-fz4 pageInpHeight' style={{color: '#243650',lineHeight:'40px'}}>
	            			手机号&emsp;
	            			</Col>
	            			<Col span={8}>
	            				<FormItem>
			                  	<Input value={this.state.phone} className='pageInpHeight' readOnly/>
			                </FormItem>
	            			</Col>
		            		<Col span={8} className='f-align-left'>
		            			<div style={{height:'40px',lineHeight:'40px',border: '1px solid #2187FF',width:'140px'}} className='f-blue f-fz4 f-radius2 f-align-center f-ml2 f-pointer getVerification'>发送验证码</div>
		            		</Col>
	            		</Row>
	            		<Row className='f-align-center'>
	            			<Col span={8} className='f-align-right f-fz4 pageInpHeight' style={{color: '#243650',lineHeight:'40px'}}>
	            			验证码&emsp;
	            			</Col>
	            			<Col span={8}>
	            				<FormItem>
			                  {getFieldDecorator('code', {
			                  	rules: [{
					              required: true,
					              message: '请输入验证码!',
					            }
					          	],
			                  })(
			                  	<Input placeholder='请输入验证码' className='pageInpHeight'/>
			                  )}
			                </FormItem>
	            			</Col>
	            		</Row>
	            		<Row>
	            			<Col span={8} className='f-align-right f-fz4 pageInpHeight' style={{color: '#243650',lineHeight:'40px'}}>
	            			新密码&emsp;
	            			</Col>
	            			<Col span={8}>
	            				<FormItem>
			                  {getFieldDecorator('password', {
			                  	rules: [{
					              required: true, message: '请输入新密码',
					            }, {
					              validator: this.checkConfirm,
					            }],
			                  })(
			                  	<Input type='password' placeholder='请输入新密码' className='pageInpHeight'/>
			                  )}
			                </FormItem>
	            			</Col>
		            		<Col span={8} className='f-align-left'>
		            			<div className='f-fz2 f-ml2' style={{color: '#979797'}}>密码由6-16个字符组成，区分大小写（不<br/>能是9位以下的纯数字，不能包含空格）</div>
		            		</Col>
	            		</Row>
	            		<Row className='f-mb5 f-align-center'>
	            			<Col span={8} className='f-align-right f-fz4 pageInpHeight' style={{color: '#243650',lineHeight:'40px'}}>
	            			确认新密码&emsp;
	            			</Col>
	            			<Col span={8}>
	            				<FormItem>
			                  {getFieldDecorator('confirm', {
			                  	rules: [{
						              required: true, message: '请确认两次输入的密码是否一致',
						            }, {
						              validator: this.checkPassword,
						            }],
			                  })(
			                  	<Input type='password' placeholder='请确认新密码' className='pageInpHeight' onBlur={this.handleConfirmBlur}/>
			                  )}
			                </FormItem>
	            			</Col>
	            		</Row>
	            		<Row className='f-align-center'>
	            		 <Button type="primary" onClick={this._next.bind(this)}>下一步</Button>
	            		</Row>
	            </div>
			</Form>
		);
	}
}
const IdentityVerify = Form.create()(Main);
export {
	IdentityVerify
}