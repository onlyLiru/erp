import React, { Component } from 'react';
import { Breadcrumb, Form, Input, Button, Table } from 'antd';

const FormItem = Form.Item;

export default class extends Component {
	render() {
		const columns = [
			{
			  title: '学员姓名 (学号)',
			  dataIndex: 'name',
			  key: 'name',
			  className:'f-align-center',
			  render: text => <a href="#">{text}</a>,
			}, {
			  title: '头像',
			  dataIndex: 'headImg',
			  key: 'headImg',
			  className:'f-align-center',
			}, {
			  title: '性别',
			  dataIndex: 'sex',
			  key: 'sex',
			  className:'f-align-center',
			}, {
			  title: '手机号码',
			  dataIndex: 'mobile',
			  key: 'mobile',
			  className:'f-align-center',
			}, {
			  title: '沟通时间',
			  dataIndex: 'communiteTime',
			  key: 'communiteTime',
			  className:'f-align-center',
			}, {
			  title: '课程名称',
			  dataIndex: 'courseName',
			  key: 'courseName',
			  className:'f-align-center',
			}, {
			  title: '班级名称',
			  dataIndex: 'className',
			  key: 'className',
			  className:'f-align-center',
			}, {
			  title: '问题反馈',
			  dataIndex: 'feedback',
			  key: 'feedback',
			  className:'f-align-center',
			}];

		const data = [{
		  key: '1',
		  name: '易磊',
		  headImg: 32,
		  sex: 'New York No. 1 Lake Park',
		  mobile: '13501726692',
		  communiteTime: '2017-08-12',
		  courseName: '体育',
		  className: '一年级',
		  feedback: '你好呀'
		}];

		return (<div>
					<Breadcrumb className="f-mb3">
					    <Breadcrumb.Item>售后服务</Breadcrumb.Item>
					    <Breadcrumb.Item>课程服务</Breadcrumb.Item>
					    <Breadcrumb.Item>售后沟通</Breadcrumb.Item>
					</Breadcrumb>
					<div className='f-bg-white f-pd4 f-box-shadow1 f-mt5 f-radius1'>
						<SearchFormCreate />
					</div>
					<div className='f-bg-white f-pd4 f-box-shadow1 f-mt5 f-radius1'>
						<Table columns={columns} dataSource={data} bordered/>
					</div>
				</div>)
	}
}

class SearchForm extends Component {
	render() {
		const { getFieldDecorator } = this.props.form;
		return (<Form style={{ textAlign:'left' }}>
					<FormItem style={{ width:'25%', margin:'0 0 0 30px' }} className='f-inline-block'>
						{getFieldDecorator('username', {
				            rules: [{
				              required: false,
				              message: '请输入学员姓名',
				            }],
				          })(
				            <Input placeholder="请输入学员姓名" />
				          )}
					</FormItem>
					<FormItem style={{ width:'25%', margin:'0 0 0 30px' }} className='f-inline-block'>
						{getFieldDecorator('username', {
				            rules: [{
				              required: false,
				              message: '请输入手机号码',
				            }],
				          })(
				            <Input placeholder="请输入手机号码" />
				          )}
					</FormItem>
					<FormItem className='f-inline-block f-ml3' style={{ marginBottom:0 }}>
						<Button type="primary" icon="search" style={{height:'40px'}}>搜索</Button>
					</FormItem>
				</Form>)
	}
}

const SearchFormCreate = Form.create()(SearchForm);
