import React, {
	Component
} from 'react';
import {
	Route
} from 'react-router-dom';
import {
	default as Layout
} from '../common/layout.js';
import {
	default as Birthday
} from './Birthday.js';

import './main.less';

export default ({
	match
}) => {
	window.store.session('tMenu', {
		selectedKeys: 'shfw'
	})

	return (<div>
		<Layout main={ Main } />
	</div>);
}

class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {

		}
	}
	render() {
		return (<div>
					<Route exact path={`/after`} component={Birthday} />
					<Route path={`/after/birthday`} component={Birthday} />		
				</div>);
	}
}