const {
	injectBabelPlugin
} = require("react-app-rewired");
const rewireLess = require("react-app-rewire-less");
module.exports = function override(config, env) {
	// do stuff with the webpack config...
	config = injectBabelPlugin(["import", {
		libraryName: "antd",
		style: true
	}], config);
	config = rewireLess.withLoaderOptions({
		modifyVars: {
			"@primary-color": "#2187FF",
			"@font-size-base": "14px",
			"@layout-header-height": "87px",
			"@layout-header-background": "#243250",
			"@input-bg": "#f3f3f8",
			"@table-header-bg": "#e7f2ff",
			"@btn-height-lg": "36px",
			"@input-height-lg": "36px"
		},
	})(config, env);
	return config;
};